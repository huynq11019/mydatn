package com.example.ducky_be.adapter.schedule;

import com.example.ducky_be.adapter.mailSending.MailService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Configuration
@EnableScheduling
@Log4j2
@Component
public class Scheduling {
    @Autowired
    private MailService mailService;

    @Scheduled(cron = "${send.mail.teams}")
    public void printOut() {
        try {
            log.info("============SendMailAndTeamsImpl thread is start============");
            this.mailService.sendMail();
            log.info("============SendMailAndTeamsImpl thread is end============");
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

    }
}
