package com.example.ducky_be.adapter.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

/**
 * @author MinhLA
 * @date 5/13/2021 9:56 PM
 */
public interface StorageService {

    String store(MultipartFile file);

    String store(MultipartFile filePart, String destName);

    Path load(String filename);

    Resource loadAsResource(String filename);

    Stream<Path> loadAll();

    InputStreamResource download(String filePath);

    Resource loadAsResourceFolder(String filename);

    Path loadFolder(String filename);
}
