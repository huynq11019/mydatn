/*
 * StorageServiceImpl.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.adapter.service.impl;

import com.example.ducky_be.adapter.service.StorageService;
import com.example.ducky_be.core.config.StorageProperties;
import com.example.ducky_be.core.exception.StorageException;
import com.example.ducky_be.core.exception.StorageFileNotFoundException;
import com.example.ducky_be.core.utils.HashUtil;
import com.example.ducky_be.core.utils.StringUtil;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * @author MinhLA
 * @date 5/13/2021
 */
@Log4j2
@Service
@Transactional
public class StorageServiceImpl implements StorageService {


    private String rootLocation;

    private final StorageProperties storageProperties;


    public StorageServiceImpl(StorageProperties storageProperties) {
        this.storageProperties = storageProperties;
        this.rootLocation = this.storageProperties.getFolderUpload();
    }

    private String getFolderPath() {
        String format = "yyyy/MM/dd";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return "/" + sdf.format(new Date()).replace("\\", "/") + "/";
    }

    @Override
    public String store(MultipartFile file) {
        String newFileName = "";
        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + file.getOriginalFilename());
            }
            String fullName = StringUtil.getSafeFileName(file.getOriginalFilename());
            String fileType = "";
            if (fullName != null) {
                int last = fullName.lastIndexOf(".");
                if (last >= 0) {
                    fileType = fullName.substring(last);
                }
            }
            newFileName = HashUtil.SHA256(file.getBytes()) + UUID.randomUUID() + fileType;

            String folder = this.getFolderPath() + newFileName.substring(0, 2) + "/";
            Path path = Paths.get(rootLocation + folder + newFileName);

            if (!Files.exists(Paths.get(rootLocation + folder))) {
                new File(rootLocation + folder).mkdirs();
            }
            Files.copy(file.getInputStream(), path);
            return path.toString();
        } catch (FileAlreadyExistsException e) {
            log.error("store file error", e);
            return newFileName;
        } catch (IOException e) {
            throw new StorageException("Failed to store file " + file.getOriginalFilename(), e);
        }
    }

    @Override
    public String store(MultipartFile filePart, String destName) {
        return null;
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            Path rootPath = Paths.get(this.storageProperties.getFolderUpload());
            return Files.walk(rootPath, 1)
                    .filter(path -> !path.equals(rootPath))
                    .map(path -> rootPath.relativize(path));
        } catch (IOException e) {
            throw new StorageException("Failed to read stored files", e);
        }

    }

    @Override
    public InputStreamResource download(String path) {
        try {
            log.info(storageProperties.getFolderUpload());
//            File file = ResourceUtils.getFile(storageProperties.getFolderUpload() + "/" + filename);
            File file = ResourceUtils.getFile(path);

            try {
                byte[] data = FileUtils.readFileToByteArray(file);

                InputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(data));

                InputStreamResource inputStreamResource = new InputStreamResource(inputStream);

                return inputStreamResource;
            } catch (IOException e) {
                log.error("Error: ", e);
            }
        } catch (FileNotFoundException e) {
            log.error("Error: ", e);
        }

        return null;
    }


    @Override
    public Resource loadAsResourceFolder(String filename) {
        return null;
    }

    @Override
    public Path loadFolder(String filename) {
        return null;
    }

    @Override
    public Path load(String filename) {
        Path path = Paths.get(this.storageProperties.getFolderUpload());
        return path.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            return new UrlResource(file.toUri());
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }
}
