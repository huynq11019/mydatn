package com.example.ducky_be.adapter.GHN;

import com.example.ducky_be.core.utils.HeaderRequestInterceptor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ShippingAPI {
    //    https://dev-online-gateway.ghn.vn/shiip/public-api/v2/shipping-order/create
    public static final String BASE_URL = "https://dev-online-gateway.ghn.vn/shiip/public-api/v2/";
    public static final String API_KEY = "339ee9f5-5b37-11ec-ac64-422c37c6de1b";
    public static final String SHOP_ID = "84307";


    /**
     * Gets the rest template.
     *
     * @return the rest template
     */
    private RestTemplate getRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
//        String token = this.jwtTokenProvider.generateTokenForSAP(null);
        ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
        interceptors.add(new HeaderRequestInterceptor("Token", this.API_KEY));
        interceptors.add(new HeaderRequestInterceptor("ShopId", SHOP_ID));
        interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json"));
        restTemplate.setInterceptors(interceptors);
        return restTemplate;
    }

    // create order
    public String createOrder(Map<String, Object> params) {
        String url = BASE_URL + "shipping-order/create";
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(params);
            return this.getRestTemplate().postForObject(builder.toUriString(), json, String.class);
        } catch (HttpClientErrorException | JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    // cancel order
    public String cancelOrder(String orderId) {
        String url = BASE_URL + "shipping-order/cancel/";
        try {
            Map<String, Object> params = new HashMap<>();
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
            params.put("order_codes", orderId);
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(params);
            return this.getRestTemplate().postForObject(builder.toUriString(), json, String.class);
        } catch (HttpClientErrorException | JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    // tracking đơn hàng
    public String trackingOrder(String orderId) {
        String url = BASE_URL + "shipping-order/detail";
        try {
            Map<String, Object> params = new HashMap<>();
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
            params.put("order_codes", orderId);
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(params);
            return this.getRestTemplate().postForObject(builder.toUriString(), json, String.class);
        } catch (HttpClientErrorException | JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    // fee order
    /**
     * @author: huynq
     * @since: 12/12/2021 8:12 PM
     * @description: api tính tiền đơn hàng
     * @param insurance_value : tổng giá trị hàng hóa
     * @param from_district_id : từ quận nào
     * @param to_district_id : đến quận nào
     * @param to_ward_code : đến phường xã nào
     * @param to_ward_code : đến phường xã nào
     * @param service_id : id dịch vụ
     * @param service_id : id loại dich vụ
     * @update:
     */
    public String feeOrder(String from_district_id, String service_id,
                           String service_type_id, String to_district_id,
                           String to_ward_code,
                           String insurance_value,
                           String coupon,
                           String weight, String length, String width, String height) {
        String url = BASE_URL + "shipping-order/fee";
        try {
            Map<String, Object> params = new HashMap<>();
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
            params.put("from_district_id", from_district_id);
            params.put("service_id", from_district_id);
            params.put("from_district_id", from_district_id);
            params.put("from_district_id", from_district_id);
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(params);
            return this.getRestTemplate().postForObject(builder.toUriString(), json, String.class);
        } catch (HttpClientErrorException | JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Gets the uri.
     *
     * @param url    the url
     * @param object the object
     * @return the uri
     */
    @SuppressWarnings("unchecked")
    private String getUri(String url, Object object) {
        ObjectMapper oMapper = new ObjectMapper();
        Map<String, Object> map = oMapper.convertValue(object, Map.class);
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
        for (String key : map.keySet()) {
            builder.queryParam(key, map.get(key));
        }
        return builder.toUriString().trim();
    }

    public static void main(String[] args) {

    }
}
