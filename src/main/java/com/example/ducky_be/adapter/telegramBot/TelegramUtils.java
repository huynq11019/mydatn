package com.example.ducky_be.adapter.telegramBot;

import lombok.extern.log4j.Log4j2;
import org.springframework.core.io.InputStreamResource;

@Log4j2
public class TelegramUtils {
    private static final String BOT_TOKEN = "2034057824:AAGGg3yTx7o_agR-VfE3ibYVsddB5Vpkejc";
    private static final String BOT_NAME = "Ducky";
    private static final String CHAT_ID = "-574753817"; // group chat id
    private static final String SEND_MESSAGE_URL = "https://api.telegram.org/bot" + BOT_TOKEN + "/sendMessage";
    private static final String SEND_PHOTO_URL = "https://api.telegram.org/bot" + BOT_TOKEN + "/sendPhoto";

    public static void sendMessage(String message) {
        try {
            String url = SEND_MESSAGE_URL + "?chat_id=" + CHAT_ID + "&text=" + message;
            TelegramUtils.sendRequest(url);
        } catch (Exception e) {
            log.error(e);
        }
    }

    public static void sendPhoto(String filePath) {
        try {
            String url = SEND_PHOTO_URL + "?chat_id=" + CHAT_ID + "&photo=" + filePath;
            TelegramUtils.sendRequest(url);
        } catch (Exception e) {
            log.error(e);
        }
    }

    private static void sendRequest(String url) {
        try {
            java.net.URL obj = new java.net.URL(url);
            java.net.HttpURLConnection con = (java.net.HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            int responseCode = con.getResponseCode();
            log.info("Response Code : " + responseCode);
        } catch (Exception e) {
            log.error(e);
        }
    }

    // send file to telegram
    public static void sendFileToTelegram(InputStreamResource inputStreamResource) {
        try {
            String url = SEND_PHOTO_URL + "?chat_id=" + CHAT_ID;
            TelegramUtils.sendFile(url, inputStreamResource);
        } catch (Exception e) {
            log.error(e);
        }
    }

    private static void sendFile(String url, InputStreamResource inputStreamResource) {
        try {
            java.net.URL obj = new java.net.URL(url);
            java.net.HttpURLConnection con = (java.net.HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/octet-stream");
            con.setRequestProperty("Content-Length", String.valueOf(inputStreamResource.contentLength()));
            con.setDoOutput(true);
            con.setDoInput(true);
            con.setUseCaches(false);
            con.setAllowUserInteraction(false);
            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            con.connect();
            java.io.OutputStream out = con.getOutputStream();
            java.io.InputStream in = inputStreamResource.getInputStream();
            byte[] buffer = new byte[1024];
            int len;
            while ((len = in.read(buffer)) > 0) {
                out.write(buffer, 0, len);
            }
            in.close();
            out.close();
            int responseCode = con.getResponseCode();
            log.info("Response Code : " + responseCode);
        } catch (Exception e) {
            log.error(e);
        }
    }
}
