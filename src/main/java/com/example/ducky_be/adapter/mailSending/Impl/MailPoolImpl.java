package com.example.ducky_be.adapter.mailSending.Impl;

import com.example.ducky_be.adapter.mailSending.MailPool;
import com.example.ducky_be.adapter.mailSending.modal.MailDomain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Service
public class MailPoolImpl implements MailPool {
//    @Autowired
//    private JavaMailSender sender;
//    List<MimeMessage> queue = new ArrayList<MimeMessage>();
//
//    // push mail infor vào hàng chờ
//    public void push(String to, String subject, String body) throws MessagingException {
//
//        MailDomain mail = new MailDomain(to, subject, body);
//        this.push(mail);
//    }
//
//    // push mail vào hàng chờ
//    public void push(MailDomain mail) throws MessagingException {
//        MimeMessage message = sender.createMimeMessage();
//        MimeMessageHelper helper = new MimeMessageHelper(message, true, "utf-8");
//        helper.setFrom(mail.getForm());
//        helper.setTo(mail.getTo());
//        helper.setSubject(mail.getSubject());
//        helper.setText(mail.getBody());
//        helper.setReplyTo(mail.getForm());
//
//        for (String email : mail.getCc()) {
//            helper.addCc(email);
//        }
//        for (String email : mail.getBcc()) {
//            helper.addBcc(email);
//        }
//        for (File file : mail.getFiles()) {
//            helper.addAttachment(file.getName(), file);
//        }
//        this.queue.add(message);
//    }
//
//    // remove mail trong hàng chờ
//    public MimeMessage remove(int index) {
//        return this.queue.remove(index);
//    }
//
//    @Scheduled(cron = "${send.mail.teams}")
//    public void run() throws InterruptedException {
//        int success = 0, error = 0;
//        while (!queue.isEmpty()) {
//            MimeMessage message = queue.remove(0);
//            try {
//                sender.send(message);
//                success++;
//            } catch (Exception e) {
//                // TODO: handle exception
//                error++;
//            }
//            Thread.sleep(1100);
//        }
//        System.out.printf(">>send %d, error%d \n", success, error);
//    }
}
