package com.example.ducky_be.adapter.mailSending;

public interface MailService {
    void sendMail() throws InterruptedException;
}
