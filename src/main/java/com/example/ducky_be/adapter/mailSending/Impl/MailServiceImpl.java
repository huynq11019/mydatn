package com.example.ducky_be.adapter.mailSending.Impl;

import com.example.ducky_be.adapter.mailSending.MailService;
import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.SendMailLogRepository;
import com.example.ducky_be.repository.entity.SendMailLog;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;

@Transactional
@Service
@Log4j2
public class MailServiceImpl implements MailService {
    @Value("${spring.mail.from.address}")
    private String fromAddress;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private SendMailLogRepository mailLogRepository;


    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}", isMultipart,
                isHtml, to, subject, content);
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, StandardCharsets.UTF_8.name());
            message.setFrom(fromAddress);
            message.setTo(to);
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sen t email to User '{}'", to);
        } catch (MailException | MessagingException e) {
            log.error("Email could not be sent to user '{}'", to, e);
        }
    }
    @Override
    public void sendMail() throws InterruptedException {
        List<SendMailLog> sendMailLogs = this.mailLogRepository.getAllBySendStatus(Constants.MAIL_STATUS.WAITING);;
        if (Validator.isNotNull(sendMailLogs)) {
            for (SendMailLog sendMailLog : sendMailLogs) {
                if (Validator.isNotNull(sendMailLog.getToEmail())
                        && Validator.isNotNull(sendMailLog.getTilte())
                        && Validator.isNotNull(sendMailLog.getContent())) {
                    log.info("============Send Mail thread is running============");
                    this.sendEmail(sendMailLog.getToEmail(), sendMailLog.getTilte(), sendMailLog.getContent(), false,
                            true);
                    Date now = new Date();
                    sendMailLog.setSendStatus(Constants.MAIL_STATUS.SUCCESS);
                    sendMailLog.setSendTime(now);
                    this.mailLogRepository.save(sendMailLog);
                    log.info("============Send Mail have been SUCCESS!============");
                } else {
                    log.info("============Send Mail have been error!============");
                    Date now = new Date();
                    sendMailLog.setSendStatus(Constants.MAIL_STATUS.ERROR);
                    sendMailLog.setSendTime(now);
                    this.mailLogRepository.save(sendMailLog);
                }
            }
            Thread.sleep(1100);
        }
    }
}
