package com.example.ducky_be.adapter.mailSending.modal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MailDomain {
    private String form = "Huy đẹp trai <vosong9201@gmail.com>";
    private String to;
    private String subject;
    private String body;
    private List<String> cc = new ArrayList<String>();
    private List<String> bcc = new ArrayList<String>();
    private List<File> files = new ArrayList<File>();


    public MailDomain(String to, String subject, String body) {
        super();
        this.to = to;
        this.subject = subject;
        this.body = body;
    }
}
