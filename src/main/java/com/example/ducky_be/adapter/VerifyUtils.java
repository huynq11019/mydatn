package com.example.ducky_be.adapter;

import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.model.resoonse.ReCapchaResponseType;
import com.example.ducky_be.core.utils.HeaderRequestInterceptor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.ArrayList;

@Log4j2
public class VerifyUtils {
    //    @Value("${recapcha.verifyUrl}")
    private static String SITE_VERIFY_URL = "https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s";

    /**
     * @param gRecaptchaResponse capcha token từ FE
     *                           8 @return true nếu token is correct
     * @author: huynq
     * @since: 10/23/2021 11:30 PM
     * @description:
     * @update:
     */
    public static boolean verify(String gRecaptchaResponse) {
        if (gRecaptchaResponse == null || gRecaptchaResponse.length() == 0) {
            return false;
        }

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                .getRequest();
        try {
            String ip = request.getRemoteAddr();
            log.info("ip =>>> " + ip);
            URI verifyUri = URI.create(String.format(
                    SITE_VERIFY_URL,
                    Constants.SECRET_KEY, gRecaptchaResponse, ip));
//            MultiValueMap<String, String> requestMap = new LinkedMultiValueMap<>();
//            requestMap.add("secret", Constants.SECRET_KEY);
//            requestMap.add("response", gRecaptchaResponse);
//            requestMap.add("remoteip", "localhost");
//            log.info(requestMap);
            ReCapchaResponseType reCapchaResponseType = getRestTemplate().getForObject(verifyUri, ReCapchaResponseType.class);
            log.info(reCapchaResponseType);
            if (reCapchaResponseType == null) {
                return false;
            }
            return Boolean.TRUE.equals(reCapchaResponseType.isSuccess());
        } catch (Exception exception) {
            exception.printStackTrace();

        }
        return false;
    }

    /**
     * Gets the rest template.
     *
     * @return the rest template
     */
    private static RestTemplate getRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
//        String token = this.jwtTokenProvider.generateTokenForSAP(null);
        ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
//        interceptors.add(new HeaderRequestInterceptor("client_id", this.clientId));
//        interceptors.add(new HeaderRequestInterceptor("client_secret", this.clientSecret));
//        interceptors.add(new HeaderRequestInterceptor("Authorization", "Bearer " + token));
        interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json"));
        restTemplate.setInterceptors(interceptors);
        return restTemplate;
    }
}
