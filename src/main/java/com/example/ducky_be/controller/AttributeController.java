package com.example.ducky_be.controller;


import com.example.ducky_be.controller.protoco.request.AttributeRequest;
import com.example.ducky_be.controller.protoco.response.AttributeResponse;
import com.example.ducky_be.service.AttributeService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@CrossOrigin("*")
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class AttributeController {

    private final AttributeService attributeService;

    @GetMapping("/categories/{category-id}/attributes")
    @ApiOperation("Lấy danh sách attribute theo category id")
    public ResponseEntity<AttributeResponse> search(@PathVariable("category-id") String categoryId,
                                                    @RequestParam MultiValueMap<String, String> queryParams, Pageable pageable) {
        log.info(queryParams.toString());
        AttributeResponse response = this.attributeService.findByAttributeByCategoryId(categoryId, queryParams, pageable);
        return ResponseEntity.ok().body(response);
    }

    @PostMapping("/categories/{category-id}/attributes")
    @ApiOperation("Lấy danh sách attribute theo category id")
    public ResponseEntity<AttributeResponse> create(@PathVariable("category-id") String categoryId,
                                                    @RequestBody @Valid AttributeRequest request) {

        AttributeResponse response = this.attributeService.createAttribute(request);
        return ResponseEntity.ok().body(response);
    }

    @PostMapping("/categories/{category-id}/attributes/{id}")
    @ApiOperation("Lấy danh sách attribute theo category id")
    public ResponseEntity<AttributeResponse> update(@PathVariable("category-id") Long categoryId,
                                                    @PathVariable("id") String id,
                                                    @RequestBody @Valid AttributeRequest request) {

        AttributeResponse response = this.attributeService.updateAttribute(request, id, categoryId);
        return ResponseEntity.ok().body(response);
    }
}
