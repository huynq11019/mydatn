package com.example.ducky_be.controller;


import com.example.ducky_be.controller.protoco.request.AddressRequest;
import com.example.ducky_be.controller.protoco.response.AddressResponse;
import com.example.ducky_be.service.AddressService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping("api")
public class AddressController {
    @Autowired
    private AddressService addressService;

    @GetMapping(value = "/address/search")
    public ResponseEntity<AddressResponse> getListPro(@RequestParam MultiValueMap<String, String> queryParams, Pageable pageable) {
        AddressResponse response = addressService.searchAddress(queryParams, pageable);
        return ResponseEntity.ok(response);

    }

    @GetMapping("/address/{addressId}")
    public ResponseEntity<AddressResponse> findById(@PathVariable(name = "addressId") Long addressId) {
        AddressResponse response = addressService.findproductById(addressId);
        return ResponseEntity.ok(response);
    }
    @GetMapping("/address/customer/{customerId}")
    public ResponseEntity<AddressResponse> findByUserId(@PathVariable(name = "customerId") String  userId) {
        AddressResponse response = addressService.findByUserId(userId);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/address/create")
    public ResponseEntity<AddressResponse> createAddress(@RequestBody @Valid AddressRequest addressRequest) {
        AddressResponse response = addressService.createAdress(addressRequest);
        return ResponseEntity.ok(response);
    }

    @PutMapping("/address/updateAddress")
    public ResponseEntity<AddressResponse> updateAddress(@RequestBody @Valid AddressRequest addressRequest) {
        AddressResponse response = addressService.createAdress(addressRequest);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/address/saveDraft")
    public ResponseEntity<AddressResponse> saveDraft(@RequestBody @Valid AddressRequest addressRequest) {
        AddressResponse response = addressService.saveOrUpdateDraft(addressRequest);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/me/address/my-address")
    @ApiOperation(value = "Get list address of user")
    public ResponseEntity<AddressResponse> getMyAddress(@RequestParam MultiValueMap<String, String> queryParams, Pageable pageable) {
        AddressResponse response = addressService.myaddress(queryParams, pageable);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/address/me/{addressId}/delete")
    @ApiOperation(value = "Delete address")
    public ResponseEntity<AddressResponse> delteMyAdress(@PathVariable("addressId") Long id) {
        addressService.deleteAddress(id);
        return ResponseEntity.ok().body(new AddressResponse());
    }

    @PutMapping("/address/me/{addressId}/default")
    public ResponseEntity<AddressResponse> selectDefault(@PathVariable("addressId") Long id) {
        addressService.setDefaultAddress(id);
        return ResponseEntity.ok(new AddressResponse());
    }
}
