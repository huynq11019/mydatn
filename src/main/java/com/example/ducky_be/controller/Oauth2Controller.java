package com.example.ducky_be.controller;

import com.example.ducky_be.controller.protoco.response.AuthenticationResponse;
import com.example.ducky_be.core.constants.SecurityConsants;
import com.example.ducky_be.core.sercurity.TokenPovider;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RestController
public class Oauth2Controller {
    @Autowired
    private TokenPovider tokenPovider;

    private long jwtExpirationInMs = SecurityConsants.JWTEXPIRATIONINMS;
    private long refreshExpirationDateInMs = SecurityConsants.REFRESH_JWTEXPIRATIONINMS;

    @GetMapping("/oauth2/login/success")
    public ResponseEntity<AuthenticationResponse> oauth2LoginSuccess(String token) {
        log.info("oauth2LoginSuccess");
        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
        // access token của user
        log.info("get access token {}", token);

//        CustomUserDetail userDetail = (CustomUserDetail) authentication.getPrincipal();
//        String token = tokenPovider.generateToken(userDetail);
//        String userId = tokenPovider.getUserIdFromtToken(token);
//        log.info("userId: " + userId);
//
//        JwtToken accessToken = new JwtToken();
//        accessToken.setToken(token);
//        accessToken.setTokenExpDate(new Date(System.currentTimeMillis() + jwtExpirationInMs));
//        accessToken.setCreatedDate(new Date(System.currentTimeMillis()));
//        authenticationResponse.setAccessToken(accessToken);
//
//        // refresh token của user
//        JwtToken refreshToken = tokenPovider.createRefreshToken(authentication);
//        authenticationResponse.setRefreshToken(refreshToken);
        return ResponseEntity.ok(authenticationResponse);
    }

    @GetMapping("/oauth2/login/failure")
    public String loginFail() {
        return "loginFail";
    }
}
