package com.example.ducky_be.controller;

import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.service.OrderService;
import lombok.extern.log4j.Log4j2;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.util.Map;
import java.util.logging.Logger;

@Log4j2
@Controller
public class ZaloPayRedirectController {
    private final OrderService orderService;
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private String key2 = "trMrHtvjo6myautxDUiAcYsVtaeQ8nhf";
    private Mac HmacSHA256;
    private String uri = "http://kubernetes.docker.internal:4200/payment-success";

    public ZaloPayRedirectController(OrderService orderService) throws Exception {
        this.orderService = orderService;
        HmacSHA256 = Mac.getInstance("HmacSHA256");
        HmacSHA256.init(new SecretKeySpec(key2.getBytes(), "HmacSHA256"));
    }

    @GetMapping("/redirect-from-zalopay")
    public RedirectView redirect(@RequestParam Map<String, String> data) {

        String checksumData = data.get("appid") + "|" + data.get("apptransid") + "|" + data.get("pmcid") + "|" + data.get("bankcode") + "|" +
                data.get("amount") + "|" + data.get("discountamount") + "|" + data.get("status");
        byte[] checksumBytes = HmacSHA256.doFinal(checksumData.getBytes());
        String checksum = DatatypeConverter.printHexBinary(checksumBytes).toLowerCase();

        JSONObject result = new JSONObject();
        if (!checksum.equals(data.get("checksum"))) {
            return null;
        } else {
            if (data.get("status").equals("1")) {
                String[] appTransId = data.get("apptransid").trim().split("_");
                String orderId = appTransId[1];
                if (orderId != null && !orderId.isBlank()) {
                    try {
                        log.info(orderId);
                        orderService.changeStatusCheckOut(orderId, Constants.OrderStatus.WAIT_FOR_CONFIRM.name());
                    } catch (Exception e) {
                        log.info(e);
                    }

                }
            }


            // kiểm tra xem đã nhận được callback hay chưa, nếu chưa thì tiến hành gọi API truy vấn trạng thái thanh toán của đơn hàng để lấy kết quả cuối cùng
            return new RedirectView(uri);
        }
    }
}
