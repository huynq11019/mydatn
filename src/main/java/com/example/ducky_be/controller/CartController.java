package com.example.ducky_be.controller;

import com.example.ducky_be.controller.protoco.request.CartRequest;
import com.example.ducky_be.controller.protoco.response.CartResponse;
import com.example.ducky_be.service.CartService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin
@RestController
@RequestMapping("/api/cart")
@RequiredArgsConstructor
public class CartController {

    private final CartService cartService;

    // api cart controller
    // search list cart current user
    @GetMapping("/me/search")
    @ApiOperation(value = "Search list cart current user")
    public ResponseEntity<CartResponse> listCart(@RequestParam  MultiValueMap<String, String> valueMap, Pageable pageable) {
       return ResponseEntity.ok().body(this.cartService.searchmyCart(valueMap, pageable));
    }

    // add cart
    @PostMapping("/me/add")
    @ApiOperation(value = "Add cart")
    public ResponseEntity<CartResponse> addCart(@Valid @RequestBody  CartRequest cartRequest) {
        return ResponseEntity.ok().body(this.cartService.addCartOrUpdate(cartRequest));
    }
    // update cart
    @ApiOperation(value = "update cart")
    @PostMapping("/me/update")
    public ResponseEntity<CartResponse> updateCart(@Valid @RequestBody  CartRequest cartRequest) {
        return ResponseEntity.ok().body(this.cartService.updateCart(cartRequest));
    }

    // delete cart
    @ApiOperation(value = "xóa sản phẩm trong giỏ hàng ")
    @PutMapping("/me/delete")
    public ResponseEntity<CartResponse> deleteCart(@RequestBody  CartRequest request) {
        return ResponseEntity.ok().body(this.cartService.deleteCartItem(request));
    }

    // get total price
    @GetMapping("/customer/{cartID}")
    @ApiOperation(value = "lấy danh sách sác sản phẩm trong giỏ hàng ")
    public ResponseEntity<CartResponse> getCartByUserId(@PathVariable("cartID") String userId) {
        return ResponseEntity.ok().body(this.cartService.getCartByCustomerId(userId));
    }

    // get total quantity
    @GetMapping("/search/{customerId}")
    @ApiOperation(value = "lấy tổng số lượng sản phẩm trong giỏ hàng ")
    public ResponseEntity<CartResponse> getTotalQuantity(@PathVariable("customerId") String userId) {
        return ResponseEntity.ok().body(this.cartService.searchCartByCustomer(userId));
    }


    // get total weight
    @ApiOperation(value = "lấy tổng trọng lượng sản phẩm trong giỏ hàng ")
    @GetMapping("cart/total/weight")
    public ResponseEntity<CartResponse> getTotalItem() {
        return ResponseEntity.ok().body(this.cartService.getMyTotalWeight());
    }

    @ApiOperation(value = "lấy tổng trọng lượng sản phẩm trong giỏ hàng ")
    @PostMapping("cart/find-by-ids")
    public ResponseEntity<CartResponse> getTotalPrice(@RequestBody  CartRequest request) {
        return ResponseEntity.ok().body(this.cartService.findByIds(request.getIds()));
    }
}
