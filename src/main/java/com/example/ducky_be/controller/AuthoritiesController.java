package com.example.ducky_be.controller;

import com.example.ducky_be.controller.protoco.request.AuthoritesRequest;
import com.example.ducky_be.controller.protoco.response.AuthoritesResponse;
import com.example.ducky_be.service.AuthorityService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

@Log4j2
@CrossOrigin("*")
@RequestMapping("/api")
@RestController
public class AuthoritiesController {

    private final AuthorityService authorityService;

    public AuthoritiesController(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    // tạo quyền truy cập
    @ApiOperation(value = "Create authority", notes = "Create authority")
    @PostMapping("/authorities")
    ResponseEntity<AuthoritesResponse> createAuthorities(AuthoritesRequest authoritesRequest) {
        log.info("createAuthorities");
        AuthoritesResponse authoritesResponse = authorityService.createAuthorities(authoritesRequest);
        return ResponseEntity.ok().body(authoritesResponse);
    }

    // gắn quyền cho user
    @ApiOperation(value = "Assign authority", notes = "Assign authority")
    @PutMapping("/authorities/{authorityId}")
    ResponseEntity<AuthoritesResponse> updateAuthority(@PathVariable String authorityId, AuthoritesRequest authorities) {
        log.info("update authorites");

        return ResponseEntity.ok().body(this.authorityService.updateAuthority(authorityId, authorities));
    }

    // xóa quyền truy cập
    @ApiOperation(value = "Delete authority", notes = "Delete authority")
    @DeleteMapping("/authorities/{authoritiesId}")
    @PreAuthorize("hasRole('ADMIN')")
    ResponseEntity<AuthoritesResponse> deleteAuthorities(@PathVariable("authoritiesId") String authoritiesId) {
        log.info("delete authorites");
        this.authorityService.deleteAuthorities(authoritiesId);
        return ResponseEntity.ok().body(new AuthoritesResponse());
    }


    // search quyền cho user
    @ApiOperation(value = "Search authority", notes = "Search authority")
    @GetMapping("/authorities/search")
    ResponseEntity<AuthoritesResponse> searchAuthorities(@RequestParam MultiValueMap<String, String> queryParams, Pageable pageable) {
        log.info("search authorites");
        AuthoritesResponse responseEntity = this.authorityService.searchAuthority(queryParams, pageable);
        return ResponseEntity.ok().body(responseEntity);
    }

    // load quyền cho user
    @GetMapping("/authorities/{userId}")
    ResponseEntity<AuthoritesResponse> loadAuthorities(String userId) {
        log.info("load authorites");
        return null;
    }

    // xóa quyền user

    // tạo quyền cho role

    // gắn quyền cho role

    // search quyền cho role

    // load quyền cho role

    // xóa quyền role


}
