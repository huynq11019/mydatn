package com.example.ducky_be.controller;

import com.example.ducky_be.controller.protoco.request.RefundRequest;
import com.example.ducky_be.controller.protoco.response.RefundResponse;
import com.example.ducky_be.service.RefundService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin("*")
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class RefundController {

    private final RefundService refundService;


    @PostMapping("/orders/{order_id}/order-detail/{order_detail_id}/refund")
    @ApiOperation("Khách hàng tạo đổi trả")
    public ResponseEntity<RefundResponse> create(@RequestBody @Valid RefundRequest request,
                                                 @Valid @PathVariable("order_detail_id") String oderDetailId,
                                                 @RequestParam(value = "files", required = false) List<MultipartFile> files) {

        RefundResponse response = this.refundService.createRefund(request, oderDetailId, files);
        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/refunds/search")
    @ApiOperation("Lấy danh sách danh sách refund ")
    public ResponseEntity<RefundResponse> searchOrder(@RequestParam MultiValueMap<String, String> queryParams,
                                                      Pageable pageable) {
        log.info(queryParams.toString());
        RefundResponse response = this.refundService.getAllRefund(queryParams, pageable);
        return ResponseEntity.ok().body(response);
    }

    @PostMapping("/refunds/update-status")
    @ApiOperation("Cập nhật tiến trình đổi trả")
    public ResponseEntity<RefundResponse> update(@RequestBody @Valid RefundRequest request) {

        RefundResponse response = this.refundService.updateStatus(request);
        return ResponseEntity.ok().body(response);
    }


}
