package com.example.ducky_be.controller;

import com.example.ducky_be.controller.protoco.request.ProductAttributeRequest;
import com.example.ducky_be.controller.protoco.response.ProductAttributeResponse;
import com.example.ducky_be.service.ProductAttributeService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@CrossOrigin("*")
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class ProductAttributeController {
    private final ProductAttributeService productAttributeService;

    @GetMapping("/products/{product-id}/product-attributes")
    @ApiOperation("Lấy danh sách attribute theo category id")
    public ResponseEntity<ProductAttributeResponse> search(@PathVariable("product-id") String productId,
                                                           @RequestParam MultiValueMap<String, String> queryParams, Pageable pageable) {
        log.info(queryParams.toString());
        ProductAttributeResponse response = this.productAttributeService.findProductAttributeByProductId(productId, queryParams, pageable);
        return ResponseEntity.ok().body(response);
    }


    @PostMapping("/products/{product-id}/product-attributes")
    @ApiOperation("Tạo giá trị theo sản phẩm")
    public ResponseEntity<ProductAttributeResponse> create(@PathVariable("product-id") String productId,
                                                           @RequestBody @Valid ProductAttributeRequest request) {
        log.info("productId: {}", productId);
        ProductAttributeResponse response = this.productAttributeService.createProductAttribute(request);
        return ResponseEntity.ok().body(response);
    }

    @PostMapping("/products/{product-id}/product-attributes/{id}")
    @ApiOperation("Lấy danh sách attribute theo category id")
    public ResponseEntity<ProductAttributeResponse> update(@PathVariable("product-id") String productId,
                                                           @PathVariable("id") String id,
                                                           @RequestBody @Valid ProductAttributeRequest request) {

        ProductAttributeResponse response = this.productAttributeService.updateProductAttribute(request, id, productId);
        return ResponseEntity.ok().body(response);
    }
}
