package com.example.ducky_be.controller.protoco.request;

import com.example.ducky_be.controller.protoco.AbstractRequestMessage;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.time.Instant;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class AttributeRequest extends AbstractRequestMessage {
    @NotNull
    private String categoryId;

    private String name;

    private String type;

    private Instant createDate;

    private String createBy;

    private Instant lastUpdate;

    private String updateBy;

    private Integer status;


}
