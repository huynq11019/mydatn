package com.example.ducky_be.controller.protoco.request;

import com.example.ducky_be.controller.protoco.AbstractRequestMessage;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetailRequest extends AbstractRequestMessage {

    private String id;

    private String orderId;

    @NotBlank(message = "Sẩn phẩm id không được để trống")
    private String productId;

    private BigDecimal productPrice; // price of product item

    @NotBlank(message = "Loại sản phẩm không được để trống")
    private String optionProductId;

    @NotNull(message = "Số lượng không được để trống")
    @Min(value = 1, message = "Số lượng phải lớn hơn 0")
    private Integer quantity; // số lượng sản phẩm của đơn hàng

    private Date warrantyTime; // thời hạn bảo hành

    @Size(max = 3000)
    private String note;

    private Integer status;

    @NotBlank(message = "ID item không được để trống")
    private String cartItemId;

}
