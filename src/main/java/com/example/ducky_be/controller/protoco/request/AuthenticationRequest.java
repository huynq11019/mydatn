package com.example.ducky_be.controller.protoco.request;

import com.example.ducky_be.controller.protoco.AbstractRequestMessage;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthenticationRequest extends AbstractRequestMessage {
    @NotNull
    @NotBlank
    private String userName;
    @NotNull
    @NotBlank
    private String passWord;

    private boolean rememberMe;
}
