package com.example.ducky_be.controller.protoco.request;

import com.example.ducky_be.controller.protoco.AbstractRequestMessage;
import com.example.ducky_be.repository.entity.OptionProduct;
import lombok.*;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.validation.constraints.*;
import java.util.List;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequest extends AbstractRequestMessage {


    private String id;

    @NotEmpty(message = "product must not be empty")
    @Size(max = 256, message = "Độ dài tên sản phẩm < 256 ký tự")
    private String productName;

    @Size(max = 256, message = "Độ dài tên subname  < 256 ký tự")
    private String subCateName; // tên ngắn

    @NotBlank(message = "Mô tả sản phẩm không được để trống")
    private String description;

//    private String thumbNail;

//    @Min(value = 0, message = "Giá sản phẩm phải lớn hơn 0")
//    private Long price;

    @Min(value = 0, message = "Discount phải lớn hơn 0")
    @Max(value = 100, message = "Discount phải nhỏ hơn 100")
    private float discount;

    private Boolean isOrder;

    private Integer estimateTime;

    private String metaData;

//    private long quantity;

    private Long serial;

    private Long categoryId;

    //option của sản phẩm
    @NotBlank(message = "option must not be empty")
//    private List<OptionProduct> optionProducts;
    private String optionProducts;


}
