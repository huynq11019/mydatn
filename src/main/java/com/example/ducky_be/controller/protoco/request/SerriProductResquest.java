package com.example.ducky_be.controller.protoco.request;

import lombok.*;

import java.time.Instant;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class SerriProductResquest {
    private Long id;

    private String serriName;

    private String serriDescroption;

    // status -1 là nháp, 0 xóa, 1 active

    private Integer status;
}
