package com.example.ducky_be.controller.protoco.response;

import com.example.ducky_be.controller.protoco.AbstractResponseMessage;
import com.example.ducky_be.service.dto.FileUploadDTO;
import com.example.ducky_be.service.dto.ProductDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

@Data
@Builder
@ToString
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@RedisHash("ProductResponse")
public class ProductResponse extends AbstractResponseMessage implements Serializable {

    private String id;

    private String productName;

    private String thumbNail;

    private String subCateName;

    private String description;

    private Long price;

    private Float discount;

    private Long supplierId;

    private Boolean isOrder;

    private String metaData;

    private Instant createDate;

    private String createBy;

    private Instant lastUpdate;

    private String updateBy;

    // status -1 là nháp, 0 xóa, 1 active
    private Integer status;

    private ProductDTO productDTO;

    private List<ProductDTO> listProductDTO;

    private Long quantity;

    private List<FileUploadDTO> images;

    private String categoryName;

    private String CategoryId;

    private String Serrial;



}
