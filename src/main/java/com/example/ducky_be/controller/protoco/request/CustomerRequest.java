package com.example.ducky_be.controller.protoco.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CustomerRequest {
    private String id;


//    private String userName;

    @NotBlank
    private String passwordHash;

    //    @NotBlank
    private String passsowordSlat;

    @NotBlank
    @Pattern(regexp = "^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$")
    private String phoneNumber;

    @NotBlank
    @Email()
    private String email;

    private String address;

    private String fullName;

    private Integer gender;

    //    @NotBlank
//    @Pattern(regexp = "^\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12][0-9]|3[01])$")
    private String dob;

    private String avatar;

    private Integer rewardPoint;

    private String classify;

    private String ipAddress;

    private String uuid;

    @NotBlank
    private String captcha;

    private boolean agree;

    /**
     * @author: huynq
     * @since: 10/23/2021 9:07 PM
     * @description: Thông tin này là bắt buộc với mỗi Entity
     * @update:
     */
    private Instant createDate = Instant.now();

    private String createBy;

    private Instant lastUpdate = Instant.now();

    private Long updateBy;

    // status -1 là nháp, 0 xóa, 1 active
    private Integer status;

}
