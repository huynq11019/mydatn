package com.example.ducky_be.controller.protoco.response;

import com.example.ducky_be.controller.protoco.AbstractResponseMessage;
import com.example.ducky_be.service.dto.CategoryDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Data
@ToString
@Builder
public class CategoryResponse extends AbstractResponseMessage implements Serializable {

    private static final long serialVersionUID = -8121712683888007843L;

    /*
     * @author: NamTH
     * @since: 31/10/2021 3:26 CH
     * @description:  category response
     * @update:
     *
     * */
    private Long id;

    private String categoryName;

    private String icon;

    private String description;

    private Integer sortOrder;

    private Instant createDate;

    private String createBy;

    private Instant lastUpdate;

    private String updateBy;

    // status -1 là nháp, 0 xóa, 1 active
    private Integer status;

    private List<CategoryDTO> categoryDTOS;

    private CategoryDTO category;


}
