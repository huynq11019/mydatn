package com.example.ducky_be.controller.protoco.request;

import com.example.ducky_be.service.dto.AuthorityDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AuthoritesRequest {
    private String authrotityCode;
    private String authroriryName;
    private String roleId;
    private List<AuthorityDTO> authorityDTOS;
}
