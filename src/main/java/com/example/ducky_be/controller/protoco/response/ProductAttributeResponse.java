package com.example.ducky_be.controller.protoco.response;

import com.example.ducky_be.controller.protoco.AbstractResponseMessage;
import com.example.ducky_be.service.dto.ProductAttributeDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.time.Instant;
import java.util.List;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductAttributeResponse extends AbstractResponseMessage {
    private String id;

    private String attributeId;

    private String productId;

    private String value;

    private Instant createDate;

    private String createBy;

    private Instant lastUpdate;

    private String updateBy;

    private Integer status;

    private ProductAttributeDTO productAttribute;

    private List<ProductAttributeDTO> productAttributeList;

}
