package com.example.ducky_be.controller.protoco.request;

import com.example.ducky_be.controller.protoco.AbstractRequestMessage;
import com.example.ducky_be.service.dto.ProductAttributeDTO;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class ProductAttributeRequest extends AbstractRequestMessage {

    private Instant createDate;

    private String createBy;

    private Instant lastUpdate;

    private String updateBy;

    private Integer status;

    private List<ProductAttributeDTO> productAttributes;
}
