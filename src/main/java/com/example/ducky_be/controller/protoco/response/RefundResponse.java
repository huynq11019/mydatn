package com.example.ducky_be.controller.protoco.response;

import com.example.ducky_be.controller.protoco.AbstractResponseMessage;
import com.example.ducky_be.service.dto.FileUploadDTO;
import com.example.ducky_be.service.dto.RefundDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

@Data
@Builder
@ToString
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@RedisHash("RefundResponse")
public class RefundResponse extends AbstractResponseMessage implements Serializable {

    private String id;

    private String orderDetailId;

    private Long quantity;

    private Long moneyBack;

    private Integer processingStatus;

    private String description;


    private Instant createDate;

    private String createBy;

    private Instant lastUpdate;

    private String updateBy;

    // status -1 là nháp, 0 xóa, 1 active
    private Integer status;

    private RefundDTO refundDTO;

    private List<RefundDTO> refundDTOList;

    private List<FileUploadDTO> images;


}
