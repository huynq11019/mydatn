package com.example.ducky_be.controller.protoco.response;

import com.example.ducky_be.controller.protoco.AbstractResponseMessage;
import com.example.ducky_be.core.sercurity.JwtToken;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Builder
@Data
@AllArgsConstructor
@Slf4j
@JsonInclude(JsonInclude.Include.NON_NULL)
@RequiredArgsConstructor
public class AuthenticationResponse extends AbstractResponseMessage {
    private JwtToken accessToken;

    private JwtToken auth;

    private JwtToken refreshToken = new JwtToken();

    public AuthenticationResponse(JwtToken jwtToken) {
        this.accessToken = jwtToken;
    }
}
