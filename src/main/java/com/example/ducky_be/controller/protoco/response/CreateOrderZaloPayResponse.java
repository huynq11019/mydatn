package com.example.ducky_be.controller.protoco.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
public class CreateOrderZaloPayResponse {
    private Integer returncode;

    private String returnmessage;

    private String orderurl;

    private String zptranstoken;
}
