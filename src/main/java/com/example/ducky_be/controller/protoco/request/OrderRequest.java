package com.example.ducky_be.controller.protoco.request;

import com.example.ducky_be.controller.protoco.AbstractRequestMessage;
import com.example.ducky_be.core.constants.Constants;
import lombok.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.List;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class OrderRequest extends AbstractRequestMessage {

    private String id;
    private String address;

    @NotBlank(message = "phone Number is required")
    @Pattern(regexp = Constants.Regex.phoneNumber, message = "phone number is invalid")
    private String phoneNumber;

    private String customerId;

    private String receiver; // tên người nhận

    private Constants.PaymentMethod paymentMethod;

    private Long shippingCost;

    private Integer status;

    private String note;

    private Integer allowReturn;

    private String signature;

    private String shippingid;

    private Constants.OrderStatus orderStatus;

    private Long addressId;

    private String capchaCode;
    @Valid
    private List<OrderDetailRequest> orderDetails;
}
