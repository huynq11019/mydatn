package com.example.ducky_be.controller.protoco.response;

import com.example.ducky_be.service.dto.AuthorityDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthoritesResponse {
    private String authrorityId;

    private String authrotityCode;
    private String authroriryName;
    private String roleId;
    private Instant createDate;
    private String createBy;
    private Instant lastUpdate;
    private String updateBy;
    private Integer status;

    private AuthorityDTO authorityDTO;
    private List<AuthorityDTO> authorityDTOS;
}
