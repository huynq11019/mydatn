package com.example.ducky_be.controller.protoco.response;

import com.example.ducky_be.controller.protoco.AbstractResponseMessage;
import com.example.ducky_be.service.dto.RatingDTO;
import lombok.*;

import java.time.Instant;
import java.util.List;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class RatingResponse extends AbstractResponseMessage {

    private String id;

    private String userRating;

    private String productId;

    private Integer point;

    private String rateContent;

    private Instant createDate;

    private String createBy;

    private Instant lastUpdate;

    private String updateBy;

    // status -1 là nháp, 0 xóa, 1 active

    private Integer status;

    private String orderId;

    private RatingDTO ratingDTO;

    private List<RatingDTO> ratingDTOList;
}
