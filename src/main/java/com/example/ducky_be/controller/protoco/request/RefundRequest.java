package com.example.ducky_be.controller.protoco.request;

import com.example.ducky_be.controller.protoco.AbstractRequestMessage;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class RefundRequest extends AbstractRequestMessage {


    private String id;

    @NotBlank(message = "Order detail not empty")
    private String orderDetailId;

    private Long quantity;

    private Long moneyBack;
    @NotNull(message = "Processing status not empty")
    private Integer processingStatus;

    @NotBlank(message = "description not empty")
    private String description;



}
