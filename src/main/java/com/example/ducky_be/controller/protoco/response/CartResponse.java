package com.example.ducky_be.controller.protoco.response;

import com.example.ducky_be.service.dto.CartDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CartResponse {
    private String id;

    private String customerId;

    private String productId;

    private Long productPrice;

    private String productName;

    private Integer quantity;

    private Integer totalPrice;

    private Instant createDate;

    private String createBy;

    private Instant lastUpdate;

    private String updateBy;

    private CartDTO cartDTO;

    private Integer totalCartItem;

    private List<CartDTO> cartDTOs;

    private Integer pageItem;
}
