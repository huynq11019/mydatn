package com.example.ducky_be.controller.protoco.response;

import com.example.ducky_be.service.dto.ProductDTO;
import com.example.ducky_be.service.dto.SerriProductDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.time.Instant;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Data
@Builder
public class SerriProductResponse extends BaseResponse {
    private Long id;

    private String serriName;

    private String serriDescroption;

    private Instant createDate;

    private String createBy;

    private Instant lastUpdate;

    private String updateBy;

    // status -1 là nháp, 0 xóa, 1 active

    private Integer status;

    private List<SerriProductDTO> serriProductDTOList;

    private SerriProductDTO serriProductDTO;

    private ProductDTO productDTO;


}
