package com.example.ducky_be.controller.protoco.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class CreateOrderZaloPayRequest {


    @NotNull
    private String appuser;

    @NotNull
    private Long apptime;

    @NotNull
    private Long amount;

    @NotNull
    private String apptransid;

    @NotNull
    private String embeddata;

    @NotNull
    private String item;

    @NotNull
    private String mac;

    @NotNull
    private String bankcode;

    private String description;

    private String phone;

    private String email;

    private String address;

    private String subappid;

    private String redirecturl;

    private String columninfo;

//    private String promotioninfo;

    private String zlppaymentid;

    @NotBlank
    private String orderId;


}
