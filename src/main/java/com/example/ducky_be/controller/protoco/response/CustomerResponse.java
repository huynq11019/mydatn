package com.example.ducky_be.controller.protoco.response;

import com.example.ducky_be.controller.protoco.AbstractResponseMessage;
import com.example.ducky_be.service.dto.AddressDTO;
import com.example.ducky_be.service.dto.CustomerDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerResponse extends AbstractResponseMessage implements Serializable {
    private static final long serialVersionUID = 7156526077883281625L;
    private String id;

//    private String userName;

    private String passwordHash;

    private String passsowordSlat;

    private String phoneNumber;

    private String email;

    private String address;

    private String fullName;

    private Integer gender;

    private String dob;

    private String avatar;

    private Integer rewardPoint;

    private String classify;

    private String ipAddress;

    private String uuid;

    /**
     * @author: huynq
     * @since: 10/23/2021 9:07 PM
     * @description: Thông tin này là bắt buộc với mỗi Entity
     * @update:
     */
    private Instant createDate;

    private String createBy;

    private Instant lastUpdate;

    private String updateBy;

    // status -1 là nháp, 0 xóa, 1 active
    private Integer status;

    private List<CustomerDTO> customerDTOS;

    private CustomerDTO customerDTO;

    private Integer recordChange;

}
