package com.example.ducky_be.controller.protoco.request;

import lombok.*;

import java.time.Instant;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class FavouritesRequest {
    private String id;

    private String customerId;

    private String productId;

    private Integer status;
}
