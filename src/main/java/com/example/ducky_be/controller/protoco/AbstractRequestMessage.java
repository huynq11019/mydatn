package com.example.ducky_be.controller.protoco;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public abstract class AbstractRequestMessage {
    /**
     * The user name.
     */
    protected String userName;

    /**
     * The user id.
     */
    protected String userId = "-1";
}
