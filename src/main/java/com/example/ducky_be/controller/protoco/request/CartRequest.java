package com.example.ducky_be.controller.protoco.request;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class CartRequest {

    private String id;

    private String customerId;

    @NotBlank(message = "cart.product.notnull")
    private String productId;

//    @NotBlank(message = "Cần chọn loại sản phẩm")
    private String optionProductId;

    @Min(value = 1, message = "Quantity must be greater than 0")
    private int quantity;

    private List<String> deletedProductIds;

    List<String> ids;

}
