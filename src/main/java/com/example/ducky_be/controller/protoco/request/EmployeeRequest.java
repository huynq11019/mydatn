package com.example.ducky_be.controller.protoco.request;

import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.service.dto.AuthorityDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class EmployeeRequest {

    private String id;

    @NotBlank(message = "Password không được để trống")
    @Size(min = 8, max = 20, message = "Password phải có độ dài từ 8 đến 20 ký tự")
    private String password;
    @NotNull
    @NotNull
    @Email(message = "Email không đúng định dạng", regexp = Constants.Regex.email)
    private String email;
    @Length(min = 0, max = 60)
    private String address;
    @NotNull
    @NotBlank
    @Pattern(regexp = Constants.Regex.phoneNumber,
            message = "Số điện thoại không đúng định dạng")
    private String phoneNumber;

    @NotBlank(message = "Họ không được để trống")
    private String fullName;

    private Integer gender;

    private Date dob;

    private Long role;

    private List<AuthorityDTO> authorities;
}
