package com.example.ducky_be.controller.protoco.response;

import com.example.ducky_be.controller.protoco.AbstractResponseMessage;
import com.example.ducky_be.service.dto.OrderDetailDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Date;
import java.util.List;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderDetailResponse extends AbstractResponseMessage {
    private String id;

    private String orderId;

    private String productId;

    private BigDecimal price;

    private String optionProductId;

    private String quantity;

    private Date warrantyTime;

    private BigDecimal totalPrice;

    private String paymentMethod;

    private String shippingPrice;

    private Instant createDate;

    private String createBy;

    private Instant lastUpdate;

    private String updateBy;

    private Integer status;

    private String note;

    private OrderDetailDTO orderDetail;

    private List<OrderDetailDTO> orderDetailList;
}
