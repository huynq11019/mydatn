package com.example.ducky_be.controller.protoco.response;

import com.example.ducky_be.controller.protoco.AbstractResponseMessage;
import com.example.ducky_be.service.dto.FileUploadDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FileUploadResponse extends AbstractResponseMessage {
    private String fileName;
    private String fileType;
    private long size;
    /**
     * The list model.
     */
    private List<FileUploadDTO> listModel;

    private FileUploadDTO fileUploadDTO;
}
