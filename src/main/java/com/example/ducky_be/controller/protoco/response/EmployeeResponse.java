package com.example.ducky_be.controller.protoco.response;

import com.example.ducky_be.controller.protoco.AbstractResponseMessage;
import com.example.ducky_be.service.dto.EmployeeDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
//import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Data
@Builder
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class EmployeeResponse extends AbstractResponseMessage implements Serializable {

    private String id;

    private String userName;

    @JsonIgnore
    private String p;

    private String email;

    private String address;

    private String fullName;

    private Integer gender;

    private Date dob;

    private String avatar;

    private String role;

    private EmployeeDTO employeeDTO;

    private Instant createDate;

    private String createBy;

    private Instant lastUpdate;

    private String updateBy;

    // status -1 là nháp, 0 xóa, 1 active
    private Integer status;

    private String phoneNumber;

    private List<EmployeeDTO> employeeDTOs = new ArrayList<>();

}
