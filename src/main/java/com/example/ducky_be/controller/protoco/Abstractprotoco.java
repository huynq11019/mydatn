package com.example.ducky_be.controller.protoco;

import java.time.Instant;

public abstract class Abstractprotoco {
    private Instant createDate = Instant.now();

    private Long createBy;

    private Instant lastUpdate = Instant.now();

    private Long updateBy;

    // status -1 là nháp, 0 xóa, 1 active
    private Integer status;

}
