package com.example.ducky_be.controller.protoco.response;

import com.example.ducky_be.controller.protoco.AbstractResponseMessage;
import com.example.ducky_be.service.dto.AddressDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.Date;
import java.util.List;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddressResponse extends AbstractResponseMessage {

    private Long id;

    private String nameOfRecipient;

    private String province;

    private String district;

    private String ward;

    private String phoneNumber;

    private String addressDetail;

    private String latitude;

    private String  createBy;

    private Date createDate;

    private Date lastUpdate;

    // status -1 là nháp, 0 xóa, 1 active
    private Integer status;

    private String updateBy;

    private String  customerId;

    private Boolean isDefault;


    private String addressType;

    private AddressDTO addressDTO;

    private List<AddressDTO> addressDTOS;
}
