package com.example.ducky_be.controller.protoco.request;

import com.example.ducky_be.controller.protoco.AbstractRequestMessage;
import com.example.ducky_be.service.dto.RatingDTO;
import lombok.*;

import java.time.Instant;
import java.util.List;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class RatingRequest extends AbstractRequestMessage {
//    private String id;


    private String productId;

    private String rateContent;

    // status -1 là nháp, 0 xóa, 1 active

    private Integer status;

    private String orderId;

    private RatingDTO ratingDTO;

}
