package com.example.ducky_be.controller.protoco.request;

import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.message.LabelKey;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class AddressRequest {

    private Long id;

    @NotBlank(message = LabelKey.ADDRESS_REQUEST_NOT_BLANK)
    private String nameOfRecipient;

    @Pattern(regexp = Constants.Regex.phoneNumber, message = "Phone number must be 10 digits")
    private String phoneNumber;

    private String province;
    @NotNull
    private Integer provinceId;

    private String district;

    @NotNull
    private Integer districtId;

    private String ward;
    @NotNull
    private String wardId;

    private String street;

    private Boolean isDefault;

    @NotBlank(message = LabelKey.ADDRESS_REQUEST_NOT_BLANK)
    private String addressDetail;

    private String latitude;

    // status -1 là nháp, 0 xóa, 1 active
    private Integer status;

    @NotBlank
    private String addressType;

}
