package com.example.ducky_be.controller.protoco.request;

import lombok.*;

import javax.validation.constraints.NotBlank;
import java.time.Instant;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class CategoryRequest {
    /*
     * @author: NamTH
     * @since: 31/10/2021 1:56 CH
     * @description:  request category
     * @update:
     *
     * */
    private Long id;

    @NotBlank
    private String categoryName;

    private String icon;

    private String description;

    private Integer sortOrder;

    private Long parentCategory;

    private Instant createDate;

    private String createBy;

    private Instant lastUpdate;

    private String updateBy;

    private Integer status;


}
