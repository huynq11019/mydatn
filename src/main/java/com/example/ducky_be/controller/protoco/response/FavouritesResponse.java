package com.example.ducky_be.controller.protoco.response;

import com.example.ducky_be.controller.protoco.AbstractResponseMessage;
import com.example.ducky_be.service.dto.FavouritesDTO;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.time.Instant;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Data
@Builder
public class FavouritesResponse extends AbstractResponseMessage {
    private String id;

    private String customerId;

    private String productId;

    private Instant dateAt;

    private Instant createDate;

    private String createBy;

    private Instant lastUpdate;

    private String updateBy;

    private Integer status;

    private String productThumnail; // huy sửa

    private String likeCount; // huy sửa

    private Long price; // huy sửa

    private List<FavouritesDTO> favouritesDTOS;

    private FavouritesDTO favouritesDTO;
}
