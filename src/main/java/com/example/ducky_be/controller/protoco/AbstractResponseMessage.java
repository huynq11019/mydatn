package com.example.ducky_be.controller.protoco;

import com.example.ducky_be.core.message.ErrorCode;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class AbstractResponseMessage   {
    private Long totalItem;
    /**
     * The error code.
     */
    protected ErrorCode errorCode = ErrorCode.ERR_00000;

    /**
     * The message.
     */
    protected String message = ErrorCode.ERR_00000.getMessage();

    /**
     * Sets the error code.
     *
     * @param errorCode the new error code
     */
    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
        this.message = errorCode.getMessage();
    }
}
