package com.example.ducky_be.controller;

import com.example.ducky_be.controller.protoco.request.AuthoritesRequest;
import com.example.ducky_be.controller.protoco.request.EmployeeRequest;
import com.example.ducky_be.controller.protoco.response.EmployeeResponse;
import com.example.ducky_be.service.EmployeeService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Log4j2
@CrossOrigin
@RestController
@RequestMapping("/api")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(value = "/employee/create", method = RequestMethod.POST)
    public ResponseEntity<EmployeeResponse> createUser(@RequestBody @Valid EmployeeRequest employeeRequest) throws Exception {
        return ResponseEntity.ok(employeeService.createEmployee(employeeRequest));
    }
    @PutMapping("/employee/{employeeid}")
    public ResponseEntity<EmployeeResponse> updateEmployee(@PathVariable("employeeid") String uuid,@RequestBody  EmployeeRequest employeeRequest) {
        employeeRequest.setId(uuid);
        return ResponseEntity.ok(employeeService.updateEmployee(employeeRequest));
    }

    @GetMapping("/employee/{employeeid}")
    public ResponseEntity<EmployeeResponse> getEmployeeById(@PathVariable("employeeid") String empoyeeId) {
        return ResponseEntity.ok(employeeService.getEmployeeById(empoyeeId));
    }

    @GetMapping("/employee/search")
    @ApiOperation(value = "Search employee ")
    public ResponseEntity<EmployeeResponse> searchEmployee(@RequestParam MultiValueMap<String, String> queryParams, Pageable pageable) {
        return ResponseEntity.ok().body(this.employeeService.searchEmployee(queryParams, pageable));
    }

    @GetMapping("/employee/account/me")
    @ApiOperation(value = "Search employee ")
    public ResponseEntity<EmployeeResponse> myCurrentEmployee() {
        return ResponseEntity.ok().body(this.employeeService.getCurrentEmployee());
    }
    @ApiOperation(value = "employee update profile")
    @PutMapping("/employee/me")
    public ResponseEntity<EmployeeResponse> meUpdateEmployee(@RequestBody EmployeeRequest employeeRequest) {
        return ResponseEntity.ok().body(this.employeeService.updateEmployee(employeeRequest));
    }

    @ApiOperation(value = "lock account employee")
    @GetMapping("/employee/{employeeid}/lock")
    public ResponseEntity<EmployeeResponse> lockAccountEmployee(@PathVariable("employeeid") String employeeId) {
        return ResponseEntity.ok().body(this.employeeService.changeStatusEmployee(employeeId));
    }

    @GetMapping("/employee/{employeeid}/rsp")
    @ApiOperation(value = "unlock account employee")
    public ResponseEntity<EmployeeResponse> resetPassword(@PathVariable("employeeid") String employeeId) {

        return ResponseEntity.ok().body(this.employeeService.resetPassword(employeeId));
    }

    @PostMapping("/employee/{employee-id}/update-authority")
    @ApiOperation(value = "Tạo author cho nhân viên")
    public ResponseEntity<EmployeeResponse> updateAuthority(@PathVariable("employee-id") String employeeId,
                                                            AuthoritesRequest requests) {
        EmployeeResponse response = this.employeeService.setAuthorityForEmployee(employeeId, requests);
        return ResponseEntity.ok().body(response);
    }
}
