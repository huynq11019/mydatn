package com.example.ducky_be.controller;

import com.example.ducky_be.controller.protoco.request.AuthenticationRequest;
import com.example.ducky_be.controller.protoco.response.AuthenticationResponse;
import com.example.ducky_be.core.constants.SecurityConsants;
import com.example.ducky_be.core.sercurity.CustomUserDetail;
import com.example.ducky_be.core.sercurity.JwtToken;
import com.example.ducky_be.core.sercurity.TokenPovider;
import io.jsonwebtoken.impl.DefaultClaims;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Log4j2
@CrossOrigin("*")
@RestController
public class AuthenticationController {
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenPovider tokenPovider;


    private long jwtExpirationInMs = SecurityConsants.JWTEXPIRATIONINMS;
    private long refreshExpirationDateInMs = SecurityConsants.REFRESH_JWTEXPIRATIONINMS;

    @RequestMapping(value = "/api/authenticate", method = RequestMethod.POST)
    @ApiOperation("Người dùng đăng nhập")
    public ResponseEntity<AuthenticationResponse> createAuthenticationToken(@RequestBody @Valid AuthenticationRequest authenticationRequest)
            throws Exception {
        AuthenticationResponse authenticationResponse = new AuthenticationResponse();

        log.info(authenticationRequest.toString());
        UsernamePasswordAuthenticationToken authReq
                = new UsernamePasswordAuthenticationToken(authenticationRequest.getUserName(), authenticationRequest.getPassWord());
        Authentication auth = authenticationManager.authenticate(authReq);
        log.info(auth.toString());
        CustomUserDetail customUserDetail = (CustomUserDetail) auth.getPrincipal();
        log.info(customUserDetail.toString());

        // access token của user
        String token = tokenPovider.generateToken(customUserDetail);
        String userId = tokenPovider.getUserIdFromtToken(token);
        log.info("userId: " + userId);

        JwtToken accessToken = new JwtToken();
        accessToken.setToken(token);
        accessToken.setTokenExpDate(new Date(System.currentTimeMillis() + jwtExpirationInMs));
        accessToken.setCreatedDate(new Date(System.currentTimeMillis()));
        authenticationResponse.setAccessToken(accessToken);

        // refresh token của user
        if (authenticationRequest.isRememberMe()) {
            JwtToken refreshToken = tokenPovider.createRefreshToken(auth);
            authenticationResponse.setRefreshToken(refreshToken);
        }
        SecurityContextHolder.getContext().setAuthentication(auth);

        return ResponseEntity.ok(authenticationResponse);

    }

//    @RequestMapping(value = "/createEmployee", method = RequestMethod.POST)
//    public ResponseEntity<EmployeeResponse> saveUser(@RequestBody @Valid EmployeeRequest user) throws Exception {
//
//
//        return ResponseEntity.ok(employeeService.createEmployee(user));
//    }

    @RequestMapping(value = "/api/refreshtoken", method = RequestMethod.GET)
    public ResponseEntity<?> refreshtoken(HttpServletRequest request) throws Exception {
        // From the HttpRequest get the claims
        DefaultClaims claims = (io.jsonwebtoken.impl.DefaultClaims) request.getAttribute("claims");

        Map<String, Object> expectedMap = getMapFromIoJsonwebtokenClaims(claims);
        String token = tokenPovider.doGenerateRefreshToken(expectedMap, expectedMap.get("sub").toString());

        JwtToken jwtToken = new JwtToken();
        jwtToken.setToken(token);

        return ResponseEntity.ok(new AuthenticationResponse(jwtToken));
    }

    public Map<String, Object> getMapFromIoJsonwebtokenClaims(DefaultClaims claims) {
        Map<String, Object> expectedMap = new HashMap<String, Object>();
        for (Map.Entry<String, Object> entry : claims.entrySet()) {
            expectedMap.put(entry.getKey(), entry.getValue());
        }
        return expectedMap;
    }

}
