package com.example.ducky_be.controller;

import com.example.ducky_be.controller.protoco.request.RatingRequest;
import com.example.ducky_be.controller.protoco.response.RatingResponse;
import com.example.ducky_be.service.RatingProductService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Log4j2
@CrossOrigin("*")
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class RatingProductControler {
    private final RatingProductService ratingProductService;

    @GetMapping(value = "/rating/product/{productID}")
    @ApiOperation("Tìm kiếm danh sách các serri của sản phẩm")
    public ResponseEntity<RatingResponse> getRatingByProduct(@PathVariable("productID") String productID,
                                                             Pageable pageable) {
        return ResponseEntity.ok().body(this.ratingProductService.getRatingProductByProductId(productID, pageable));
    }

    @GetMapping(value = "/rating/{ratingId}")
    @ApiOperation("láy thông tin rating theo id")
    public ResponseEntity<RatingResponse> getRatingByid(@PathVariable("ratingId") String id) {
        return ResponseEntity.ok().body(this.ratingProductService.getRatingById(id));
    }

    @PostMapping(value = "/rating/{productID}")
    @ApiOperation("Tạo đánh giá theo sản phẩm")
    public ResponseEntity<RatingResponse> createRating(@PathVariable("productID") String productId,
                                                       @RequestBody RatingRequest ratingRequest) {
        this.ratingProductService.addRatingProduct(productId, ratingRequest);
        return ResponseEntity.ok().build();
    }

//    @GetMapping(value = "/rating/product/{productID}")
//    public ResponseEntity<RatingResponse> updateRating(@PathVariable("productId") String ratingId, Pageable pageable) {
//        return ResponseEntity.ok().body(this.ratingProductService.getRatingProductByProductId(ratingId, pageable));
//    }

    @PutMapping(value = "/rating/{ratetingID}")
    @ApiOperation("Cập nhật đánh giá")
    public ResponseEntity<RatingResponse> updateRating(@PathVariable("ratetingID") String ratingId,
                                                       @RequestBody RatingRequest ratingRequest) {
        this.ratingProductService.updateRatingProduct(ratingId, ratingRequest);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping(value = "/rating/{ratingId}")
    @ApiOperation("Xóa đánh giá")
    public ResponseEntity<RatingResponse> deleteRating(@PathVariable("ratingId") String ratingId) {
        this.ratingProductService.deleteRatingProduct(ratingId);
        return ResponseEntity.ok().build();
    }

}
