package com.example.ducky_be.controller;

import com.example.ducky_be.repository.PostRepository;
import com.example.ducky_be.repository.RoleRepository;
import com.example.ducky_be.repository.entity.Post;
import com.example.ducky_be.repository.entity.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("test")
public class TestController {
    @Autowired
    PostRepository postRepository;

    @Autowired
    RoleRepository roleRepository;

    @GetMapping("post")
    public ResponseEntity<List<Post>> post() {
        return ResponseEntity.ok(postRepository.findAll());
    }

    @GetMapping("role")
    public ResponseEntity<List<Role>> role() {
        return ResponseEntity.ok(roleRepository.findAll());
    }
}
