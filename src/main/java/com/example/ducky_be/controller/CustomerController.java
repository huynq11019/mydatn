package com.example.ducky_be.controller;

import com.example.ducky_be.controller.protoco.request.CustomerRequest;
import com.example.ducky_be.controller.protoco.response.CustomerResponse;
import com.example.ducky_be.service.CustomerService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@CrossOrigin("*")
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService customerService;

    @GetMapping("/customer/search")
    @ApiOperation("Tìm kism tài khoản khách hàng")
    public ResponseEntity<CustomerResponse> search(@RequestParam MultiValueMap<String, String> queryParams, Pageable pageable) {
        log.info(queryParams.toString());
        CustomerResponse response = this.customerService.searchCustomer(queryParams, pageable);
        return ResponseEntity.ok().body(response);
    }

    /**
     * @author: huynq
     * @since: 10/23/2021 9:47 PM
     * @description: Khách hàng đăng ký tài khoản
     * @update:
     */

    @PostMapping("/customer/register")
    @ApiOperation("khách hàng thực hiện đăng ký tài khảon")
    public ResponseEntity<CustomerResponse> customerRegister(@RequestBody @Valid CustomerRequest request) {
        log.info("customer request=>>>>> " + request);
        CustomerResponse response = this.customerService.registerUser(request);

        return ResponseEntity.ok().body(response);
    }

    // fin customer

    /**
     * @author: huynq
     * @since: 10/23/2021 9:46 PM
     * @description: tìm customer theo Id người dùng
     * @update:
     */

    @GetMapping("/customer/{customerId}")
    public ResponseEntity<CustomerResponse> findByCustomerId(@PathVariable("customerId") String customerId) {
        CustomerResponse response = this.customerService.findCustomerByid(customerId);
        return ResponseEntity.ok().body(response);
    }

    // lock customer

    /**
     * @author: huynq
     * @since: 10/23/2021 9:44 PM
     * @description: admin khóa tài khoản người dùng
     * @update:
     */
    @GetMapping("/customer/{customerId}/{status}")
    @ApiOperation("Khóa hoặc ở khóa customer")
    public ResponseEntity<CustomerResponse> customerEx(@PathVariable("customerId") String customerId,
                                                       @PathVariable("status") Integer status) {

        CustomerResponse response = this.customerService.lockCustomer(customerId, status);
        return ResponseEntity.ok().body(response);
    }

    /**
     * @author: huynq
     * @since: 10/23/2021 9:43 PM
     * @description: customer thực hiện xác thự tài khảon
     * @update:
     */
    @GetMapping("/customer/{customer_id}/verify")
    @ApiOperation("customer thực hiện xác thực tài khoản")
    public ResponseEntity<CustomerResponse> customerVerify(@PathVariable("customer_id") String customerId) {
        CustomerResponse response = this.customerService.verifyAccount(customerId);

        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/customer/forgot-password")
    public ResponseEntity<CustomerResponse> customerForgotPassword(@RequestParam("email") String email) {
        CustomerResponse response = this.customerService.forgotPassword(email);

        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/account/my-profile")
    @ApiOperation("customer lấy thông tin tài khoản người dùng")
    public ResponseEntity<CustomerResponse> myProfile() {

        CustomerResponse response = this.customerService.myProfile();

        return ResponseEntity.ok().body(response);
    }

    @PutMapping("/customer/account/update-profile")
    @ApiOperation("customer cập nhật thông tin tài khoản người dùng")
    public ResponseEntity<CustomerResponse> updateProfile(@Valid @RequestBody CustomerRequest request) {
        CustomerResponse response = this.customerService.updateProfile(request);

        return ResponseEntity.ok().body(response);
    }

    /**
     * @author: NamTH
     * @since: 07/12/2021 9:51 PM
     * @description: kích hoạt tài khoản
     */
    @GetMapping("/account/active")
    @ApiOperation("active tài khoản customer")
    public ResponseEntity<CustomerResponse> activeAccount() {
        CustomerResponse response = this.customerService.activeAccount();

        return ResponseEntity.ok().body(response);
    }

    @PostMapping("/customer/reset-password")
    @ApiOperation("active tài khoản customer")
    public ResponseEntity<CustomerResponse> resetPassword(@RequestParam("newPassword") String password) {
        CustomerResponse response = this.customerService.resetPassword(password);

        return ResponseEntity.ok().body(response);
    }

}
