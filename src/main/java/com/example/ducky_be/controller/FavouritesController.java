package com.example.ducky_be.controller;

import com.example.ducky_be.controller.protoco.request.FavouritesRequest;
import com.example.ducky_be.controller.protoco.response.FavouritesResponse;
import com.example.ducky_be.service.FavouritesService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@CrossOrigin("*")
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class FavouritesController {

    private final FavouritesService favouritesService;

    @GetMapping(value = "/favourites/user/search")
    public ResponseEntity<FavouritesResponse> getListFvByUser(@RequestParam MultiValueMap<String, String> queryParams, String userId, Pageable pageable) {
        FavouritesResponse response = favouritesService.searchFavouritesByUser(queryParams, userId, pageable);
        return ResponseEntity.ok(response);
    }

    @GetMapping(value = "/favourites/account/search") // lấy theo current user
    public ResponseEntity<FavouritesResponse> getListFv(@RequestParam MultiValueMap<String, String> queryParams, Pageable pageable) {

        FavouritesResponse response = favouritesService.searchMyFavourites(queryParams, pageable);
        return ResponseEntity.ok(response);
    }

    @GetMapping(value = "/favourites/user/{userId}")
    public ResponseEntity<FavouritesResponse> getFvByUserId(@PathVariable(name = "userId") String userId) {
        FavouritesResponse response = favouritesService.findFavouritesByUserId(userId);
        return ResponseEntity.ok(response);
    }

    @GetMapping(value = "/favourites/{id}")
    public ResponseEntity<FavouritesResponse> findById(@PathVariable(name = "favourites") String favouritesId) {

        FavouritesResponse response = favouritesService.findFavouritesById(favouritesId);

        return ResponseEntity.ok(response);
    }

    @PostMapping("favourites/account/create")
    public ResponseEntity<FavouritesResponse> createFavourites(@RequestBody @Valid FavouritesRequest favouritesRequest) {
        FavouritesResponse response = favouritesService.createFavourites(favouritesRequest);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/favourites/update")
    public ResponseEntity<FavouritesResponse> updateFavourites(@RequestBody @Valid FavouritesRequest favouritesRequest) {
        FavouritesResponse response = favouritesService.createFavourites(favouritesRequest);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/me/favourites/{favouritesId}")
    public ResponseEntity<FavouritesResponse> deleteFavourites(@PathVariable(name = "favouritesId") String favouritesId) {
        FavouritesResponse response = favouritesService.deleteFavourites(favouritesId);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/favourites/{favouritesId}/{status}/changestatus")
    @ApiOperation("Thay đổi trạng tháy của favourites")
    public ResponseEntity<FavouritesResponse> deleteProductFavorite(@PathVariable("favouritesId") String favouritesId, @PathVariable("status") Integer status) {
        log.info("id favourites muốn xóa {} trạng thái", favouritesId, status);
        FavouritesResponse response = new FavouritesResponse();
//        return ResponseEntity.ok(response);
        return ResponseEntity.ok(response);
    }
}
