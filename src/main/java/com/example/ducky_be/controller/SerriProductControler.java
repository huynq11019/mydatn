package com.example.ducky_be.controller;

import com.example.ducky_be.controller.protoco.request.SerriProductResquest;
import com.example.ducky_be.controller.protoco.response.SerriProductResponse;
import com.example.ducky_be.core.exception.BadRequestAlertException;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.service.SerriProductService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@Controller
@AllArgsConstructor
@RequestMapping("/api")
public class SerriProductControler {

    private final SerriProductService serriProductService;

    @GetMapping(value = "/serri")
    @ApiOperation("Tìm kiếm danh sách các serri của sản phẩm")
    public ResponseEntity<SerriProductResponse> getListPro(@RequestParam(value = "", name = "keyword", required = false ) String keyword, Pageable pageable) {
        SerriProductResponse serriProductResponse = this.serriProductService.serriProductSearch(keyword, pageable);
        return ResponseEntity.ok(serriProductResponse);
    }

    @PostMapping("/serri/create-update")
    @ApiOperation("Tạo hoặc cập nhật tài serri sản phẩm")
    public ResponseEntity<SerriProductResponse> createOrUpdate(@RequestBody SerriProductResquest resquest) {
        if (Validator.isNull(resquest)) {
            throw new BadRequestAlertException(ErrorCode.ERROR_DATA_INVALID);
        }
        SerriProductResponse productResponse = this.serriProductService.createOrupdateSerri(resquest);

        return ResponseEntity.ok(productResponse);
    }

    @DeleteMapping("/serri/{id}/delete")
    @ApiOperation("Xóa serri sản phẩm")
    public ResponseEntity<SerriProductResponse> deleteSerri(@PathVariable("id") Long serrialId) {
        SerriProductResponse productResponse = this.serriProductService.deleteSerri(serrialId);
        return ResponseEntity.ok(productResponse);
    }

}
