package com.example.ducky_be.controller;

import com.example.ducky_be.controller.protoco.request.ProductRequest;
import com.example.ducky_be.controller.protoco.response.ProductResponse;
import com.example.ducky_be.service.ProductService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@CrossOrigin("*")
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class ProductController {


    private final ProductService productService;

    @GetMapping(value = "/product/search")
    public ResponseEntity<ProductResponse> getListPro(@RequestParam MultiValueMap<String, String> queryParams, Pageable pageable) {

        ProductResponse response = productService.searchProduct(queryParams, pageable);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/product/{productId}")
    public ResponseEntity<ProductResponse> fidnById(@PathVariable(name = "productId") String productId) {
        ProductResponse response = productService.findproductById(productId);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/product/create")
    public ResponseEntity<ProductResponse> createProduct(@ModelAttribute @Valid ProductRequest productRequest,
                                                         @RequestParam(value = "files", required = false) List<MultipartFile> files) {
        ProductResponse response = productService.createProduct(productRequest, files);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/product/updateproduct/{productId}")
    public ResponseEntity<ProductResponse> updateProduct(@PathVariable String productId,
                                                         @ModelAttribute @Valid ProductRequest
                                                                 productRequest, @RequestParam(value = "files", required = false) List<MultipartFile> files) {
        ProductResponse response = productService.updateProduct(productId, productRequest, files);
        return ResponseEntity.ok(response);
    }

//    @PostMapping("/product/saveDraft")
//    public ResponseEntity<ProductResponse> saveDraft(@RequestBody @Valid ProductRequest productRequest) {
////        ProductResponse response = productService.saveOrUpdateDraft(productRequest);
//        return ResponseEntity.ok(new ProductResponse());
//    }

    @DeleteMapping("/product/{productId}/{status}/changestatus")
    @ApiOperation("Thay đổi trạng tháy của sản phẩm")
    public ResponseEntity<ProductResponse> changeStatus(@PathVariable("productId") String productId, @PathVariable("status") Integer status) {
        log.info("id của sản phẩm muốn xóa {} trạng thái {}", productId, status);
        ProductResponse response = this.productService.changeStatus(productId, status);
//        return ResponseEntity.ok(response);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/product/{productId}/richer")
    public ProductResponse viewProductInfor(@PathVariable("productId") String productId) {
        return productService.getRicherProductById(productId);
    }
}
