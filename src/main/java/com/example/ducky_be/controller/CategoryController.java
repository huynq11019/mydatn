package com.example.ducky_be.controller;

import com.example.ducky_be.controller.protoco.request.CategoryRequest;
import com.example.ducky_be.controller.protoco.response.CategoryResponse;
import com.example.ducky_be.service.CategoryService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Log4j2
@CrossOrigin("*")
@RestController
@RequestMapping("/api")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    /*
     * @author: NamTH
     * @since: 31/10/2021 4:37 CH
     * @description:  tìm kiếm category
     * @update:
     *
     * */
    @GetMapping("/category/search-tree")
    @ApiOperation("Tìm kiếm danh sách loại sản phẩm theo tree")
    public ResponseEntity<CategoryResponse> searchTree(@RequestParam MultiValueMap<String, String> queryParams, Pageable pageable) {
        CategoryResponse response = this.categoryService.searchTreeCategory(queryParams, pageable);
        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/category/search")
    @ApiOperation("Lấy ra tất cả category")
    public ResponseEntity<CategoryResponse> search(@RequestParam MultiValueMap<String, String> queryParams, Pageable pageable) {
        CategoryResponse response = this.categoryService.searchAllCategory(queryParams, pageable);
        return ResponseEntity.ok().body(response);
    }

    /*
     * @author: NamTH
     * @since: 31/10/2021 4:39 CH
     * @description:  tạo category
     * @update:
     *
     * */

    @PostMapping("/category/create")
    @ApiOperation("Tạo loại sản phẩm")
    public ResponseEntity<CategoryResponse> create(@RequestBody @Valid CategoryRequest request) {
        CategoryResponse response = this.categoryService.createCategory(request);

        return ResponseEntity.ok().body(response);
    }


    /*
     * @author: NamTH
     * @since: 31/10/2021 4:45 CH
     * @description:  cập nhật loại sản phẩm
     * @update:
     *
     * */
    @PutMapping("/category/{categoryId}")
    @ApiOperation("Cập nhật loại sản phẩm")
    public ResponseEntity<CategoryResponse> update(@RequestBody @Valid CategoryRequest request,
                                                   @PathVariable("categoryId") Long id) {
        log.info("{}{}", request, id);
        CategoryResponse response = this.categoryService.updateCategory(request, id);
        return ResponseEntity.ok().body(response);
    }


}
