package com.example.ducky_be.controller;

import com.example.ducky_be.controller.protoco.response.FileUploadResponse;
import com.example.ducky_be.core.config.ftp.FTPFileSystemStorageService;
import com.example.ducky_be.core.exception.BadRequestAlertException;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.service.FileUploadService;
import com.example.ducky_be.service.dto.FileUploadDTO;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
@Log4j2
@AllArgsConstructor
public class FileUploadController {

    /**
     * The file service.
     */
    private final FileUploadService fileService;

//    private final FileuploadRepository fileDAO;
//
//    private final FileUploadMapper fileUploadMapper;

    private final FTPFileSystemStorageService fileSystemStorageService;

    /**
     * Upload file.
     *
     * @param fileID the file
     * @return the upload file response
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @GetMapping("/file/download-file/ftp/{fileID}")
    @ApiOperation(value = "download file từ ftp")
    public ResponseEntity<Resource> dowloadFile(@PathVariable(name = "fileID") String fileID) {

        FileUploadDTO fileUploadModel = this.fileService.findByFileId(fileID);
        log.info(fileUploadModel.getFilePath());
        String path = fileUploadModel.getFilePath().substring(1);
        Resource inputStreamResource = fileSystemStorageService.loadAsResource(path);
        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        responseHeader.set("Content-disposition", "attachment; filename=" + fileUploadModel.getOriginalName());
        return new ResponseEntity<>(inputStreamResource, responseHeader, HttpStatus.OK);
    }

    /**
     * @author: huynq
     * @since: 11/11/2021 1:24 PM
     * @description: tải file theo id
     * @update:
     */
    @GetMapping("/file/download-file/{fileId}")
    @ApiOperation(value = "download file từ store ftp")
    public ResponseEntity<Resource> dowloadFleById(@PathVariable(name = "fileId") String fileID) {

        FileUploadDTO fileUploadModel = this.fileService.findByFileId(fileID);

        String path = fileUploadModel.getFilePath().substring(1);
        Resource inputStreamResource = fileSystemStorageService.loadAsResource(path);
        HttpHeaders responseHeader = new HttpHeaders();
        responseHeader.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        responseHeader.set("Content-disposition", "attachment; filename=" + fileUploadModel.getOriginalName());
        return new ResponseEntity<>(inputStreamResource, responseHeader, HttpStatus.OK);
    }

    // inputsream to base64
    @GetMapping("/file/download-file-base64/{fileId}")
    @ApiOperation(value = "download file từ store ftp")
    public ResponseEntity<String> dowloadFileBase64(@PathVariable(name = "fileId") String fileID) {

        FileUploadDTO fileUploadModel = this.fileService.findByFileId(fileID);

        String path = fileUploadModel.getFilePath().substring(1);
//        String base64 = fileSystemStorageService.loadAsBase64(path);
//        HttpHeaders responseHeader = new HttpHeaders();
//        responseHeader.setContentType(MediaType.APPLICATION_OCTET_STREAM);
//        responseHeader.set("Content-disposition", "attachment; filename=" + fileUploadModel.getOriginalName());
        return new ResponseEntity<>(fileSystemStorageService.loadAsBase64(path), HttpStatus.OK);
    }

    @GetMapping("/file/view-image{fileID}")
    @ApiOperation(value = "View File")
    public ResponseEntity<?> viewFile(@PathVariable(name = "fileID") String fileID) {

        FileUploadDTO fileUploadModel = this.fileService.findByFileId(fileID);
        String path = fileUploadModel.getFilePath();
        Resource inputStreamResource = fileSystemStorageService.loadAsResource(path);
        return new ResponseEntity<>(inputStreamResource, HttpStatus.OK);
    }

    @GetMapping("/file/view-frist/{ownerId}/{ownerType}")
    @ApiOperation(value = "View frist")
    public ResponseEntity<?> viewFrist(@PathVariable(name = "ownerId") String ownerId
            , @PathVariable(name = "ownerType") String ownerType) {

        List<FileUploadDTO> fileUploadModel = this.fileService.findAllByOwnerIdAndOwnerType(ownerId, ownerType);
        if (fileUploadModel.isEmpty()) {
            throw new BadRequestAlertException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT);
        }
        FileUploadDTO fileUploadDTO = fileUploadModel.get(0);
        log.info(fileUploadDTO.getFilePath());
        String path = fileUploadDTO.getFilePath();
        Resource inputStreamResource = fileSystemStorageService.loadAsResource(path);
        return new ResponseEntity<>(inputStreamResource, HttpStatus.OK);
    }

    @PostMapping("/file/uploadFile")
    @ApiOperation(value = "Upload File")
    public ResponseEntity<FileUploadResponse> uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
        // String nameFile = file.getOriginalFilename();
        FileUploadResponse response = new FileUploadResponse();
        // Validate input
        if (Validator.isNull(file) || file.isEmpty()) {
            log.error("file không hợp lệ!");
            response.setErrorCode(ErrorCode.ERR_INPUT_INVALID);
            return ResponseEntity.badRequest().body(response);
        }
        this.fileService.createFile(file, "A", "W"); // lưu vào FTP
//        log.info(path);
//        String fileName = storageService.store(file);
//        if (Validator.isNotNull(fileName)) {
//            // save db
//            FileUploadDTO fileModel = new FileUploadDTO();
//            fileModel.setOriginalName(file.getOriginalFilename());
//            fileModel.setFilePath(fileName);
//            // fileModel.setOwnerId(BigInteger.valueOf(OWNERID));
//            fileModel.setOwnerType(OWNERTYPE);
//            fileModel.setCreatedDate(new Date());
//            fileModel.setStatus(Constants.EntityStatus.ACTIVE);
//            this.fileService.saveOrUpdate(fileModel);
//            // end save db
//            response.setFileName(fileName);
//        } else {
//            response.setErrorCode(ErrorCode.ERR_FILE_STORAGE_FAILED);
//        }

        return ResponseEntity.ok(response);
    }

    /**
     * Gets the list file.
     *
     * @param ownerId   the owner id
     * @param ownerType the owner type
     * @return the list file
     */
    @GetMapping("/file/getListFile")
    @ApiOperation(value = "lấy danh sách file theo woner ID và wonner Type")
    public ResponseEntity<FileUploadResponse> getListFile(@RequestParam String ownerId,
                                                          @RequestParam String ownerType) {
        FileUploadResponse response = new FileUploadResponse();

        // Validate input
        if (Validator.isNull(ownerId) || Validator.isNull(ownerType)) {
            log.error("queryParams không hợp lệ!");
            response.setErrorCode(ErrorCode.ERR_INPUT_INVALID);

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }

        List<FileUploadDTO> page = this.fileService.getListByOwnerAndOwnerType(ownerId, ownerType, false);
        response.setListModel(page);
        return ResponseEntity.ok(response);
    }

    // getFileByid
    @GetMapping("/file/getFileByid/{fileId}")
    @ApiOperation("Lấy thông tin file theo id")
    public ResponseEntity<FileUploadResponse> getFile(@PathVariable("fileId") String fileId) {
        FileUploadResponse response = new FileUploadResponse();
        FileUploadDTO fileUploadDTO = this.fileService.findByFileId(fileId);
        response.setFileUploadDTO(fileUploadDTO);
        return ResponseEntity.ok(response);

    }

    @DeleteMapping("/file/delete/{id}")
    @ApiOperation(value = "Xóa file theo Id")
    public ResponseEntity<FileUploadResponse> deleteFile(@PathVariable String id) {
        return ResponseEntity.ok(fileService.removeFileById(id));
    }

    @PostMapping("/file/fakeUpload")
    public ResponseEntity<FileUploadResponse> fakeUploadFIle() {
        return ResponseEntity.ok().body(new FileUploadResponse());
    }
}
