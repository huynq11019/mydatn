package com.example.ducky_be.controller;

import com.example.ducky_be.controller.protoco.request.CreateOrderZaloPayRequest;
import com.example.ducky_be.controller.protoco.request.OrderRequest;
import com.example.ducky_be.controller.protoco.response.OrderResponse;
import com.example.ducky_be.service.OrderService;
import com.example.ducky_be.service.ZaloPayCheckoutService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.json.JSONObject;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Log4j2
@CrossOrigin("*")
@RestController
@AllArgsConstructor
@RequestMapping("/api")
public class OrderController {

    private final OrderService orderService;

    private final ZaloPayCheckoutService zaloPayCheckoutService;
    @PostMapping("/orders/{order-id}/payment/status")
    @ApiOperation("Đổi trạng thái mua hàng")
    public ResponseEntity<OrderResponse> changeStatusPayment(@PathVariable("order-id") String orderId,
                                                             @RequestParam(name = "orderStatus") String status) {
        OrderResponse response = this.orderService.changeStatusCheckOut(orderId, status);
        return ResponseEntity.ok().body(response);
    }

    @PostMapping("/me/orders/create")
    @ApiOperation("Khách hàng tạo order")
    public ResponseEntity<OrderResponse> create(@RequestBody @Valid OrderRequest request) {

        OrderResponse response = this.orderService.createMyOrder(request);
        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/orders")
    @ApiOperation("Lấy danh sách danh sách order ")
    public ResponseEntity<OrderResponse> searchOrder(@RequestParam MultiValueMap<String, String> queryParams, Pageable pageable) {
        log.info(queryParams.toString());
        OrderResponse response = this.orderService.searchOrder(queryParams, pageable);
        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/orders/my-order")
    @ApiOperation("Tìm kiếm theo tài khoản thiện tại ")
    public ResponseEntity<OrderResponse> findOrderByCustomerId(Pageable pageable) {
        OrderResponse response = this.orderService.findMyOrder(pageable);
        return ResponseEntity.ok().body(response);
    }

    @ApiOperation("Lấy chi tiết order")
    @GetMapping("/orders/{order-id}")
    public ResponseEntity<OrderResponse> findOrderById(@PathVariable("order-id") String orderId) {
        OrderResponse response = this.orderService.findOrderById(orderId);
        return ResponseEntity.ok().body(response);
    }

    @ApiOperation("Lấy chi tiết order")
    @GetMapping("/orders/{orderId}/richer")
    public ResponseEntity<OrderResponse> getRicherOrder(@PathVariable("orderId") String orderId) {
        OrderResponse response = this.orderService.getOrderRicher(orderId);
        return ResponseEntity.ok().body(response);
    }

    @PostMapping(value = "/order/zalo-pay/checkout")
    @ApiOperation("Phương thức chekout qua zalopay")
    public ResponseEntity<HashMap<String, Object>> getCheckOutZaloPay(@RequestBody CreateOrderZaloPayRequest request) {
        JSONObject response = this.zaloPayCheckoutService.createOrder(request);
        log.info(response);
        HashMap<String, Object> resultMap = new HashMap<>();
        for (String key : response.keySet()) {
            System.out.format("%s = %s\n", key, response.get(key));
            resultMap.put(key, response.get(key));
        }
        return ResponseEntity.ok().body(resultMap);
    }

    @ApiOperation("Lấy danh sách order theo id khách hàng")
    @GetMapping("/orders/customer/{customerId}")
    public ResponseEntity<OrderResponse> findOrderByCustomerId(@PathVariable("customerId") String customerId, Pageable pageable) {
        OrderResponse response = this.orderService.getOrderRicherByOrderId(customerId, pageable);
        return ResponseEntity.ok().body(response);
    }

    @ApiOperation("Lấy thống kê đơn hàng được mua trong tuần")
    @GetMapping("/orders/statistic-order")
    public Map<String, Long> getStatistic(@RequestParam MultiValueMap<String, String> queryParams) {
        return this.orderService.statisticOrder(queryParams);

    }

    @ApiOperation("Lấy thống kê đơn hàng được mua trong tuần")
    @GetMapping("/orders/statistic-money")
    public Map<String, Long> getStatisticMoney(@RequestParam MultiValueMap<String, String> queryParams) {
        return this.orderService.statisticMoney(queryParams);

    }
}
