package com.example.ducky_be.repository;

import com.example.ducky_be.repository.entity.MailTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MailTemplateRepository extends JpaRepository<MailTemplate, Long> {

//    @Query("select count(a) > 0 from EmailTemplate a where a.templateId <> :templateId and templateCode = :templateCode and status = 1")
//    boolean existsByTemplateIdAndTemplateCode(@Param("templateId") Long templateId, @Param("templateCode") String templateCode);
//
//    int deleteByTemplateId(Long templateId);
//
//    List<MailTemplate> getAllByStatus(Integer status);
//
//    MailTemplate findEmailTemplateByTemplateCode(String templateCode);
//
//    @Query("select count(a) > 0 from EmailTemplate a where  templateCode = :templateCode and status = 1")
//    boolean existsByTemplateCode(@Param("templateCode") String templateCode);
//
//    Optional<MailTemplate> findByTemplateCodeAndStatus(String templateCode, Integer status);
}
