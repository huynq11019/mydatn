package com.example.ducky_be.repository;

import com.example.ducky_be.repository.custom.AddressRepositoryCustom;
import com.example.ducky_be.repository.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long>, AddressRepositoryCustom {

    List<Address> findAllByCreateBy(String customerId);

    Optional<Address> findByIdAndStatus(Long  id, Integer status);

}
