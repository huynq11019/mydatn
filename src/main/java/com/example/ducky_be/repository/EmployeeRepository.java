package com.example.ducky_be.repository;

import com.example.ducky_be.repository.custom.EmployeeRepositoryCustom;
import com.example.ducky_be.repository.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, String>, EmployeeRepositoryCustom {

    Optional<Employee> findEmployeeByEmail(String email);

    Optional<Employee> findEmployeeByEmailAndStatus(String email, Integer status);

    //    @Cacheable(cacheNames = CacheConstants.Entity.USERS_BY_USERNAME, key = "{#root.methodName, #employeeId}",
//            unless = "#result == null")
    Optional<Employee> findByIdAndStatus(String employeeId, Integer status);
}
