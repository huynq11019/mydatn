package com.example.ducky_be.repository;

import com.example.ducky_be.repository.custom.OrderRepositoryCustom;
import com.example.ducky_be.repository.entity.Order;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order,String> , OrderRepositoryCustom {


    List<Order> findAllByCustomerId(String customerId, Pageable pageable);

    @Query(value = "SELECT o FROM Order o WHERE o.customerId = ?1 ")
    List<Order> searchOrderByCustomerId(String customerId, Pageable pageable);
}
