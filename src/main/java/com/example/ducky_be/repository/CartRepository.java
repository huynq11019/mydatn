package com.example.ducky_be.repository;

import com.example.ducky_be.repository.custom.CartRepositoryCustom;
import com.example.ducky_be.repository.entity.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CartRepository extends JpaRepository<Cart, String> , CartRepositoryCustom {
    List<Cart> findAllByCustomerId(String customerId);

    Optional<Cart> findByCustomerIdAndOptionProductIdAndStatus(String customerId, String optionProductId, Integer status);

    Long countByCustomerId(String customerId);
}
