package com.example.ducky_be.repository;

import com.example.ducky_be.repository.custom.ProductRepositoryCustom;
import com.example.ducky_be.repository.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<Product, String>, ProductRepositoryCustom {

    Optional<Product> findByIdAndStatus(String productId, Integer status);


}
