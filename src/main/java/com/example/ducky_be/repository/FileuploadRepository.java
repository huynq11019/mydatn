package com.example.ducky_be.repository;

import com.example.ducky_be.repository.custom.FileuploadRepositoryCustom;
import com.example.ducky_be.repository.entity.FileUpload;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FileuploadRepository extends JpaRepository<FileUpload, String>, FileuploadRepositoryCustom {
    /**
     * Find one file by OwnerId and OwnerType
     *
     * @param ownerId
     * @param ownerType
     * @return
     */
    List<FileUpload> findOneByOwnerIdAndOwnerTypeAndStatusOrderByCreateDateDesc(String ownerId, String ownerType,
                                                                                Integer status);

    /**
     * Find by id and status
     *
     * @param id
     * @param status
     * @return
     */
    Optional<FileUpload> findByIdAndStatus(String id, Integer status);

    List<FileUpload> findByOwnerIdAndOwnerType(String ownerId, String ownerType);

    List<FileUpload> findAllByOriginalName(String name);
}
