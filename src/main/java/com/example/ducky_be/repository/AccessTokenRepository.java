package com.example.ducky_be.repository;


import com.example.ducky_be.repository.custom.AccessTokenRepositoryCustom;
import com.example.ducky_be.repository.entity.AccessToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.Instant;
import java.util.Optional;

public interface AccessTokenRepository extends JpaRepository<AccessToken, Long>, AccessTokenRepositoryCustom {

    /**
     * Delete by expired time less than equal or expired.
     *
     * @param expiredInstant the expired instant
     * @param expired        the expired
     * @return the long
     */
    long deleteByExpiredTimeLessThanEqualOrExpired(Instant expiredInstant, boolean expired);

    /**
     * Find by username and value.
     *
     * @param username the username
     * @param value    the value
     * @return the optional
     */
    Optional<AccessToken> findByUsernameAndValue(String username, String value);

    /**
     * Find by value.
     *
     * @param value the value
     * @return the optional
     */
    Optional<AccessToken> findByValue(String value);

    /**
     * Find by token id.
     *
     * @param tokenId the token id
     * @return the optional
     */
    Optional<AccessToken> findByTokenId(Long tokenId);
}
