package com.example.ducky_be.repository;

import com.example.ducky_be.repository.custom.AuthorityRepositoryCustom;
import com.example.ducky_be.repository.entity.Authorities;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthorityRepository extends JpaRepository<Authorities, String>, AuthorityRepositoryCustom {

    Optional<Authorities> findByAuthorityCode(String authroityCode);
}
