package com.example.ducky_be.repository.custom.impl;

import com.example.ducky_be.core.utils.GetterUtil;
import com.example.ducky_be.core.utils.PageUtil;
import com.example.ducky_be.core.utils.StringUtil;
import com.example.ducky_be.repository.custom.EmployeeRepositoryCustom;
import com.example.ducky_be.repository.entity.Employee;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Log4j2
public class EmployeeRepositoryCustomImpl implements EmployeeRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Employee> findUserByRoleId(Long roleId) {
        return null;
    }

    @Override
    public List<Employee> findUserInfo(MultiValueMap<String, Object> queryParams, Pageable pageable) {
        return null;
    }

    @Override
    public Long countUserInfo(MultiValueMap<String, String> queryParams) {
        StringBuilder hql = new StringBuilder("SELECT COUNT(e) FROM Employee e");
        Map<String, Object> values = new java.util.HashMap<>();
        hql.append(createWhereQuery(queryParams, values));
        Query query = entityManager.createQuery(hql.toString(), Long.class);
        return GetterUtil.getLong(query.getSingleResult(), 0L);
    }

    @Override
    public List<Employee> searchEmployee(MultiValueMap<String, String> queryParams, Pageable pageable) {
        StringBuilder hql = new StringBuilder("SELECT e FROM Employee e");
        Map<String, Object> values = new java.util.HashMap<>();
        hql.append(createWhereQuery(queryParams, values));
        log.info(pageable);
        hql.append(PageUtil.PageToSqlQueryOrderBy(pageable, "e"));
//        hql.append(this.createMyOrder(pageable.getSort().))
        Query query = entityManager.createQuery(hql.toString(), Employee.class);
        values.forEach(query::setParameter);
        query.setFirstResult((pageable.getPageNumber()) * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());
        return query.getResultList();
    }

    @Override
    public List<Employee> fuzzySearchByFullnameAndEmail(String searchTerm) {
        return null;
    }

    @Override
    public List<Employee> getListEmployeeByRole(String roleId) {

        return null;
    }

    StringBuilder createWhereQuery(MultiValueMap<String, String> queryParams, Map<String, Object> values) {
        StringBuilder hql = new StringBuilder(" WHERE 1=1");
        if (queryParams.containsKey("keyword") && !StringUtil.isBlank(queryParams.getFirst("keyword"))) {
            hql.append(" AND ( ");
            hql.append(" LOWER(e.email) LIKE LOWER(:keyword)  ");
            hql.append(" OR LOWER(e.fullName) LIKE LOWER(:keyword)  ");
            hql.append("  ) ");
            values.put("keyword", "%" + queryParams.get("keyword").get(0).trim() + "%");
        }
        if (queryParams.containsKey("email") && !StringUtil.isBlank(queryParams.getFirst("email"))) {
            hql.append(" AND e.email LIKE :email");
            values.put("email", "%" + queryParams.getFirst("email") + "%");
        }
        if (queryParams.containsKey("fullName") && !StringUtil.isBlank(queryParams.getFirst("fullName"))) {
            hql.append(" AND e.fullName LIKE :fullName");
            values.put("fullName", "%" + queryParams.getFirst("fullName") + "%");
        }
        if (queryParams.containsKey("status") && !StringUtil.isBlank(queryParams.getFirst("status"))) {
            hql.append(" AND e.status = :status");
            values.put("status", Integer.valueOf(Objects.requireNonNull(queryParams.getFirst("status"))));
        }else {
            hql.append(" AND e.status = 1");
        }
        return hql;
    }

    private StringBuilder createOrder(String orderBy, String orderType, Map<String, Object> values) {
        StringBuilder hql = new StringBuilder(" ");
        if (StringUtil.isBlank(orderBy)) {
            return hql.append(" ORDER BY e.id DESC ");
        }
        hql.append(" ORDER BY ").append(orderBy).append(" ").append(orderType);
        return hql;
    }
}
