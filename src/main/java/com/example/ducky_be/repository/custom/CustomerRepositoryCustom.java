package com.example.ducky_be.repository.custom;

import com.example.ducky_be.repository.entity.Customer;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface CustomerRepositoryCustom {

    Long countCus(MultiValueMap<String, String> queryParams);

    List<Customer> searchCustomer(MultiValueMap<String, String> queryParams, Pageable pageable);

    Integer changeStatusProduct(String customerId, Integer status);
}
