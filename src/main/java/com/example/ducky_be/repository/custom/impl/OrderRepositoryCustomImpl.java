package com.example.ducky_be.repository.custom.impl;

import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.utils.GetterUtil;
import com.example.ducky_be.core.utils.PageUtil;
import com.example.ducky_be.core.utils.StringUtil;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.custom.OrderRepositoryCustom;
import com.example.ducky_be.repository.entity.Order;
import lombok.extern.log4j.Log4j2;
import org.hibernate.query.NativeQuery;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Log4j2
public class OrderRepositoryCustomImpl implements OrderRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Order> search(MultiValueMap<String, String> queryParams, Pageable pageable) {
        StringBuilder hql = new StringBuilder();
        hql.append("SELECT o FROM Order o WHERE 1=1  ");
        Map<String, Object> values = new HashMap<>();
        hql.append(this.createWhereQuery(queryParams, values));
        hql.append(PageUtil.PageToSqlQueryOrderBy(pageable, "o"));
        Query query = this.entityManager.createQuery(hql.toString(), Order.class);
        values.forEach(query::setParameter);
        log.info(hql.toString());
        if (Validator.isNotNull(pageable) && pageable.getPageSize() > 0 && pageable.getPageNumber() >= 0) {
            query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
            query.setMaxResults(pageable.getPageSize());
        }
        return query.getResultList();
    }

    @Override
    public Long countAll(MultiValueMap<String, String> queryParams) {
        StringBuilder hql = new StringBuilder();
        hql.append("SELECT COUNT(o) FROM Order o WHERE 1=1  ");
        Map<String, Object> values = new HashMap<>();
        hql.append(this.createWhereQuery(queryParams, values));
        Query query = this.entityManager.createQuery(hql.toString(), Long.class);
        values.forEach(query::setParameter);
        log.info(hql.toString());
        values.forEach(query::setParameter);
        return GetterUtil.getLong(query.getSingleResult(), 0L);
    }

    @Override
    public List<Order> findAllOrderByCustomerId(MultiValueMap<String, String> queryParams, Pageable pageable) {
        return null;
    }
    private StringBuilder createWhereQuery(MultiValueMap<String, String> queryParams, Map<String, Object> values) {
        StringBuilder hql = new StringBuilder(" ");
        if (queryParams.containsKey("keyword") && !StringUtil.isBlank(queryParams.getFirst("keyword"))) {
            hql.append(" AND (o.address LIKE :keyword OR o.phoneNumber LIKE :keyword OR o.receiver LIKE :keyword) ");
            values.put("keyword", "%" + queryParams.getFirst("keyword") + "%");
        }
        if (queryParams.containsKey("customerId") && !StringUtil.isBlank(queryParams.get("customerId").get(0))) {
            hql.append(" AND o.customerId = :customerId ");
            values.put("customerId", queryParams.getFirst("customerId"));
        }
        if (queryParams.containsKey("orderStatus") && !StringUtil.isBlank(queryParams.get("orderStatus").get(0))) {
            hql.append(" AND o.orderStatus = :orderStatus ");
            values.put("orderStatus", Constants.OrderStatus.valueOf(queryParams.getFirst("orderStatus")));
        }

        return hql;
    }

    @Override
    public Map<String, Long> statisticOrderByDay(MultiValueMap<String, String> queryParams) {
        Map<String, Map<String, List<Object>>> shippingControl_dataChart = new HashMap<>();
        StringBuilder sql = new StringBuilder();
        // viết lại query theo approval
        sql.append("SELECT DAYNAME(co.create_date) AS label1, ");
        sql.append("  COUNT(co.id) AS value1 ");
        sql.append(" FROM customer_order co ");
        sql.append("  WHERE 1=1 ");
        Map<String, Object> values = new HashMap<>();
        sql.append(this.createDashboardWhereQuery(queryParams, values));
        sql.append(" GROUP BY DAYNAME(co.create_date) ");
        sql.append(" ORDER BY DAY(co.create_date) ");
        Query query = entityManager.createNativeQuery(sql.toString());
        values.forEach(query::setParameter);

        // lấy value theo tên cột
        NativeQuery<Object[]> nativeQuery = query.unwrap(NativeQuery.class)
                .addScalar("label1", StringType.INSTANCE)
                .addScalar("value1", LongType.INSTANCE);

        List<Object[]> orderValue = nativeQuery.getResultList();
        log.info("-------------------------------------------" + orderValue);


        Map<String, Long> data = new HashMap<>();
        for (Object[] obj : orderValue) {
            data.put(obj[0].toString(), Long.valueOf(obj[1].toString()));

        }

        return data;
    }

    @Override
    public Map<String, Long> statisticMoney(MultiValueMap<String, String> queryParams) {
        StringBuilder sql = new StringBuilder();
        // viết lại query theo approval
        sql.append("SELECT DAYNAME(co.create_date) AS label1, SUM(od.price) AS value1");
        sql.append(" FROM customer_order co INNER JOIN order_detail od ON od.order_id=co.id ");
        sql.append(" WHERE co.order_status = 'SUCCESS' ");
        Map<String, Object> values = new HashMap<>();
        sql.append(this.createDashboardWhereQuery(queryParams, values));
        sql.append(" GROUP BY DAYNAME(co.create_date) ");
        sql.append(" ORDER BY DAY(co.create_date) ");
        Query query = entityManager.createNativeQuery(sql.toString());
        values.forEach(query::setParameter);

        // lấy value theo tên cột
        NativeQuery<Object[]> nativeQuery = query.unwrap(NativeQuery.class)
                .addScalar("label1", StringType.INSTANCE)
                .addScalar("value1", LongType.INSTANCE);

        List<Object[]> orderValue = nativeQuery.getResultList();
        log.info("-------------------------------------------" + orderValue);


        Map<String, Long> data = new HashMap<>();
        for (Object[] obj : orderValue) {
            data.put(obj[0].toString(), Long.valueOf(obj[1].toString()));

        }

        return data;
    }

    private StringBuilder createDashboardWhereQuery(MultiValueMap<String, String> queryParams, Map<String, Object> values) {
        StringBuilder hql = new StringBuilder(" ");
        if (queryParams.containsKey("week") && !StringUtil.isBlank(queryParams.getFirst("week"))) {
            hql.append(" AND WEEK(co.create_date,1)= :week");
            values.put("week", queryParams.getFirst("week"));
        } else {
            hql.append(" AND WEEK(co.create_date,1)=WEEK(NOW(),1)");
        }
        return hql;
    }
}
