package com.example.ducky_be.repository.custom;

import com.example.ducky_be.repository.entity.Refund;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface RefundRepositoryCustom {
    List<Refund> getAllRefund(MultiValueMap<String, String> queryParams, Pageable pageable);

    Long countAll(MultiValueMap<String, String> queryParams, Pageable pageable);
}
