package com.example.ducky_be.repository.custom;

public interface RatingProductCustom {

    Float countAverageRating(String productId);
}
