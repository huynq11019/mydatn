package com.example.ducky_be.repository.custom.impl;

import com.example.ducky_be.core.utils.GetterUtil;
import com.example.ducky_be.core.utils.PageUtil;
import com.example.ducky_be.core.utils.StringUtil;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.custom.CategoryRepositoryCustom;
import com.example.ducky_be.repository.entity.Categories;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
public class CategoryRepositoryCustomImpl implements CategoryRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    /*
     * @author: NamTH
     * @since: 31/10/2021 4:25 CH
     * @description:  sql lấy danh sách category
     * @update:
     *
     * */
    @Override
    public Long countCategory(MultiValueMap<String, String> queryParams) {
        try {
            StringBuilder hql = new StringBuilder();
            hql.append("SELECT COUNT(c) FROM Categories c WHERE 1 = 1  ");
            Map<String, Object> values = new HashMap<>();
            hql.append(this.createWhereQuery(queryParams, values));

            Query query = this.entityManager.createQuery(hql.toString(), Long.class);
            values.forEach(query::setParameter);
            return GetterUtil.getLong(query.getSingleResult(), 0L);
        } catch (Exception exception) {
            log.error(exception.getMessage());
        }
        return 0L;
    }

    @Override
    public List<Categories> searchAllCategory(MultiValueMap<String, String> queryParams, Pageable pageable) {
        try {
            StringBuilder hql = new StringBuilder();

            hql.append("SELECT c FROM Categories c WHERE 1=1");
            Map<String, Object> values = new HashMap<>();
            hql.append(this.createWhereQuery(queryParams, values));

            hql.append(PageUtil.PageToSqlQueryOrderBy(pageable, "c"));
            Query query = this.entityManager.createQuery(hql.toString(), Categories.class);
            values.forEach(query::setParameter);

            if (Validator.isNotNull(pageable) && pageable.getPageSize() > 0 && pageable.getPageNumber() >= 0) {
                query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
                query.setMaxResults(pageable.getPageSize());
            }
            return query.getResultList();

        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    @Override
    public List<Categories> searchTree(MultiValueMap<String, String> queryParams) {
        try {
            StringBuilder hql = new StringBuilder();
//            b1 :
            hql.append("SELECT c FROM Categories c WHERE c.parentCategory  IS NULL ");
            Map<String, Object> values = new HashMap<>();
            hql.append(this.createWhereQuery(queryParams, values));
            hql.append(this.createOrderQuery(false));
            Query query = this.entityManager.createQuery(hql.toString(), Categories.class);
//            query.setParameter("status", Constants.EntityStatus.ACTIVE);
            values.forEach(query::setParameter);

//            if (Validator.isNotNull(pageable) && pageable.getPageSize() > 0 && pageable.getPageNumber() >= 0) {
//                query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
//                query.setMaxResults(pageable.getPageSize());
//            }
            return query.getResultList();
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException();
        }
    }

    private StringBuilder createWhereQuery(MultiValueMap<String, String> queryParams, Map<String, Object> values) {
        StringBuilder sql = new StringBuilder(" ");


        if (queryParams.containsKey("keyword") && !StringUtil.isBlank(queryParams.getFirst("keyword"))) {
            sql.append(" AND c.categoryName like (:keyword) ");
            values.put("keyword", "%" + queryParams.get("keyword").get(0).trim() + "%");
        }

        if (queryParams.containsKey("parentCategory") && !StringUtil.isBlank(queryParams.getFirst("parentCategory"))) {
            sql.append(" AND c.parentCategory.id = :parentCategory ");
            values.put("parentcate_id", GetterUtil.getLongValue(queryParams.getFirst("parentCategory")));
        }
        if (queryParams.containsKey("startDate") && !StringUtil.isBlank(queryParams.getFirst("startDate"))) {
            sql.append(" AND c.createDate >= :startDate ");
            values.put("startDate", Instant.ofEpochMilli(Long.parseLong(queryParams.getFirst("startDate"))));
        }

        if (queryParams.containsKey("endDate") && !StringUtil.isBlank(queryParams.getFirst("endDate"))) {
            sql.append(" AND c.createDate  <= :endDate ");
            values.put("endDate", Instant.ofEpochMilli(Long.parseLong(queryParams.getFirst("endDate"))));
        }
        if (queryParams.containsKey("status") && !StringUtil.isBlank(queryParams.getFirst("status"))) {
            sql.append(" AND c.status = :status ");
            values.put("status", queryParams.getFirst("status"));

        } else {
            sql.append(" AND c.status = 1 ");
        }
        if (queryParams.containsKey("status") && !StringUtil.isBlank(queryParams.get("status").get(0))) {
            sql.append(" AND c.status = :status ");
            values.put("status", queryParams.getFirst("status"));

        } else {
            sql.append(" AND c.status = 1 ");
        }
        return sql;
    }

    //
    private StringBuilder createOrderQuery(boolean order) {
        StringBuilder sql = new StringBuilder();
        String isOrder = order ? "asc" : "desc";
        sql.append(" order by c.sortOrder  " + isOrder);
        return sql;
    }

}
