package com.example.ducky_be.repository.custom;

import com.example.ducky_be.repository.entity.Attributes;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface AttributesRepositoryCustom {
    List<Attributes> findAttributeByCategoryId(String categoryId, MultiValueMap<String, String> queryParams, Pageable pageable);

    Long countParam(String categoryId, MultiValueMap<String, String> queryParams);

}
