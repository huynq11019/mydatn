package com.example.ducky_be.repository.custom.impl;

import com.example.ducky_be.core.utils.GetterUtil;
import com.example.ducky_be.core.utils.StringUtil;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.custom.OrderDetailRepositoryCustom;
import com.example.ducky_be.repository.entity.OrderDetail;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Log4j2
public class OrderDetailRepositoryCustomImpl implements OrderDetailRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<OrderDetail> findAllOrderDetailByOrderId(MultiValueMap<String, String> queryParams, Pageable pageable,String orderId) {
        StringBuilder hql = new StringBuilder();
        hql.append("SELECT od FROM OrderDetail od WHERE od.orderId =: orderId  ");
        Map<String, Object> values = new HashMap<>();
        hql.append(this.createWhereQuery(queryParams, values));
        Query query = this.entityManager.createQuery(hql.toString(), OrderDetail.class);
        query.setParameter("orderId", orderId);
        values.forEach(query::setParameter);
        log.info(hql.toString());
        if (Validator.isNotNull(pageable) && pageable.getPageSize() > 0 && pageable.getPageNumber() >= 0) {
            query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
            query.setMaxResults(pageable.getPageSize());
        }
        return query.getResultList();
    }

    @Override
    public Long countByOrderId(MultiValueMap<String, String> queryParams,String orderId) {
        StringBuilder hql = new StringBuilder();
        hql.append("SELECT COUNT(od) FROM OrderDetail od WHERE od.orderId =: orderId   ");
        Map<String, Object> values = new HashMap<>();
        hql.append(this.createWhereQuery(queryParams, values));
        Query query = this.entityManager.createQuery(hql.toString(), Long.class);
        query.setParameter("orderId", orderId);
        values.forEach(query::setParameter);
        log.info(hql.toString());
        values.forEach(query::setParameter);
        return GetterUtil.getLong(query.getSingleResult(), 0L);
    }
    private StringBuilder createWhereQuery(MultiValueMap<String, String> queryParams, Map<String, Object> values) {
        StringBuilder hql = new StringBuilder(" ");

        if (queryParams.containsKey("orderId") && !StringUtil.isBlank(queryParams.get("orderId").get(0))) {
            hql.append(" AND od.orderId = :orderId ");
            values.put("orderId", queryParams.get("orderId"));
        }
        if (!queryParams.containsKey("status")) {
            hql.append(" AND od.status = 1");
        }
        return hql;
    }
}
