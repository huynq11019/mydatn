package com.example.ducky_be.repository.custom;

import com.example.ducky_be.repository.entity.ProductAttributes;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface ProductAttributeRepositoryCustom {
    List<ProductAttributes> findAttributeProductByProductId(String productId, MultiValueMap<String, String> queryParams, Pageable pageable);

    Long countByparam(String productId, MultiValueMap<String, String> queryParams);
}
