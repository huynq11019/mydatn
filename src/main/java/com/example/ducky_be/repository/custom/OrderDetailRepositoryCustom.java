package com.example.ducky_be.repository.custom;

import com.example.ducky_be.repository.entity.OrderDetail;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface OrderDetailRepositoryCustom {
    List<OrderDetail>  findAllOrderDetailByOrderId(MultiValueMap<String, String> queryParams, Pageable pageable,String orderId);

    Long countByOrderId(MultiValueMap<String, String> queryParams,String orderId);
}
