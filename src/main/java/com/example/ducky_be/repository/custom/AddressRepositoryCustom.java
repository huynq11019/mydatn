package com.example.ducky_be.repository.custom;

import com.example.ducky_be.repository.entity.Address;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface AddressRepositoryCustom {

    Long countAddress(MultiValueMap<String, String> queryParams);

    List<Address> searchAddress(MultiValueMap<String, String> queryParams, Pageable pageable);
}
