package com.example.ducky_be.repository.custom;

import com.example.ducky_be.repository.entity.SerriProduct;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface SerriProductrepositoryCustom {
    List<SerriProduct> searchSerriProduct(MultiValueMap<String, String> queryParams, Pageable pageable);

    long countSerriProduct(MultiValueMap<String, String> queryParams);

    Integer changeSerriStatusById(Long id, Integer status);
}
