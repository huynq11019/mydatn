package com.example.ducky_be.repository.custom.impl;

import com.example.ducky_be.repository.custom.RatingProductCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class RatingProductCustomImpl implements RatingProductCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Float countAverageRating(String productId) {
        String hql = "SELECT AVG(r.point) FROM RatingProduct r WHERE r.productId = :productId";
        return (Float) entityManager.createQuery(hql).setParameter("productId", productId).getSingleResult();
    }
}
