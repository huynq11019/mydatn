package com.example.ducky_be.repository.custom.impl;

import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.utils.GetterUtil;
import com.example.ducky_be.core.utils.StringUtil;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.custom.AddressRepositoryCustom;
import com.example.ducky_be.repository.entity.Address;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class AddressRepositoryCustomImpl implements AddressRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Long countAddress(MultiValueMap<String, String> queryParams) {
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT COUNT(a) FROM Address a WHERE 1 = 1 AND a.status = :status ");
            Map<String, Object> values = new HashMap<>();
            sql.append(this.createWhereQuery(queryParams, values));

            Query query = this.entityManager.createQuery(sql.toString(), Long.class);
            query.setParameter("status", Constants.EntityStatus.ACTIVE);
            values.forEach(query::setParameter);
            return GetterUtil.getLong(query.getSingleResult(), 0L);
        } catch (Exception exception) {
            log.error(exception.getMessage());
        }
        return 0L;
    }

    @Override
    public List<Address> searchAddress(MultiValueMap<String, String> queryParams, Pageable pageable) {
        try {
            StringBuilder hql = new StringBuilder();
            hql.append("SELECT a FROM Address a WHERE 1 = 1 AND a.status = :status ");
            Map<String, Object> values = new HashMap<>();
            hql.append(this.createWhereQuery(queryParams, values));
            hql.append(this.createOrderQuery());
            Query query = this.entityManager.createQuery(hql.toString(), Address.class);
            query.setParameter("status", Constants.EntityStatus.ACTIVE);
            values.forEach(query::setParameter);

            if (Validator.isNotNull(pageable) && pageable.getPageSize() > 0 && pageable.getPageNumber() >= 0) {
                query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
                query.setMaxResults(pageable.getPageSize());
            }
            return query.getResultList();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    private StringBuilder createWhereQuery(MultiValueMap<String, String> queryParams, Map<String, Object> values) {
        StringBuilder sql = new StringBuilder(" ");

        if (queryParams.containsKey("keyword") && !StringUtil.isBlank(queryParams.get("keyword").get(0))) {
            sql.append(" AND ( LOWER(a.nameOfRecipient) LIKE LOWER(:keyword) ");
            sql.append(" OR LOWER(a.province) LIKE LOWER(:keyword) ) ");
            sql.append(" OR LOWER(a.district) LIKE LOWER(:keyword) ) ");
            sql.append(" OR LOWER(a.ward) LIKE LOWER(:keyword) ) ");
            sql.append(" OR LOWER(a.addressDetail) LIKE LOWER(:keyword) ) ");
            sql.append(" OR LOWER(a.latitude) LIKE LOWER(:keyword) ) ");
            values.put("keyword", "%" + queryParams.get("keyword").get(0).trim() + "%");
        }

        if (queryParams.containsKey("addressId") && !StringUtil.isBlank(queryParams.get("addressId").get(0))) {
            sql.append(" AND a.id = :addressId ");
            values.put("addressId", GetterUtil.getLongValue(queryParams.get("addressId")));
        }

        if (queryParams.containsKey("startDate") && !StringUtil.isBlank(queryParams.get("startDate").get(0))) {
            sql.append(" AND a.createDate >= :startDate ");
            values.put("startDate", Instant.ofEpochMilli(Long.parseLong(queryParams.get("startDate").get(0))));
        }
        if (queryParams.containsKey("userId") && !StringUtil.isBlank(queryParams.getFirst("userId"))) {
            sql.append(" AND a.createBy = :userId ");
            values.put("userId", queryParams.getFirst("userId"));
        }
        if (queryParams.containsKey("endDate") && !StringUtil.isBlank(queryParams.get("endDate").get(0))) {
            sql.append(" AND a.createDate  <= :endDate ");
            values.put("endDate", Instant.ofEpochMilli(Long.parseLong(queryParams.get("endDate").get(0))));
        }
        return sql;
    }

    private StringBuilder createOrderQuery() {
        StringBuilder sql = new StringBuilder();
        sql.append(" order by a.createDate asc ");
        return sql;
    }
}
