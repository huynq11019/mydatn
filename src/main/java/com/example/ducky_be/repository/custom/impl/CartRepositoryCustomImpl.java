package com.example.ducky_be.repository.custom.impl;

import com.example.ducky_be.core.exception.ResponseException;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.core.utils.GetterUtil;
import com.example.ducky_be.repository.custom.CartRepositoryCustom;
import com.example.ducky_be.repository.entity.Cart;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Log4j2
public class CartRepositoryCustomImpl implements CartRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Cart> searchMyCustomer(MultiValueMap<String, String> params, Pageable pageable) {
        try {
            StringBuilder hql = new StringBuilder("SELECT c FROM Cart c ");
            Map<String, Object> values = new HashMap<>();
            hql.append(this.getQuery(params, values));
            Query query = entityManager.createQuery(hql.toString());
            values.forEach((k, v) -> query.setParameter(k, v));
            query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
            query.setMaxResults(pageable.getPageSize());

            return query.getResultList();
        }catch (Exception e){
            log.error(e.getMessage());
             throw new ResponseException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT);
        }
    }

    @Override
    public Long countCartOfCustomer(MultiValueMap<String, String> params, Pageable pageable) {
        try {
            StringBuilder hql = new StringBuilder("SELECT COUNT(c) FROM Cart c ");
            Map<String, Object> values = new HashMap<>();
            hql.append(this.getQuery(params, values));
            Query query = entityManager.createQuery(hql.toString());
            values.forEach((k, v) -> query.setParameter(k, v));
            return GetterUtil.getLong(query.getSingleResult(), 0L);
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return 0L;
    }

    @Override
    public Integer deleteListCart(List<String> ids, Integer  status) {
            StringBuilder hql = new StringBuilder("UPDATE Cart c SET c.status = :status ");
            hql.append("WHERE c.id IN :ids ");
            Query query = entityManager.createQuery(hql.toString());
            query.setParameter("status", status);
            query.setParameter("ids", ids);
            return query.executeUpdate();
    }

    @Override
    public int deleteCartItems(List<String> ids) {
        Query query = entityManager.createQuery("UPDATE Cart  c SET c.status = -1 " + " WHERE c.id IN :ids ");
        query.setParameter("ids", ids);
        return query.executeUpdate();

    }

    private String getQuery(MultiValueMap<String, String> params, Map<String, Object> value) {
        StringBuilder query = new StringBuilder(" WHERE 1=1 ");
        if (params.containsKey("customerId")) {
            query.append("AND c.customerId = :customerId ");
            value.put("customerId", params.getFirst("customerId"));
        }
        if (params.containsKey("productId")) {
            query.append("AND c.productId = :productId ");
        }
        if (params.containsKey("status")) {
            query.append("AND c.status = :status ");
        }else {
            query.append("AND c.status = 1 ");
        }
        return query.toString();
    }
}
