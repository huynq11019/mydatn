package com.example.ducky_be.repository.custom;

import com.example.ducky_be.repository.entity.Employee;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface EmployeeRepositoryCustom {

    public List<Employee> findUserByRoleId(Long roleId);

    public List<Employee> findUserInfo(MultiValueMap<String, Object> queryParams, Pageable pageable);

    Long countUserInfo(MultiValueMap<String, String> queryParams);

    List<Employee> searchEmployee(MultiValueMap<String, String> queryParams, Pageable pageable);

    List<Employee> fuzzySearchByFullnameAndEmail(String searchTerm);

    List<Employee> getListEmployeeByRole(String roleId);
}
