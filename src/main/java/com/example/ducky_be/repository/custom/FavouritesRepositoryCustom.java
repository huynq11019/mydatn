package com.example.ducky_be.repository.custom;

import com.example.ducky_be.repository.entity.Favourites;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface FavouritesRepositoryCustom {
    Long countFavourites(MultiValueMap<String, String> queryParams);

    List<Favourites> searchFavourites(MultiValueMap<String, String> queryParams, Pageable pageable);

    List<Favourites> searchFavouritesByUser(MultiValueMap<String, String> queryParams, String userId, Pageable pageable);
}
