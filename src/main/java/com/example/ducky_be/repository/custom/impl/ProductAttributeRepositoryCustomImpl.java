package com.example.ducky_be.repository.custom.impl;

import com.example.ducky_be.core.utils.GetterUtil;
import com.example.ducky_be.core.utils.StringUtil;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.custom.ProductAttributeRepositoryCustom;
import com.example.ducky_be.repository.entity.ProductAttributes;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
public class ProductAttributeRepositoryCustomImpl implements ProductAttributeRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<ProductAttributes> findAttributeProductByProductId(String productId, MultiValueMap<String, String> queryParams, Pageable pageable) {
        StringBuilder hql = new StringBuilder();
        hql.append("SELECT pa FROM ProductAttributes pa WHERE pa.productId =: productId  ");
        Map<String, Object> values = new HashMap<>();
        hql.append(this.createWhereQuery(queryParams, values));
        Query query = this.entityManager.createQuery(hql.toString(), ProductAttributes.class);
        query.setParameter("productId", productId);
        values.forEach(query::setParameter);
        log.info(hql.toString());
        if (Validator.isNotNull(pageable) && pageable.getPageSize() > 0 && pageable.getPageNumber() >= 0) {
            query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
            query.setMaxResults(pageable.getPageSize());
        }
        return query.getResultList();
    }

    @Override
    public Long countByparam(String productId, MultiValueMap<String, String> queryParams) {
        StringBuilder hql = new StringBuilder();
        hql.append("SELECT COUNT(pa) FROM ProductAttributes pa WHERE pa.productId =: productId  ");
        Map<String, Object> values = new HashMap<>();
        hql.append(this.createWhereQuery(queryParams, values));
        Query query = this.entityManager.createQuery(hql.toString(), Long.class);
        query.setParameter("productId", productId);
        values.forEach(query::setParameter);
        log.info(hql.toString());
        values.forEach(query::setParameter);
        return GetterUtil.getLong(query.getSingleResult(), 0L);
    }

    private StringBuilder createWhereQuery(MultiValueMap<String, String> queryParams, Map<String, Object> values) {
        StringBuilder hql = new StringBuilder(" ");

        if (queryParams.containsKey("productId") && !StringUtil.isBlank(queryParams.get("productId").get(0))) {
            hql.append(" AND pa.productId = :productId ");
            values.put("productId", queryParams.get("productId"));
        }
        if (!queryParams.containsKey("status")) {
            hql.append(" AND pa.status = 1");
        }
        return hql;
    }
}
