package com.example.ducky_be.repository.custom;

import com.example.ducky_be.repository.entity.FileUpload;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface FileuploadRepositoryCustom {
    Long countFile(MultiValueMap<String, String> queryParams);

    List<FileUpload> getListByOwner(String ownerId, String ownerType);

    List<FileUpload> getListByFilePath(String filePath);

    List<FileUpload> getListByFileName(String name);
}
