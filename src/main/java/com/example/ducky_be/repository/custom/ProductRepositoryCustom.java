package com.example.ducky_be.repository.custom;

import com.example.ducky_be.repository.entity.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface ProductRepositoryCustom {
    Long countProduct(MultiValueMap<String, String> queryParams);

    List<Product> searchProduct(MultiValueMap<String, String> queryParams, Pageable pageable);

    /**
     * @author: huynq
     * @since: 10/31/2021 4:59 PM
     * @description: Thay đổi trạng thái của sản phẩm
     * @update:
     */
    Integer changeStatusProduct(String productId, Integer Status);
}
