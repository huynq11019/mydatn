package com.example.ducky_be.repository.custom;

import com.example.ducky_be.repository.entity.Order;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.Map;

public interface OrderRepositoryCustom {
    List<Order> search(MultiValueMap<String, String> queryParams, Pageable pageable);

    Long countAll(MultiValueMap<String, String> queryParams);

    List<Order> findAllOrderByCustomerId(MultiValueMap<String, String> queryParams, Pageable pageable);

    Map<String, Long> statisticOrderByDay(MultiValueMap<String, String> queryParams);

    Map<String, Long> statisticMoney(MultiValueMap<String, String> queryParams);
}
