package com.example.ducky_be.repository.custom.impl;

import com.example.ducky_be.core.utils.PageUtil;
import com.example.ducky_be.core.utils.StringUtil;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.custom.AuthorityRepositoryCustom;
import com.example.ducky_be.repository.entity.Authorities;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
public class AuthorityRepositoryCustomImpl implements AuthorityRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Authorities> getListAuthority(MultiValueMap<String, String> params, Pageable pageable) {
        try {
            StringBuilder hql = new StringBuilder();

            hql.append("SELECT a FROM Authorities a WHERE 1=1");
            Map<String, Object> values = new HashMap<>();
            hql.append(this.createWhereQuery(params, values));

            hql.append(PageUtil.PageToSqlQueryOrderBy(pageable, "a"));
            Query query = this.entityManager.createQuery(hql.toString(), Authorities.class);
            values.forEach(query::setParameter);

            if (Validator.isNotNull(pageable) && pageable.getPageSize() > 0 && pageable.getPageNumber() >= 0) {
                query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
                query.setMaxResults(pageable.getPageSize());
            }
            return query.getResultList();

        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    private StringBuilder createWhereQuery(MultiValueMap<String, String> queryParams, Map<String, Object> values) {
        StringBuilder hql = new StringBuilder(" ");

        if (queryParams.containsKey("roleId") && !StringUtil.isBlank(queryParams.get("roleId").get(0))) {
            hql.append(" AND a.role_id = :roleId ");
            values.put("roleId", queryParams.getFirst("roleId"));
        }
        if (!queryParams.containsKey("status")) {
            hql.append(" AND a.status = 1");
        }
        return hql;
    }
}
