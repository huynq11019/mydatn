package com.example.ducky_be.repository.custom.impl;

import com.example.ducky_be.repository.custom.AccessTokenRepositoryCustom;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Slf4j
@Repository
public class AccessTokenRepositoryCustomImpl implements AccessTokenRepositoryCustom {
    /**
     * The entity manager.
     */
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public int expiredTokenByUsername(String username) {
        int result = 0;

        try {
            StringBuilder sb = new StringBuilder(1);
            sb.append("update AccessToken set expired = 0 where username = :username ");

            Query query = this.entityManager.createQuery(sb.toString());
            query.setParameter("username", username);

            result = query.executeUpdate();
        } catch (Exception e) {
            log.error("Error occurred when expired token by username", e);
        }

        return result;
    }
}
