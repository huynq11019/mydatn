package com.example.ducky_be.repository.custom;

import com.example.ducky_be.repository.entity.Cart;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface CartRepositoryCustom {
    List<Cart> searchMyCustomer(MultiValueMap<String, String> params, Pageable pageable);

    Long countCartOfCustomer(MultiValueMap<String, String> params, Pageable pageable);
//    List<Cart> findByCustomerId(MultiValueMap<String, String> params);

    Integer deleteListCart(List<String > ids, Integer status);

    int deleteCartItems(List<String> ids);
}
