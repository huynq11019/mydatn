package com.example.ducky_be.repository.custom.impl;

import com.example.ducky_be.repository.custom.FileuploadRepositoryCustom;
import com.example.ducky_be.repository.entity.FileUpload;
import lombok.extern.log4j.Log4j2;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Log4j2
public class FileuploadRepositoryCustomImpl implements FileuploadRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Long countFile(MultiValueMap<String, String> queryParams) {
        return 100L;
    }

    @Override
    public List<FileUpload> getListByOwner(String ownerId, String ownerType) {
        String sql = "SELECT f FROM FileUpload f WHERE f.ownerId =:ownerId and f.ownerType =: ownerType and f.status >-1 order by f.createDate desc ";
        Query query = this.entityManager.createQuery(sql, FileUpload.class);
        query.setParameter("ownerId", ownerId);
        query.setParameter("ownerType", ownerType);
        return query.getResultList();
    }

    @Override
    public List<FileUpload> getListByFilePath(String filePath) {
        String sql = "SELECT f FROM FileUpload f WHERE f.filePath =: filePath";
        Query query = this.entityManager.createQuery(sql, FileUpload.class);
        query.setParameter("filePath", filePath);
        List<FileUpload> test = query.getResultList();
        return test;

    }

    @Override
    public List<FileUpload> getListByFileName(String name) {
        String sql = "SELECT f FROM FileUpload f WHERE f.originalName =: originalName";
        Query query = this.entityManager.createQuery(sql, FileUpload.class);
        query.setParameter("originalName", name);
        List<FileUpload> test = query.getResultList();
        return test;
    }
}
