package com.example.ducky_be.repository.custom.impl;

import com.example.ducky_be.core.utils.GetterUtil;
import com.example.ducky_be.core.utils.StringUtil;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.custom.AttributesRepositoryCustom;
import com.example.ducky_be.repository.entity.Attributes;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2

public class AttributesRepositoryCustomImpl implements AttributesRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    /*
     * @author: NamTH
     * @since: 18/11/2021 10:09 PM
     * @description:  Lấy attribute theo category
     * @update:
     *
     * */
    @Override
    public List<Attributes> findAttributeByCategoryId(String categoryId, MultiValueMap<String, String> queryParams, Pageable pageable) {
        try {
            StringBuilder hql = new StringBuilder();
            hql.append("SELECT a FROM Attributes a WHERE a.categoryId = :categoryId ");
            Map<String, Object> values = new HashMap<>();
            hql.append(this.createWhereQuery(queryParams, values));
            Query query = this.entityManager.createQuery(hql.toString(), Attributes.class);
            query.setParameter("categoryId", categoryId);
            values.forEach(query::setParameter);
            log.info(hql.toString());
            if (Validator.isNotNull(pageable) && pageable.getPageSize() > 0 && pageable.getPageNumber() >= 0) {
                query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
                query.setMaxResults(pageable.getPageSize());
            }
            return query.getResultList();
        } catch (Exception exception) {
            log.error(exception);
        }
        return new ArrayList<>();
    }


    @Override
    public Long countParam(String categoryId, MultiValueMap<String, String> queryParams) {
        try {
            StringBuilder hql = new StringBuilder();
            hql.append("SELECT COUNT(a) FROM Attributes a WHERE a.categoryId = :categoryId ");
            Map<String, Object> values = new HashMap<>();
            hql.append(this.createWhereQuery(queryParams, values));
            Query query = this.entityManager.createQuery(hql.toString(), Long.class);
            query.setParameter("categoryId", categoryId);
            log.info(categoryId);
            values.forEach(query::setParameter);
            log.info(hql.toString());
            return GetterUtil.getLong(query.getSingleResult(), 0L);
        } catch (Exception e) {
            log.error(e);
        }

        return 0L;
    }

    private StringBuilder createWhereQuery(MultiValueMap<String, String> queryParams, Map<String, Object> values) {
        StringBuilder hql = new StringBuilder(" ");

        if (queryParams.containsKey("categoryId") && !StringUtil.isBlank(queryParams.get("categoryId").get(0))) {
            hql.append(" AND a.categoryId = :categoryId ");
            values.put("categoryId", queryParams.getFirst("categoryId"));
        }
        if (!queryParams.containsKey("status")) {
            hql.append(" AND a.status = 1");
        }
        // status = -1
        // tìm theo tên
        // tìm thep type

        return hql;
    }

}
