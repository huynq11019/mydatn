package com.example.ducky_be.repository.custom;

import com.example.ducky_be.repository.entity.Authorities;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface AuthorityRepositoryCustom {
    List<Authorities> getListAuthority(MultiValueMap<String, String> params, Pageable pageable);


}
