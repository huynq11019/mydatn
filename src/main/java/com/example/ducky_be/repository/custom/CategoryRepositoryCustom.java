package com.example.ducky_be.repository.custom;


import com.example.ducky_be.repository.entity.Categories;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface CategoryRepositoryCustom {

    Long countCategory(MultiValueMap<String, String> queryParams);

    List<Categories> searchAllCategory(MultiValueMap<String, String> queryParams, Pageable pageable);

    List<Categories> searchTree(MultiValueMap<String, String> queryParams);
}
