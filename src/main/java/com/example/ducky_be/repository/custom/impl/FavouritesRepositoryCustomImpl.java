package com.example.ducky_be.repository.custom.impl;

import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.core.sercurity.SecurityUtils;
import com.example.ducky_be.core.utils.GetterUtil;
import com.example.ducky_be.core.utils.StringUtil;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.custom.FavouritesRepositoryCustom;
import com.example.ducky_be.repository.entity.Favourites;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
public class FavouritesRepositoryCustomImpl implements FavouritesRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    /*
     * @author: HungHV
     * @since: 7/11/2021 3:00 CH
     * @description:  sql lấy danh sách favourites
     * @update:
     *
     * */

    @Override
    public Long countFavourites(MultiValueMap<String, String> queryParams) {
        String currentUserId = SecurityUtils.getLoginUserId().orElseThrow(() ->
                new BadCredentialsException(ErrorCode.AUTHENTICATION_REQUIRED.getMessage()));
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT COUNT(f) FROM Favourites f WHERE f.createBy = :currentUserId AND f.status = :status ");
            Map<String, Object> values = new HashMap<>();
            sql.append(this.createWhereQuery(queryParams, values));

            Query query = this.entityManager.createQuery(sql.toString(), Long.class);
            query.setParameter("status", Constants.EntityStatus.ACTIVE);
            query.setParameter("currentUserId", currentUserId);
            values.forEach(query::setParameter);
            return GetterUtil.getLong(query.getSingleResult(), 0L);
        } catch (Exception exception) {
            log.error(exception.getMessage());
        }
        return 0L;
    }

    @Override
    public List<Favourites> searchFavourites(MultiValueMap<String, String> queryParams, Pageable pageable) {
        String currentUserId = SecurityUtils.getLoginUserId().orElseThrow(() ->
                new BadCredentialsException(ErrorCode.AUTHENTICATION_REQUIRED.getMessage()));

        try {
            StringBuilder hql = new StringBuilder();
            hql.append("SELECT f FROM Favourites f WHERE f.createBy = :currentUserId AND f.status = :status ");
            Map<String, Object> values = new HashMap<>();
            hql.append(this.createWhereQuery(queryParams, values));
            hql.append(this.createOrderQuery());
            Query query = this.entityManager.createQuery(hql.toString(), Favourites.class);
            query.setParameter("status", Constants.EntityStatus.ACTIVE);
            query.setParameter("currentUserId", currentUserId);
            values.forEach(query::setParameter);

            if (Validator.isNotNull(pageable) && pageable.getPageSize() > 0 && pageable.getPageNumber() >= 0) {
                query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
                query.setMaxResults(pageable.getPageSize());
            }
            return query.getResultList();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    @Override
    public List<Favourites> searchFavouritesByUser(MultiValueMap<String, String> queryParams, String userId, Pageable pageable) {
        try {
            StringBuilder hql = new StringBuilder();
            hql.append("SELECT f FROM Favourites f WHERE 1 = 1 AND f.status = :status AND f.customerId = :userId");
            Map<String, Object> values = new HashMap<>();
            hql.append(this.createWhereQuery(queryParams, values));
            hql.append(this.createOrderQuery());
            Query query = this.entityManager.createQuery(hql.toString(), Favourites.class);
            query.setParameter("status", Constants.EntityStatus.ACTIVE);
            query.setParameter("userId", userId);
            values.forEach(query::setParameter);

            if (Validator.isNotNull(pageable) && pageable.getPageSize() > 0 && pageable.getPageNumber() >= 0) {
                query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
                query.setMaxResults(pageable.getPageSize());
            }
            return query.getResultList();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    private StringBuilder createWhereQuery(MultiValueMap<String, String> queryParams, Map<String, Object> values) {
        StringBuilder sql = new StringBuilder(" ");

        if (queryParams.containsKey("customerId") && !StringUtil.isBlank(queryParams.get("customerId").get(0))) {
            sql.append(" AND f.customerId = :customerId ");
            values.put("customerId", GetterUtil.getLongValue(queryParams.get("customerId")));
        }

        if (queryParams.containsKey("productId") && !StringUtil.isBlank(queryParams.get("productId").get(0))) {
            sql.append(" AND f.productId = :productId ");
            values.put("productId", GetterUtil.getLongValue(queryParams.get("productId")));
        }

        if (queryParams.containsKey("startDate") && !StringUtil.isBlank(queryParams.get("startDate").get(0))) {
            sql.append(" AND f.createDate >= :startDate ");
            values.put("startDate", Instant.ofEpochMilli(Long.parseLong(queryParams.get("startDate").get(0))));
        }

        if (queryParams.containsKey("endDate") && !StringUtil.isBlank(queryParams.get("endDate").get(0))) {
            sql.append(" AND f.createDate  <= :endDate ");
            values.put("endDate", Instant.ofEpochMilli(Long.parseLong(queryParams.get("endDate").get(0))));
        }

        return sql;
    }

    private StringBuilder createOrderQuery() {
        StringBuilder sql = new StringBuilder();
        sql.append(" order by f.createDate asc ");
        return sql;
    }


}
