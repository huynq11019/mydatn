package com.example.ducky_be.repository.custom.impl;

import com.example.ducky_be.core.utils.GetterUtil;
import com.example.ducky_be.core.utils.StringUtil;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.custom.CustomerRepositoryCustom;
import com.example.ducky_be.repository.entity.Customer;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
public class CustomerRepositoryCustomImpl implements CustomerRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Long countCus(MultiValueMap<String, String> queryParams) {
        try {
            log.info("param =>>>" + queryParams.toString());
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT COUNT(c) FROM Customer c ");
            Map<String, Object> values = new HashMap<>();
            sql.append(this.createWhereQuerỵ̣(queryParams, values));
            log.info(sql.toString());
            Query query = this.entityManager.createQuery(sql.toString(), Long.class);
//            query.setParameter("status", Constants.EntityStatus.ACTIVE);
            values.forEach(query::setParameter);
            return GetterUtil.getLong(query.getSingleResult(), 0L);
        } catch (Exception exception) {
            exception.printStackTrace();
            log.error(exception.getMessage());
        }
        return 0L;
    }

    @Override
    public List<Customer> searchCustomer(MultiValueMap<String, String> queryParams, Pageable pageable) {
        StringBuilder hql = new StringBuilder();
        Map<String, Object> values = new HashMap<>();
        hql.append("SELECT c FROM Customer c ");
        hql.append(this.createWhereQuerỵ̣(queryParams, values));
        hql.append(this.createOrderQuery(true));
        log.info(hql.toString());
        Query query = this.entityManager.createQuery(hql.toString(), Customer.class);
        values.forEach(query::setParameter);

        if (Validator.isNotNull(pageable) && pageable.getPageSize() > 0 && pageable.getPageNumber() >= 0) {
            query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
            query.setMaxResults(pageable.getPageSize());
        }
        query.setMaxResults(pageable.getPageSize());

        return query.getResultList();
    }

    @Override
    public Integer changeStatusProduct(String customerId, Integer status) {
        String hql = "UPDATE Customer c SET c.status = :status WHERE  c.id = :customerId";

        Query query = this.entityManager.createQuery(hql);

        query.setParameter("status", status);
        query.setParameter("customerId", customerId);
        return query.executeUpdate();
    }

    private StringBuilder createWhereQuerỵ̣(MultiValueMap<String, String> queryParams, Map<String, Object> values) {
        StringBuilder sql = new StringBuilder(" WHERE 1 = 1 ");

        if (queryParams.containsKey("keyword") && !StringUtil.isBlank(queryParams.get("keyword").get(0))) {
//            sql.append(" AND ( LOWER(c.username) LIKE LOWER(:keyword) ");
            sql.append(" AND (  LOWER(c.phoneNumber) LIKE LOWER(:keyword)  ");
            sql.append(" OR LOWER(c.email) LIKE LOWER(:keyword)  ");
            sql.append(" OR LOWER(c.address) LIKE LOWER(:keyword)  ");
            sql.append(" OR LOWER(c.fullName) LIKE LOWER(:keyword)  ");
            sql.append("  ) ");
            values.put("keyword", "%" + queryParams.get("keyword").get(0).trim() + "%");
        }
        if (queryParams.containsKey("customerId") && !StringUtil.isBlank(queryParams.get("customerId").get(0))) {
            sql.append(" AND c.id = :customerId ");
            values.put("customerId", GetterUtil.getLongValue(queryParams.get("customerId")));
        }
        if (queryParams.containsKey("startDate") && !StringUtil.isBlank(queryParams.get("startDate").get(0))) {
            sql.append(" AND c.createDate >= :startDate ");
            values.put("startDate", Instant.ofEpochMilli(Long.parseLong(queryParams.get("startDate").get(0))));
        }

        if (queryParams.containsKey("endDate") && !StringUtil.isBlank(queryParams.get("endDate").get(0))) {
            sql.append(" AND c.createDate  <= :endDate ");
            values.put("endDate", Instant.ofEpochMilli(Long.parseLong(queryParams.get("endDate").get(0))));
        }
        if (queryParams.containsKey("status") && !StringUtil.isBlank(queryParams.get("status").get(0))) {
            sql.append(" AND c.status = :status");
            values.put("status", Integer.parseInt(queryParams.get("status").get(0)));
            log.info(queryParams.get("status").get(0));
        }
        return sql;
    }

    private StringBuilder createOrderQuery(boolean order) {
        StringBuilder sql = new StringBuilder();
        String orderb = order ? "desc" : "desc";
        sql.append(" order by c.createDate " + orderb);
        return sql;
    }
    // Converse long to instants
}
