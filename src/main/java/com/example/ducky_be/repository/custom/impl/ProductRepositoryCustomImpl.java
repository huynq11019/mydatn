package com.example.ducky_be.repository.custom.impl;

import com.example.ducky_be.core.utils.GetterUtil;
import com.example.ducky_be.core.utils.PageUtil;
import com.example.ducky_be.core.utils.StringUtil;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.custom.ProductRepositoryCustom;
import com.example.ducky_be.repository.entity.Product;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
public class ProductRepositoryCustomImpl implements ProductRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Long countProduct(MultiValueMap<String, String> queryParams) {
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT COUNT(p) FROM Product p WHERE 1 = 1  ");
            Map<String, Object> values = new HashMap<>();
            sql.append(this.createWhereQuery(queryParams, values));
            log.info(sql.toString());
            Query query = this.entityManager.createQuery(sql.toString(), Long.class);
//            query.setParameter("status", Constants.EntityStatus.ACTIVE);
            values.forEach(query::setParameter);
            return GetterUtil.getLong(query.getSingleResult(), 0L);
        } catch (Exception exception) {
            log.error(exception.getMessage());
        }
        return 0L;
    }

    @Override
    public List<Product> searchProduct(MultiValueMap<String, String> queryParams, Pageable pageable) {
        try {
            StringBuilder hql = new StringBuilder();
            hql.append("SELECT p FROM Product p WHERE 1 = 1  ");
            Map<String, Object> values = new HashMap<>();
            hql.append(this.createWhereQuery(queryParams, values));
            hql.append(PageUtil.PageToSqlQueryOrderBy(pageable, "p"));
            log.info(hql.toString());
            Query query = this.entityManager.createQuery(hql.toString(), Product.class);
            values.forEach(query::setParameter);

            if (Validator.isNotNull(pageable) && pageable.getPageSize() > 0 && pageable.getPageNumber() >= 0) {
                query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
                query.setMaxResults(pageable.getPageSize());
            }
            return query.getResultList();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Integer changeStatusProduct(String productId, Integer Status) {
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE Product p SET p.status = :status WHERE p.productId = :productId ");
            Query query = this.entityManager.createQuery(sql.toString());
            query.setParameter("status", Status);
            query.setParameter("productId", productId);
            return query.executeUpdate();
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return null;
    }

    private StringBuilder createWhereQuery(MultiValueMap<String, String> queryParams, Map<String, Object> values) {
        StringBuilder sql = new StringBuilder(" ");


        if (queryParams.containsKey("keyword") && !StringUtil.isBlank(queryParams.get("keyword").get(0))) {
            sql.append(" AND ( LOWER(p.productName) LIKE LOWER(:keyword) ");
            sql.append(" OR LOWER(p.subCateName) LIKE LOWER(:keyword) ) ");
            values.put("keyword", "%" + queryParams.get("keyword").get(0).trim() + "%");
        }
        if (queryParams.containsKey("categoryId") && !StringUtil.isBlank(queryParams.getFirst("categoryId"))) {
            sql.append(" AND p.categoryId IN (:categoryId) ");
            values.put("categoryId", Arrays.stream(Objects.requireNonNull(queryParams
                                    .getFirst("categoryId"))
                            .split(","))
                    .map(Long::valueOf).collect(Collectors.toList()));
        }

        if (queryParams.containsKey("productId") && !StringUtil.isBlank(queryParams.get("productId").get(0))) {
            sql.append(" AND p.id = :productId ");
            values.put("productId", GetterUtil.getLongValue(queryParams.get("productId")));
        }
        if (queryParams.containsKey("startDate") && !StringUtil.isBlank(queryParams.get("startDate").get(0))) {
            sql.append(" AND p.createDate >= :startDate ");
            values.put("startDate", Instant.ofEpochMilli(Long.parseLong(queryParams.get("startDate").get(0))));
        }
        if (queryParams.containsKey("serial") && !StringUtil.isBlank(queryParams.get("serial").get(0))) {
            sql.append(" AND p.serial = :serial");
            values.put("serial", Long.valueOf(queryParams.getFirst("serial")));
        }
        if (queryParams.containsKey("endDate") && !StringUtil.isBlank(queryParams.get("endDate").get(0))) {
            sql.append(" AND p.createDate  <= :endDate ");
            values.put("endDate", Instant.ofEpochMilli(Long.parseLong(queryParams.get("endDate").get(0))));
        }
        if (queryParams.containsKey("maxPrice") && !StringUtil.isBlank(queryParams.getFirst("maxPrice"))) {
            sql.append(" AND p.price <= :maxPrice ");
            values.put("maxPrice", GetterUtil.getLongValue(queryParams.getFirst("maxPrice")));
        }
        if (queryParams.containsKey("minPrice") && !StringUtil.isBlank(queryParams.getFirst("minPrice"))) {
            sql.append(" AND p.price >= :minPrice ");
            values.put("minPrice", GetterUtil.getLongValue(queryParams.getFirst("minPrice")));
        }
        if (queryParams.containsKey("status") && !StringUtil.isBlank(queryParams.get("status").get(0))) {
            sql.append(" AND p.status = :status");
            values.put("status", Integer.parseInt(queryParams.get("status").get(0)));
        } else {
            sql.append(" AND p.status = 1");
        }
        return sql;
    }

}
