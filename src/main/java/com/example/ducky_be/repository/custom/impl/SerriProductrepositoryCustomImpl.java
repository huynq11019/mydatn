package com.example.ducky_be.repository.custom.impl;

import com.example.ducky_be.repository.custom.SerriProductrepositoryCustom;
import com.example.ducky_be.repository.entity.SerriProduct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class SerriProductrepositoryCustomImpl implements SerriProductrepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<SerriProduct> searchSerriProduct(MultiValueMap<String, String> queryParams, Pageable pageable) {
        StringBuilder HQL = new StringBuilder("SELECT p FROM SerriProduct p WHERE 1=1 ");
        Map<String, Object> values = new HashMap<>();
        HQL.append(createWhereQuery(queryParams, values));
        HQL.append(createOrderQuery(pageable));
        Query query = entityManager.createQuery(HQL.toString(), SerriProduct.class);
        return query.getResultList();
    }

    @Override
    public long countSerriProduct(MultiValueMap<String, String> queryParams) {
        String HQL = "SELECT COUNT(p.id) FROM SerriProduct p WHERE 1=1 ";
        Query query = entityManager.createQuery(HQL);
        return (long) query.getSingleResult();
    }

    @Override
    public Integer changeSerriStatusById(Long id, Integer status) {
        String HQL = "UPDATE SerriProduct p SET p.status = :status WHERE p.id = :id";
        Query query = entityManager.createQuery(HQL);
        query.setParameter("status", status);
        query.setParameter("id", id);
        return query.executeUpdate();
    }

    private StringBuilder createWhereQuery(MultiValueMap<String, String> queryParams, Map<String, Object> values) {
        StringBuilder whereQuery = new StringBuilder();
        if (queryParams.containsKey("serriName")) {
            whereQuery.append(" AND p.serriName LIKE :serriName");
            values.put("serriName", "%" + queryParams.getFirst("serriName") + "%");
        }
        return whereQuery;
    }

    // create order by query
    private StringBuilder createOrderQuery(Pageable pageable) {
        StringBuilder stringBuilder = new StringBuilder();
        if (pageable.getSort().isSorted()) {
            stringBuilder.append(" ORDER BY ");
            pageable.getSort().forEach(order -> {
                stringBuilder.append(order.getProperty())
                        .append(" ")
                        .append(order.getDirection())
                        .append(", ");
            });
        }
        return stringBuilder;
    }
}
