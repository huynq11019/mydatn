package com.example.ducky_be.repository.custom.impl;

import com.example.ducky_be.core.utils.PageUtil;
import com.example.ducky_be.core.utils.StringUtil;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.custom.RefundRepositoryCustom;
import com.example.ducky_be.repository.entity.Refund;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log4j2
public class RefundRepositoryCustomImpl implements RefundRepositoryCustom {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Refund> getAllRefund(MultiValueMap<String, String> queryParams, Pageable pageable) {
        try {
            StringBuilder hql = new StringBuilder();
            hql.append("SELECT r FROM Refund r WHERE 1 = 1  ");
            Map<String, Object> values = new HashMap<>();
            hql.append(this.createWhereQuery(queryParams, values));
            hql.append(PageUtil.PageToSqlQueryOrderBy(pageable, "r"));
            log.info(hql.toString());
            Query query = this.entityManager.createQuery(hql.toString(), Refund.class);
            values.forEach(query::setParameter);

            if (Validator.isNotNull(pageable) && pageable.getPageSize() > 0 && pageable.getPageNumber() >= 0) {
                query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
                query.setMaxResults(pageable.getPageSize());
            }
            return query.getResultList();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Long countAll(MultiValueMap<String, String> queryParams, Pageable pageable) {
        return null;
    }

    private StringBuilder createWhereQuery(MultiValueMap<String, String> queryParams, Map<String, Object> values) {
        StringBuilder hql = new StringBuilder(" ");

        if (queryParams.containsKey("orderDetailId") && !StringUtil.isBlank(queryParams.get("OrderDetailId").get(0))) {
            hql.append(" AND r.orderDetailId = :orderDetailId ");
            values.put("orderDetailId", queryParams.get("orderDetailId"));
        }
        if (queryParams.containsKey("processingStatus") && !StringUtil.isBlank(queryParams.get("processingStatus").get(0))) {
            hql.append(" AND r.processingStatus = :processingStatus ");
            values.put("processingStatus", queryParams.get("processingStatus"));
        }
        if (!queryParams.containsKey("status")) {
            hql.append(" AND r.status = 1");
        }
        return hql;
    }
}
