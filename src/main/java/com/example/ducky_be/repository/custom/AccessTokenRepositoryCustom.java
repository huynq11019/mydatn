package com.example.ducky_be.repository.custom;

public interface AccessTokenRepositoryCustom {
    int expiredTokenByUsername(String username);
}
