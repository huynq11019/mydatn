package com.example.ducky_be.repository;

import com.example.ducky_be.repository.custom.CustomerRepositoryCustom;
import com.example.ducky_be.repository.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, String>, CustomerRepositoryCustom {

    Optional<Customer> findByEmailAndStatus(String email, Integer status);

    long countAllByStatus(Integer status);

    Optional<Customer> findByEmail(String email);

}
