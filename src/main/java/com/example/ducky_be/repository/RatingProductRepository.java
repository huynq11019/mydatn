package com.example.ducky_be.repository;

import com.example.ducky_be.repository.custom.RatingProductCustom;
import com.example.ducky_be.repository.entity.RatingProduct;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RatingProductRepository extends JpaRepository<RatingProduct, String>, RatingProductCustom {
    List<RatingProduct> findAllByProductIdOrderByPointDesc(String productId, Pageable pageable);
}
