package com.example.ducky_be.repository;

import com.example.ducky_be.repository.custom.ProductAttributeRepositoryCustom;
import com.example.ducky_be.repository.entity.ProductAttributes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductAttributeRepository extends JpaRepository<ProductAttributes, String>, ProductAttributeRepositoryCustom {
}
