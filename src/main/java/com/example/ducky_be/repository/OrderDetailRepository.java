package com.example.ducky_be.repository;

import com.example.ducky_be.repository.custom.OrderDetailRepositoryCustom;
import com.example.ducky_be.repository.entity.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetail,String> , OrderDetailRepositoryCustom {
    List<OrderDetail> findAllByOrderId(String orderId);

    Optional<List<OrderDetail>> findAllByOrderIdIn(List<String> orderIds);
}
