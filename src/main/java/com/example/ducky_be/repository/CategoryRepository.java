package com.example.ducky_be.repository;

import com.example.ducky_be.repository.custom.CategoryRepositoryCustom;
import com.example.ducky_be.repository.entity.Categories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CategoryRepository extends JpaRepository<Categories, Long>, CategoryRepositoryCustom {
    /*
     * @author: NamTH
     * @since: 31/10/2021 3:41 CH
     * @description:  category repository
     * @update:
     *
     * */


}
