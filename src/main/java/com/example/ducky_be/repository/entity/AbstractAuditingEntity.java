package com.example.ducky_be.repository.entity;

import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.Instant;


@Data
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractAuditingEntity implements Serializable {

    @CreatedDate
    @Column(name = "createDate", nullable = false, updatable = false)
    private Instant createDate = Instant.now();

    @CreatedBy
    @Column(name = "createBy", updatable = false)
    private String createBy;

    @Column(name = "lastUpdate")
    @LastModifiedDate
    private Instant lastUpdate = Instant.now();

    @LastModifiedBy
    @Column(name = "updateBy")
    private String updateBy;

    // status -1 là nháp, 0 xóa, 1 active
    @Column(name = "status")
    private Integer status = 1;

    public void changeLock() {
        this.status = this.status == 1 ? -1 : 1;
    }
}
