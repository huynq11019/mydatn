package com.example.ducky_be.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
//@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Table(name = "my_employee")
public class Employee extends AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 7020271006196714571L;

    @Id
    @Basic(optional = false)
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "uuid4", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", columnDefinition = "VARCHAR(36)")
    private String id;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "password_hash")
    private String password;

    @Column(name = "password_slat")
    private String passwordSlat;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "address")
    private String address;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "gender")
    private Integer gender;

    @Column(name = "dob")
    private Date dob;

    @Column(name = "avatar")
    private String avatar;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "empl_author", joinColumns = {
            @JoinColumn(name = "empl_id", referencedColumnName = "id")}, inverseJoinColumns = {
            @JoinColumn(name = "auth_id", referencedColumnName = "authrority_id")})
    @BatchSize(size = 20)
    private List<Authorities> authorities;

    public void updateAuthorities(List<Authorities> authorities) {
        this.authorities = authorities;
    }
}
