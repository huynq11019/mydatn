package com.example.ducky_be.repository.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "products")
public class Product extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    @Id
    @Basic(optional = false)
//    @GeneratedValue(generator = "uuid4")
//    @GenericGenerator(name = "uuid4", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", columnDefinition = "VARCHAR(36)")
    private String id;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "description")
    private String description;

    @Column(name = "sub_cate_name")
    private String subCateName;

    @Column(name = "price")
    private Long price;

    @Column(name = "discount")
    private float discount;

    @Column(name = "estimate_time")
    private Integer estimateTime;

    @Column(name = "is_order")
    private Boolean isOrder;

    @Column(name = "meta_data")
    private String metaData;

    @Column(name = "quantity")
    private long quantity;

    @Column(name = "serrial")
    private Long serial;

//    @Column(name = "category_id")
//    private String categoryId;

    @Column(name = "likeCount")
    private Long likeCount;

    @Column(name = "category_id")
    private Long categoryId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        if (!super.equals(o)) return false;
        Product product = (Product) o;
        return this.id != null && Objects.equals(this.id, product.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

}
