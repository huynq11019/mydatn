package com.example.ducky_be.repository.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Getter
@Setter
@ToString
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "product_attributes")
public class ProductAttributes extends AbstractAuditingEntity implements Serializable {
    @Id
    @Basic(optional = false)
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "uuid4", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", columnDefinition = "VARCHAR(36)")
    private String id;

    @Column(name = "attributes_id")
    private String attributeId;

    @Column(name = "product_id")
    private String productId;

    @Column(name = "value")
    private String value;

    @Column(name = "attribute_name")
    private String attributeName;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProductAttributes)) return false;
        if (!super.equals(o)) return false;
        ProductAttributes product = (ProductAttributes) o;
        return this.id != null && Objects.equals(this.id, product.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }


    @Override
    public String toString() {
        return "ProductAttributes [id=" + id + ", attributesId=" + attributeId + ", productId=" + productId + ", value="
                + value;
    }
}
