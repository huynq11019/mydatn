package com.example.ducky_be.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
@Table(name = "customer")
public class Customer extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "uuid4", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", columnDefinition = "VARCHAR(36)")
    private String id;

//	@Column(name = "username")
//	private String userName;

    @Column(name = "password_hash")
    private String passwordHash;

    @Column(name = "password_slat")
    private String passwordSlat;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "address")
    private String address;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "gender")
    private Integer gender;

    @Column(name = "dob")
    private String dob;

    @Column(name = "avatar")
    private String avatar;

    @Column(name = "reward_point")
    private Integer rewardPoint;

    @Column(name = "classify")
    private String classify;

    @Column(name = "ip_address")
    private String ipAddress;

    public void changePassword(String password) {
        this.passwordHash = password;
    }
}
