package com.example.ducky_be.repository.entity;

import com.example.ducky_be.core.constants.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "customer_order")

public class Order extends AbstractAuditingEntity implements Serializable {
    @Id
    @Basic(optional = false)
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "uuid4", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", columnDefinition = "VARCHAR(36)")
    private String id;

    @Column(name = "username")
    private String receiver; // tên người nhận

    @Column(name = "address")
    private String address;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "customer_id")
    private String customerId;

    @Column(name = "shipng_cost")
    private Long shippingCost;

    @Lob
    @Column(name = "signature")
    private String signature;

    @Column(name = "order_status")
    @Enumerated(EnumType.STRING)
    private Constants.OrderStatus orderStatus;

    @Column(name = "payment_method")
    @Enumerated(EnumType.STRING)
    private Constants.PaymentMethod paymentMethod;

    @Column(name = "note")
    private String note;

    @Column(name = "shipping_id")
    private String shippingid;

    @Column(name = "address_id")
    private Long addressId;


}
