package com.example.ducky_be.repository.entity;

import com.example.ducky_be.core.constants.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "options_product")
public class OptionProduct extends AbstractAuditingEntity {
    @Id
    @Basic(optional = false)
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "uuid4", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", columnDefinition = "VARCHAR(36)")
    private String id;

    @NotBlank(message = "Tên phân loại không được để trống")
    @Column(name = "name", columnDefinition = "VARCHAR(255)")
    private String name;

    @NotNull(message = "Product id must not null")
    @Column(name = "product_id")
    private String productId;

    @Column(name = "description")
    private String description;

    @Column(name = "image")
    private String image;

    @Min(value = 0, message = "Giá sản phẩm tối > 0")
    @Column(name = "price")
    private Long price;

    @Column(name = "discount")
    @Range(min = 0, max = 100)
    private float discount;

    @Column(name = "stock_quantity")
    private Integer stockQuantity;

    public OptionProduct(OptionProduct optionProduct) {
        this.name = optionProduct.getName();
        this.productId = optionProduct.getProductId();
        this.description = optionProduct.getDescription();
        this.image = optionProduct.getImage();
        this.price = optionProduct.getPrice();
        this.discount = optionProduct.getDiscount();
        this.stockQuantity = optionProduct.getStockQuantity();
    }
    public OptionProduct(String name, String productId, String description, String image, Long price, float discount, Integer stockQuantity) {
//        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.productId = productId;
        this.description = description;
        this.image = image;
        this.price = price;
        this.discount = discount;
        this.stockQuantity = stockQuantity;
    }

    public void delete() {
        this.setStatus(Constants.EntityStatus.REMOTED);
    }

    public void update(OptionProduct optionProduct) {
        this.setDescription(optionProduct.getDescription());
        this.setImage(optionProduct.getImage());
        this.setPrice(optionProduct.getPrice());
        this.setDiscount(optionProduct.getDiscount());
        this.stockQuantity = optionProduct.getStockQuantity();

        this.setStatus(Constants.EntityStatus.ACTIVE);
    }
}
