package com.example.ducky_be.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "address")
@Entity
public class Address extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name_of_recipient")
    private String nameOfRecipient;

    @Column(name = "province")
    private String province;

    @Column(name = "district")
    private String district;

    @Column(name = "ward")
    private String ward;

    @Column(name = "address_detail")
    private String addressDetail;

    @Column(name = "latitude")
    private String latitude;

    @Column(name = "status")
    private Integer status;

    @Column(name = "update_by")
    private String updateBy;

    @Column(name = "customer_id")
    private String customerId;

    @Column(name = "address_type")
    private String addressType;

    @Column(name = "is_adrresDefault")
    private Boolean isDefault;

    @Column(name = "phone_number")
    private String phoneNumber;
    @Column(name ="province_id")
    private Integer provinceId;
    @Column(name ="district_id")
    private Integer districtId;
    @Column(name ="ward_id")
    private String wardId;
}
