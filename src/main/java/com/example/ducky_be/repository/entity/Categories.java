package com.example.ducky_be.repository.entity;

import com.example.ducky_be.core.constants.Constants;
import lombok.*;
import org.springframework.util.CollectionUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "categories")
public class Categories extends AbstractAuditingEntity implements Serializable {
    /**
     * @author: NamTH
     * @since: 31/10/2021 3:18 CH
     * @description:
     * @update: bỏ active
     */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "category_name", columnDefinition = "nvarchar(255)")
    private String categoryName;

    @Column(name = "icon")
    private String icon;

    @Column(name = "description", columnDefinition = "nvarchar(255)")
    private String description;

    @Column(name = "sort_order")
    private Integer sortOrder;

    @ManyToOne
    @JoinColumn(name = "parentcate_id")
    @ToString.Exclude // Khoonhg sử dụng trong toString()
    private Categories parentCategory;

    @OneToMany(mappedBy = "parentCategory", cascade = CascadeType.ALL)
//	 MapopedBy trỏ tới tên biến Address ở trong Person.
    @EqualsAndHashCode.Exclude // không sử dụng trường này trong equals và hashcode
//	@ToString.Exclude // Khoonhg sử dụng trong toString()
    private List<Categories> child;

    @ToString.Exclude
    @OneToMany(mappedBy = "categoryId", cascade = CascadeType.DETACH)
    private List<Product> products;

    public Integer getCountProduct() {
        if (!CollectionUtils.isEmpty(products)) {
            return products.stream().filter(p -> Objects.equals(p.getStatus(), Constants.EntityStatus.ACTIVE) ).collect(Collectors.toList()).size();
        }
        return 0;
    }
}
