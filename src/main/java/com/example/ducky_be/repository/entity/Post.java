package com.example.ducky_be.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Table(name = "post")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Post extends AbstractAuditingEntity {
    @Id
    @Basic(optional = false)
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "uuid4", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", columnDefinition = "VARCHAR(36)")
    private String id;

    @Column(name = "employee_id")
    private String employeeId;

    @Column(name = "src")
    private String src;

    @Column(name = "views")
    private String views;


}
