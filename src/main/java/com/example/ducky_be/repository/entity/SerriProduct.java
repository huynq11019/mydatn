package com.example.ducky_be.repository.entity;

import com.example.ducky_be.core.constants.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "serial")
public class SerriProduct extends AbstractAuditingEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "product_serri")
    private Long id;

    @Column(name = "serri_name")
    private String serriName;

    @Column(name = "serri_description")
    private String serriDescroption;

    public void deleteSerriProduct() {
        this.setStatus(Constants.EntityStatus.REMOTED);
    }

}
