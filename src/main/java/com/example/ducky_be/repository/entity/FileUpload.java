package com.example.ducky_be.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "file_upload")
public class FileUpload extends AbstractAuditingEntity implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The file path.
     */
    @Column(name = "file_path")
    private String filePath;

    /**
     * The file size.
     */
    @Column(name = "file_size")
    private Long fileSize;

    /**
     * The file type.
     */
    @Column(name = "file_type")
    private String fileType;

    /**
     * The id.
     */
    @Id
    @Basic(optional = false)
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "uuid4", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", columnDefinition = "VARCHAR(36)")
    private String id;

    /**
     * The original name.
     */
    @Column(name = "original_name")
    private String originalName;

    /**
     * sở hữu bởi thằng àno.
     */
    @Column(name = "owner_id")
    private String ownerId;

    /**
     * loại của thằng sở hữu.
     */
    @Column(name = "owner_type")
    private String ownerType;

    /**
     * The status.
     */
    @Basic(optional = false)
    @Column(name = "status")
    private Integer status;


}
