package com.example.ducky_be.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "product_voucher")
public class ProductVoucher extends AbstractAuditingEntity implements Serializable {
    @Id
    @Basic(optional = false)
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "uuid4", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", columnDefinition = "VARCHAR(36)")
    private String id;

    @Column(name = "voucher_name")
    private String voucherName;

    @Column(name = "voucher_description")
    private String voucherDescription;

    @Column(name = "img")
    private String img;

    @Column(name = "use_scope")
    private String scope; //

    @Column(name = "active_date")
    private Date activeDate;

    @Column(name = "exprice_date")
    private Date expriceDate;
}
