package com.example.ducky_be.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "employee_role")
public class Role extends AbstractAuditingEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "uuid4", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", columnDefinition = "VARCHAR(36)")
    private String id;

    @Column(name = "role_code", updatable = false)
    private String roleCode;

    @Column(name = "role_name")
    private String roleName;

    @Column(name = "description")
    private String description;

    @Column(name = "employee_id")
    private String employeeId;

//    @ManyToMany(fetch = FetchType.LAZY)
//    @JoinTable(name = "role_authro", joinColumns = {
//            @JoinColumn(name = "id", referencedColumnName = "id") }, inverseJoinColumns = {
//            @JoinColumn(name = "authrority_id", referencedColumnName = "authrority_id") })
//    @BatchSize(size = 20)
//    private List<Authorities> authorities;
}
