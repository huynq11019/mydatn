package com.example.ducky_be.repository.entity;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Objects;
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "email_template")
public class MailTemplate extends  AbstractAuditingEntity{
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The content.
     */
    @Column(name = "content")
    private String content;

    /**
     * The subject.
     */
    @Column(name = "subject")
    private String subject;

    /**
     * The template code.
     */
    @Column(name = "template_code")
    private String templateCode;

    /**
     * The template id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "template_id")
    private Long templateId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        MailTemplate that = (MailTemplate) o;
        return templateId != null && Objects.equals(templateId, that.templateId);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
