package com.example.ducky_be.repository.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "access_token")
public class AccessToken extends AbstractAuditingEntity implements Serializable {
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The token id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "token_id")
    private Long tokenId;

    /**
     * The value.
     */
    @Basic(optional = false)
    @Column(name = "value")
    private String value;

    /**
     * The user name.
     */
    @Column(name = "user_name")
    private String username;

    /**
     * The expired time.
     */
    @Basic(optional = false)
    @Column(name = "expired_time")
    private Instant expiredTime;

    /**
     * The expired.
     */
    @Column(name = "expired")
    private boolean expired;


}
