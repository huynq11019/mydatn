package com.example.ducky_be.repository.entity;

import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.service.dto.AuthorityDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "authorities")
public class Authorities extends AbstractAuditingEntity implements Serializable {

    @Id
    @Basic(optional = false)
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "uuid4", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "authrority_id", columnDefinition = "VARCHAR(36)")
    private String authorityId;

    @Column(name = "authrority_code")
    private String authorityCode;

    @Column(name = "authrory_name")
    private String authorityName;

    @Column(name = "role_id")
    private String roleId;

    //	// cái này là many to many to ROle
//	@Column(name = "role_id")
//	private Long role;
    public void update(Authorities authorities) {
        this.authorityCode = authorities.getAuthorityCode();
        this.authorityName = authorities.getAuthorityName();
        this.roleId = authorities.getRoleId();
    }

    public void delete() {
        this.setStatus(Constants.EntityStatus.REMOTED);
    }

    public Authorities(AuthorityDTO dto) {
        this.authorityCode = dto.getAuthorityCode();
        this.authorityName = dto.getAuthorityName();
        this.setCreateDate(Instant.now());
        this.setLastUpdate(Instant.now());
    }
}
