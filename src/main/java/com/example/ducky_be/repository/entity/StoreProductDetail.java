package com.example.ducky_be.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Table(name = "stock_product_detail")
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class StoreProductDetail extends AbstractAuditingEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "product_id")
    private Integer productId;

    @Column(name = "stock_id")
    private Integer stock_id;

    @Column(name = "quantity")
    private String quantity;


}
