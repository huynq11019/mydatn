package com.example.ducky_be.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Table(name = "order_detail")
@Entity
@AllArgsConstructor
@RequiredArgsConstructor
public class OrderDetail extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "uuid4", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", columnDefinition = "VARCHAR(36)")
    private String id;

    @Column(name = "order_id")
    private String orderId;

    @Column(name = "product_id")
    private String productId;

    @Column(name ="cart_item_id")
    private String cartItemId;


    @Column(name = "price")
    private Long productPrice;

    @Column(name = "option_id")
    private String optionProductId;

    @Column(name = "option_price")
    private String productName;

    @Column(name = "quantity")
    private Integer quantity;

    @Column(name = "warranty_time")
    private Date warrantyTime;

    @Column(name = "shipping_address")
    private String note;

}
