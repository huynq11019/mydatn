package com.example.ducky_be.repository.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "user_login")
public class UserLogin extends AbstractAuditingEntity implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The login log id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "login_log_id")
    private Long loginLogId;

    /**
     * The ip.
     */
    @Column(name = "ip")
    private String ip;

    /**
     * The login time.
     */
    @Column(name = "login_time")
    private Instant loginTime;

    /**
     * The success.
     */
    @Column(name = "success")
    private boolean success;

    /**
     * The username.
     */
    @Column(name = "username")
    private String username;

    /**
     * The description.
     */
    @Column(name = "description")
    private String description;
}
