package com.example.ducky_be.repository.entity;

import com.example.ducky_be.controller.protoco.request.CartRequest;
import com.example.ducky_be.core.constants.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cart")
public class Cart extends AbstractAuditingEntity implements Serializable {
    @Id
    @Basic(optional = false)
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "uuid4", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", columnDefinition = "VARCHAR(36)")
    private String id;

    @Column(name = "customer_id")
    private String customerId;

    @Column(name = "product_id")
    private String productId;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "option_id")
    private String optionProductId;

    @Column(name = "option_name")
    private String optionProductName;

    @Column(name="disount_price")
    private Float discount;

    @Column(name="product_price")
    private Long productPrice;

    public void deleteCartItem(){
        this.setStatus(Constants.EntityStatus.REMOTED);
    }
    public void updateCartItem(CartRequest cartRequest){
        this.setQuantity(cartRequest.getQuantity());
    }
    public Cart (CartRequest cartRequest) {
        this.customerId = cartRequest.getCustomerId();
        this.productId = cartRequest.getProductId();
        this.quantity  = cartRequest.getQuantity() ;
        this.optionProductId = cartRequest.getOptionProductId();
        this.setStatus(Constants.EntityStatus.ACTIVE);
    }
}
