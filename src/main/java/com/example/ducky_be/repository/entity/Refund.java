package com.example.ducky_be.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "refund")
public class Refund extends AbstractAuditingEntity implements Serializable {
    @Id
    @Basic(optional = false)
    @GeneratedValue(generator = "uuid4")
    @GenericGenerator(name = "uuid4", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", columnDefinition = "VARCHAR(36)")
    private String id;

    @Column(name = "order_detail_id")
    private String orderDetailId;

    @Column(name = "quantity")
    private Long quantity;

    @Column(name = "money_back")
    private Long moneyBack;
    // processing status = 0 : Chưa sử lí
    //                   = 1 : Đã sử lí
    //                   = 2 : Không sử lý
    //                   = 3 : Đang chờ

    @Column(name = "processing_status")
    private Integer processingStatus;

    @Column(name = "description")
    private String description;



}
