package com.example.ducky_be.repository;

import com.example.ducky_be.repository.custom.SerriProductrepositoryCustom;
import com.example.ducky_be.repository.entity.SerriProduct;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SerriProductrepository extends JpaRepository<SerriProduct, Long>, SerriProductrepositoryCustom {

    @Query("select f from SerriProduct f where 1 = 1" +
            " and (:keyword is null or (" +
            " lower(f.serriName) like %:keyword% or" +
            " lower(f.serriDescroption) like %:keyword% " +
            " ))")
    List<SerriProduct> simpleSearch(@Param("keyword") String keyword, Pageable pageable);

    @Query("select count(f) from SerriProduct f where 1 = 1" +
            " and (:keyword is null or (" +
            " lower(f.serriName) like %:keyword% or" +
            " lower(f.serriDescroption) like %:keyword% " +
            " ))")
    Long coutnSerriByKyword(@Param("keyword") String keyword);
}
