package com.example.ducky_be.repository;

import com.example.ducky_be.repository.custom.AttributesRepositoryCustom;
import com.example.ducky_be.repository.entity.Attributes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AttributesRepository extends JpaRepository<Attributes, String>, AttributesRepositoryCustom {


}
