package com.example.ducky_be.repository;

import com.example.ducky_be.repository.entity.OptionProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OptionRepository extends JpaRepository<OptionProduct, String> {

    List<OptionProduct> findAllByProductIdAndStatus(String productId, Integer status);

//    Long countByProductId(String productIds);

    @Query("select o from OptionProduct o where o.productId in ?1")
    List<OptionProduct> findAllByProductId(List<String> productIds);

    @Query("select o from OptionProduct o where o.id IN :ids")
    List<OptionProduct> findAllByIds(@Param("ids") List<String> ids);
}
