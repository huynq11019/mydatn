package com.example.ducky_be.repository;

import com.example.ducky_be.repository.custom.RefundRepositoryCustom;
import com.example.ducky_be.repository.entity.Refund;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RefundRepository extends JpaRepository<Refund, String>, RefundRepositoryCustom {
}
