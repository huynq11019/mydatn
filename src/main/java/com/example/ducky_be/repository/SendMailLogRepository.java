package com.example.ducky_be.repository;

import com.example.ducky_be.repository.entity.SendMailLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface SendMailLogRepository extends JpaRepository<SendMailLog, Long> {
    List<SendMailLog> getAllBySendStatus(Integer sendStatus);

}
