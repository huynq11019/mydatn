package com.example.ducky_be.repository;

import com.example.ducky_be.repository.custom.FavouritesRepositoryCustom;
import com.example.ducky_be.repository.entity.Favourites;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FavouritesRepository extends JpaRepository<Favourites, String>, FavouritesRepositoryCustom {

    Long countByProductId(String productId);

    Optional<Favourites> findByCustomerIdAndProductId(String customerid, String productId);

    List<Favourites> findAllByCustomerIdAndStatus(String customerId, Integer status);
}
