package com.example.ducky_be.repository;

import com.example.ducky_be.repository.entity.ContentTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContentTemplateRepository
		extends JpaRepository<ContentTemplate, String> {
	ContentTemplate findContentTemplateByTemplateCodeAndStatus(String templateCode, int status);

}
