package com.example.ducky_be;

//import com.example.ducky_be.utils.AciiArt;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.util.StringUtils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@EnableCaching
@SpringBootApplication
@EnableConfigurationProperties
public class DuckyBeApplication {
    private static final String SPRING_PROFILE_DEFAULT = "spring.profiles.default";
    private static final String HTTP = "http";
    public static final String SERVER_SSL_KEY_STORE = "server.ssl.key-store";
    public static final String HTTPS = "https";
    public static final String SERVER_PORT = "server.port";
    public static final String SERVER_SERVLET_CONTEXT_PATH = "server.servlet.context-path";

    public static void main(String[] args) {
//        SpringApplication.run(DuckyBeApplication.class, args);
        SpringApplication app = new SpringApplication(DuckyBeApplication.class);

        addDefaultProfile(app);
        Environment env = app.run(args).getEnvironment();
        logApplicationStartup(env);
    }

    public static void addDefaultProfile(SpringApplication app) {
        Map<String, Object> defProperties = new HashMap<>();
        /*
         * The default profile to use when no other profiles are defined
         * This cannot be set in the application.yml file.
         * See https://github.com/spring-projects/spring-boot/issues/1219
         */
        defProperties.put(SPRING_PROFILE_DEFAULT, "dev");
        app.setDefaultProperties(defProperties);
    }

    private static void logApplicationStartup(Environment env) {
        String protocol = HTTP;

        if (env.getProperty(SERVER_SSL_KEY_STORE) != null) {
            protocol = HTTPS;
        }

        String serverPort = env.getProperty(SERVER_PORT);
        String contextPath = env.getProperty(SERVER_SERVLET_CONTEXT_PATH);

        if (StringUtils.isEmpty(contextPath)) {
            contextPath = "/";
        }

        String hostAddress = "localhost";

        try {
            hostAddress = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            log.warn("The host name could not be determined, using `localhost` as fallback");
        }

        log.info(
                "\n----------------------------------------------------------\n\t"
                        + "Application '{}' is running! Access URLs:\n\t"
                        + "Local: \t\t{}://localhost:{}{}\n\t"
                        + "External: \t{}://{}:{}{}\n\t"
                        + "Profile(s): \t{}\n----------------------------------------------------------",
//				env.getProperty("spring.application.name")
                DuckyBeApplication.class.getName(), protocol, serverPort, contextPath,
                protocol, hostAddress,

                serverPort, contextPath, env.getActiveProfiles());
        System.out.println("" +
                "\n" +
                "         -`+yMMMMdMdNMdy.                                                                           \n" +
                "      `oNmNMd+MN:+Mmo``                                                                             \n" +
                "    `omMm-mM:-d/.dd-                                                                                \n" +
                "   .hmyMy -mohhNNMMNhhh/-`                                                                          \n" +
                "  ohMN//h+yNMMMNhhhhhhhhymo:                                                                        \n" +
                " :NysMh.yNMMMhys.````````..`                                                                        \n" +
                ".hMNs/:hMMhdMdNMmo.`               `-//                                       `...`                 \n" +
                ":MhmNhhMMhmNo.:+MMmdyo:-        `:+mNMm.  /yyyyyyyyyo.                        ommm+                 \n" +
                "-NMdo-sMydNo`  .MMMMMdNh     .-shMMMMMM+  sMMMdddMMMMm/                      `yMMM      .---`   .---\n" +
                "-NMmdsyN/Md    .mNMNh`dm+++ydmMMMMMMMMh  -NMMM` `-+MMMd:hdds  /ddd.`/oddddddo+MMMh-ydddsyMMMy  /mMMm\n" +
                ":NMy+-yN/MN+    `-oo+sMMMMMMMMMMMMMMd+`  /MMMo     NMMMNMMMo `mMMN:dMMMNydMNhNMMMdNMMd+`.mMMm.sNMMd-\n" +
                "/mMmmhysoMMNy-````dMMMMMMMMNNNNNdds/`    NMMM:   .oMMMhdMMM  -MMMshMMNs- `/:.MMMMMMN+`   dMMMmMMMh` \n" +
                "NNmyyydmoomMMNddddmMNNNMMMMdhh+:`       /MMMh-..+mMMMm+MMMh``hMMM+dMMd``./+`sMMMMMMN-    +MMMMMNy`  \n" +
                "::`       `+yNMMNys+:::ssyNNMMMNo.      hMMMMMMMMMMm+`:NMMMhmMMMh hMMMmhMMMhmMMN+mMMMo`  `MMMMM+    \n" +
                "             :my`         ``/ohNMd     .shhhhhhhhs/`   +hdNhhhhh: `ohhNNhh/ohhhs -hhhyo``:MMMm+     \n" +
                "            `hm`               .:/                       `.           ..              smmMMMh-      \n" +
                "           `ym:                                                                      .hNNNd:        \n" +
                "           oMMMy       ");
//        serverStarted();
    }

//    private static void serverStarted() {
//        AciiArt asciiArt = new AciiArt();
//
//        String text = "HACKING";
//
//        AciiArt.Settings settings = asciiArt.new Settings(new Font("Roboto", Font.TYPE1_FONT, 10), text.length() * 15, 15); // 30 pixel width per character
//
//        asciiArt.drawString(text, "#", settings);
//    }
}
