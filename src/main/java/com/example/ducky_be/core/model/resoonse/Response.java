package com.example.ducky_be.core.model.resoonse;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.io.Serializable;
import java.time.Instant;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
public class Response<T> implements Serializable {

    private boolean success;
    private int code;
    private String message;
    protected T data;
    private long timestamp;

    public Response() {
        timestamp = Instant.now().toEpochMilli();
        success = true;
        code = 200;
    }

    public Response<T> data(T res) {
        data = res;
        return this;
    }

    public static <T> Response<T> of(T res) {
        Response<T> response = new Response<>();
        response.data = res;
        response.success();
        return response;
    }

    public static <T> Response<T> ok() {
        Response<T> response = new Response<>();
        response.success();
        return response;
    }

    public Response<T> success() {
        success = true;
        code = 200;
        return this;
    }

    public Response<T> success(String message) {
        success = true;
        this.message = message;
        code = 200;
        return this;
    }
}
