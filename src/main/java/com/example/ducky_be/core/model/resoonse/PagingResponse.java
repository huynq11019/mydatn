package com.example.ducky_be.core.model.resoonse;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PagingResponse<T> extends Response<List<T>> {

    private int pageIndex;
    private int pageSize;
    private long total;
}
