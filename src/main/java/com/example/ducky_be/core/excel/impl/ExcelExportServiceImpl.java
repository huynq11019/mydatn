package com.example.ducky_be.core.excel.impl;

import com.example.ducky_be.core.ApplicationProperties;
import com.example.ducky_be.core.excel.ExcelExportService;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.*;

@Log4j2
@Service
public class ExcelExportServiceImpl implements ExcelExportService {
//    @Autowired
//    private ApplicationProperties applicationProperties;

//    @Override
//    public InputStreamResource download(String filename) {
//        try {
//            File file = ResourceUtils.getFile(applicationProperties.getFolderUpload() + "/" + filename);
//            try {
//                byte[] data = FileUtils.readFileToByteArray(file);
//                InputStream inputStream = new BufferedInputStream(new ByteArrayInputStream(data));
//                InputStreamResource inputStreamResource = new InputStreamResource(inputStream);
//                return inputStreamResource;
//            } catch (IOException e) {
//                log.error("Error: ", e);
//            }
//        } catch (FileNotFoundException e) {
//            log.error("Error: ", e);
//        }
//        return null;
//    }
//
//    @Override
//    public InputStream downloadExcelTemplateFromResource(String filename) throws IOException {
//        return new ClassPathResource(applicationProperties.getTemplate() + "/" + filename).getInputStream();
//    }
}
