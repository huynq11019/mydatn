/*
 * HeaderUtil.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;

import com.example.ducky_be.core.constants.ApiConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * The Class HeaderUtil.
 */
public class HeaderUtil {

    /**
     * The Constant log.
     */
    private static final Logger log = LoggerFactory.getLogger(HeaderUtil.class);

    /**
     * <p>
     * createAlert.
     * </p>
     *
     * @param -       a {@link String} object.
     * @param message a {@link String} object.
     * @param param   a {@link String} object.
     * @return a {@link HttpHeaders} object.
     */
    public static HttpHeaders createAlert(String message, String param) {
        HttpHeaders headers = new HttpHeaders();

        headers.add(ApiConstants.HttpHeaders.X_ACTION_MESAGE, message);

        try {
            headers.add(ApiConstants.HttpHeaders.X_ACTION_PARAMS,
                    URLEncoder.encode(param, StandardCharsets.UTF_8.toString()));
        } catch (UnsupportedEncodingException e) {
            // StandardCharsets are supported by every Java implementation so this exception
            // will never happen
        }

        return headers;
    }

    /**
     * <p>
     * createEntityCreationAlert.
     * </p>
     *
     * @param applicationName   a {@link String} object.
     * @param enableTranslation a boolean.
     * @param entityName        a {@link String} object.
     * @param param             a {@link String} object.
     * @return a {@link HttpHeaders} object.
     */
    public static HttpHeaders createEntityCreationAlert(String applicationName, boolean enableTranslation,
                                                        String entityName, String param) {
        String message = enableTranslation ? applicationName + "." + entityName + ".created"
                : "A new " + entityName + " is created with identifier " + param;

        return createAlert(message, param);
    }

    /**
     * <p>
     * createEntityDeletionAlert.
     * </p>
     *
     * @param applicationName   a {@link String} object.
     * @param enableTranslation a boolean.
     * @param entityName        a {@link String} object.
     * @param param             a {@link String} object.
     * @return a {@link HttpHeaders} object.
     */
    public static HttpHeaders createEntityDeletionAlert(String applicationName, boolean enableTranslation,
                                                        String entityName, String param) {
        String message = enableTranslation ? applicationName + "." + entityName + ".deleted"
                : "A " + entityName + " is deleted with identifier " + param;

        return createAlert(message, param);
    }

    /**
     * <p>
     * createEntityUpdateAlert.
     * </p>
     *
     * @param applicationName   a {@link String} object.
     * @param enableTranslation a boolean.
     * @param entityName        a {@link String} object.
     * @param param             a {@link String} object.
     * @return a {@link HttpHeaders} object.
     */
    public static HttpHeaders createEntityUpdateAlert(String applicationName, boolean enableTranslation,
                                                      String entityName, String param) {
        String message = enableTranslation ? applicationName + "." + entityName + ".updated"
                : "A " + entityName + " is updated with identifier " + param;

        return createAlert(message, param);
    }

    /**
     * <p>
     * createFailureAlert.
     * </p>
     *
     * @param -                 a {@link String} object.
     * @param enableTranslation a boolean.
     * @param entityName        a {@link String} object.
     * @param errorKey          a {@link String} object.
     * @param defaultMessage    a {@link String} object.
     * @return a {@link HttpHeaders} object.
     */
    public static HttpHeaders createFailureAlert(boolean enableTranslation, String entityName, String errorKey,
                                                 String defaultMessage) {
        log.error("Entity processing failed, {}", defaultMessage);

        String message = enableTranslation ? errorKey : defaultMessage;

        HttpHeaders headers = new HttpHeaders();

        headers.add(ApiConstants.HttpHeaders.X_ACTION_MESAGE, message);
        headers.add(ApiConstants.HttpHeaders.X_ACTION_PARAMS, entityName);

        return headers;
    }

    private HeaderUtil() {
    }
}
