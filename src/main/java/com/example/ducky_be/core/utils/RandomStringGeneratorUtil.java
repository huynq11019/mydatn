/*
 * RandomStringGeneratorUtil.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * The Class RandomStringGeneratorUtil.
 */
public class RandomStringGeneratorUtil {

    /**
     * The Enum Mode.
     */
    public enum Mode {

        /**
         * The mode alpha lower case.
         */
        MODE_ALPHA_LOWER_CASE,

        /**
         * The mode alpha upper case.
         */
        MODE_ALPHA_UPPER_CASE,

        /**
         * The mode numeric.
         */
        MODE_NUMERIC,

        /**
         * The mode special character.
         */
        MODE_SPECIAL_CHARACTER
    }

    /**
     * The Constant ALPHA_LOWER_CASE.
     */
    private static final String ALPHA_LOWER_CASE = "abcdefghijklmnopqrstuvwxyz";

    /**
     * The Constant ALPHA_UPPER_CASE.
     */
    private static final String ALPHA_UPPER_CASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /**
     * The Constant DEFAULT_LENGTH.
     */
    private static final int DEFAULT_LENGTH = 8;

    /**
     * The Constant NUMERIC.
     */
    private static final String NUMERIC = "0123456789";

    /**
     * The otp store.
     */
    private static List<String> otpStore = new CopyOnWriteArrayList<>();

    /**
     * The Constant SPECIAL_CHARACTER.
     */
    private static final String SPECIAL_CHARACTER = "!@#$%^&*_=+-/";

    /**
     * Generate.
     *
     * @return the string
     */
    public static String generate() {
        return generate(DEFAULT_LENGTH);
    }

    /**
     * Generate.
     *
     * @param length the length
     * @return the string
     */
    public static String generate(int length) {
        return generate(length, new Mode[]{Mode.MODE_ALPHA_UPPER_CASE, Mode.MODE_ALPHA_LOWER_CASE, Mode.MODE_NUMERIC,
                Mode.MODE_SPECIAL_CHARACTER});
    }

    /**
     * Generate.
     *
     * @param length the length
     * @param modes  the modes
     * @return the string
     */
    public static String generate(int length, Mode[] modes) {
        StringBuilder characterGroup = new StringBuilder();

        StringBuilder sb = new StringBuilder();

        if (modes == null || modes.length == 0) {
            characterGroup.append(ALPHA_LOWER_CASE);
        } else {
            for (Mode mode : modes) {
                switch (mode) {
                    case MODE_ALPHA_UPPER_CASE:
                        characterGroup.append(ALPHA_UPPER_CASE);

                        break;

                    case MODE_ALPHA_LOWER_CASE:
                        characterGroup.append(ALPHA_LOWER_CASE);

                        break;
                    case MODE_NUMERIC:
                        characterGroup.append(NUMERIC);

                        break;
                    case MODE_SPECIAL_CHARACTER:
                        characterGroup.append(SPECIAL_CHARACTER);

                        break;
                    default:
                        characterGroup.append(ALPHA_UPPER_CASE);

                        break;
                }
            }
        }

        Random random = new SecureRandom();

        for (int i = 0; i < length; i++) {
            double index = random.nextDouble() * characterGroup.length();

            sb.append(characterGroup.charAt((int) index));
        }

        return sb.toString();
    }

    /**
     * Generate gift code.
     *
     * @param length          the length
     * @param includeNumber   the include number
     * @param includeAlphabet the include alphabet
     * @return the string
     */
    public static String generateGiftCode(int length, boolean includeNumber, boolean includeAlphabet) {
        List<Mode> modes = new ArrayList<>();

        if (includeNumber || !includeNumber && !includeAlphabet) {
            modes.add(Mode.MODE_NUMERIC);
        }

        if (includeAlphabet) {
            modes.add(Mode.MODE_ALPHA_UPPER_CASE);
        }

        return generate(length, modes.toArray(new Mode[0]));
    }

    /**
     * Generate numeric.
     *
     * @param length the length
     * @return the string
     */
    public static String generateNumeric(int length) {
        return generate(length, new Mode[]{Mode.MODE_NUMERIC});
    }

    /**
     * Generate otp.
     *
     * @param length the length
     * @return the string
     */
    public static String generateOtp(int length) {
        String otp = generateNumeric(length);

        if (otpStore.size() >= Math.pow(10, length)) {
            length++;
        }

        if (otpStore.contains(otp)) {
            otp = generateOtp(length);
        } else {
            otpStore.add(otp);
        }

        return otp;
    }

    /**
     * Removes the otp.
     *
     * @param verifyCode the verify code
     */
    public static void removeOtp(String verifyCode) {
        otpStore.remove(verifyCode);
    }
}
