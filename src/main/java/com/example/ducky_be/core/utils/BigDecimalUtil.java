/*
 * BigDecimalUtil.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * The Class BigDecimalUtil.
 */
public class BigDecimalUtil {

    /**
     * Adds the.
     *
     * @param x the x
     * @param y the y
     * @return the double
     */
    public static double add(double x, double y) {
        BigDecimal xBigDecimal = new BigDecimal(String.valueOf(x));
        BigDecimal yBigDecimal = new BigDecimal(String.valueOf(y));

        BigDecimal resultBigDecimal = xBigDecimal.add(yBigDecimal);

        return resultBigDecimal.doubleValue();
    }

    /**
     * Divide.
     *
     * @param x            the x
     * @param y            the y
     * @param scale        the scale
     * @param roundingMode the rounding mode
     * @return the double
     */
    public static double divide(double x, double y, int scale, RoundingMode roundingMode) {

        BigDecimal xBigDecimal = new BigDecimal(String.valueOf(x));
        BigDecimal yBigDecimal = new BigDecimal(String.valueOf(y));

        BigDecimal resultBigDecimal = xBigDecimal.divide(yBigDecimal, scale, roundingMode);

        return resultBigDecimal.doubleValue();
    }

    /**
     * Divide.
     *
     * @param x            the x
     * @param y            the y
     * @param scale        the scale
     * @param roundingMode the rounding mode
     * @return the double
     */
    public static double divide(int x, int y, int scale, RoundingMode roundingMode) {

        BigDecimal xBigDecimal = new BigDecimal(String.valueOf(x));
        BigDecimal yBigDecimal = new BigDecimal(String.valueOf(y));

        BigDecimal resultBigDecimal = xBigDecimal.divide(yBigDecimal, scale, roundingMode);

        return resultBigDecimal.doubleValue();
    }

    /**
     * Multiply.
     *
     * @param x the x
     * @param y the y
     * @return the double
     */
    public static double multiply(double x, double y) {
        BigDecimal xBigDecimal = new BigDecimal(String.valueOf(x));
        BigDecimal yBigDecimal = new BigDecimal(String.valueOf(y));

        BigDecimal resultBigDecimal = xBigDecimal.multiply(yBigDecimal);

        return resultBigDecimal.doubleValue();
    }

    /**
     * Scale.
     *
     * @param x            the x
     * @param scale        the scale
     * @param roundingMode the rounding mode
     * @return the double
     */
    public static double scale(double x, int scale, RoundingMode roundingMode) {
        BigDecimal xBigDecimal = new BigDecimal(String.valueOf(x));

        xBigDecimal.setScale(scale, roundingMode);

        return xBigDecimal.doubleValue();
    }

    /**
     * Subtract.
     *
     * @param x the x
     * @param y the y
     * @return the double
     */
    public static double subtract(double x, double y) {
        BigDecimal xBigDecimal = new BigDecimal(String.valueOf(x));
        BigDecimal yBigDecimal = new BigDecimal(String.valueOf(y));

        BigDecimal resultBigDecimal = xBigDecimal.subtract(yBigDecimal);

        return resultBigDecimal.doubleValue();
    }
}
