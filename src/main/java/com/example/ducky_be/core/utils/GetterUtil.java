/*
 * GetterUtil.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Date;

/**
 * The Class GetterUtil.
 */
public class GetterUtil {

    /**
     * The booleans.
     */
    public static String[] BOOLEANS = {"true", "t", "y", "on", "1"};

    /**
     * The Constant DEFAULT_BOOLEAN.
     */
    public static final boolean DEFAULT_BOOLEAN = false;

    /**
     * The Constant DEFAULT_BOOLEAN_VALUES.
     */
    public static final boolean[] DEFAULT_BOOLEAN_VALUES = new boolean[0];

    /**
     * The Constant DEFAULT_BOOLEANS.
     */
    public static final Boolean[] DEFAULT_BOOLEANS = new Boolean[0];

    /**
     * The Constant DEFAULT_DOUBLE.
     */
    public static final double DEFAULT_DOUBLE = 0.0;

    /**
     * The Constant DEFAULT_DOUBLE_VALUES.
     */
    public static final double[] DEFAULT_DOUBLE_VALUES = new double[0];

    /**
     * The Constant DEFAULT_DOUBLES.
     */
    public static final Double[] DEFAULT_DOUBLES = new Double[0];

    /**
     * The Constant DEFAULT_FLOAT.
     */
    public static final float DEFAULT_FLOAT = 0;

    /**
     * The Constant DEFAULT_FLOAT_VALUES.
     */
    public static final float[] DEFAULT_FLOAT_VALUES = new float[0];

    /**
     * The Constant DEFAULT_FLOATS.
     */
    public static final Float[] DEFAULT_FLOATS = new Float[0];

    /**
     * The Constant DEFAULT_INTEGER.
     */
    public static final int DEFAULT_INTEGER = 0;

    /**
     * The Constant DEFAULT_INTEGER_VALUES.
     */
    public static final int[] DEFAULT_INTEGER_VALUES = new int[0];

    /**
     * The Constant DEFAULT_INTEGERS.
     */
    public static final Integer[] DEFAULT_INTEGERS = new Integer[0];

    /**
     * The Constant DEFAULT_LONG.
     */
    public static final long DEFAULT_LONG = 0;

    /**
     * The Constant DEFAULT_LONG_VALUES.
     */
    public static final long[] DEFAULT_LONG_VALUES = new long[0];

    /**
     * The Constant DEFAULT_LONGS.
     */
    public static final Long[] DEFAULT_LONGS = new Long[0];

    /**
     * The Constant DEFAULT_PASSWORD.
     */
    public static final String DEFAULT_PASSWORD = "123456a@";

    /**
     * The Constant DEFAULT_SHORT.
     */
    public static final short DEFAULT_SHORT = 0;

    /**
     * The Constant DEFAULT_SHORT_VALUES.
     */
    public static final short[] DEFAULT_SHORT_VALUES = new short[0];

    /**
     * The Constant DEFAULT_SHORTS.
     */
    public static final Short[] DEFAULT_SHORTS = new Short[0];

    /**
     * The Constant DEFAULT_STRING.
     */
    public static final String DEFAULT_STRING = StringPool.BLANK;

    /**
     * Trim.
     *
     * @param value the value
     * @return the string
     */
    private static String _trim(String value) {
        if (value != null) {
            value = value.trim();

            StringBuilder sb = new StringBuilder();

            char[] charArray = value.toCharArray();

            for (int i = 0; i < charArray.length; i++) {
                if (Character.isDigit(charArray[i])
                        || charArray[i] == CharPool.DASH && i == 0
                        || charArray[i] == CharPool.PERIOD
                        || charArray[i] == CharPool.UPPER_CASE_E
                        || charArray[i] == CharPool.LOWER_CASE_E) {

                    sb.append(charArray[i]);
                }
            }

            value = sb.toString();
        }

        return value;
    }

    /**
     * Gets the.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return true, if successful
     */
    public static boolean get(String value, boolean defaultValue) {
        if (value != null) {
            try {
                value = value.trim();

                if (value.equalsIgnoreCase(BOOLEANS[0])
                        || value.equalsIgnoreCase(BOOLEANS[1])
                        || value.equalsIgnoreCase(BOOLEANS[2])
                        || value.equalsIgnoreCase(BOOLEANS[3])
                        || value.equalsIgnoreCase(BOOLEANS[4])) {

                    return true;
                }
                return false;
            } catch (Exception e) {
            }
        }

        return defaultValue;
    }

    /**
     * Gets the.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the boolean
     */
    public static Boolean get(String value, Boolean defaultValue) {
        try {
            return Boolean.valueOf(_trim(value));
        } catch (Exception e) {
        }

        return defaultValue;
    }

    /**
     * Gets the.
     *
     * @param value        the value
     * @param df           the df
     * @param defaultValue the default value
     * @return the date
     */
    public static Date get(String value, DateFormat df, Date defaultValue) {
        try {
            Date date = df.parse(value.trim());

            if (date != null) {
                return date;
            }
        } catch (Exception e) {
        }

        return defaultValue;
    }

    /**
     * Gets the.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the double
     */
    public static double get(String value, double defaultValue) {
        try {
            return Double.parseDouble(_trim(value));
        } catch (Exception e) {
        }

        return defaultValue;
    }

    /**
     * Gets the.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the double
     */
    public static Double get(String value, Double defaultValue) {
        try {
            return Double.valueOf(_trim(value));
        } catch (Exception e) {
        }

        return defaultValue;
    }

    /**
     * Gets the.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the float
     */
    public static float get(String value, float defaultValue) {
        try {
            return Float.parseFloat(_trim(value));
        } catch (Exception e) {
        }

        return defaultValue;
    }

    /**
     * Gets the.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the float
     */
    public static Float get(String value, Float defaultValue) {
        try {
            return Float.valueOf(_trim(value));
        } catch (Exception e) {
        }

        return defaultValue;
    }

    /**
     * Gets the.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the int
     */
    public static int get(String value, int defaultValue) {
        try {
            return Integer.parseInt(_trim(value));
        } catch (Exception e) {
        }

        return defaultValue;
    }

    /**
     * Gets the.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the integer
     */
    public static Integer get(String value, Integer defaultValue) {
        try {
            return Integer.valueOf(_trim(value));
        } catch (Exception e) {
        }

        return defaultValue;
    }

    /**
     * Gets the.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the long
     */
    public static long get(String value, long defaultValue) {
        try {
            return Long.parseLong(_trim(value));
        } catch (Exception e) {
        }

        return defaultValue;
    }

    /**
     * Gets the.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the long
     */
    public static Long get(String value, Long defaultValue) {
        try {
            return Long.valueOf(_trim(value));
        } catch (Exception e) {
        }

        return defaultValue;
    }

    /**
     * Gets the.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the short
     */
    public static short get(String value, short defaultValue) {
        try {
            return Short.parseShort(_trim(value));
        } catch (Exception e) {
        }

        return defaultValue;
    }

    /**
     * Gets the.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the short
     */
    public static Short get(String value, Short defaultValue) {
        try {
            return Short.valueOf(_trim(value));
        } catch (Exception e) {
        }

        return defaultValue;
    }

    /**
     * Gets the.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the string
     */
    public static String get(String value, String defaultValue) {
        if (value != null) {
            value = value.trim();
            value = StringUtil.replace(value, StringPool.RETURN_NEW_LINE, StringPool.NEW_LINE);

            return value;
        }

        return defaultValue;
    }

    /**
     * Gets the array.
     *
     * @param str   the str
     * @param regex the regex
     * @return the array
     */
    public static String[] getArray(String str, String regex) {
        if (str == null) {
            return new String[0];
        }

        return str.split(regex);
    }

    /**
     * Gets the boolean.
     *
     * @param value the value
     * @return the boolean
     */
    public static Boolean getBoolean(Object value) {
        if (value == null) {
            return null;
        }

        return getBoolean(value.toString());

    }

    /**
     * Gets the boolean.
     *
     * @param value the value
     * @return the boolean
     */
    public static Boolean getBoolean(String value) {
        return getBoolean(value, null);
    }

    /**
     * Gets the boolean.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the boolean
     */
    public static Boolean getBoolean(String value, Boolean defaultValue) {
        return get(value, defaultValue);
    }

    /**
     * Gets the booleans.
     *
     * @param values the values
     * @return the booleans
     */
    public static Boolean[] getBooleans(String[] values) {
        return getBooleans(values, DEFAULT_BOOLEANS);
    }

    /**
     * Gets the booleans.
     *
     * @param values       the values
     * @param defaultValue the default value
     * @return the booleans
     */
    public static Boolean[] getBooleans(String[] values, Boolean[] defaultValue) {

        if (values == null) {
            return defaultValue;
        }

        Boolean[] booleanValues = new Boolean[values.length];

        for (int i = 0; i < values.length; i++) {
            booleanValues[i] = getBoolean(values[i]);
        }

        return booleanValues;
    }

    public static boolean getBooleanValue(Object value) {
        return getBooleanValue(value, DEFAULT_BOOLEAN);
    }

    /**
     * Gets the boolean value.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the boolean value
     */
    public static boolean getBooleanValue(Object value, boolean defaultValue) {
        if (value == null) {
            return defaultValue;
        }

        return get(value.toString(), defaultValue);
    }

    /**
     * Gets the boolean value.
     *
     * @param value the value
     * @return the boolean value
     */
    public static boolean getBooleanValue(String value) {
        return getBooleanValue(value, DEFAULT_BOOLEAN);
    }

    /**
     * Gets the boolean value.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the boolean value
     */
    public static boolean getBooleanValue(String value, boolean defaultValue) {
        return get(value, defaultValue);
    }

    /**
     * Gets the boolean values.
     *
     * @param values the values
     * @return the boolean values
     */
    public static boolean[] getBooleanValues(String[] values) {
        return getBooleanValues(values, DEFAULT_BOOLEAN_VALUES);
    }

    /**
     * Gets the boolean values.
     *
     * @param values       the values
     * @param defaultValue the default value
     * @return the boolean values
     */
    public static boolean[] getBooleanValues(String[] values, boolean[] defaultValue) {

        if (values == null) {
            return defaultValue;
        }

        boolean[] booleanValues = new boolean[values.length];

        for (int i = 0; i < values.length; i++) {
            booleanValues[i] = getBoolean(values[i]);
        }

        return booleanValues;
    }

    /**
     * Gets the data iso.
     *
     * @param value the value
     * @return the data iso
     */
    public static Date getDataIso(String value) {
        return getDate(value, DateUtils.getISOFormat(value));
    }

    /**
     * Gets the date.
     *
     * @param date    the date
     * @param pattern the pattern
     * @return the date
     */
    public static String getDate(Date date, String pattern) {
        if (date == null) {
            return null;
        }

        return DateUtils.getDate(date, pattern);
    }

    /**
     * Gets the date.
     *
     * @param value the value
     * @param df    the df
     * @return the date
     */
    public static Date getDate(String value, DateFormat df) {
        return getDate(value, df, new Date());
    }

    /**
     * Gets the date.
     *
     * @param value        the value
     * @param df           the df
     * @param defaultValue the default value
     * @return the date
     */
    public static Date getDate(String value, DateFormat df, Date defaultValue) {
        return get(value, df, defaultValue);
    }

    /**
     * Gets the date.
     *
     * @param value   the value
     * @param pattern the pattern
     * @return the date
     */
    public static Date getDate(String value, String pattern) {
        DateFormat df = DateFormatFactoryUtil.getSimpleDateFormat(pattern);

        return getDate(value, df);
    }

    /**
     * Gets the double.
     *
     * @param value the value
     * @return the double
     */
    public static Double getDouble(Object value) {
        if (value == null) {
            return null;
        }

        return getDouble(value.toString());

    }

    /**
     * Gets the double.
     *
     * @param value the value
     * @return the double
     */
    public static Double getDouble(String value) {
        return getDouble(value, null);
    }

    /**
     * Gets the double.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the double
     */
    public static Double getDouble(String value, Double defaultValue) {
        return get(value, defaultValue);
    }

    /**
     * Gets the doubles.
     *
     * @param values the values
     * @return the doubles
     */
    public static Double[] getDoubles(String[] values) {
        return getDoubles(values, DEFAULT_DOUBLES);
    }

    /**
     * Gets the doubles.
     *
     * @param values       the values
     * @param defaultValue the default value
     * @return the doubles
     */
    public static Double[] getDoubles(String[] values, Double[] defaultValue) {

        if (values == null) {
            return defaultValue;
        }

        Double[] doubleValues = new Double[values.length];

        for (int i = 0; i < values.length; i++) {
            doubleValues[i] = getDouble(values[i]);
        }

        return doubleValues;
    }

    /**
     * Gets the integer value.
     *
     * @param value the value
     * @return the integer value
     */
    public static double getDoubleValue(Object value) {
        return getDoubleValue(value, DEFAULT_DOUBLE);
    }

    public static double getDoubleValue(Object value, double defaultValue) {
        if (value == null) {
            return defaultValue;
        }

        return getDoubleValue(getString(value), defaultValue);
    }

    /**
     * Gets the double value.
     *
     * @param value the value
     * @return the double value
     */
    public static double getDoubleValue(String value) {
        return getDoubleValue(value, DEFAULT_DOUBLE);
    }

    /**
     * Gets the double value.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the double value
     */
    public static double getDoubleValue(String value, double defaultValue) {
        return get(value, defaultValue);
    }

    /**
     * Gets the double values.
     *
     * @param values the values
     * @return the double values
     */
    public static double[] getDoubleValues(String[] values) {
        return getDoubleValues(values, DEFAULT_DOUBLE_VALUES);
    }

    /**
     * Gets the double values.
     *
     * @param values       the values
     * @param defaultValue the default value
     * @return the double values
     */
    public static double[] getDoubleValues(String[] values, double[] defaultValue) {

        if (values == null) {
            return defaultValue;
        }

        double[] doubleValues = new double[values.length];

        for (int i = 0; i < values.length; i++) {
            doubleValues[i] = getDouble(values[i]);
        }

        return doubleValues;
    }

    /**
     * Gets the float.
     *
     * @param value the value
     * @return the float
     */
    public static Float getFloat(Object value) {
        if (value == null) {
            return null;
        }

        return getFloat(value.toString());

    }

    /**
     * Gets the float.
     *
     * @param value the value
     * @return the float
     */
    public static Float getFloat(String value) {
        return getFloat(value, null);
    }

    /**
     * Gets the float.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the float
     */
    public static Float getFloat(String value, Float defaultValue) {
        return get(value, defaultValue);
    }

    /**
     * Gets the floats.
     *
     * @param values the values
     * @return the floats
     */
    public static Float[] getFloats(String[] values) {
        return getFloats(values, DEFAULT_FLOATS);
    }

    /**
     * Gets the floats.
     *
     * @param values       the values
     * @param defaultValue the default value
     * @return the floats
     */
    public static Float[] getFloats(String[] values, Float[] defaultValue) {

        if (values == null) {
            return defaultValue;
        }

        Float[] floatValues = new Float[values.length];

        for (int i = 0; i < values.length; i++) {
            floatValues[i] = getFloat(values[i]);
        }

        return floatValues;
    }

    /**
     * Gets the integer value.
     *
     * @param value the value
     * @return the integer value
     */
    public static float getFloatValue(Object value) {
        return getFloatValue(value, DEFAULT_FLOAT);
    }

    public static float getFloatValue(Object value, float defaultValue) {
        if (value == null) {
            return defaultValue;
        }

        return getFloatValue(getString(value), defaultValue);
    }

    /**
     * Gets the float value.
     *
     * @param value the value
     * @return the float value
     */
    public static float getFloatValue(String value) {
        return getFloatValue(value, DEFAULT_FLOAT);
    }

    /**
     * Gets the float value.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the float value
     */
    public static float getFloatValue(String value, float defaultValue) {
        return get(value, defaultValue);
    }

    /**
     * Gets the float values.
     *
     * @param values the values
     * @return the float values
     */
    public static float[] getFloatValues(String[] values) {
        return getFloatValues(values, DEFAULT_FLOAT_VALUES);
    }

    /**
     * Gets the float values.
     *
     * @param values       the values
     * @param defaultValue the default value
     * @return the float values
     */
    public static float[] getFloatValues(String[] values, float[] defaultValue) {

        if (values == null) {
            return defaultValue;
        }

        float[] floatValues = new float[values.length];

        for (int i = 0; i < values.length; i++) {
            floatValues[i] = getFloat(values[i]);
        }

        return floatValues;
    }

    /**
     * Gets the format.
     *
     * @param value the value
     * @return the format
     */
    public static String getFormat(Long value) {
        if (value == null) {
            return StringPool.BLANK;
        }

        return NumberFormat.getInstance().format(value);
    }

    /**
     * Gets the integer.
     *
     * @param value the value
     * @return the integer
     */
    public static Integer getInteger(Object value) {
        if (value == null) {
            return null;
        }

        return getInteger(value.toString());

    }

    /**
     * Gets the integer.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the integer
     */
    public static Integer getInteger(Object value, Integer defaultValue) {
        if (value == null) {
            return defaultValue;
        }

        return getInteger(value.toString());

    }

    /**
     * Gets the integer.
     *
     * @param value the value
     * @return the integer
     */
    public static Integer getInteger(String value) {
        return getInteger(value, null);
    }

    /**
     * Gets the integer.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the integer
     */
    public static Integer getInteger(String value, Integer defaultValue) {
        return get(value, defaultValue);
    }

    /**
     * Gets the integers.
     *
     * @param values the values
     * @return the integers
     */
    public static Integer[] getIntegers(String[] values) {
        return getIntegers(values, DEFAULT_INTEGERS);
    }

    /**
     * Gets the integers.
     *
     * @param values       the values
     * @param defaultValue the default value
     * @return the integers
     */
    public static Integer[] getIntegers(String[] values, Integer[] defaultValue) {
        if (values == null) {
            return defaultValue;
        }

        Integer[] intValues = new Integer[values.length];

        for (int i = 0; i < values.length; i++) {
            intValues[i] = getInteger(values[i]);
        }

        return intValues;
    }

    /**
     * Gets the integer value.
     *
     * @param value the value
     * @return the integer value
     */
    public static int getIntegerValue(Object value) {
        return getIntegerValue(value, DEFAULT_INTEGER);
    }

    /**
     * Gets the integer value.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the integer value
     */
    public static int getIntegerValue(Object value, int defaultValue) {
        if (value == null) {
            return defaultValue;
        }

        return getIntegerValue(getString(value), defaultValue);
    }

    /**
     * Gets the integer value.
     *
     * @param value the value
     * @return the integer value
     */
    public static int getIntegerValue(String value) {
        return getIntegerValue(value, DEFAULT_INTEGER);
    }

    /**
     * Gets the integer value.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the integer value
     */
    public static int getIntegerValue(String value, int defaultValue) {
        return get(value, defaultValue);
    }

    /**
     * Gets the integer values.
     *
     * @param values the values
     * @return the integer values
     */
    public static int[] getIntegerValues(String[] values) {
        return getIntegerValues(values, DEFAULT_INTEGER_VALUES);
    }

    /**
     * Gets the integer values.
     *
     * @param values       the values
     * @param defaultValue the default value
     * @return the integer values
     */
    public static int[] getIntegerValues(String[] values, int[] defaultValue) {
        if (values == null) {
            return defaultValue;
        }

        int[] intValues = new int[values.length];

        for (int i = 0; i < values.length; i++) {
            intValues[i] = getInteger(values[i]);
        }

        return intValues;
    }

    /**
     * Gets the long.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the long
     */
    // Hung add
    public static long getLong(Long value, long defaultValue) {
        if (value == null) {
            return -1;
        }

        return value.longValue();
    }

    /**
     * Gets the long.
     *
     * @param value the value
     * @return the long
     */
    public static Long getLong(Object value) {
        if (value == null) {
            return null;
        }

        return getLong(value.toString());

    }

    /**
     * Gets the long.
     *
     * @param value       the value
     * @param defaultLong the default long
     * @return the long
     */
    public static Long getLong(Object value, Long defaultLong) {
        if (value == null) {
            return defaultLong;
        }

        return getLong(value.toString());

    }

    /**
     * Gets the long.
     *
     * @param value the value
     * @return the long
     */
    public static Long getLong(String value) {
        return getLong(value, null);
    }

    /**
     * Gets the long.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the long
     */
    public static Long getLong(String value, Long defaultValue) {
        return get(value, defaultValue);
    }

    /**
     * Gets the longs.
     *
     * @param values the values
     * @return the longs
     */
    public static Long[] getLongs(String[] values) {
        return getLongs(values, DEFAULT_LONGS);
    }

    /**
     * Gets the longs.
     *
     * @param values       the values
     * @param defaultValue the default value
     * @return the longs
     */
    public static Long[] getLongs(String[] values, Long[] defaultValue) {
        if (values == null) {
            return defaultValue;
        }

        Long[] longValues = new Long[values.length];

        for (int i = 0; i < values.length; i++) {
            longValues[i] = getLong(values[i]);
        }

        return longValues;
    }

    /**
     * Gets the long value.
     *
     * @param value the value
     * @return the long value
     */
    public static long getLongValue(Object value) {
        return getLongValue(value, DEFAULT_LONG);
    }

    /**
     * Gets the long value.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the long value
     */
    public static long getLongValue(Object value, long defaultValue) {
        if (value == null) {
            return defaultValue;
        }

        return getLongValue(getString(value), defaultValue);
    }

    /**
     * Gets the long value.
     *
     * @param value the value
     * @return the long value
     */
    public static long getLongValue(String value) {
        return getLongValue(value, DEFAULT_LONG);
    }

    /**
     * Gets the long value.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the long value
     */
    public static long getLongValue(String value, long defaultValue) {
        return get(value, defaultValue);
    }

    /**
     * Gets the long values.
     *
     * @param values the values
     * @return the long values
     */
    public static long[] getLongValues(String[] values) {
        return getLongValues(values, DEFAULT_LONG_VALUES);
    }

    /**
     * Gets the long values.
     *
     * @param values       the values
     * @param defaultValue the default value
     * @return the long values
     */
    public static long[] getLongValues(String[] values, long[] defaultValue) {
        if (values == null) {
            return defaultValue;
        }

        long[] longValues = new long[values.length];

        for (int i = 0; i < values.length; i++) {
            longValues[i] = getLong(values[i]);
        }

        return longValues;
    }

    /**
     * Gets the short.
     *
     * @param value the value
     * @return the short
     */
    public static Short getShort(Object value) {
        if (value == null) {
            return null;
        }

        return getShort(value.toString());

    }

    /**
     * Gets the short.
     *
     * @param value the value
     * @return the short
     */
    public static Short getShort(String value) {
        return getShort(value, DEFAULT_SHORT);
    }

    /**
     * Gets the short.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the short
     */
    public static Short getShort(String value, Short defaultValue) {
        return get(value, defaultValue);
    }

    /**
     * Gets the shorts.
     *
     * @param values the values
     * @return the shorts
     */
    public static Short[] getShorts(String[] values) {
        return getShorts(values, DEFAULT_SHORTS);
    }

    /**
     * Gets the shorts.
     *
     * @param values       the values
     * @param defaultValue the default value
     * @return the shorts
     */
    public static Short[] getShorts(String[] values, Short[] defaultValue) {

        if (values == null) {
            return defaultValue;
        }

        Short[] shortValues = new Short[values.length];

        for (int i = 0; i < values.length; i++) {
            shortValues[i] = getShort(values[i]);
        }

        return shortValues;
    }

    /**
     * Gets the integer value.
     *
     * @param value the value
     * @return the integer value
     */
    public static short getShortValue(Object value) {
        return getShortValue(value, DEFAULT_SHORT);
    }

    public static short getShortValue(Object value, short defaultValue) {
        if (value == null) {
            return defaultValue;
        }

        return getShortValue(getString(value), defaultValue);
    }

    /**
     * Gets the short value.
     *
     * @param value the value
     * @return the short value
     */
    public static short getShortValue(String value) {
        return getShortValue(value, DEFAULT_SHORT);
    }

    /**
     * Gets the short value.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the short value
     */
    public static short getShortValue(String value, short defaultValue) {
        return get(value, defaultValue);
    }

    /**
     * Gets the short values.
     *
     * @param values the values
     * @return the short values
     */
    public static short[] getShortValues(String[] values) {
        return getShortValues(values, DEFAULT_SHORT_VALUES);
    }

    /**
     * Gets the short values.
     *
     * @param values       the values
     * @param defaultValue the default value
     * @return the short values
     */
    public static short[] getShortValues(String[] values, short[] defaultValue) {

        if (values == null) {
            return defaultValue;
        }

        short[] shortValues = new short[values.length];

        for (int i = 0; i < values.length; i++) {
            shortValues[i] = getShort(values[i]);
        }

        return shortValues;
    }

    /**
     * Gets the status color name.
     *
     * @param status           the status
     * @param statusColorArray the status color array
     * @return the status color name
     */
    public static String getStatusColorName(Long status, String[] statusColorArray) {
        String statusName = StringPool.BLANK;

        if (status != null && status > 0 && status < statusColorArray.length) {
            statusName = "color:#" + statusColorArray[status.intValue()];
        }

        return statusName;
    }

    /**
     * Gets the status name.
     *
     * @param status      the status
     * @param statusArray the status array
     * @return the status name
     */
    public static String getStatusName(Long status, String[] statusArray) {
        String statusName = StringPool.BLANK;

        if (status != null && status < statusArray.length) {
            statusName = statusArray[status.intValue()];
        }

        return statusName;
    }

    /**
     * Gets the string.
     *
     * @param value the value
     * @return the string
     */
    public static String getString(Object value) {
        if (value == null) {
            return null;
        }

        return getString(value.toString());
    }

    /**
     * Gets the string.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the string
     */
    public static String getString(Object value, String defaultValue) {
        if (value == null) {
            return StringPool.BLANK;
        }

        return value.toString();

    }

    /**
     * Gets the string.
     *
     * @param value the value
     * @return the string
     */
    public static String getString(String value) {
        return getString(value, DEFAULT_STRING);
    }

    /**
     * Gets the string.
     *
     * @param value        the value
     * @param defaultValue the default value
     * @return the string
     */
    public static String getString(String value, String defaultValue) {
        return get(value, defaultValue);
    }

    /**
     * Gets the string lower.
     *
     * @param value the value
     * @return the string lower
     */
    public static String getStringLower(String value) {
        if (value == null) {
            return null;
        }

        return getString(value).toLowerCase();
    }

    /**
     * Gets the string upper.
     *
     * @param value the value
     * @return the string upper
     */
    public static String getStringUpper(String value) {
        if (value == null) {
            return null;
        }

        return getString(value).toUpperCase();
    }

    /**
     * Gets the unsigned double value.
     *
     * @param value the value
     * @return the unsigned double value
     */
    public static double getUnsignedDoubleValue(double value) {
        if (value < 0) {
            return 0;
        }

        return value;
    }

    /**
     * Gets the unsigned integer value.
     *
     * @param value
     *            the value
     * @return the unsigned integer value
     */
    /**
     * @param value
     * @return
     */
    public static int getUnsignedIntegerValue(int value) {
        if (value < 0) {
            return 0;
        }

        return value;
    }

    /**
     * Gets the unsigned long value.
     *
     * @param value the value
     * @return the unsigned long value
     */
    public static long getUnsignedLongValue(long value) {
        if (value < 0) {
            return 0;
        }

        return value;
    }
}
