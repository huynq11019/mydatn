/*
 * MakerStats.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Class MakerStats.
 */
public class MakerStats {

    /**
     * The Class SizeSample.
     */
    private static class SizeSample implements Comparable<SizeSample> {

        /**
         * The caller.
         */
        private String _caller;

        /**
         * The init size.
         */
        private int _initSize;

        /**
         * The max size.
         */
        private int _maxSize;

        /**
         * The min size.
         */
        private int _minSize;

        /**
         * The samples size.
         */
        private int _samplesSize;

        /**
         * The total size.
         */
        private int _totalSize;

        /**
         * Instantiates a new size sample.
         *
         * @param caller   the caller
         * @param initSize the init size
         */
        public SizeSample(String caller, int initSize) {
            this._caller = caller;
            this._initSize = initSize;
            this._minSize = Integer.MAX_VALUE;
            this._maxSize = Integer.MIN_VALUE;
        }

        /**
         * Adds the.
         *
         * @param finalSize the final size
         */
        public void add(int finalSize) {
            if (finalSize < this._minSize) {
                this._minSize = finalSize;
            }

            if (finalSize > this._maxSize) {
                this._maxSize = finalSize;
            }

            this._samplesSize++;
            this._totalSize += finalSize;
        }

        /*
         * (non-Javadoc)
         * @see java.lang.Comparable#compareTo(java.lang.Object)
         */
        @Override
        public int compareTo(SizeSample other) {
            int thisAvg = 0;

            if (this._samplesSize > 0) {
                thisAvg = this._totalSize / this._samplesSize;
            }

            int otherAvg = 0;

            if (other.getSamplesSize() > 0) {
                otherAvg = other.getTotalSize() / other.getSamplesSize();
            }

            return otherAvg - thisAvg;
        }

        /**
         * Gets the caller.
         *
         * @return the caller
         */
        public String getCaller() {
            return this._caller;
        }

        /**
         * Gets the inits the size.
         *
         * @return the inits the size
         */
        public int getInitSize() {
            return this._initSize;
        }

        /**
         * Gets the max size.
         *
         * @return the max size
         */
        public int getMaxSize() {
            return this._maxSize;
        }

        /**
         * Gets the min size.
         *
         * @return the min size
         */
        public int getMinSize() {
            return this._minSize;
        }

        /**
         * Gets the samples size.
         *
         * @return the samples size
         */
        public int getSamplesSize() {
            return this._samplesSize;
        }

        /**
         * Gets the total size.
         *
         * @return the total size
         */
        public int getTotalSize() {
            return this._totalSize;
        }

    }

    /** The map. */
    // private int _count;

    /**
     * The map.
     */
    private Map<String, SizeSample> _map = new HashMap<>();

    /**
     * Instantiates a new maker stats.
     *
     * @param name the name
     */
    public MakerStats(String name) {
        // _name = name;
    }

    /**
     * Adds the.
     *
     * @param caller    the caller
     * @param initSize  the init size
     * @param finalSize the final size
     */
    public void add(String caller, int initSize, int finalSize) {
        SizeSample stat = null;

        synchronized (this._map) {
            stat = this._map.get(caller);

            if (stat == null) {
                stat = new SizeSample(caller, initSize);

                this._map.put(caller, stat);
            }

            // this._count++;
        }

        synchronized (stat) {
            stat.add(finalSize);
        }
    }

    /**
     * Display.
     *
     * @param printer the printer
     */
    public void display(PrintStream printer) {
        printer.println("caller,min,max,range,samples,average,initial");

        List<SizeSample> list = new ArrayList<>(this._map.size());

        list.addAll(this._map.values());

        list = ListUtil.sort(list);

        int maxSize = 0;
        int sampleSize = 0;
        int totalSize = 0;

        for (SizeSample stat : list) {
            printer.print(stat.getCaller());
            printer.print(",");
            printer.print(stat.getMinSize());
            printer.print(",");
            printer.print(stat.getMaxSize());
            printer.print(",");
            printer.print(stat.getMaxSize() - stat.getMinSize());
            printer.print(",");
            printer.print(stat.getSamplesSize());
            printer.print(",");
            printer.print(stat.getTotalSize() / stat.getSamplesSize());
            printer.print(",");
            printer.println(stat.getInitSize());

            sampleSize += stat.getSamplesSize();
            totalSize += stat.getTotalSize();

            if (stat.getMaxSize() > maxSize) {
                maxSize = stat.getMaxSize();
            }
        }

        int avg = 0;

        if (sampleSize > 0) {
            avg = totalSize / sampleSize;
        }

        printer.print("SAMPLES=");
        printer.print(sampleSize);
        printer.print(", AVERAGE=");
        printer.print(avg);
        printer.print(", MAX=");
        printer.println(maxSize);
    }
}
