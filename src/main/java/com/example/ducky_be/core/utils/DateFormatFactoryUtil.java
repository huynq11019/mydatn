/*
 * DateFormatFactoryUtil.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

/**
 * The Class DateFormatFactoryUtil.
 */
public class DateFormatFactoryUtil {

    /**
     * The locale.
     */
    private static Locale _locale = new Locale("vi", "VN");

    /**
     * Gets the date.
     *
     * @param locale the locale
     * @return the date
     */
    public static DateFormat getDate(Locale locale) {
        return getDate(locale, null);
    }

    /**
     * Gets the date.
     *
     * @param locale   the locale
     * @param timeZone the time zone
     * @return the date
     */
    public static DateFormat getDate(Locale locale, TimeZone timeZone) {
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.SHORT, locale);

        if (timeZone != null) {
            dateFormat.setTimeZone(timeZone);
        }

        return dateFormat;
    }

    /**
     * Gets the date.
     *
     * @param timeZone the time zone
     * @return the date
     */
    public static DateFormat getDate(TimeZone timeZone) {
        return getDate(_locale, timeZone);
    }

    /**
     * Gets the date time.
     *
     * @param locale the locale
     * @return the date time
     */
    public static DateFormat getDateTime(Locale locale) {
        return getDateTime(locale, null);
    }

    /**
     * Gets the date time.
     *
     * @param locale   the locale
     * @param timeZone the time zone
     * @return the date time
     */
    public static DateFormat getDateTime(Locale locale, TimeZone timeZone) {
        DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, locale);

        if (timeZone != null) {
            dateFormat.setTimeZone(timeZone);
        }

        return dateFormat;
    }

    /**
     * Gets the date time.
     *
     * @param timeZone the time zone
     * @return the date time
     */
    public static DateFormat getDateTime(TimeZone timeZone) {
        return getDateTime(_locale, timeZone);
    }

    /**
     * Gets the simple date format.
     *
     * @param pattern the pattern
     * @return the simple date format
     */
    public static DateFormat getSimpleDateFormat(String pattern) {
        return getSimpleDateFormat(pattern, _locale, null);
    }

    /**
     * Gets the simple date format.
     *
     * @param pattern the pattern
     * @param locale  the locale
     * @return the simple date format
     */
    public static DateFormat getSimpleDateFormat(String pattern, Locale locale) {
        return getSimpleDateFormat(pattern, locale, null);
    }

    /**
     * Gets the simple date format.
     *
     * @param pattern  the pattern
     * @param locale   the locale
     * @param timeZone the time zone
     * @return the simple date format
     */
    public static DateFormat getSimpleDateFormat(String pattern, Locale locale, TimeZone timeZone) {

        DateFormat dateFormat = new SimpleDateFormat(pattern, locale);

        if (timeZone != null) {
            dateFormat.setTimeZone(timeZone);
        }

        return dateFormat;
    }

    /**
     * Gets the simple date format.
     *
     * @param pattern  the pattern
     * @param timeZone the time zone
     * @return the simple date format
     */
    public static DateFormat getSimpleDateFormat(String pattern, TimeZone timeZone) {
        return getSimpleDateFormat(pattern, _locale, timeZone);
    }

    /**
     * Gets the time.
     *
     * @param locale the locale
     * @return the time
     */
    public static DateFormat getTime(Locale locale) {
        return getTime(locale, null);
    }

    /**
     * Gets the time.
     *
     * @param locale   the locale
     * @param timeZone the time zone
     * @return the time
     */
    public static DateFormat getTime(Locale locale, TimeZone timeZone) {
        DateFormat dateFormat = DateFormat.getTimeInstance(DateFormat.SHORT, locale);

        if (timeZone != null) {
            dateFormat.setTimeZone(timeZone);
        }

        return dateFormat;
    }

    /**
     * Gets the time.
     *
     * @param timeZone the time zone
     * @return the time
     */
    public static DateFormat getTime(TimeZone timeZone) {
        return getTime(_locale, timeZone);
    }
}
