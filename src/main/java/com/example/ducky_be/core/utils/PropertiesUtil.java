/*
 * PropertiesUtil.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.*;

/**
 * The Class PropertiesUtil.
 */
public class PropertiesUtil {

    /**
     * Copy properties.
     *
     * @param from the from
     * @param to   the to
     */
    @SuppressWarnings("rawtypes")
    public static void copyProperties(Properties from, Properties to) {
        Iterator<?> itr = from.entrySet().iterator();

        while (itr.hasNext()) {
            Map.Entry entry = (Map.Entry) itr.next();

            to.setProperty((String) entry.getKey(), (String) entry.getValue());
        }
    }

    /**
     * From map.
     *
     * @param map the map
     * @return the properties
     */
    public static Properties fromMap(Map<?, ?> map) {
        if (map instanceof Properties) {
            return (Properties) map;
        }

        Properties p = new Properties();

        Iterator<?> itr = map.entrySet().iterator();

        while (itr.hasNext()) {
            @SuppressWarnings("rawtypes")
            Map.Entry entry = (Map.Entry) itr.next();

            String key = (String) entry.getKey();
            String value = (String) entry.getValue();

            if (value != null) {
                p.setProperty(key, value);
            }
        }

        return p;
    }

    /**
     * From properties.
     *
     * @param p   the p
     * @param map the map
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static void fromProperties(Properties p, Map map) {
        map.clear();

        Iterator itr = p.entrySet().iterator();

        while (itr.hasNext()) {
            Map.Entry entry = (Map.Entry) itr.next();

            map.put(entry.getKey(), entry.getValue());
        }
    }

    /**
     * List.
     *
     * @param map the map
     * @param out the out
     */
    @SuppressWarnings("rawtypes")
    public static void list(Map map, PrintWriter out) {
        Properties props = fromMap(map);

        props.list(out);
    }

    /**
     * List.
     *
     * @param map the map
     * @return the string
     */
    public static String list(Map<?, ?> map) {
        Properties props = fromMap(map);

        ByteArrayMaker bam = new ByteArrayMaker();
        PrintStream ps = new PrintStream(bam);

        props.list(ps);

        return bam.toString();
    }

    /**
     * List.
     *
     * @param map the map
     * @param out the out
     */
    public static void list(Map<?, ?> map, PrintStream out) {
        Properties props = fromMap(map);

        props.list(out);
    }

    /**
     * Load.
     *
     * @param p the p
     * @param s the s
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @SuppressWarnings("rawtypes")
    public static void load(Properties p, String s) throws IOException {
        if (Validator.isNotNull(s)) {
            s = UnicodeFormatter.toString(s);

            s = StringUtil.replace(s, "\\u003d", "=");
            s = StringUtil.replace(s, "\\u000a", "\n");
            s = StringUtil.replace(s, "\\u0021", "!");
            s = StringUtil.replace(s, "\\u0023", "#");
            s = StringUtil.replace(s, "\\u0020", " ");
            s = StringUtil.replace(s, "\\u005c", "\\");

            p.load(new ByteArrayInputStream(s.getBytes()));

            List propertyNames = Collections.list(p.propertyNames());

            for (Object propertyName : propertyNames) {
                String key = (String) propertyName;

                String value = p.getProperty(key);

                // Trim values because it may leave a trailing \r in certain
                // Windows environments. This is a known case for loading SQL
                // scripts in SQL Server.
                if (value != null) {
                    value = value.trim();

                    p.setProperty(key, value);
                }
            }
        }
    }

    /**
     * Load.
     *
     * @param s the s
     * @return the properties
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static Properties load(String s) throws IOException {
        Properties p = new Properties();

        load(p, s);

        return p;
    }

    /**
     * Merge.
     *
     * @param p1 the p 1
     * @param p2 the p 2
     */
    @SuppressWarnings("rawtypes")
    public static void merge(Properties p1, Properties p2) {
        Enumeration enu = p2.propertyNames();

        while (enu.hasMoreElements()) {
            String key = (String) enu.nextElement();
            String value = p2.getProperty(key);

            p1.setProperty(key, value);
        }
    }

    /**
     * To string.
     *
     * @param p
     *            the p
     * @return the string
     */
//    @SuppressWarnings("rawtypes")
//    public static String toString(Properties p) {
//        SafeProperties safeProperties = null;
//
//        if (p instanceof SafeProperties) {
//            safeProperties = (SafeProperties) p;
//        }
//
//        StringBuilder sb = new StringBuilder();
//
//        Enumeration enu = p.propertyNames();
//
//        while (enu.hasMoreElements()) {
//            String key = (String) enu.nextElement();
//
//            sb.append(key);
//            sb.append(StringPool.EQUAL);
//
//            if (safeProperties != null) {
//                sb.append(safeProperties.getEncodedProperty(key));
//            } else {
//                sb.append(p.getProperty(key));
//            }
//
//            sb.append(StringPool.NEW_LINE);
//        }
//
//        return sb.toString();
//    }

    /**
     * Trim keys.
     *
     * @param p the p
     */
    @SuppressWarnings("rawtypes")
    public static void trimKeys(Properties p) {
        Enumeration enu = p.propertyNames();

        while (enu.hasMoreElements()) {
            String key = (String) enu.nextElement();
            String value = p.getProperty(key);

            String trimmedKey = key.trim();

            if (!key.equals(trimmedKey)) {
                p.remove(key);
                p.setProperty(trimmedKey, value);
            }
        }
    }
}
