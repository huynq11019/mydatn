/*
 * QueryUtil.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;

import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

/**
 * The Class QueryUtil.
 */
public class QueryUtil {

    /**
     * The Constant STATUS_DEACTIVE.
     */
    public static final int STATUS_DEACTIVE = 0;

    /**
     * The Constant STATUS_ACTIVE.
     */
    public static final int STATUS_ACTIVE = 1;

    /**
     * The Constant ALL.
     */
    public static final int ALL = -1;

    /**
     * The Constant FIRST_INDEX.
     */
    public static final int FIRST_INDEX = 0;

    /**
     * The Constant GET_ALL.
     */
    public static final int GET_ALL = -1;

    /**
     * Replace special character.
     *
     * @param param the param
     * @return the string
     */
    private static String _replaceSpecialCharacter(String param) {
        return param.replaceAll("%", "\\\\%").replaceAll("_", "\\\\_");
    }

    /**
     * Adds the order.
     *
     * @param c             the c
     * @param orderByColumn the order by column
     * @param orderByType   the order by type
     * @return the string
     */
    public static String addOrder(Class<?> c, String orderByColumn, String orderByType) {
        StringBuilder sb = new StringBuilder();

        if (hasProperty(c, orderByColumn)) {
            sb.append(" order by ");
            sb.append(orderByColumn);
            sb.append(StringPool.SPACE);
            sb.append(orderByType);
        }

        return sb.toString();
    }

    /**
     * Gets the full string param.
     *
     * @param param the param
     * @return the full string param
     */
    public static String getFullStringParam(String param) {
        StringBuilder sb = new StringBuilder(5);

        sb.append(StringPool.PERCENT);
        sb.append(_replaceSpecialCharacter(param));
        sb.append(StringPool.PERCENT);

        return sb.toString();
    }

    public static String getFullTextSearchParam(String param) {
        StringBuilder sb = new StringBuilder(5);

        sb.append(StringPool.STAR);
        sb.append(_replaceSpecialCharacter(param));
        sb.append(StringPool.STAR);

        return sb.toString();
    }

    /**
     * Gets the left string param.
     *
     * @param param the param
     * @return the left string param
     */
    public static String getLeftStringParam(String param) {
        StringBuilder sb = new StringBuilder(2);

        sb.append(StringPool.PERCENT);
        sb.append(_replaceSpecialCharacter(param));

        return sb.toString();
    }

    /**
     * Gets the right string param.
     *
     * @param param the param
     * @return the right string param
     */
    public static String getRightStringParam(String param) {
        StringBuilder sb = new StringBuilder(2);

        sb.append(_replaceSpecialCharacter(param));
        sb.append(StringPool.PERCENT);

        return sb.toString();
    }

    /**
     * Checks for property.
     *
     * @param c    the c
     * @param name the name
     * @return true, if successful
     */
    private static boolean hasProperty(Class<?> c, String name) {
        boolean has = false;

        try {
            Field field = c.getDeclaredField(name);

            if (Validator.isNotNull(field)) {
                has = true;
            }
        } catch (Exception e) {
        }

        return has;
    }

    /**
     * Sets the paramerter map.
     *
     * @param sql       the sql
     * @param mapParams the map params
     */
    public static void setParamerterMap(NativeQuery<?> sql, Map<String, Object> mapParams) {
        for (Map.Entry<String, Object> entry : mapParams.entrySet()) {
            String _key = entry.getKey();
            Object _value = entry.getValue();

            if (_value instanceof List) {
                sql.setParameterList(_key, (List<?>) _value);
            } else {
                sql.setParameter(_key, _value);
            }
        }
    }

    /**
     * Sets the paramerter map.
     *
     * @param q         the q
     * @param mapParams the map params
     */
    public static void setParamerterMap(Query<?> q, Map<String, Object> mapParams) {
        for (Map.Entry<String, Object> entry : mapParams.entrySet()) {
            String _key = entry.getKey();
            Object _value = entry.getValue();

            if (_value instanceof List) {
                q.setParameterList(_key, (List<?>) _value);
            } else {
                q.setParameter(_key, _value);
            }
        }
    }
}
