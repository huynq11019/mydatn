package com.example.ducky_be.core.utils;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

/**
 * <PRE>
 * </BR>
 * </PRE>
 * <p>
 * 31/08/2021 - hieu.daominh: Create new
 *
 * @author hieu.daominh
 */
public class CustomDeserializerInstant extends JsonDeserializer<Instant> {

    private DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneOffset.UTC);

    @Override
    public Instant deserialize(com.fasterxml.jackson.core.JsonParser p, DeserializationContext ctxt) throws IOException {
        return Instant.from(fmt.parse(p.getText()));
    }
}
