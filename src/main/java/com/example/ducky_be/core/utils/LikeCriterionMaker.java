/*
 * LikeCriterionMaker.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;

/**
 * The Class LikeCriterionMaker.
 */
public class LikeCriterionMaker {

    /**
     * Ilike.
     *
     * @param propertyName the property name
     * @param value        the value
     * @return the criterion
     */
    public static Criterion ilike(String propertyName, Object value) {
        return new IlikeExpression(propertyName, value);
    }

    /**
     * Ilike.
     *
     * @param propertyName the property name
     * @param value        the value
     * @param escapeChar   the escape char
     * @return the criterion
     */
    public static Criterion ilike(String propertyName, String value, Character escapeChar) {
        return new IlikeExpression(propertyName, value, escapeChar);
    }

    /**
     * Ilike.
     *
     * @param propertyName the property name
     * @param value        the value
     * @param matchMode    the match mode
     * @return the criterion
     */
    public static Criterion ilike(String propertyName, String value, MatchMode matchMode) {
        return new IlikeExpression(propertyName, value, matchMode);
    }

    /**
     * Like.
     *
     * @param propertyName the property name
     * @param value        the value
     * @return the criterion
     */
    public static Criterion like(String propertyName, Object value) {
        return new LikeExpression(propertyName, value);
    }

    /**
     * Like.
     *
     * @param propertyName the property name
     * @param value        the value
     * @param escapeChar   the escape char
     * @return the criterion
     */
    public static Criterion like(String propertyName, String value, Character escapeChar) {
        return new LikeExpression(propertyName, value, escapeChar);
    }

    /**
     * Like.
     *
     * @param propertyName the property name
     * @param value        the value
     * @param matchMode    the match mode
     * @return the criterion
     */
    public static Criterion like(String propertyName, String value, MatchMode matchMode) {
        return new LikeExpression(propertyName, value, matchMode);
    }
}
