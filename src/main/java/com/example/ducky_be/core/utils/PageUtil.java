package com.example.ducky_be.core.utils;


import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

public class PageUtil {
    public static String PageToSqlQueryOrderBy(Pageable pageable, String defaultTable) {
        String page_ = "";
        Sort sort = pageable.getSort();
        List<Sort.Order> orders = sort.toList();
        for (Sort.Order order : orders) {
            String prop = order.getProperty();
            if (prop.split("\\.").length == 1) {
                prop = defaultTable + "." + prop;
            }
            if (page_ != "") {
                page_ += " ," + prop;
            } else {
                page_ = " order by " + prop;
            }
            Sort.Direction direction = order.getDirection();
            if (direction.isAscending()) {
                page_ += " asc";
            } else {
                page_ += " desc";
            }
        }
        long offSet = pageable.getOffset();
        long pageSize = pageable.getPageSize();
        if (page_ == "") {
            page_ = " order by "+ defaultTable + ".createDate desc";
        }
//        page_ += " OFFSET " + offSet + " ROWS FETCH NEXT " + pageSize + " ROWS ONLY";
//        page_ += " limit "+pageSize + " , " + offSet;
        return page_;
    }

    public static String PageToSqlQueryOrderBy(Pageable pageable, String defaultTable, String []allowFields ) {
        // validate allowFields



        String page_ = "";
        Sort sort = pageable.getSort();
        List<Sort.Order> orders = sort.toList();
        for (Sort.Order order : orders) {
            String prop = order.getProperty();
            if (prop.split("\\.").length == 1) {
                prop = defaultTable + "." + prop;
            }
            if (page_ != "") {
                page_ += " ," + prop;
            } else {
                page_ = " order by " + prop;
            }
            Sort.Direction direction = order.getDirection();
            if (direction.isAscending()) {
                page_ += " asc";
            } else {
                page_ += " desc";
            }
        }
        long offSet = pageable.getOffset();
        long pageSize = pageable.getPageSize();
        if (page_ == "") {
            page_ = " order by 1";
        }
//        page_ += " OFFSET " + offSet + " ROWS FETCH NEXT " + pageSize + " ROWS ONLY";
//        page_ += " limit "+pageSize + " , " + offSet;
        return page_;
    }
}
