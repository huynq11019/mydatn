/*
 * EncryptUtil.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

/**
 * The Class EncryptUtil.
 */
public class EncryptUtil {

    /**
     * The encryptor.
     */
    private final StandardPBEStringEncryptor encryptor;

    /**
     * Instantiates a new encrypt util.
     */
    public EncryptUtil() {
        this.encryptor = new StandardPBEStringEncryptor();
        this.encryptor.setAlgorithm("PBEWITHSHA1ANDRC2_40");
        this.encryptor.setPassword("%FR[4ty6TR>E7`g");
    }

    /**
     * Instantiates a new encrypt util.
     *
     * @param encryptor the encryptor
     */
    public EncryptUtil(StandardPBEStringEncryptor encryptor) {
        this.encryptor = encryptor;
    }

    /**
     * Decrypt.
     *
     * @param str the str
     * @return the string
     */
    public String decrypt(String str) {
        return this.encryptor.decrypt(str);
    }

    /**
     * Encrypt.
     *
     * @param str the str
     * @return the string
     */
    public String encrypt(String str) {
        return this.encryptor.encrypt(str);
    }
}
