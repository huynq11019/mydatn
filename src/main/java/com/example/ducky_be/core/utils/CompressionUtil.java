/*
 * CompressionUtil.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.*;

/**
 * The Class CompressionUtil.
 */
public final class CompressionUtil {

    /**
     * The Constant _log.
     */
    private final static Logger _log = LogManager.getLogger(CompressionUtil.class);

    /**
     * Compress byte array.
     *
     * @param input the input
     * @return the byte[]
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static byte[] compressByteArray(byte[] input) throws IOException {
        return compressByteArray(input, 1024);
    }

    /**
     * Compress byte array.
     *
     * @param input        the input
     * @param bufferLength the buffer length
     * @return the byte[]
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static byte[] compressByteArray(byte[] input, int bufferLength) throws IOException {
        // Compressor with highest level of compression
        Deflater compressor = new Deflater();
        compressor.setLevel(Deflater.BEST_COMPRESSION);
        // Give the compressor the data to compress
        compressor.setInput(input);
        compressor.finish();
        // Create an expandable byte array to hold the compressed data.
        // It is not necessary that the compressed data will be smaller than
        // the uncompressed data.
        ByteArrayOutputStream bos = new ByteArrayOutputStream(input.length);
        // Compress the data
        byte[] buf = new byte[bufferLength];

        try {
            while (!compressor.finished()) {
                int count = compressor.deflate(buf);

                bos.write(buf, 0, count);
            }
        } catch (Exception ex) {
            _log.error("Problem compressing.", ex);
        }

        try {
            bos.close();
        } catch (IOException ex) {
            _log.error("Problem closing stream.", ex);
        }
        // Get the compressed data
        return bos.toByteArray();
    }

    /**
     * Compress gzip byte array.
     *
     * @param input the input
     * @return the byte[]
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static byte[] compressGzipByteArray(byte[] input) throws IOException {
        return compressGzipByteArray(input, 1024);
    }

    /**
     * Compress gzip byte array.
     *
     * @param input        the input
     * @param bufferLength the buffer length
     * @return the byte[]
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static byte[] compressGzipByteArray(byte[] input, int bufferLength) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(input);
        // Create an expandable byte array to hold the compressed data.
        // It is not necessary that the compressed data will be smaller than
        // the uncompressed data.
        ByteArrayOutputStream bos = new ByteArrayOutputStream(input.length);

        GZIPOutputStream gos = new GZIPOutputStream(bos);

        // Compress the data
        byte[] buffer = new byte[bufferLength];

        int index = -1;

        try {
            while ((index = bis.read(buffer)) != -1) {
                gos.write(buffer, 0, index);
            }
        } catch (IOException ex) {
            _log.error("Problem decompressing.", ex);
        }
        // close the input stream

        try {
            bis.close();
        } catch (IOException ex) {
            _log.error("Problem closing input stream.", ex);
        }
        // finish and close the gzip output

        try {
            gos.finish();
            gos.close();
        } catch (IOException ex) {
            _log.error("Problem closing gzip output stream.", ex);
        }
        // close the output stream
        try {
            bos.close();
        } catch (IOException ex) {
            _log.error("Problem closing output stream.", ex);
        }
        // Get the compressed data
        return bos.toByteArray();
    }

    /**
     * Decompress byte array.
     *
     * @param input the input
     * @return the byte[]
     */
    public static byte[] decompressByteArray(final byte[] input) {
        return decompressByteArray(input, 1024);
    }

    /**
     * Decompress byte array.
     *
     * @param input        the input
     * @param bufferLength the buffer length
     * @return the byte[]
     */
    public static byte[] decompressByteArray(final byte[] input, final int bufferLength) {
        if (null == input) {
            throw new IllegalArgumentException("Input was null");
        }
        // Create the decompressor and give it the data to compress
        final Inflater decompressor = new Inflater();

        decompressor.setInput(input);
        // Create an expandable byte array to hold the decompressed data
        final ByteArrayOutputStream baos = new ByteArrayOutputStream(input.length);
        // Decompress the data
        final byte[] buf = new byte[bufferLength];

        try {
            while (!decompressor.finished()) {
                int count = decompressor.inflate(buf);
                baos.write(buf, 0, count);
            }
        } catch (DataFormatException ex) {
            _log.error("Problem decompressing.", ex);
        }

        try {
            baos.close();
        } catch (IOException ex) {
            _log.error("Problem closing stream.", ex);
        }

        return baos.toByteArray();
    }

    /**
     * Decompress gzip byte array.
     *
     * @param compressedByteArray the compressed byte array
     * @return the byte[]
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static byte[] decompressGzipByteArray(byte[] compressedByteArray) throws IOException {
        return decompressGzipByteArray(compressedByteArray, 1024);
    }

    /**
     * Decompress gzip byte array.
     *
     * @param compressedByteArray the compressed byte array
     * @param bufferlength        the bufferlength
     * @return the byte[]
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static byte[] decompressGzipByteArray(byte[] compressedByteArray, int bufferlength) throws IOException {
        ByteArrayOutputStream uncompressedStream = new ByteArrayOutputStream();

        GZIPInputStream compressedStream = new GZIPInputStream(new ByteArrayInputStream(compressedByteArray));

        byte[] buffer = new byte[bufferlength];

        int index = -1;

        try {
            while ((index = compressedStream.read(buffer)) != -1) {
                uncompressedStream.write(buffer, 0, index);
            }
        } catch (IOException ex) {
            _log.error("Problem decompressing.", ex);
        }

        try {
            uncompressedStream.close();
        } catch (IOException ex) {
            _log.error("Problem closing output stream.", ex);
        }

        try {
            compressedStream.close();
        } catch (IOException ex) {
            _log.error("Problem closing input stream.", ex);
        }

        return uncompressedStream.toByteArray();
    }

    /**
     * Instantiates a new compression util.
     */
    private CompressionUtil() {
        // NO OP
    }
}
