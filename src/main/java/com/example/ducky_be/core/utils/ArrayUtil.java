/*
 * ArrayUtil.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.util.*;

/**
 * The Class ArrayUtil.
 */
public class ArrayUtil {

    /**
     * Append.
     *
     * @param arrays the arrays
     * @return the boolean[]
     */
    public static boolean[] append(boolean[]... arrays) {
        int length = 0;

        for (boolean[] array : arrays) {
            length += array.length;
        }

        boolean[] newArray = new boolean[length];

        int previousLength = 0;

        for (boolean[] array : arrays) {
            System.arraycopy(array, 0, newArray, previousLength, array.length);

            previousLength += array.length;
        }

        return newArray;
    }

    /**
     * Append.
     *
     * @param array the array
     * @param value the value
     * @return the boolean[]
     */
    public static boolean[] append(boolean[] array, boolean value) {
        boolean[] newArray = new boolean[array.length + 1];

        System.arraycopy(array, 0, newArray, 0, array.length);

        newArray[newArray.length - 1] = value;

        return newArray;
    }

    /**
     * Append.
     *
     * @param arrays the arrays
     * @return the byte[]
     */
    public static byte[] append(byte[]... arrays) {
        int length = 0;

        for (byte[] array : arrays) {
            length += array.length;
        }

        byte[] newArray = new byte[length];

        int previousLength = 0;

        for (byte[] array : arrays) {
            System.arraycopy(array, 0, newArray, previousLength, array.length);

            previousLength += array.length;
        }

        return newArray;
    }

    /**
     * Append.
     *
     * @param array the array
     * @param value the value
     * @return the byte[]
     */
    public static byte[] append(byte[] array, byte value) {
        byte[] newArray = new byte[array.length + 1];

        System.arraycopy(array, 0, newArray, 0, array.length);

        newArray[newArray.length - 1] = value;

        return newArray;
    }

    /**
     * Append.
     *
     * @param arrays the arrays
     * @return the char[]
     */
    public static char[] append(char[]... arrays) {
        int length = 0;

        for (char[] array : arrays) {
            length += array.length;
        }

        char[] newArray = new char[length];

        int previousLength = 0;

        for (char[] array : arrays) {
            System.arraycopy(array, 0, newArray, previousLength, array.length);

            previousLength += array.length;
        }

        return newArray;
    }

    /**
     * Append.
     *
     * @param array the array
     * @param value the value
     * @return the char[]
     */
    public static char[] append(char[] array, char value) {
        char[] newArray = new char[array.length + 1];

        System.arraycopy(array, 0, newArray, 0, array.length);

        newArray[newArray.length - 1] = value;

        return newArray;
    }

    /**
     * Append.
     *
     * @param arrays the arrays
     * @return the double[]
     */
    public static double[] append(double[]... arrays) {
        int length = 0;

        for (double[] array : arrays) {
            length += array.length;
        }

        double[] newArray = new double[length];

        int previousLength = 0;

        for (double[] array : arrays) {
            System.arraycopy(array, 0, newArray, previousLength, array.length);

            previousLength += array.length;
        }

        return newArray;
    }

    /**
     * Append.
     *
     * @param array the array
     * @param value the value
     * @return the double[]
     */
    public static double[] append(double[] array, double value) {
        double[] newArray = new double[array.length + 1];

        System.arraycopy(array, 0, newArray, 0, array.length);

        newArray[newArray.length - 1] = value;

        return newArray;
    }

    /**
     * Append.
     *
     * @param arrays the arrays
     * @return the float[]
     */
    public static float[] append(float[]... arrays) {
        int length = 0;

        for (float[] array : arrays) {
            length += array.length;
        }

        float[] newArray = new float[length];

        int previousLength = 0;

        for (float[] array : arrays) {
            System.arraycopy(array, 0, newArray, previousLength, array.length);

            previousLength += array.length;
        }

        return newArray;
    }

    /**
     * Append.
     *
     * @param array the array
     * @param value the value
     * @return the float[]
     */
    public static float[] append(float[] array, float value) {
        float[] newArray = new float[array.length + 1];

        System.arraycopy(array, 0, newArray, 0, array.length);

        newArray[newArray.length - 1] = value;

        return newArray;
    }

    /**
     * Append.
     *
     * @param arrays the arrays
     * @return the int[]
     */
    public static int[] append(int[]... arrays) {
        int length = 0;

        for (int[] array : arrays) {
            length += array.length;
        }

        int[] newArray = new int[length];

        int previousLength = 0;

        for (int[] array : arrays) {
            System.arraycopy(array, 0, newArray, previousLength, array.length);

            previousLength += array.length;
        }

        return newArray;
    }

    /**
     * Append.
     *
     * @param array the array
     * @param value the value
     * @return the int[]
     */
    public static int[] append(int[] array, int value) {
        int[] newArray = new int[array.length + 1];

        System.arraycopy(array, 0, newArray, 0, array.length);

        newArray[newArray.length - 1] = value;

        return newArray;
    }

    /**
     * Append.
     *
     * @param arrays the arrays
     * @return the long[]
     */
    public static long[] append(long[]... arrays) {
        int length = 0;

        for (long[] array : arrays) {
            length += array.length;
        }

        long[] newArray = new long[length];

        int previousLength = 0;

        for (long[] array : arrays) {
            System.arraycopy(array, 0, newArray, previousLength, array.length);

            previousLength += array.length;
        }

        return newArray;
    }

    /**
     * Append.
     *
     * @param array the array
     * @param value the value
     * @return the long[]
     */
    public static long[] append(long[] array, long value) {
        long[] newArray = new long[array.length + 1];

        System.arraycopy(array, 0, newArray, 0, array.length);

        newArray[newArray.length - 1] = value;

        return newArray;
    }

    /**
     * Append.
     *
     * @param arrays the arrays
     * @return the short[]
     */
    public static short[] append(short[]... arrays) {
        int length = 0;

        for (short[] array : arrays) {
            length += array.length;
        }

        short[] newArray = new short[length];

        int previousLength = 0;

        for (short[] array : arrays) {
            System.arraycopy(array, 0, newArray, previousLength, array.length);

            previousLength += array.length;
        }

        return newArray;
    }

    /**
     * Append.
     *
     * @param array the array
     * @param value the value
     * @return the short[]
     */
    public static short[] append(short[] array, short value) {
        short[] newArray = new short[array.length + 1];

        System.arraycopy(array, 0, newArray, 0, array.length);

        newArray[newArray.length - 1] = value;

        return newArray;
    }

    /**
     * Append.
     *
     * @param <T>    the generic type
     * @param arrays the arrays
     * @return the t[]
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] append(T[]... arrays) {
        int length = 0;

        for (T[] array : arrays) {
            length += array.length;
        }

        Class<?> arraysClass = arrays[0].getClass();

        T[] newArray = (T[]) Array.newInstance(arraysClass.getComponentType(), length);

        int previousLength = 0;

        for (T[] array : arrays) {
            System.arraycopy(array, 0, newArray, previousLength, array.length);

            previousLength += array.length;
        }

        return newArray;
    }

    /**
     * Append.
     *
     * @param <T>   the generic type
     * @param array the array
     * @param value the value
     * @return the t[]
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] append(T[] array, T value) {
        Class<?> arrayClass = array.getClass();

        T[] newArray = (T[]) Array.newInstance(arrayClass.getComponentType(), array.length + 1);

        System.arraycopy(array, 0, newArray, 0, array.length);

        newArray[array.length] = value;

        return newArray;
    }

    /**
     * Append.
     *
     * @param <T>    the generic type
     * @param array1 the array 1
     * @param array2 the array 2
     * @return the t[]
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] append(T[] array1, T[] array2) {
        Class<?> array1Class = array1.getClass();

        T[] newArray = (T[]) Array.newInstance(array1Class.getComponentType(), array1.length + array2.length);

        System.arraycopy(array1, 0, newArray, 0, array1.length);

        System.arraycopy(array2, 0, newArray, array1.length, array2.length);

        return newArray;
    }

    /**
     * Append.
     *
     * @param <T>    the generic type
     * @param array1 the array 1
     * @param value  the value
     * @return the t[][]
     */
    @SuppressWarnings("unchecked")
    public static <T> T[][] append(T[][] array1, T[] value) {
        Class<?> array1Class = array1.getClass();

        T[][] newArray = (T[][]) Array.newInstance(array1Class.getComponentType(), array1.length + 1);

        System.arraycopy(array1, 0, newArray, 0, array1.length);

        newArray[array1.length] = value;

        return newArray;
    }

    /**
     * Append.
     *
     * @param <T>    the generic type
     * @param array1 the array 1
     * @param array2 the array 2
     * @return the t[][]
     */
    @SuppressWarnings("unchecked")
    public static <T> T[][] append(T[][] array1, T[][] array2) {
        Class<?> array1Class = array1.getClass();

        T[][] newArray = (T[][]) Array.newInstance(array1Class.getComponentType(), array1.length + array2.length);

        System.arraycopy(array1, 0, newArray, 0, array1.length);
        System.arraycopy(array2, 0, newArray, array1.length, array2.length);

        return newArray;
    }

    /**
     * Clone.
     *
     * @param array the array
     * @return the boolean[]
     */
    public static boolean[] clone(boolean[] array) {
        boolean[] newArray = new boolean[array.length];

        System.arraycopy(array, 0, newArray, 0, array.length);

        return newArray;
    }

    /**
     * Clone.
     *
     * @param array the array
     * @param from  the from
     * @param to    the to
     * @return the boolean[]
     */
    public static boolean[] clone(boolean[] array, int from, int to) {
        boolean[] newArray = new boolean[to - from];

        System.arraycopy(array, from, newArray, 0, Math.min(array.length - from, newArray.length));

        return newArray;
    }

    /**
     * Clone.
     *
     * @param array the array
     * @return the byte[]
     */
    public static byte[] clone(byte[] array) {
        byte[] newArray = new byte[array.length];

        System.arraycopy(array, 0, newArray, 0, array.length);

        return newArray;
    }

    /**
     * Clone.
     *
     * @param array the array
     * @param from  the from
     * @param to    the to
     * @return the byte[]
     */
    public static byte[] clone(byte[] array, int from, int to) {
        byte[] newArray = new byte[to - from];

        System.arraycopy(array, from, newArray, 0, Math.min(array.length - from, newArray.length));

        return newArray;
    }

    /**
     * Clone.
     *
     * @param array the array
     * @return the char[]
     */
    public static char[] clone(char[] array) {
        char[] newArray = new char[array.length];

        System.arraycopy(array, 0, newArray, 0, array.length);

        return newArray;
    }

    /**
     * Clone.
     *
     * @param array the array
     * @param from  the from
     * @param to    the to
     * @return the char[]
     */
    public static char[] clone(char[] array, int from, int to) {
        char[] newArray = new char[to - from];

        System.arraycopy(array, from, newArray, 0, Math.min(array.length - from, newArray.length));

        return newArray;
    }

    /**
     * Clone.
     *
     * @param array the array
     * @return the double[]
     */
    public static double[] clone(double[] array) {
        double[] newArray = new double[array.length];

        System.arraycopy(array, 0, newArray, 0, array.length);

        return newArray;
    }

    /**
     * Clone.
     *
     * @param array the array
     * @param from  the from
     * @param to    the to
     * @return the double[]
     */
    public static double[] clone(double[] array, int from, int to) {
        double[] newArray = new double[to - from];

        System.arraycopy(array, from, newArray, 0, Math.min(array.length - from, newArray.length));

        return newArray;
    }

    /**
     * Clone.
     *
     * @param array the array
     * @return the float[]
     */
    public static float[] clone(float[] array) {
        float[] newArray = new float[array.length];

        System.arraycopy(array, 0, newArray, 0, array.length);

        return newArray;
    }

    /**
     * Clone.
     *
     * @param array the array
     * @param from  the from
     * @param to    the to
     * @return the float[]
     */
    public static float[] clone(float[] array, int from, int to) {
        float[] newArray = new float[to - from];

        System.arraycopy(array, from, newArray, 0, Math.min(array.length - from, newArray.length));

        return newArray;
    }

    /**
     * Clone.
     *
     * @param array the array
     * @return the int[]
     */
    public static int[] clone(int[] array) {
        int[] newArray = new int[array.length];

        System.arraycopy(array, 0, newArray, 0, array.length);

        return newArray;
    }

    /**
     * Clone.
     *
     * @param array the array
     * @param from  the from
     * @param to    the to
     * @return the int[]
     */
    public static int[] clone(int[] array, int from, int to) {
        int[] newArray = new int[to - from];

        System.arraycopy(array, from, newArray, 0, Math.min(array.length - from, newArray.length));

        return newArray;
    }

    /**
     * Clone.
     *
     * @param array the array
     * @return the long[]
     */
    public static long[] clone(long[] array) {
        long[] newArray = new long[array.length];

        System.arraycopy(array, 0, newArray, 0, array.length);

        return newArray;
    }

    /**
     * Clone.
     *
     * @param array the array
     * @param from  the from
     * @param to    the to
     * @return the long[]
     */
    public static long[] clone(long[] array, int from, int to) {
        long[] newArray = new long[to - from];

        System.arraycopy(array, from, newArray, 0, Math.min(array.length - from, newArray.length));

        return newArray;
    }

    /**
     * Clone.
     *
     * @param array the array
     * @return the short[]
     */
    public static short[] clone(short[] array) {
        short[] newArray = new short[array.length];

        System.arraycopy(array, 0, newArray, 0, array.length);

        return newArray;
    }

    /**
     * Clone.
     *
     * @param array the array
     * @param from  the from
     * @param to    the to
     * @return the short[]
     */
    public static short[] clone(short[] array, int from, int to) {
        short[] newArray = new short[to - from];

        System.arraycopy(array, from, newArray, 0, Math.min(array.length - from, newArray.length));

        return newArray;
    }

    /**
     * Clone.
     *
     * @param <T>   the generic type
     * @param array the array
     * @return the t[]
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] clone(T[] array) {
        Class<?> arrayClass = array.getClass();

        T[] newArray = (T[]) Array.newInstance(arrayClass.getComponentType(), array.length);

        System.arraycopy(array, 0, newArray, 0, array.length);

        return newArray;
    }

    /**
     * Clone.
     *
     * @param <T>   the generic type
     * @param array the array
     * @param from  the from
     * @param to    the to
     * @return the t[]
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] clone(T[] array, int from, int to) {
        Class<?> arrayClass = array.getClass();

        T[] newArray = (T[]) Array.newInstance(arrayClass.getComponentType(), to - from);

        System.arraycopy(array, from, newArray, 0, Math.min(array.length - from, newArray.length));

        return newArray;
    }

    /**
     * Clone.
     *
     * @param <T>   the generic type
     * @param array the array
     * @return the t[][]
     */
    @SuppressWarnings("unchecked")
    public static <T> T[][] clone(T[][] array) {
        Class<?> arrayClass = array.getClass();

        T[][] newArray = (T[][]) Array.newInstance(arrayClass.getComponentType(), array.length);

        System.arraycopy(array, 0, newArray, 0, array.length);

        return newArray;
    }

    /**
     * Clone.
     *
     * @param <T>   the generic type
     * @param array the array
     * @param from  the from
     * @param to    the to
     * @return the t[][]
     */
    @SuppressWarnings("unchecked")
    public static <T> T[][] clone(T[][] array, int from, int to) {
        Class<?> arrayClass = array.getClass();

        T[][] newArray = (T[][]) Array.newInstance(arrayClass.getComponentType(), to - from);

        System.arraycopy(array, from, newArray, 0, Math.min(array.length - from, newArray.length));

        return newArray;
    }

    /**
     * Combine.
     *
     * @param array1        the array 1
     * @param array2        the array 2
     * @param combinedArray the combined array
     */
    public static void combine(Object[] array1, Object[] array2, Object[] combinedArray) {

        System.arraycopy(array1, 0, combinedArray, 0, array1.length);

        System.arraycopy(array2, 0, combinedArray, array1.length, array2.length);
    }

    /**
     * Consigns.
     *
     * @param array1 the array 1
     * @param array2 the array 2
     * @return true, if successful
     */
    public static boolean consigns(Object[] array1, Object[] array2) {
        if (array1 == null || array1.length == 0 || array2 == null || array2.length == 0) {
            return false;
        }

        for (Object element : array1) {
            if (contains(array2, element)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Contains.
     *
     * @param array the array
     * @param value the value
     * @return true, if successful
     */
    public static boolean contains(boolean[] array, boolean value) {
        if (array == null || array.length == 0) {
            return false;
        }

        for (boolean element : array) {
            if (value == element) {
                return true;
            }
        }

        return false;
    }

    /**
     * Contains.
     *
     * @param array the array
     * @param value the value
     * @return true, if successful
     */
    public static boolean contains(byte[] array, byte value) {
        if (array == null || array.length == 0) {
            return false;
        }

        for (byte element : array) {
            if (value == element) {
                return true;
            }
        }

        return false;
    }

    /**
     * Contains.
     *
     * @param array the array
     * @param value the value
     * @return true, if successful
     */
    public static boolean contains(char[] array, char value) {
        if (array == null || array.length == 0) {
            return false;
        }

        for (char element : array) {
            if (value == element) {
                return true;
            }
        }

        return false;
    }

    /**
     * Contains.
     *
     * @param array the array
     * @param value the value
     * @return true, if successful
     */
    public static boolean contains(double[] array, double value) {
        if (array == null || array.length == 0) {
            return false;
        }

        for (double element : array) {
            if (value == element) {
                return true;
            }
        }

        return false;
    }

    /**
     * Contains.
     *
     * @param array the array
     * @param value the value
     * @return true, if successful
     */
    public static boolean contains(float[] array, float value) {
        if (array == null || array.length == 0) {
            return false;
        }

        for (float element : array) {
            if (value == element) {
                return true;
            }
        }

        return false;
    }

    /**
     * Contains.
     *
     * @param array the array
     * @param value the value
     * @return true, if successful
     */
    public static boolean contains(int[] array, int value) {
        if (array == null || array.length == 0) {
            return false;
        }

        for (int element : array) {
            if (value == element) {
                return true;
            }
        }

        return false;
    }

    /**
     * Contains.
     *
     * @param array the array
     * @param value the value
     * @return true, if successful
     */
    public static boolean contains(long[] array, long value) {
        if (array == null || array.length == 0) {
            return false;
        }

        for (long element : array) {
            if (value == element) {
                return true;
            }
        }

        return false;
    }

    /**
     * Contains.
     *
     * @param array the array
     * @param value the value
     * @return true, if successful
     */
    public static boolean contains(Object[] array, Object value) {
        if (array == null || array.length == 0 || value == null) {
            return false;
        }

        for (Object element : array) {
            if (value.equals(element)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Contains.
     *
     * @param array the array
     * @param value the value
     * @return true, if successful
     */
    public static boolean contains(short[] array, short value) {
        if (array == null || array.length == 0) {
            return false;
        }

        for (short element : array) {
            if (value == element) {
                return true;
            }
        }

        return false;
    }

    /**
     * Distinct.
     *
     * @param array the array
     * @return the string[]
     */
    public static String[] distinct(String[] array) {
        return distinct(array, null);
    }

    /**
     * Distinct.
     *
     * @param array      the array
     * @param comparator the comparator
     * @return the string[]
     */
    public static String[] distinct(String[] array, Comparator<String> comparator) {

        if (array == null || array.length == 0) {
            return array;
        }

        Set<String> set = null;

        if (comparator == null) {
            set = new TreeSet<>();
        } else {
            set = new TreeSet<>(comparator);
        }

        for (String s : array) {
            if (!set.contains(s)) {
                set.add(s);
            }
        }

        return set.toArray(new String[set.size()]);
    }

    /**
     * Gets the length.
     *
     * @param array the array
     * @return the length
     */
    public static int getLength(Object[] array) {
        if (array == null) {
            return 0;
        }
        return array.length;
    }

    /**
     * Gets the value.
     *
     * @param array the array
     * @param pos   the pos
     * @return the value
     */
    public static Object getValue(Object[] array, int pos) {
        if (array == null || array.length <= pos) {
            return null;
        }
        return array[pos];
    }

    /**
     * Removes the.
     *
     * @param array the array
     * @param value the value
     * @return the boolean[]
     */
    public static boolean[] remove(boolean[] array, boolean value) {
        List<Boolean> list = new ArrayList<>();

        for (boolean element : array) {
            if (value != element) {
                list.add(new Boolean(element));
            }
        }

        return toArray(list.toArray(new Boolean[list.size()]));
    }

    /**
     * Removes the.
     *
     * @param array the array
     * @param value the value
     * @return the byte[]
     */
    public static byte[] remove(byte[] array, byte value) {
        List<Byte> list = new ArrayList<>();

        for (byte element : array) {
            if (value != element) {
                list.add(new Byte(element));
            }
        }

        return toArray(list.toArray(new Byte[list.size()]));
    }

    /**
     * Removes the.
     *
     * @param array the array
     * @param value the value
     * @return the char[]
     */
    public static char[] remove(char[] array, char value) {
        List<Character> list = new ArrayList<>();

        for (char element : array) {
            if (value != element) {
                list.add(new Character(element));
            }
        }

        return toArray(list.toArray(new Character[list.size()]));
    }

    /**
     * Removes the.
     *
     * @param array the array
     * @param value the value
     * @return the double[]
     */
    public static double[] remove(double[] array, double value) {
        List<Double> list = new ArrayList<>();

        for (double element : array) {
            if (value != element) {
                list.add(new Double(element));
            }
        }

        return toArray(list.toArray(new Double[list.size()]));
    }

    /**
     * Removes the.
     *
     * @param array the array
     * @param value the value
     * @return the int[]
     */
    public static int[] remove(int[] array, int value) {
        List<Integer> list = new ArrayList<>();

        for (int element : array) {
            if (value != element) {
                list.add(new Integer(element));
            }
        }

        return toArray(list.toArray(new Integer[list.size()]));
    }

    /**
     * Removes the.
     *
     * @param array the array
     * @param value the value
     * @return the long[]
     */
    public static long[] remove(long[] array, long value) {
        List<Long> list = new ArrayList<>();

        for (long element : array) {
            if (value != element) {
                list.add(new Long(element));
            }
        }

        return toArray(list.toArray(new Long[list.size()]));
    }

    /**
     * Removes the.
     *
     * @param array the array
     * @param value the value
     * @return the short[]
     */
    public static short[] remove(short[] array, short value) {
        List<Short> list = new ArrayList<>();

        for (short element : array) {
            if (value != element) {
                list.add(new Short(element));
            }
        }

        return toArray(list.toArray(new Short[list.size()]));
    }

    /**
     * Removes the.
     *
     * @param array the array
     * @param value the value
     * @return the string[]
     */
    public static String[] remove(String[] array, String value) {
        List<String> list = new ArrayList<>();

        for (String s : array) {
            if (!s.equals(value)) {
                list.add(s);
            }
        }

        return list.toArray(new String[list.size()]);
    }

    /**
     * Removes the by prefix.
     *
     * @param array  the array
     * @param prefix the prefix
     * @return the string[]
     */
    public static String[] removeByPrefix(String[] array, String prefix) {
        List<String> list = new ArrayList<>();

        for (String s : array) {
            if (!s.startsWith(prefix)) {
                list.add(s);
            }
        }

        return list.toArray(new String[list.size()]);
    }

    /**
     * Reverse.
     *
     * @param array the array
     */
    public static void reverse(byte[] array) {
        for (int left = 0, right = array.length - 1; left < right; left++, right--) {

            byte value = array[left];

            array[left] = array[right];
            array[right] = value;
        }
    }

    /**
     * Reverse.
     *
     * @param array the array
     */
    public static void reverse(char[] array) {
        for (int left = 0, right = array.length - 1; left < right; left++, right--) {

            char value = array[left];

            array[left] = array[right];
            array[right] = value;
        }
    }

    /**
     * Reverse.
     *
     * @param array the array
     */
    public static void reverse(double[] array) {
        for (int left = 0, right = array.length - 1; left < right; left++, right--) {

            double value = array[left];

            array[left] = array[right];
            array[right] = value;
        }
    }

    /**
     * Reverse.
     *
     * @param array the array
     */
    public static void reverse(float[] array) {
        for (int left = 0, right = array.length - 1; left < right; left++, right--) {

            float value = array[left];

            array[left] = array[right];
            array[right] = value;
        }
    }

    /**
     * Reverse.
     *
     * @param array the array
     */
    public static void reverse(int[] array) {
        for (int left = 0, right = array.length - 1; left < right; left++, right--) {

            int value = array[left];

            array[left] = array[right];
            array[right] = value;
        }
    }

    /**
     * Reverse.
     *
     * @param array the array
     */
    public static void reverse(long[] array) {
        for (int left = 0, right = array.length - 1; left < right; left++, right--) {

            long value = array[left];

            array[left] = array[right];
            array[right] = value;
        }
    }

    /**
     * Reverse.
     *
     * @param array the array
     */
    public static void reverse(Object[] array) {
        for (int left = 0, right = array.length - 1; left < right; left++, right--) {

            Object value = array[left];

            array[left] = array[right];
            array[right] = value;
        }
    }

    /**
     * Reverse.
     *
     * @param array the array
     */
    public static void reverse(short[] array) {
        for (int left = 0, right = array.length - 1; left < right; left++, right--) {

            short value = array[left];

            array[left] = array[right];
            array[right] = value;
        }
    }

    /**
     * Shuffle.
     *
     * @param array the array
     * @return the double[]
     */
    public static double[] shuffle(double[] array) {
        for (int index = 0; index < array.length; index++) {
            // between i and n-1
            int randomIndex = index + (int) (Math.random() * (array.length - index));

            double e = array[index]; // swap

            array[index] = array[randomIndex];

            array[randomIndex] = e;
        }

        return array;
    }

    /**
     * Shuffle.
     *
     * @param array the array
     * @return the int[]
     */
    public static int[] shuffle(int[] array) {
        for (int index = 0; index < array.length; index++) {
            // between i and n-1
            int randomIndex = index + (int) (Math.random() * (array.length - index));

            int e = array[index]; // swap

            array[index] = array[randomIndex];

            array[randomIndex] = e;
        }

        return array;
    }

    /**
     * Shuffle.
     *
     * @param array the array
     * @return the long[]
     */
    public static long[] shuffle(long[] array) {
        for (int index = 0; index < array.length; index++) {
            // between i and n-1
            int randomIndex = index + (int) (Math.random() * (array.length - index));

            long e = array[index]; // swap

            array[index] = array[randomIndex];

            array[randomIndex] = e;
        }

        return array;
    }

    /**
     * Shuffle.
     *
     * @param <T>   the generic type
     * @param array the array
     * @return the t[]
     */
    public static <T> T[] shuffle(T[] array) {
        for (int index = 0; index < array.length; index++) {
            // between i and n-1
            int randomIndex = index + (int) (Math.random() * (array.length - index));

            T e = array[index]; // swap

            array[index] = array[randomIndex];

            array[randomIndex] = e;
        }

        return array;
    }

    /**
     * Subset.
     *
     * @param array the array
     * @param start the start
     * @param end   the end
     * @return the boolean[]
     */
    public static boolean[] subset(boolean[] array, int start, int end) {
        if (start < 0 || end < 0 || end - start < 0) {
            return array;
        }

        boolean[] newArray = new boolean[end - start];

        System.arraycopy(array, start, newArray, 0, end - start);

        return newArray;
    }

    /**
     * Subset.
     *
     * @param array the array
     * @param start the start
     * @param end   the end
     * @return the byte[]
     */
    public static byte[] subset(byte[] array, int start, int end) {
        if (start < 0 || end < 0 || end - start < 0) {
            return array;
        }

        byte[] newArray = new byte[end - start];

        System.arraycopy(array, start, newArray, 0, end - start);

        return newArray;
    }

    /**
     * Subset.
     *
     * @param array the array
     * @param start the start
     * @param end   the end
     * @return the char[]
     */
    public static char[] subset(char[] array, int start, int end) {
        if (start < 0 || end < 0 || end - start < 0) {
            return array;
        }

        char[] newArray = new char[end - start];

        System.arraycopy(array, start, newArray, 0, end - start);

        return newArray;
    }

    /**
     * Subset.
     *
     * @param array the array
     * @param start the start
     * @param end   the end
     * @return the double[]
     */
    public static double[] subset(double[] array, int start, int end) {
        if (start < 0 || end < 0 || end - start < 0) {
            return array;
        }

        double[] newArray = new double[end - start];

        System.arraycopy(array, start, newArray, 0, end - start);

        return newArray;
    }

    /**
     * Subset.
     *
     * @param array the array
     * @param start the start
     * @param end   the end
     * @return the float[]
     */
    public static float[] subset(float[] array, int start, int end) {
        if (start < 0 || end < 0 || end - start < 0) {
            return array;
        }

        float[] newArray = new float[end - start];

        System.arraycopy(array, start, newArray, 0, end - start);

        return newArray;
    }

    /**
     * Subset.
     *
     * @param array the array
     * @param start the start
     * @param end   the end
     * @return the int[]
     */
    public static int[] subset(int[] array, int start, int end) {
        if (start < 0 || end < 0 || end - start < 0) {
            return array;
        }

        int[] newArray = new int[end - start];

        System.arraycopy(array, start, newArray, 0, end - start);

        return newArray;
    }

    /**
     * Subset.
     *
     * @param array the array
     * @param start the start
     * @param end   the end
     * @return the long[]
     */
    public static long[] subset(long[] array, int start, int end) {
        if (start < 0 || end < 0 || end - start < 0) {
            return array;
        }

        long[] newArray = new long[end - start];

        System.arraycopy(array, start, newArray, 0, end - start);

        return newArray;
    }

    /**
     * Subset.
     *
     * @param array the array
     * @param start the start
     * @param end   the end
     * @return the short[]
     */
    public static short[] subset(short[] array, int start, int end) {
        if (start < 0 || end < 0 || end - start < 0) {
            return array;
        }

        short[] newArray = new short[end - start];

        System.arraycopy(array, start, newArray, 0, end - start);

        return newArray;
    }

    /**
     * Subset.
     *
     * @param <T>   the generic type
     * @param array the array
     * @param start the start
     * @param end   the end
     * @return the t[]
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] subset(T[] array, int start, int end) {
        if (start < 0 || end < 0 || end - start < 0) {
            return array;
        }

        Class<?> arrayClass = array.getClass();

        T[] newArray = (T[]) Array.newInstance(arrayClass.getComponentType(), end - start);

        System.arraycopy(array, start, newArray, 0, end - start);

        return newArray;
    }

    /**
     * To array.
     *
     * @param array the array
     * @return the boolean[]
     */
    public static Boolean[] toArray(boolean[] array) {
        Boolean[] newArray = new Boolean[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = Boolean.valueOf(array[i]);
        }

        return newArray;
    }

    /**
     * To array.
     *
     * @param array the array
     * @return the boolean[]
     */
    public static boolean[] toArray(Boolean[] array) {
        boolean[] newArray = new boolean[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[i].booleanValue();
        }

        return newArray;
    }

    /**
     * To array.
     *
     * @param array the array
     * @return the byte[]
     */
    public static Byte[] toArray(byte[] array) {
        Byte[] newArray = new Byte[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = Byte.valueOf(array[i]);
        }

        return newArray;
    }

    /**
     * To array.
     *
     * @param array the array
     * @return the byte[]
     */
    public static byte[] toArray(Byte[] array) {
        byte[] newArray = new byte[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[i].byteValue();
        }

        return newArray;
    }

    /**
     * To array.
     *
     * @param array the array
     * @return the character[]
     */
    public static Character[] toArray(char[] array) {
        Character[] newArray = new Character[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = Character.valueOf(array[i]);
        }

        return newArray;
    }

    /**
     * To array.
     *
     * @param array the array
     * @return the char[]
     */
    public static char[] toArray(Character[] array) {
        char[] newArray = new char[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[i].charValue();
        }

        return newArray;
    }

    /**
     * To array.
     *
     * @param array the array
     * @return the double[]
     */
    public static Double[] toArray(double[] array) {
        Double[] newArray = new Double[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = new Double(array[i]);
        }

        return newArray;
    }

    /**
     * To array.
     *
     * @param array the array
     * @return the double[]
     */
    public static double[] toArray(Double[] array) {
        double[] newArray = new double[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[i].doubleValue();
        }

        return newArray;
    }

    /**
     * To array.
     *
     * @param array the array
     * @return the float[]
     */
    public static Float[] toArray(float[] array) {
        Float[] newArray = new Float[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = new Float(array[i]);
        }

        return newArray;
    }

    /**
     * To array.
     *
     * @param array the array
     * @return the float[]
     */
    public static float[] toArray(Float[] array) {
        float[] newArray = new float[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[i].floatValue();
        }

        return newArray;
    }

    /**
     * To array.
     *
     * @param array the array
     * @return the integer[]
     */
    public static Integer[] toArray(int[] array) {
        Integer[] newArray = new Integer[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = new Integer(array[i]);
        }

        return newArray;
    }

    /**
     * To array.
     *
     * @param array the array
     * @return the int[]
     */
    public static int[] toArray(Integer[] array) {
        int[] newArray = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[i].intValue();
        }

        return newArray;
    }

    /**
     * To array.
     *
     * @param array the array
     * @return the long[]
     */
    public static Long[] toArray(long[] array) {
        Long[] newArray = new Long[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = new Long(array[i]);
        }

        return newArray;
    }

    /**
     * To array.
     *
     * @param array the array
     * @return the long[]
     */
    public static long[] toArray(Long[] array) {
        long[] newArray = new long[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[i].longValue();
        }

        return newArray;
    }

    /**
     * To array.
     *
     * @param array the array
     * @return the short[]
     */
    public static Short[] toArray(short[] array) {
        Short[] newArray = new Short[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = new Short(array[i]);
        }

        return newArray;
    }

    /**
     * To array.
     *
     * @param array the array
     * @return the short[]
     */
    public static short[] toArray(Short[] array) {
        short[] newArray = new short[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[i].shortValue();
        }

        return newArray;
    }

    /**
     * To byte array.
     *
     * @param value the value
     * @return the byte[]
     */
    public static byte[] toByteArray(byte value) {
        return new byte[]{value};
    }

    /**
     * To byte array.
     *
     * @param value the value
     * @return the byte[]
     */
    public static byte[] toByteArray(int value) {
        return new byte[]{(byte) (value >>> 24), (byte) (value >>> 16), (byte) (value >>> 8), (byte) value};
    }

    /**
     * To int.
     *
     * @param b the b
     * @return the int
     */
    public static int toInt(byte[] b) {
        return (b[0] << 24) + ((b[1] & 0xFF) << 16) + ((b[2] & 0xFF) << 8) + (b[3] & 0xFF);
    }

    /**
     * To int array.
     *
     * @param array the array
     * @return the int[]
     */
    public static int[] toIntArray(String[] array) {
        int[] newArray = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = Integer.parseInt(array[i].trim());
        }

        return newArray;
    }

    /**
     * To long array.
     *
     * @param array the array
     * @return the long[]
     */
    public static Long[] toLongArray(Object[] array) {
        Long[] newArray = new Long[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = (Long) array[i];
        }

        return newArray;
    }

    /**
     * To long array.
     *
     * @param array the array
     * @return the long[]
     */
    public static long[] toLongArray(String[] array) {
        long[] newArray = new long[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = Long.parseLong(array[i].trim());
        }

        return newArray;
    }

    /**
     * To string array.
     *
     * @param array the array
     * @return the string[]
     */
    public static String[] toStringArray(boolean[] array) {
        String[] newArray = new String[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = String.valueOf(array[i]);
        }

        return newArray;
    }

    /**
     * To string array.
     *
     * @param array the array
     * @return the string[]
     */
    public static String[] toStringArray(byte[] array) {
        String[] newArray = new String[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = String.valueOf(array[i]);
        }

        return newArray;
    }

    /**
     * To string array.
     *
     * @param array the array
     * @return the string[]
     */
    public static String[] toStringArray(char[] array) {
        String[] newArray = new String[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = String.valueOf(array[i]);
        }

        return newArray;
    }

    /**
     * To string array.
     *
     * @param array      the array
     * @param dateFormat the date format
     * @return the string[]
     */
    public static String[] toStringArray(Date[] array, DateFormat dateFormat) {
        String[] newArray = new String[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = dateFormat.format(array[i]);
        }

        return newArray;
    }

    /**
     * To string array.
     *
     * @param array the array
     * @return the string[]
     */
    public static String[] toStringArray(double[] array) {
        String[] newArray = new String[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = String.valueOf(array[i]);
        }

        return newArray;
    }

    /**
     * To string array.
     *
     * @param array the array
     * @return the string[]
     */
    public static String[] toStringArray(float[] array) {
        String[] newArray = new String[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = String.valueOf(array[i]);
        }

        return newArray;
    }

    /**
     * To string array.
     *
     * @param array the array
     * @return the string[]
     */
    public static String[] toStringArray(int[] array) {
        String[] newArray = new String[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = String.valueOf(array[i]);
        }

        return newArray;
    }

    /**
     * To string array.
     *
     * @param array the array
     * @return the string[]
     */
    public static String[] toStringArray(long[] array) {
        String[] newArray = new String[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = String.valueOf(array[i]);
        }

        return newArray;
    }

    /**
     * To string array.
     *
     * @param array the array
     * @return the string[]
     */
    public static String[] toStringArray(Object[] array) {
        String[] newArray = new String[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = String.valueOf(array[i]);
        }

        return newArray;
    }

    /**
     * To string array.
     *
     * @param array the array
     * @return the string[]
     */
    public static String[] toStringArray(short[] array) {
        String[] newArray = new String[array.length];

        for (int i = 0; i < array.length; i++) {
            newArray[i] = String.valueOf(array[i]);
        }

        return newArray;
    }
}
