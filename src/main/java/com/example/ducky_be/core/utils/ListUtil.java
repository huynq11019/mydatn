/*
 * ListUtil.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;


import org.apache.commons.collections4.list.UnmodifiableList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The Class ListUtil.
 */
public class ListUtil {

    /**
     * The Constant FIRST_INDEX.
     */
    public static final int FIRST_INDEX = 0;

    /**
     * Copy.
     *
     * @param <T>    the generic type
     * @param master the master
     * @return the list
     */
    public static <T> List<T> copy(List<T> master) {
        if (master == null) {
            return null;
        }

        return new ArrayList<>(master);
    }

    /**
     * Copy.
     *
     * @param <T>    the generic type
     * @param master the master
     * @param copy   the copy
     */
    public static <T> void copy(List<T> master, List<T> copy) {
        if (master == null || copy == null) {
            return;
        }

        copy.clear();

        Iterator<T> itr = master.iterator();

        while (itr.hasNext()) {
            T obj = itr.next();

            copy.add(obj);
        }
    }

    /**
     * Distinct.
     *
     * @param <T>  the generic type
     * @param list the list
     */
    public static <T> void distinct(List<T> list) {
        distinct(list, null);
    }

    /**
     * Distinct.
     *
     * @param <T>        the generic type
     * @param list       the list
     * @param comparator the comparator
     */
    public static <T> void distinct(List<T> list, Comparator<T> comparator) {
        if (list == null || list.size() == 0) {
            return;
        }

        Set<T> set = null;

        if (comparator == null) {
            set = new TreeSet<>();
        } else {
            set = new TreeSet<>(comparator);
        }

        Iterator<T> itr = list.iterator();

        while (itr.hasNext()) {
            T obj = itr.next();

            if (set.contains(obj)) {
                itr.remove();
            } else {
                set.add(obj);
            }
        }
    }

    /**
     * From array.
     *
     * @param array the array
     * @return the list
     */
    public static List<Integer> fromArray(int[] array) {
        if (array == null || array.length == 0) {
            return new ArrayList<>();
        }

        List<Integer> list = new ArrayList<>(array.length);

        for (int element : array) {
            list.add(element);
        }

        return list;
    }

    /**
     * From array.
     *
     * @param <T>   the generic type
     * @param array the array
     * @return the list
     */
    public static <T> List<T> fromArray(T[] array) {
        if (array == null || array.length == 0) {
            return new ArrayList<>();
        }

        List<T> list = new ArrayList<>(array.length);

        for (T element : array) {
            list.add(element);
        }

        return list;
    }

    /**
     * From collection.
     *
     * @param <T> the generic type
     * @param c   the c
     * @return the list
     */
    public static <T> List<T> fromCollection(Collection<T> c) {
        if (c != null && c instanceof List) {
            return (List<T>) c;
        }

        if (c == null || c.size() == 0) {
            return new ArrayList<>();
        }

        List<T> list = new ArrayList<>(c.size());

        Iterator<T> itr = c.iterator();

        while (itr.hasNext()) {
            list.add(itr.next());
        }

        return list;
    }

    /**
     * From enumeration.
     *
     * @param <T> the generic type
     * @param enu the enu
     * @return the list
     */
    public static <T> List<T> fromEnumeration(Enumeration<T> enu) {
        List<T> list = new ArrayList<>();

        while (enu.hasMoreElements()) {
            T obj = enu.nextElement();

            list.add(obj);
        }

        return list;
    }

    /**
     * From file.
     *
     * @param file the file
     * @return the list
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static List<String> fromFile(File file) throws IOException {
        List<String> list = new ArrayList<>();

        BufferedReader br = new BufferedReader(new FileReader(file));

        String s = StringPool.BLANK;

        while ((s = br.readLine()) != null) {
            list.add(s);
        }

        br.close();

        return list;
    }

    /**
     * From file.
     *
     * @param fileName the file name
     * @return the list
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static List<String> fromFile(String fileName) throws IOException {
        return fromFile(new File(fileName));
    }

    /**
     * From string.
     *
     * @param s the s
     * @return the list
     */
    public static List<String> fromString(String s) {
        return fromArray(StringUtil.split(s, StringPool.NEW_LINE));
    }

    /**
     * Random.
     *
     * @param <T>  the generic type
     * @param list the list
     * @return the t
     */
    public static <T> T random(List<T> list) {
        if (Validator.isNull(list)) {
            return null;
        }

        int index = new Random().nextInt(list.size());

        return list.get(index);
    }

    /**
     * Removes the first.
     *
     * @param <T>  the generic type
     * @param list the list
     * @return the list
     */
    public static <T> List<T> removeFirst(List<T> list) {
        if (Validator.isNotNull(list)) {
            list.remove(FIRST_INDEX);
        }

        return list;
    }

    /**
     * Removes the first element.
     *
     * @param <T>     the generic type
     * @param list    the list
     * @param element the element
     * @return the list
     */
    public static <T> List<T> removeFirstElement(List<T> list, T element) {
        int firstIndex = list.indexOf(element);

        if (firstIndex >= 0) {
            list.remove(firstIndex);
        }

        return list;
    }

    /**
     * Removes the last.
     *
     * @param <T>  the generic type
     * @param list the list
     * @return the list
     */
    public static <T> List<T> removeLast(List<T> list) {
        if (Validator.isNotNull(list)) {
            list.remove(list.size() - 1);
        }

        return list;
    }

    /**
     * Removes the last element.
     *
     * @param <T>     the generic type
     * @param list    the list
     * @param element the element
     * @return the list
     */
    public static <T> List<T> removeLastElement(List<T> list, T element) {
        int lastIndex = list.lastIndexOf(element);

        if (lastIndex >= 0) {
            list.remove(lastIndex);
        }

        return list;
    }

    /**
     * Sort.
     *
     * @param <T>  the generic type
     * @param list the list
     * @return the list
     */
    public static <T> List<T> sort(List<T> list) {
        return sort(list, null);
    }

    /**
     * Sort.
     *
     * @param <T>        the generic type
     * @param list       the list
     * @param comparator the comparator
     * @return the list
     */
    public static <T> List<T> sort(List<T> list, Comparator<T> comparator) {
        if (list instanceof UnmodifiableList) {
            list = copy(list);
        }

        Collections.sort(list, comparator);

        return list;
    }

    /**
     * Sub list.
     *
     * @param <T>   the generic type
     * @param list  the list
     * @param start the start
     * @param end   the end
     * @return the list
     */
    public static <T> List<T> subList(List<T> list, int start, int end) {
        List<T> newList = new ArrayList<>();

        int normalizedSize = list.size() - 1;

        if (start < 0 || start > normalizedSize || end < 0 || start > end) {

            return newList;
        }

        for (int i = start; i < end && i <= normalizedSize; i++) {
            newList.add(list.get(i));
        }

        return newList;
    }

    /**
     * To list.
     *
     * @param array the array
     * @return the list
     */
    public static List<Boolean> toList(boolean[] array) {
        if (array == null || array.length == 0) {
            return Collections.emptyList();
        }

        List<Boolean> list = new ArrayList<>(array.length);

        for (boolean value : array) {
            list.add(value);
        }

        return list;
    }

    /**
     * To list.
     *
     * @param array the array
     * @return the list
     */
    public static List<Boolean> toList(Boolean[] array) {
        if (array == null || array.length == 0) {
            return Collections.emptyList();
        }

        List<Boolean> list = new ArrayList<>(array.length);

        for (Boolean value : array) {
            list.add(value);
        }

        return list;
    }

    /**
     * To list.
     *
     * @param array the array
     * @return the list
     */
    public static List<Double> toList(double[] array) {
        if (array == null || array.length == 0) {
            return Collections.emptyList();
        }

        List<Double> list = new ArrayList<>(array.length);

        for (double value : array) {
            list.add(value);
        }

        return list;
    }

    /**
     * To list.
     *
     * @param array the array
     * @return the list
     */
    public static List<Double> toList(Double[] array) {
        if (array == null || array.length == 0) {
            return Collections.emptyList();
        }

        List<Double> list = new ArrayList<>(array.length);

        for (Double value : array) {
            list.add(value);
        }

        return list;
    }

    /**
     * To list.
     *
     * @param array the array
     * @return the list
     */
    public static List<Float> toList(float[] array) {
        if (array == null || array.length == 0) {
            return Collections.emptyList();
        }

        List<Float> list = new ArrayList<>(array.length);

        for (float value : array) {
            list.add(value);
        }

        return list;
    }

    /**
     * To list.
     *
     * @param array the array
     * @return the list
     */
    public static List<Float> toList(Float[] array) {
        if (array == null || array.length == 0) {
            return Collections.emptyList();
        }

        List<Float> list = new ArrayList<>(array.length);

        for (Float value : array) {
            list.add(value);
        }

        return list;
    }

    /**
     * To list.
     *
     * @param array the array
     * @return the list
     */
    public static List<Integer> toList(int[] array) {
        if (array == null || array.length == 0) {
            return Collections.emptyList();
        }

        List<Integer> list = new ArrayList<>(array.length);

        for (int value : array) {
            list.add(value);
        }

        return list;
    }

    /**
     * To list.
     *
     * @param array the array
     * @return the list
     */
    public static List<Integer> toList(Integer[] array) {
        if (array == null || array.length == 0) {
            return Collections.emptyList();
        }

        List<Integer> list = new ArrayList<>(array.length);

        for (Integer value : array) {
            list.add(value);
        }

        return list;
    }

    /**
     * To list.
     *
     * @param array the array
     * @return the list
     */
    public static List<Long> toList(long[] array) {
        if (array == null || array.length == 0) {
            return Collections.emptyList();
        }

        List<Long> list = new ArrayList<>(array.length);

        for (long value : array) {
            list.add(value);
        }

        return list;
    }

    /**
     * To list.
     *
     * @param array the array
     * @return the list
     */
    public static List<Long> toList(Long[] array) {
        if (array == null || array.length == 0) {
            return Collections.emptyList();
        }

        List<Long> list = new ArrayList<>(array.length);

        for (Long value : array) {
            list.add(value);
        }

        return list;
    }

    /**
     * To list.
     *
     * @param array the array
     * @return the list
     */
    public static List<Short> toList(short[] array) {
        if (array == null || array.length == 0) {
            return Collections.emptyList();
        }

        List<Short> list = new ArrayList<>(array.length);

        for (short value : array) {
            list.add(value);
        }

        return list;
    }

    /**
     * To list.
     *
     * @param array the array
     * @return the list
     */
    public static List<Short> toList(Short[] array) {
        if (array == null || array.length == 0) {
            return Collections.emptyList();
        }

        List<Short> list = new ArrayList<>(array.length);

        for (Short value : array) {
            list.add(value);
        }

        return list;
    }

    /**
     * To list.
     *
     * @param array the array
     * @return the list
     */
    public static List<String> toList(String[] array) {
        if (array == null || array.length == 0) {
            return Collections.emptyList();
        }

        List<String> list = new ArrayList<>(array.length);

        for (String value : array) {
            list.add(value.trim());
        }

        return list;
    }

    /**
     * To list integer.
     *
     * @param list string
     * @return the list
     */
    public static List<Integer> toListInteger(List<String> list) {
        if (list == null || list.size() == 0) {
            return Collections.emptyList();
        }
        return list.stream().map(Integer::parseInt).collect(Collectors.toList());
    }

    /**
     * To list long.
     *
     * @param str        the str
     * @param -delimiter the delimiter
     * @return the list
     */
    public static List<Integer> toListInteger(String str) {
        if (str == null || str.trim().isEmpty()) {
            return null;
        }

        String[] arrStr = StringUtil.split(str);
        if (arrStr == null || arrStr.length == 0) {
            return null;
        }

        return toListInteger(toList(arrStr));
    }

    /**
     * To list long.
     *
     * @param str        the str
     * @param -delimiter the delimiter
     * @return the list
     */
    public static List<Long> toListLong(String str) {
        if (str == null || str.trim().isEmpty()) {
            return null;
        }

        String[] arrStr = StringUtil.split(str);
        if (arrStr == null || arrStr.length == 0) {
            return null;
        }

        return toListLong(toList(arrStr));
    }

    /**
     * To list Long.
     *
     * @param list string
     * @return the list
     */
    public static List<Long> toListLong(List<String> list) {
        if (list == null || list.size() == 0) {
            return Collections.emptyList();
        }
        return list.stream().map(Long::parseLong).collect(Collectors.toList());
    }
}
