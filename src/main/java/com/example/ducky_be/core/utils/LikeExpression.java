/*
 * LikeExpression.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.spi.TypedValue;

/**
 * The Class LikeExpression.
 */
public class LikeExpression implements Criterion {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The escape char.
     */
    private final Character escapeChar;

    /**
     * The property name.
     */
    private final String propertyName;

    /**
     * The value.
     */
    private final String value;

    /**
     * Instantiates a new like expression.
     *
     * @param propertyName the property name
     * @param value        the value
     */
    protected LikeExpression(String propertyName, Object value) {
        this(propertyName, value.toString(), (Character) null);
    }

    /**
     * Instantiates a new like expression.
     *
     * @param propertyName the property name
     * @param value        the value
     * @param escapeChar   the escape char
     */
    protected LikeExpression(String propertyName, String value, Character escapeChar) {
        this.propertyName = propertyName;
        this.value = value;
        this.escapeChar = escapeChar;
    }

    /**
     * Instantiates a new like expression.
     *
     * @param propertyName the property name
     * @param value        the value
     * @param matchMode    the match mode
     */
    protected LikeExpression(String propertyName, String value, MatchMode matchMode) {
        this(propertyName, matchMode.toMatchString(
                value.toString().replaceAll("!", "!!").replaceAll("%", "!%").replaceAll("_", "!_")), '!');
    }

    /*
     * (non-Javadoc)
     * @see org.hibernate.criterion.Criterion#getTypedValues(org.hibernate.Criteria,
     * org.hibernate.criterion.CriteriaQuery)
     */
    @Override
    public TypedValue[] getTypedValues(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {

        return new TypedValue[]{criteriaQuery.getTypedValue(criteria, this.propertyName, this.typedValue(this.value)),
                criteriaQuery.getTypedValue(criteria, this.propertyName, this.escapeChar.toString())};
    }

    /**
     * Lhs.
     *
     * @param dialect the dialect
     * @param column  the column
     * @return the string
     */
    protected String lhs(Dialect dialect, String column) {
        return column;
    }

    /*
     * (non-Javadoc)
     * @see org.hibernate.criterion.Criterion#toSqlString(org.hibernate.Criteria, org.hibernate.criterion.CriteriaQuery)
     */
    @Override
    @SuppressWarnings("deprecation")
    public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
        Dialect dialect = criteriaQuery.getFactory().getDialect();

        String[] columns = criteriaQuery.getColumnsUsingProjection(criteria, this.propertyName);

        if (columns.length != 1) {
            throw new HibernateException("Like may only be used with single-column properties");
        }

        String lhs = this.lhs(dialect, columns[0]);

        return lhs + " like ?" + (this.escapeChar == null ? "" : " escape ?");

    }

    /**
     * Typed value.
     *
     * @param value the value
     * @return the string
     */
    protected String typedValue(String value) {
        return value;
    }
}
