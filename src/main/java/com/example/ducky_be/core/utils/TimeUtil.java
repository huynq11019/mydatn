/*
 * TimeUtil.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;

import java.sql.Timestamp;
import java.text.Format;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * The Class TimeUtil.
 */
public class TimeUtil {

    /**
     * The Constant SECOND.
     */
    public static final long SECOND = 1000;

    /**
     * The Constant MINUTE.
     */
    public static final long MINUTE = SECOND * 60;

    /**
     * The Constant HOUR.
     */
    public static final long HOUR = MINUTE * 60;

    /**
     * The Constant DAY.
     */
    public static final long DAY = HOUR * 24;

    /**
     * The Constant MONTH.
     */
    public static final long MONTH = DAY * 30;

    /**
     * The Constant RFC822_FORMAT.
     */
    public static final String RFC822_FORMAT = "EEE, dd MMM yyyy HH:mm:ss Z";

    /**
     * The Constant SHORT_TIMESTAMP_FORMAT.
     */
    // public static final String SHORT_TIMESTAMP_FORMAT = "yyyyMMddkkmm";
    public static final String SHORT_TIMESTAMP_FORMAT = "yyyyMMdd";

    /**
     * The Constant TIMESTAMP_FORMAT.
     */
    public static final String TIMESTAMP_FORMAT = "yyyyMMddkkmmssSSS";

    /**
     * The Constant WEEK.
     */
    public static final long WEEK = DAY * 7;

    /**
     * The Constant YEAR.
     */
    public static final long YEAR = DAY * 365;

    /**
     * Gets the date.
     *
     * @param cal the cal
     * @return the date
     */
    public static Date getDate(Calendar cal) {
        Calendar adjustedCal = Calendar.getInstance();

        adjustedCal.set(Calendar.YEAR, cal.get(Calendar.YEAR));
        adjustedCal.set(Calendar.MONTH, cal.get(Calendar.MONTH));
        adjustedCal.set(Calendar.DATE, cal.get(Calendar.DATE));
        adjustedCal.set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY));
        adjustedCal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE));
        adjustedCal.set(Calendar.SECOND, cal.get(Calendar.SECOND));
        adjustedCal.set(Calendar.MILLISECOND, cal.get(Calendar.MILLISECOND));

        return adjustedCal.getTime();
    }

    /**
     * Gets the date.
     *
     * @param date the date
     * @param tz   the tz
     * @return the date
     */
    public static Date getDate(Date date, TimeZone tz) {
        Calendar cal = Calendar.getInstance(tz);

        cal.setTime(date);

        return getDate(cal);
    }

    /**
     * Gets the date.
     *
     * @param tz the tz
     * @return the date
     */
    public static Date getDate(TimeZone tz) {
        Calendar cal = Calendar.getInstance(tz);

        return getDate(cal);
    }

    /**
     * Gets the description.
     *
     * @param milliseconds the milliseconds
     * @return the description
     */
    public static String getDescription(long milliseconds) {
        return getDescription(milliseconds, false);
    }

    /**
     * Gets the description.
     *
     * @param milliseconds the milliseconds
     * @param approximate  the approximate
     * @return the description
     */
    public static String getDescription(long milliseconds, boolean approximate) {

        String s = StringPool.BLANK;

        int x = 0;

        if (approximate) {
            if (milliseconds <= 0) {
                s = "0 Seconds";
            } else if (milliseconds < MINUTE) {
                x = (int) (milliseconds / SECOND);

                s = x + " Second";
            } else if (milliseconds < HOUR) {
                x = (int) (milliseconds / MINUTE);

                s = x + " Minute";
            } else if (milliseconds < DAY) {
                x = (int) (milliseconds / HOUR);

                s = x + " Hour";
            } else if (milliseconds < MONTH) {
                x = (int) (milliseconds / DAY);

                s = x + " Day";
            } else if (milliseconds < YEAR) {
                x = (int) (milliseconds / MONTH);

                s = x + " Month";
            } else {
                x = (int) (milliseconds / YEAR);

                s = x + " Year";
            }
        } else if (milliseconds % WEEK == 0) {
            x = (int) (milliseconds / WEEK);

            s = x + " Week";
        } else if (milliseconds % DAY == 0) {
            x = (int) (milliseconds / DAY);

            s = x + " Day";
        } else if (milliseconds % HOUR == 0) {
            x = (int) (milliseconds / HOUR);

            s = x + " Hour";
        } else if (milliseconds % MINUTE == 0) {
            x = (int) (milliseconds / MINUTE);

            s = x + " Minute";
        } else if (milliseconds % SECOND == 0) {
            x = (int) (milliseconds / SECOND);

            s = x + " Second";
        } else {
            x = (int) milliseconds;

            s = x + " Millisecond";
        }

        if (x > 1) {
            s += "s";
        }

        return s;
    }

    /**
     * Gets the rfc822.
     *
     * @return the rfc822
     */
    public static String getRFC822() {
        return getRFC822(new Date());
    }

    /**
     * Gets the rfc822.
     *
     * @param date the date
     * @return the rfc822
     */
    public static String getRFC822(Date date) {
        return getSimpleDate(date, RFC822_FORMAT);
    }

    /**
     * Gets the short timestamp.
     *
     * @return the short timestamp
     */
    public static String getShortTimestamp() {
        return getShortTimestamp(new Date());
    }

    /**
     * Gets the short timestamp.
     *
     * @param date the date
     * @return the short timestamp
     */
    public static String getShortTimestamp(Date date) {
        return getSimpleDate(date, SHORT_TIMESTAMP_FORMAT);
    }

    /**
     * Gets the simple date.
     *
     * @param date   the date
     * @param format the format
     * @return the simple date
     */
    public static String getSimpleDate(Date date, String format) {
        String s = StringPool.BLANK;

        if (date != null) {
            Format dateFormat = FastDateFormatFactoryUtil.getSimpleDateFormat(format);

            s = dateFormat.format(date);
        }

        return s;
    }

    /**
     * Gets the sql timestamp.
     *
     * @return the sql timestamp
     */
    public static Timestamp getSqlTimestamp() {
        Date date = new Date();

        return new Timestamp(date.getTime());
    }

    /**
     * Gets the sql timestamp.
     *
     * @param date the date
     * @return the sql timestamp
     */
    public static Timestamp getSqlTimestamp(Date date) {

        return new Timestamp(date.getTime());
    }

    /**
     * Gets the timestamp.
     *
     * @return the timestamp
     */
    public static String getTimestamp() {
        return getTimestamp(new Date());
    }

    /**
     * Gets the timestamp.
     *
     * @param date the date
     * @return the timestamp
     */
    public static String getTimestamp(Date date) {
        return getSimpleDate(date, TIMESTAMP_FORMAT);
    }
}
