package com.example.ducky_be.core.utils;

import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class PasswordUtil {

    /**
     * The Constant passwordEncoder.
     */
    private static final SCryptPasswordEncoder passwordEncoder = new SCryptPasswordEncoder();

    /**
     * Encode password.
     *
     * @param password the password
     * @param userId   the user id
     * @return the string
     */
    public static String encodePassword(String password, Long userId) {
        return encodePassword(password, userId.toString());
    }

    /**
     * Encode password.
     *
     * @param password the password
     * @param userId   the user id
     * @return the string
     */
    public static String encodePassword(String password, String userId) {
        return passwordEncoder.encode(password);
    }

}
