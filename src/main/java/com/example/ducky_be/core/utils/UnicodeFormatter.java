/*
 * UnicodeFormatter.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The Class UnicodeFormatter.
 */
public class UnicodeFormatter {

    /**
     * The hex digit.
     */
    public static char HEX_DIGIT[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /**
     * The log.
     */
    private static Logger _log = LogManager.getLogger(UnicodeFormatter.class);

    /**
     * Byte to hex.
     *
     * @param b the b
     * @return the string
     */
    public static String byteToHex(byte b) {
        char[] array = {HEX_DIGIT[b >> 4 & 0x0f], HEX_DIGIT[b & 0x0f]};

        return new String(array);
    }

    /**
     * Char to hex.
     *
     * @param c the c
     * @return the string
     */
    public static String charToHex(char c) {
        byte hi = (byte) (c >>> 8);
        byte lo = (byte) (c & 0xff);

        return byteToHex(hi) + byteToHex(lo);
    }

    /**
     * Parses the string.
     *
     * @param hexString the hex string
     * @return the string
     */
    public static String parseString(String hexString) {
        StringBuilder sb = new StringBuilder();

        char[] array = hexString.toCharArray();

        if (array.length % 6 != 0) {
            _log.error("String is not in hex format");

            return hexString;
        }

        for (int i = 2; i < hexString.length(); i = i + 6) {
            String s = hexString.substring(i, i + 4);

            try {
                char c = (char) Integer.parseInt(s, 16);

                sb.append(c);
            } catch (Exception e) {
                _log.error(e, e);

                return hexString;
            }
        }

        return sb.toString();
    }

    /**
     * To string.
     *
     * @param array the array
     * @return the string
     */
    public static String toString(char[] array) {
        StringBuilder sb = new StringBuilder();

        for (char element : array) {
            sb.append("\\u");
            sb.append(charToHex(element));
        }

        return sb.toString();
    }

    /**
     * To string.
     *
     * @param s the s
     * @return the string
     */
    public static String toString(String s) {
        if (s == null) {
            return null;
        }

        return toString(s.toCharArray());
    }

}
