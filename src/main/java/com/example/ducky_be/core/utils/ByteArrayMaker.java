/*
 * ByteArrayMaker.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;

import java.io.ByteArrayOutputStream;

/**
 * The Class ByteArrayMaker.
 */
public class ByteArrayMaker extends ByteArrayOutputStream {

    /**
     * The collect.
     */
    static boolean collect = false;

    /**
     * The default init size.
     */
    static int defaultInitSize = 8000;

    /**
     * The stats.
     */
    static MakerStats stats = null;

    static {
        String collectString = System.getProperty(MakerStats.class.getName());

        if (collectString != null && collectString.equals("true")) {
            collect = true;
        }
    }

    static {
        if (collect) {
            stats = new MakerStats(ByteArrayMaker.class.toString());
        }
    }

    static {
        String defaultInitSizeString = System.getProperty(ByteArrayMaker.class.getName() + ".initial.size");

        if (defaultInitSizeString != null) {
            try {
                defaultInitSize = Integer.parseInt(defaultInitSizeString);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Gets the statistics.
     *
     * @return the statistics
     */
    public static MakerStats getStatistics() {
        return stats;
    }

    /**
     * The caller.
     */
    private String _caller;

    /**
     * The init size.
     */
    private int _initSize;

    /**
     * Instantiates a new byte array maker.
     */
    public ByteArrayMaker() {
        super(defaultInitSize);

        if (collect) {
            this._getInfo(new Throwable());
        }
    }

    /**
     * Instantiates a new byte array maker.
     *
     * @param size the size
     */
    public ByteArrayMaker(int size) {
        super(size);

        if (collect) {
            this._getInfo(new Throwable());
        }
    }

    /**
     * Gets the info.
     *
     * @param t the t
     */
    private void _getInfo(Throwable t) {
        this._initSize = this.buf.length;

        StackTraceElement[] elements = t.getStackTrace();

        if (elements.length > 1) {
            StackTraceElement el = elements[1];

            this._caller = el.getClassName()
                    + StringPool.PERIOD
                    + el.getMethodName()
                    + StringPool.COLON
                    + el.getLineNumber();
        }
    }

    /*
     * (non-Javadoc)
     * @see java.io.ByteArrayOutputStream#toByteArray()
     */
    @Override
    public byte[] toByteArray() {
        if (collect) {
            stats.add(this._caller, this._initSize, this.count);
        }

        return super.toByteArray();
    }

    /*
     * (non-Javadoc)
     * @see java.io.ByteArrayOutputStream#toString()
     */
    @Override
    public String toString() {
        return super.toString();
    }

}
