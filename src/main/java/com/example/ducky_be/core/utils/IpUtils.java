/*
 * IpUtils.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class IpUtils.
 */
public class IpUtils {

    /**
     * The Constant INTEGER_VALUE_0.
     */
    public static final int INTEGER_VALUE_0 = 0;

    /**
     * The Constant INTEGER_VALUE_1.
     */
    public static final int INTEGER_VALUE_1 = 1;

    /**
     * The Constant INTEGER_VALUE_2.
     */
    public static final int INTEGER_VALUE_2 = 2;

    /**
     * The Constant INTEGER_VALUE_32.
     */
    public static final int INTEGER_VALUE_32 = 32;

    /**
     * The Constant INVALID.
     */
    public static final int INVALID = 1;

    /**
     * The Constant INVALID_CHARACTER.
     */
    public static final int INVALID_CHARACTER = 2;

    /**
     * The Constant INVALID_FORMAT.
     */
    public static final int INVALID_FORMAT = 3;

    /**
     * The Constant IS_VALID.
     */
    public static final int IS_VALID = 0;

    /**
     * The Constant STRING_ARRAY_32_BIT_1.
     */
    public static final String STRING_ARRAY_32_BIT_1 = "11111111111111111111111111111111";

    /**
     * The Constant _log.
     */
    private static final Logger _log = LogManager.getLogger(IpUtils.class);

    /**
     * Convert decima to binary.
     *
     * @param subnet the subnet
     * @return the string
     */
    public static String convertDecimaToBinary(Integer subnet) {
        String result = Integer.toBinaryString(subnet);
        return result;
    }

    /**
     * Gets the valid range of IP.
     *
     * @param ipOfSubadmin the ip of subadmin
     * @return the valid range of IP
     */
    private static Integer getValidRangeOfIP(String ipOfSubadmin) {
        int maximumHosts = 0;
        try {
            String[] parseIP = ipOfSubadmin.split("/");

            // Get subnet
            String subnet = parseIP[1];

            // Get number of bits for hosts.
            Integer numberOfBitForHostAddr = INTEGER_VALUE_32 - Integer.valueOf(subnet);

            // Define maximum bits for host.
            String maximumBitsForHost = STRING_ARRAY_32_BIT_1.substring(INTEGER_VALUE_0, numberOfBitForHostAddr);

            // Convert binary value to decimal value
            maximumHosts = Integer.parseInt(maximumBitsForHost, INTEGER_VALUE_2) - INTEGER_VALUE_1;

        } catch (Exception e) {
            _log.error("BigKen ERROR: " + e);
        }
        return maximumHosts;
    }

    /**
     * Checks if is valid characters.
     *
     * @param ipRegex the ip regex
     * @return true, if is valid characters
     */
    public static boolean isValidCharacters(String ipRegex) {
        return validateCharacters(ipRegex) == IS_VALID;
    }

    /**
     * Checks if is valid client ip.
     *
     * @param ip the ip
     * @return true, if is valid client ip
     */
    public static boolean isValidClientIp(String ip) {
        if (!IpUtils.isValidCharacters(ip)) {
            return false;
        }
        return validateClientIp(ip) == IS_VALID;
    }

    /**
     * Checks if is valid ip.
     *
     * @param ip the ip
     * @return true, if is valid ip
     */
    public static boolean isValidIp(String ip) {
        if (!IpUtils.isValidCharacters(ip)) {
            return false;
        }
        return IpUtils.isValidIpRegex(ip);
    }

    /**
     * Checks if is valid ip regex.
     *
     * @param ipRegex the ip regex
     * @return true, if is valid ip regex
     */
    public static boolean isValidIpRegex(String ipRegex) {
        return validateIpRegex(ipRegex) == IS_VALID;
    }

    /**
     * Match allow ip.
     *
     * @param ip      the ip
     * @param ipRegex the ip regex
     * @return the int
     */
    public static int matchAllowIp(String ip, String ipRegex) {
        if (ip == null || ip.length() < 1 || ipRegex == null || ipRegex.length() < 1) {
            return INVALID_FORMAT;
        }
        int iResult = validateCharacters(ipRegex);
        if (iResult != IS_VALID) {
            return iResult;
        }
        String[] ipOfUser = ipRegex.split("\\|");
        for (String regex : ipOfUser) {
            String[] octets = regex.split("\\.");
            String[] ots = ip.split("\\.");
            if (matchOctet(ots[0], octets[0]) == IS_VALID
                    && matchOctet(ots[1], octets[1]) == IS_VALID
                    && matchOctet(ots[2], octets[2]) == IS_VALID
                    && matchOctet(ots[3], octets[3]) == IS_VALID) {
                return IS_VALID;
            }
        }
        return INVALID;
    }

    /**
     * Match ip.
     *
     * @param ip      the ip
     * @param ipRegex the ip regex
     * @return the int
     */
    public static int matchIp(String ip, String ipRegex) {
        if (ip == null || ip.length() < 1 || ipRegex == null || ipRegex.length() < 1) {
            return INVALID_FORMAT;
        }
        int iResult = validateIpRegex(ipRegex);
        if (iResult != IS_VALID) {
            return iResult;
        }
        String[] ipOfUser = ipRegex.split("\\|");
        for (String regex : ipOfUser) {
            String[] octets = regex.split("\\.");
            String[] ots = ip.split("\\.");
            if (octets.length != 4 || ots.length != 4) {
                return INVALID_FORMAT;
            }
            if ((ots[0] + ots[1] + ots[2]).equalsIgnoreCase(octets[0] + octets[1] + octets[2])
                    && matchOctet(ots[3], octets[3]) == IS_VALID) {
                return IS_VALID;
            }
        }
        return INVALID;
    }

    /**
     * Match issue ip.
     *
     * @param ipRegex      the ip regex
     * @param issueIpRegex the issue ip regex
     * @return the int
     */
    public static int matchIssueIp(String ipRegex, String issueIpRegex) {
        int iRet = validateIpRegex(ipRegex);
        if (iRet != IS_VALID) {
            return iRet;
        }
        iRet = validateIpRegex(issueIpRegex);
        if (iRet != IS_VALID) {
            return iRet;
        }

        // Get list of user's IP.
        String[] ipOfUser = ipRegex.split("\\|");

        // Get list of sub-admin's IP.
        String[] ipOfSubadmin = issueIpRegex.split("\\|");

        for (String element : ipOfUser) {
            String anIpOfUser = element.trim();
            String[] arrayOctetOfIpOfUser = anIpOfUser.split("\\.");
            boolean flag = false;

            for (String element2 : ipOfSubadmin) {
                String anIpOfSubadmin = element2.trim();
                String[] arrayOctetOfIpOfSubadmin = anIpOfSubadmin.split("\\.");

                // Issue allowed sub-admin'IP by subnet index.
                if (anIpOfSubadmin.contains("/")) {
                    if (validateUserIp(arrayOctetOfIpOfUser, arrayOctetOfIpOfSubadmin)) {
                        flag = true;
                        break;
                    }

                    // Keeping follow before R6244
                } else if (matchIssueOctet(arrayOctetOfIpOfUser[0], arrayOctetOfIpOfSubadmin[0]) == IS_VALID
                        && matchIssueOctet(arrayOctetOfIpOfUser[1], arrayOctetOfIpOfSubadmin[1]) == IS_VALID
                        && matchIssueOctet(arrayOctetOfIpOfUser[2], arrayOctetOfIpOfSubadmin[2]) == IS_VALID
                        && matchIssueOctet(arrayOctetOfIpOfUser[3], arrayOctetOfIpOfSubadmin[3]) == IS_VALID) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                return INVALID;
            }
        }
        return IS_VALID;
    }

    /**
     * Match issue octet.
     *
     * @param octet      the octet
     * @param issueOctet the issue octet
     * @return the int
     */
    public static int matchIssueOctet(String octet, String issueOctet) {

        int iRet = validateOctetRegex(octet);
        if (iRet != IS_VALID) {
            return iRet;
        }
        iRet = validateOctetRegex(issueOctet);
        if (iRet != IS_VALID) {
            return iRet;
        }

        boolean[] flags = new boolean[256];
        Arrays.fill(flags, false);
        if ("*".equalsIgnoreCase(issueOctet)) {
            return IS_VALID;
        }
        if (issueOctet.charAt(0) == '[' && issueOctet.charAt(issueOctet.length() - 1) == ']') {
            issueOctet = issueOctet.substring(1, issueOctet.length() - 1);
            int pos = issueOctet.indexOf("-");
            if (pos >= 0) {
                String[] nums = issueOctet.split("-");
                try {
                    int begin = Integer.parseInt(nums[0]);
                    int end = Integer.parseInt(nums[1]);
                    for (int j = begin; j <= end; j++) {
                        flags[j] = true;
                    }
                } catch (Exception e) {
                    return INVALID;
                }
            } else {
                String[] nums = issueOctet.split(",");
                for (String num : nums) {
                    try {
                        int value = Integer.parseInt(num);
                        flags[value] = true;
                    } catch (Exception e) {
                        return INVALID;
                    }
                }
            }
        } else {
            try {
                int value = Integer.parseInt(issueOctet);
                flags[value] = true;
                // temp=value;

            } catch (Exception e) {
                return INVALID;
            }
        }

        if ("*".equalsIgnoreCase(octet)) {
            for (boolean flag : flags) {
                if (!flag) {
                    return INVALID;
                }
            }
        } else if (octet.charAt(0) == '[' && octet.charAt(octet.length() - 1) == ']') {
            octet = octet.substring(1, octet.length() - 1);
            int pos = octet.indexOf("-");
            if (pos >= 0) {
                String[] nums = octet.split("-");
                try {
                    int begin = Integer.parseInt(nums[0]);
                    int end = Integer.parseInt(nums[1]);

                    // if(temp!=Integer.MIN_VALUE){
                    //
                    // if(begin<=temp && temp<=end)
                    // return IS_VALID;
                    // }

                    for (int j = begin; j <= end; j++) {
                        if (!flags[j]) {
                            return INVALID;
                        }
                    }
                } catch (Exception e) {
                    return INVALID;
                }
            } else {
                String[] nums = octet.split(",");
                for (String num : nums) {
                    try {
                        int value = Integer.parseInt(num);
                        if (!flags[value]) {
                            return INVALID;
                        }
                    } catch (Exception e) {
                        return INVALID;
                    }
                }
            }
        } else {
            try {
                int value = Integer.parseInt(octet);
                if (!flags[value]) {
                    return INVALID;
                }
            } catch (Exception e) {
                return INVALID;
            }
        }
        return IS_VALID;
    }

    /**
     * Match octet.
     *
     * @param octet      the octet
     * @param octetRegex the octet regex
     * @return the int
     */
    public static int matchOctet(String octet, String octetRegex) {
        if ("*".equalsIgnoreCase(octetRegex)) {
            return IS_VALID;
        }
        if (octetRegex.charAt(0) == '[' && octetRegex.charAt(octetRegex.length() - 1) == ']') {
            octetRegex = octetRegex.substring(1, octetRegex.length() - 1);
            int pos = octetRegex.indexOf("-");
            if (pos >= 0) {
                String[] nums = octetRegex.split("-");
                try {
                    int begin = Integer.parseInt(nums[0]);
                    int end = Integer.parseInt(nums[1]);
                    int value = Integer.parseInt(octet);
                    if (value >= begin && value <= end) {
                        return IS_VALID;
                    }
                } catch (Exception e) {
                }
            } else {
                String[] nums = octetRegex.split(",");
                for (String num : nums) {
                    if (num.equalsIgnoreCase(octet)) {
                        return IS_VALID;
                    }
                }
            }
        } else if (octetRegex.equalsIgnoreCase(octet)) {
            return IS_VALID;
        }
        return INVALID;
    }

    /**
     * Validate characters.
     *
     * @param ipRegex the ip regex
     * @return the int
     */
    public static int validateCharacters(String ipRegex) {
        String expression = "^[0-9\\/\\.,*\\[\\]\\s*\\|-]*$";
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(ipRegex);
        return matcher.matches() ? IS_VALID : INVALID_CHARACTER;
    }

    /**
     * Validate client ip.
     *
     * @param clientIp the client ip
     * @return the int
     */
    public static int validateClientIp(String clientIp) {
        if (clientIp == null || clientIp.length() < 1) {
            return INVALID_FORMAT;
        }
        int iResult = validateCharacters(clientIp);
        if (iResult != IS_VALID) {
            return iResult;
        }
        if (clientIp.charAt(0) == '|' || clientIp.charAt(clientIp.length() - 1) == '|') {
            return INVALID_FORMAT;
        }
        String[] ipOfUser = clientIp.split("\\|");
        for (String regex : ipOfUser) {
            if (regex == null
                    || regex.length() < 1
                    || regex.charAt(0) == '.'
                    || regex.charAt(regex.length() - 1) == '.') {
                return INVALID_FORMAT;
            }
            String[] octets = regex.split("\\.");
            if (octets.length != 4) {
                return INVALID_FORMAT;
            }
            if (validateOctet(octets[0]) != IS_VALID
                    || validateOctet(octets[1]) != IS_VALID
                    || validateOctet(octets[2]) != IS_VALID
                    || validateOctet(octets[3]) != IS_VALID) {
                return INVALID_FORMAT;
            }
        }
        return IS_VALID;
    }

    /**
     * Validate ip regex.
     *
     * @param ipRegex the ip regex
     * @return the int
     */
    public static int validateIpRegex(String ipRegex) {
        if (ipRegex == null || ipRegex.length() < 1) {
            return INVALID_FORMAT;
        }
        int iResult = validateCharacters(ipRegex);
        if (iResult != IS_VALID) {
            return iResult;
        }
        if (ipRegex.charAt(0) == '|' || ipRegex.charAt(ipRegex.length() - 1) == '|') {
            return INVALID_FORMAT;
        }
        String[] ipOfUser = ipRegex.split("\\|");

        for (String element : ipOfUser) {
            String regex = element;
            regex = regex.trim();
            if (regex == null
                    || regex.length() < 1
                    || regex.charAt(0) == '.'
                    || regex.charAt(regex.length() - 1) == '.') {
                return INVALID_FORMAT;
            }
            String[] octets = regex.split("\\.");
            if ((octets.length != 4) || validateOctet(octets[0]) != IS_VALID
                    || validateOctet(octets[1]) != IS_VALID
                    || validateOctet(octets[2]) != IS_VALID) {
                return INVALID_FORMAT;
            }
            if (validateOctetRegex(octets[3]) != IS_VALID) {
                return INVALID_FORMAT;
            }
        }
        return IS_VALID;
    }

    /**
     * Validate octet.
     *
     * @param octet the octet
     * @return the int
     */
    public static int validateOctet(String octet) {
        if (octet == null || octet.length() == 0 || octet.length() > 1 && octet.charAt(0) == '0') {
            return INVALID_FORMAT;
        }

        int value = -1;
        try {
            value = Integer.parseInt(octet);
        } catch (Exception e) {
            return INVALID_FORMAT;
        }

        if (value < 0 || value > 255) {
            return INVALID;
        }
        return IS_VALID;
    }

    /**
     * Validate octet regex.
     *
     * @param octet the octet
     * @return the int
     */
    public static int validateOctetRegex(String octet) {
        if (octet == null || octet.length() < 1) {
            return INVALID_FORMAT;
        }

        if (!octet.equalsIgnoreCase("*")) {
            if (octet.charAt(0) == '[' && octet.charAt(octet.length() - 1) == ']') {
                octet = octet.substring(1, octet.length() - 1);
                if (octet.length() < 1) {
                    return INVALID_FORMAT;
                }
                int pos = octet.indexOf("-");
                if (pos >= 0) {
                    if (octet.charAt(0) == '-' || octet.charAt(octet.length() - 1) == '-') {
                        return INVALID_FORMAT;
                    }
                    String[] nums = octet.split("-");
                    if (nums.length != 2 || validateOctet(nums[0]) != IS_VALID || validateOctet(nums[1]) != IS_VALID) {
                        return INVALID_FORMAT;
                    }

                    try {
                        int begin = Integer.parseInt(nums[0]);
                        int end = Integer.parseInt(nums[1]);
                        if (begin >= end) {
                            return INVALID_FORMAT;
                        }
                    } catch (Exception e) {
                        return INVALID_FORMAT;
                    }
                } else {
                    if (octet.charAt(0) == ',' || octet.charAt(octet.length() - 1) == ',') {
                        return INVALID_FORMAT;
                    }
                    String[] nums = octet.split(",");
                    if (nums.length < 2) {
                        return INVALID_FORMAT;
                    }
                    for (String num : nums) {
                        if (validateOctet(num) != IS_VALID) {
                            return INVALID_FORMAT;
                        }
                    }
                }

                // Update code for IP WAN with network range.
            } else if (octet.contains("/")) {
                String[] octetParse = octet.split("/");
                // validate subnet
                if (octetParse.length > 2
                        || validateOctet(octetParse[0]) != IS_VALID
                        || validateOctet(octetParse[1]) != IS_VALID) {
                    return INVALID_FORMAT;
                }
                int networkRange = Integer.parseInt(octetParse[1]);
                if (networkRange == 0 || networkRange >= 32) {
                    return INVALID_CHARACTER;
                }
            } else if (validateOctet(octet) != IS_VALID) {
                return INVALID_FORMAT;
            }
        }
        return IS_VALID;
    }

    /**
     * Validate user ip.
     *
     * @param anOctetOfIpOfUser the an octet of ip of user
     * @param allOfOctets       the all of octets
     * @return true, if successful
     */
    private static boolean validateUserIp(String[] anOctetOfIpOfUser, String[] allOfOctets) {
        if (anOctetOfIpOfUser[0].equals(allOfOctets[0])
                && anOctetOfIpOfUser[1].equals(allOfOctets[1])
                && anOctetOfIpOfUser[2].equals(allOfOctets[2])
                && allOfOctets[3].contains("/")) {
            Integer maximumHost = getValidRangeOfIP(allOfOctets[3]);
            final int hostOctetValue = Integer.parseInt(anOctetOfIpOfUser[3]);
            if (hostOctetValue >= 1 && hostOctetValue <= maximumHost) {
                return true;
            }
        }
        return false;
    }
}
