/*
 * ThreadUtil.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The Class ThreadUtil.
 */
public class ThreadUtil {

    /**
     * The Constant _log.
     */
    private static final Logger _log = LogManager.getLogger(ThreadUtil.class);

    /**
     * Sleep.
     *
     * @param time the time
     */
    public static void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException ex) {
            _log.error("Error occurred, thread Interrupted", ex);
        }
    }

    /**
     * Wait.
     *
     * @param obj  the obj
     * @param time the time
     */
    public static void wait(Object obj, long time) {
        try {
            synchronized (obj) {
                obj.wait(time);
            }
        } catch (InterruptedException ex) {
            _log.error("Error occurred, thread Interrupted", ex);
        }
    }
}
