/*
 * NumberUtil.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * The Class NumberUtil.
 */
public class NumberUtil {

    /**
     * Next double.
     *
     * @return the double
     */
    public static double nextDouble() {
        return ThreadLocalRandom.current().nextDouble();
    }

    /**
     * Random between.
     *
     * @param min the min
     * @param max the max
     * @return the double
     */
    public static double randomBetween(double min, double max) {
        return ThreadLocalRandom.current().nextDouble(min, max);
    }

    /**
     * Random between.
     *
     * @param min the min
     * @param max the max
     * @return the int
     */
    public static int randomBetween(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    /**
     * Random between.
     *
     * @param min   the min
     * @param max   the max
     * @param notEq the not eq
     * @return the int
     */
    public static int randomBetween(int min, int max, int notEq) {
        int result = randomBetween(min, max);

        if (notEq == result) {
            return randomBetween(min, max, notEq);
        }

        return result;
    }

    /**
     * Random between.
     *
     * @param min   the min
     * @param max   the max
     * @param notEq the not eq
     * @return the int
     */
    public static int randomBetween(int min, int max, List<Integer> notEq) {
        int result = randomBetween(min, max);

        if (Validator.isNotNull(notEq) && notEq.contains(result)) {
            return randomBetween(min, max, notEq);
        }

        return result;
    }

    /**
     * Random in list.
     *
     * @param integerArr the integer arr
     * @return the int
     */
    public static int randomInList(int[] integerArr) {
        if (Validator.isNull(integerArr)) {
            return 0;
        }

        int index = ThreadLocalRandom.current().nextInt(integerArr.length);

        return integerArr[index];
    }

    /**
     * Random in list.
     *
     * @param lstInteger the lst integer
     * @return the int
     */
    public static int randomInList(List<Integer> lstInteger) {
        if (Validator.isNull(lstInteger)) {
            return 0;
        }

        int index = ThreadLocalRandom.current().nextInt(lstInteger.size());

        return lstInteger.get(index);
    }
}
