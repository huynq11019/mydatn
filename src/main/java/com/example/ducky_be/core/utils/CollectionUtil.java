/*
 * CollectionUtil.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;


import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * The Class CollectionUtil.
 */
public class CollectionUtil {

    /**
     * First key.
     *
     * @param <K> the key type
     * @param <V> the value type
     * @param map the map
     * @return the k
     */
    public static <K, V> K firstKey(Map<K, V> map) {
        if (map == null || map.isEmpty()) {
            return null;
        }

        List<K> keySet = new ArrayList<>(map.keySet());

        return keySet.get(0);
    }

    /**
     * First value.
     *
     * @param <K> the key type
     * @param <V> the value type
     * @param map the map
     * @return the v
     */
    public static <K, V> V firstValue(Map<K, V> map) {
        K key = firstKey(map);

        return key != null ? map.get(key) : null;
    }

    /**
     * Last key.
     *
     * @param <K> the key type
     * @param <V> the value type
     * @param map the map
     * @return the k
     */
    public static <K, V> K lastKey(Map<K, V> map) {
        if (map == null || map.isEmpty()) {
            return null;
        }

        List<K> keySet = new ArrayList<>(map.keySet());

        return keySet.get(keySet.size() - 1);
    }

    /**
     * Last value.
     *
     * @param <K> the key type
     * @param <V> the value type
     * @param map the map
     * @return the v
     */
    public static <K, V> V lastValue(Map<K, V> map) {
        K key = lastKey(map);

        return key != null ? map.get(key) : null;
    }

    /**
     * Subtract.
     *
     * @param first  the first
     * @param second the second
     * @return the collection
     */
    public static Collection<?> subtract(Collection<?> first, Collection<?> second) {
        return CollectionUtils.subtract(first, second);
    }
}
