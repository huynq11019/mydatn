/*
 * IlikeExpression.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;

import org.hibernate.criterion.MatchMode;
import org.hibernate.dialect.Dialect;

/**
 * The Class IlikeExpression.
 */
public class IlikeExpression extends LikeExpression {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new ilike expression.
     *
     * @param propertyName the property name
     * @param value        the value
     */
    protected IlikeExpression(String propertyName, Object value) {
        super(propertyName, value);
    }

    /**
     * Instantiates a new ilike expression.
     *
     * @param propertyName the property name
     * @param value        the value
     * @param escapeChar   the escape char
     */
    protected IlikeExpression(String propertyName, String value, Character escapeChar) {
        super(propertyName, value, escapeChar);
    }

    /**
     * Instantiates a new ilike expression.
     *
     * @param propertyName the property name
     * @param value        the value
     * @param matchMode    the match mode
     */
    protected IlikeExpression(String propertyName, String value, MatchMode matchMode) {
        super(propertyName, value, matchMode);
    }

    /*
     * (non-Javadoc)
     * @see com.vinsmart.portal.util.LikeExpression#lhs(org.hibernate.dialect.Dialect, java.lang.String)
     */
    @Override
    protected String lhs(Dialect dialect, String column) {
        return dialect.getLowercaseFunction() + '(' + column + ')';
    }

    /*
     * (non-Javadoc)
     * @see com.vinsmart.portal.util.LikeExpression#typedValue(java.lang.String)
     */
    @Override
    protected String typedValue(String value) {
        return super.typedValue(value).toLowerCase();
    }
}
