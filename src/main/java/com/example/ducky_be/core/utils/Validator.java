/*
 * Validator.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.utils;

import org.apache.commons.net.util.SubnetUtils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The Class Validator.
 */
public class Validator {

    /**
     * The Constant _CHAR_BEGIN.
     */
    private static final int _CHAR_BEGIN = 65;

    /**
     * The Constant _CHAR_END.
     */
    private static final int _CHAR_END = 122;

    /**
     * The Constant _codeStringPattern.
     */
    private static final Pattern _codeStringPattern = Pattern.compile("[a-zA-Z0-9]*");

    /**
     * The Constant _DIGIT_BEGIN.
     */
    private static final int _DIGIT_BEGIN = 48;

    /**
     * The Constant _DIGIT_END.
     */
    private static final int _DIGIT_END = 57;

    /**
     * The Constant _EMAIL_ADDRESS_SPECIAL_CHAR.
     */
    private static final char[] _EMAIL_ADDRESS_SPECIAL_CHAR = new char[]{'.', '!', '#', '$', '%', '&', '\'', '*', '+',
            '-', '/', '=', '?', '^', '_', '`', '{', '|', '}', '~'};

    /**
     * The Constant _emailAddressPattern.
     */
    private static final Pattern _emailAddressPattern = Pattern
            .compile("[\\w!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_`{|}~-]+)*@"
                    + "(?:[\\w](?:[\\w-]*[\\w])?\\.)+[\\w](?:[\\w-]*[\\w])?");

    /**
     * The Constant _LOCALHOST.
     */
    private static final String _LOCALHOST = "localhost";

    /**
     * The Constant _numberPattern.
     */
    private static final Pattern _numberPattern = Pattern.compile("[+-]?\\d*(\\.\\d+)?");

    /**
     * The Constant _userNamePattern.
     */
    private static final Pattern _userNamePattern = Pattern.compile("[_a-zA-Z0-9]*");

    /**
     * The Constant _VARIABLE_TERM_BEGIN.
     */
    private static final String _VARIABLE_TERM_BEGIN = "[$";

    /**
     * The Constant _VARIABLE_TERM_END.
     */
    private static final String _VARIABLE_TERM_END = "$]";

    /**
     * The Constant _variableNamePattern.
     */
    private static final Pattern _variableNamePattern = Pattern.compile("[_a-zA-Z]+[_a-zA-Z0-9]*");

    /**
     * The Constant _XML_BEGIN.
     */
    private static final String _XML_BEGIN = "<?xml";

    /**
     * The Constant _XML_EMPTY.
     */
    private static final String _XML_EMPTY = "<root />";

    /**
     * Equals.
     *
     * @param boolean1 the boolean 1
     * @param boolean2 the boolean 2
     * @return true, if successful
     */
    public static boolean equals(boolean boolean1, boolean boolean2) {
        return boolean1 == boolean2;
    }

    /**
     * Equals.
     *
     * @param byte1 the byte 1
     * @param byte2 the byte 2
     * @return true, if successful
     */
    public static boolean equals(byte byte1, byte byte2) {
        return byte1 == byte2;
    }

    /**
     * Equals.
     *
     * @param char1 the char 1
     * @param char2 the char 2
     * @return true, if successful
     */
    public static boolean equals(char char1, char char2) {
        return char1 == char2;
    }

    /**
     * Equals.
     *
     * @param date1 the date 1
     * @param date2 the date 2
     * @return true, if successful
     */
    public static boolean equals(Date date1, Date date2) {
        if (date1 == null && date2 != null || date1 != null && date2 == null) {
            return false;
        }
        if (date1 == null && date2 == null) {
            return true;
        }

        return date1.compareTo(date2) == 0;
    }

    /**
     * Equals.
     *
     * @param double1 the double 1
     * @param double2 the double 2
     * @return true, if successful
     */
    public static boolean equals(double double1, double double2) {
        return Double.compare(double1, double2) == 0;
    }

    /**
     * Equals.
     *
     * @param float1 the float 1
     * @param float2 the float 2
     * @return true, if successful
     */
    public static boolean equals(float float1, float float2) {
        return Float.compare(float1, float2) == 0;
    }

    /**
     * Equals.
     *
     * @param int1 the int 1
     * @param int2 the int 2
     * @return true, if successful
     */
    public static boolean equals(int int1, int int2) {
        return int1 == int2;
    }

    /**
     * Equals.
     *
     * @param long1 the long 1
     * @param long2 the long 2
     * @return true, if successful
     */
    public static boolean equals(long long1, long long2) {
        return long1 == long2;
    }

    /**
     * Equals.
     *
     * @param obj1 the obj 1
     * @param obj2 the obj 2
     * @return true, if successful
     */
    public static boolean equals(Object obj1, Object obj2) {
        if (obj1 == null && obj2 == null) {
            return true;
        }
        if (obj1 == null || obj2 == null) {
            return false;
        }
        return obj1.equals(obj2);
    }

    /**
     * Equals.
     *
     * @param short1 the short 1
     * @param short2 the short 2
     * @return true, if successful
     */
    public static boolean equals(short short1, short short2) {
        return short1 == short2;
    }

    /**
     * Gets the not null string.
     *
     * @param object the object
     * @return the not null string
     */
    public static String getNotNullString(Object object) {
        return object != null ? object.toString() : StringPool.BLANK;
    }

    /**
     * Gets the safe file name.
     *
     * @param input the input
     * @return the safe file name
     */
    public static String getSafeFileName(String input) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c != '/' && c != '\\' && c != 0) {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    /**
     * Checks if is address.
     *
     * @param address the address
     * @return true, if is address
     */
    public static boolean isAddress(String address) {
        if (isNull(address)) {
            return false;
        }

        String[] tokens = address.split(StringPool.AT);

        if (tokens.length != 2) {
            return false;
        }

        for (String token : tokens) {
            for (char c : token.toCharArray()) {
                if (Character.isWhitespace(c)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Checks if is alphanumeric name.
     *
     * @param name the name
     * @return true, if is alphanumeric name
     */
    public static boolean isAlphanumericName(String name) {
        if (isNull(name)) {
            return false;
        }

        for (char c : name.trim().toCharArray()) {
            if (!isChar(c) && !isDigit(c) && !Character.isWhitespace(c)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks if is ascii.
     *
     * @param c the c
     * @return true, if is ascii
     */
    public static boolean isAscii(char c) {
        int i = c;

        return i >= 32 && i <= 126;
    }

    /**
     * Checks if is char.
     *
     * @param c the c
     * @return true, if is char
     */
    public static boolean isChar(char c) {
        int x = c;

        return x >= _CHAR_BEGIN && x <= _CHAR_END;
    }

    /**
     * Checks if is char.
     *
     * @param s the s
     * @return true, if is char
     */
    public static boolean isChar(String s) {
        if (isNull(s)) {
            return false;
        }

        for (char c : s.toCharArray()) {
            if (!isChar(c)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks if is code string.
     *
     * @param codeString the code string
     * @return true, if is code string
     */
    public static boolean isCodeString(String codeString) {
        if (isNull(codeString)) {
            return false;
        }

        Matcher matcher = _codeStringPattern.matcher(codeString);

        return matcher.matches();
    }

    /**
     * Checks if is date.
     *
     * @param month the month
     * @param day   the day
     * @param year  the year
     * @return true, if is date
     */
    public static boolean isDate(int month, int day, int year) {
        return isGregorianDate(month, day, year);
    }

    /**
     * Checks if is digit.
     *
     * @param c the c
     * @return true, if is digit
     */
    public static boolean isDigit(char c) {
        int x = c;

        return x >= _DIGIT_BEGIN && x <= _DIGIT_END;
    }

    /**
     * Checks if is digit.
     *
     * @param s the s
     * @return true, if is digit
     */
    public static boolean isDigit(String s) {
        if (isNull(s)) {
            return false;
        }

        for (char c : s.toCharArray()) {
            if (!isDigit(c)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks if is domain.
     *
     * @param domainName the domain name
     * @return true, if is domain
     */
    public static boolean isDomain(String domainName) {
        // See RFC-1034 (section 3), RFC-1123 (section 2.1), and RFC-952
        // (section B. Lexical grammar)
        if (isNull(domainName)
                || domainName.length() > 255
                || domainName.startsWith(StringPool.PERIOD)
                || domainName.endsWith(StringPool.PERIOD)) {
            return false;
        }

        if (!domainName.contains(StringPool.PERIOD) && !domainName.equals(_LOCALHOST)) {
            return false;
        }

        String[] domainNameArray = StringUtil.split(domainName, CharPool.PERIOD);

        for (String domainNamePart : domainNameArray) {
            char[] domainNamePartCharArray = domainNamePart.toCharArray();

            for (int i = 0; i < domainNamePartCharArray.length; i++) {
                char c = domainNamePartCharArray[i];

                if ((i == 0 && c == CharPool.DASH) || (i == domainNamePartCharArray.length - 1 && c == CharPool.DASH)) {
                    return false;
                }

                if (!isChar(c) && !isDigit(c) && c != CharPool.DASH) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Checks if is email address.
     *
     * @param emailAddress the email address
     * @return true, if is email address
     */
    public static boolean isEmailAddress(String emailAddress) {
        Matcher matcher = _emailAddressPattern.matcher(emailAddress);

        return matcher.matches();
    }

    /**
     * Checks if is email address special char.
     *
     * @param c the c
     * @return true, if is email address special char
     */
    public static boolean isEmailAddressSpecialChar(char c) {

        // LEP-1445
        for (char element : _EMAIL_ADDRESS_SPECIAL_CHAR) {
            if (c == element) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if is gregorian date.
     *
     * @param month the month
     * @param day   the day
     * @param year  the year
     * @return true, if is gregorian date
     */
    public static boolean isGregorianDate(int month, int day, int year) {
        if (month < 0 || month > 11) {
            return false;
        }

        int[] months = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        if (month == 1) {
            int febMax = 28;

            if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
                febMax = 29;
            }

            if (day < 1 || day > febMax) {
                return false;
            }
        } else if (day < 1 || day > months[month]) {
            return false;
        }

        return true;
    }

    /**
     * Checks if is hex.
     *
     * @param s the s
     * @return true, if is hex
     */
    public static boolean isHex(String s) {
        return !isNull(s);
    }

    /**
     * Checks if is html.
     *
     * @param s the s
     * @return true, if is html
     */
    public static boolean isHTML(String s) {
        if (isNull(s)) {
            return false;
        }

        return (s.indexOf("<html>") != -1 || s.indexOf("<HTML>") != -1)
                && (s.indexOf("</html>") != -1 || s.indexOf("</HTML>") != -1);
    }

    /**
     * Checks if is IP address.
     *
     * @param ipAddress
     *            the ip address
     * @return true, if is IP address
     */
//    public static boolean isIPAddress(String ipAddress) {
//        InetAddressValidator iav = InetAddressValidator.getInstance();
//
//        return iav.isValidInet4Address(ipAddress) || iav.isValidInet6Address(ipAddress);
//    }

    /**
     * Checks if is IP address in range.
     *
     * @param ipRange the ip range
     * @param ip      the ip
     * @return true, if is IP address in range
     */
    public static boolean isIPAddressInRange(String ipRange, String ip) {
        SubnetUtils util = new SubnetUtils(ipRange);

        return util.getInfo().isInRange(ip);
    }

    /**
     * Checks if is julian date.
     *
     * @param month the month
     * @param day   the day
     * @param year  the year
     * @return true, if is julian date
     */
    public static boolean isJulianDate(int month, int day, int year) {
        if (month < 0 || month > 11) {
            return false;
        }

        int[] months = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        if (month == 1) {
            int febMax = 28;

            if (year % 4 == 0) {
                febMax = 29;
            }

            if (day < 1 || day > febMax) {
                return false;
            }
        } else if (day < 1 || day > months[month]) {
            return false;
        }

        return true;
    }

    /**
     * Checks if is luhn.
     *
     * @param number the number
     * @return true, if is luhn
     */
    public static boolean isLUHN(String number) {
        if (number == null) {
            return false;
        }

        number = StringUtil.reverse(number);

        int total = 0;

        for (int i = 0; i < number.length(); i++) {
            int x = 0;

            if ((i + 1) % 2 == 0) {
                x = Integer.parseInt(number.substring(i, i + 1)) * 2;

                if (x >= 10) {
                    String s = String.valueOf(x);

                    x = Integer.parseInt(s.substring(0, 1)) + Integer.parseInt(s.substring(1, 2));
                }
            } else {
                x = Integer.parseInt(number.substring(i, i + 1));
            }

            total = total + x;
        }

        return total % 10 == 0;
    }

    /**
     * Checks if is match.
     *
     * @param value the value
     * @param regex the regex
     * @return true, if is match
     */
    public static boolean isMatch(String value, String regex) {

        Pattern p = Pattern.compile(regex);

        Matcher m = p.matcher(value);

        return m.matches();
    }

    /**
     * Checks if is name.
     *
     * @param name the name
     * @return true, if is name
     */
    public static boolean isName(String name) {
        if (isNull(name)) {
            return false;
        }

        for (char c : name.trim().toCharArray()) {
            if (!isChar(c) && !Character.isWhitespace(c)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks if is not null.
     *
     * @param collection the collection
     * @return true, if is not null
     */
    public static boolean isNotNull(Collection<?> collection) {
        return !isNull(collection);
    }

    /**
     * Checks if is not null.
     *
     * @param i the i
     * @return true, if is not null
     */
    public static boolean isNotNull(Integer i) {
        return !isNull(i);
    }

    /**
     * Checks if is not null.
     *
     * @param l the l
     * @return true, if is not null
     */
    public static boolean isNotNull(Long l) {
        return !isNull(l);
    }

    /**
     * Checks if is not null.
     *
     * @param obj the obj
     * @return true, if is not null
     */
    public static boolean isNotNull(Object obj) {
        return !isNull(obj);
    }

    /**
     * Checks if is not null.
     *
     * @param array the array
     * @return true, if is not null
     */
    public static boolean isNotNull(Object[] array) {
        return !isNull(array);
    }

    /**
     * Checks if is not null.
     *
     * @param s the s
     * @return true, if is not null
     */
    public static boolean isNotNull(String s) {
        return !isNull(s);
    }

    /**
     * Checks if is null.
     *
     * @param collection the collection
     * @return true, if is null
     */
    public static boolean isNull(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    /**
     * Checks if is null.
     *
     * @param l the l
     * @return true, if is null
     */
    public static boolean isNull(Integer l) {
        return l == null || l <= 0;
    }

    /**
     * Checks if is null.
     *
     * @param l the l
     * @return true, if is null
     */
    public static boolean isNull(Long l) {
        return l == null || l <= 0;
    }

    /**
     * Checks if is null.
     *
     * @param obj the obj
     * @return true, if is null
     */
    public static boolean isNull(Object obj) {
        if (obj instanceof Long) {
            return isNull((Long) obj);
        }
        if (obj instanceof Integer) {
            return isNull((Integer) obj);
        }
        if (obj instanceof String) {
            return isNull((String) obj);
        } else {
            return obj == null;
        }
    }

    /**
     * Checks if is null.
     *
     * @param array the array
     * @return true, if is null
     */
    public static boolean isNull(Object[] array) {
        return array == null || array.length == 0;
    }

    /**
     * Checks if is null.
     *
     * @param s the s
     * @return true, if is null
     */
    public static boolean isNull(String s) {
        if (s == null) {
            return true;
        }

        int counter = 0;

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            if (c == CharPool.SPACE) {
                continue;
            }
            if (counter > 3) {
                return false;
            }

            switch (counter) {
                case 0:
                    if (c != CharPool.LOWER_CASE_N) {
                        return false;
                    }
                    break;
                case 1:
                    if (c != CharPool.LOWER_CASE_U) {
                        return false;
                    }
                    break;
                case 2:
                case 3:
                    if (c != CharPool.LOWER_CASE_L) {
                        return false;
                    }
                    break;
                default:
                    break;
            }

            counter++;
        }

        return counter == 0 || counter == 4;
    }

    /**
     * Checks if is number.
     *
     * @param number the number
     * @return true, if is number
     */
    public static boolean isNumber(String number) {
        if (isNull(number)) {
            return false;
        }

        for (char c : number.toCharArray()) {
            if (!isDigit(c)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks if is numberic.
     *
     * @param number the number
     * @return true, if is numberic
     */
    public static boolean isNumberic(String number) {
        if (isNull(number)) {
            return false;
        }

        Matcher matcher = _numberPattern.matcher(number);

        return matcher.matches();
    }

    /**
     * Checks if is password.
     *
     * @param password the password
     * @return true, if is password
     */
    public static boolean isPassword(String password) {
        if (isNull(password) || password.length() < 4) {
            return false;
        }

        for (char c : password.toCharArray()) {
            if (!isChar(c) && !isDigit(c)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks if is phone number.
     *
     * @param phoneNumber the phone number
     * @return true, if is phone number
     */
    public static boolean isPhoneNumber(String phoneNumber) {
        return isNumber(phoneNumber);
    }

    /**
     * Checks if is url.
     *
     * @param url the url
     * @return true, if is url
     */
    public static boolean isUrl(String url) {
        if (Validator.isNotNull(url)) {
            if (url.indexOf(CharPool.COLON) == -1) {
                return false;
            }

            try {
                new URL(url);

                return true;
            } catch (MalformedURLException murle) {
            }
        }

        return false;
    }

    /**
     * Checks if is user name.
     *
     * @param userName the user name
     * @return true, if is user name
     */
    public static boolean isUserName(String userName) {
        if (isNull(userName)) {
            return false;
        }

        Matcher matcher = _userNamePattern.matcher(userName);

        return matcher.matches();
    }

    /**
     * Checks if is variable name.
     *
     * @param variableName the variable name
     * @return true, if is variable name
     */
    public static boolean isVariableName(String variableName) {
        if (isNull(variableName)) {
            return false;
        }

        Matcher matcher = _variableNamePattern.matcher(variableName);

        return matcher.matches();
    }

    /**
     * Checks if is variable term.
     *
     * @param s the s
     * @return true, if is variable term
     */
    public static boolean isVariableTerm(String s) {
        return s.startsWith(_VARIABLE_TERM_BEGIN) && s.endsWith(_VARIABLE_TERM_END);
    }

    /**
     * Checks if is whitespace.
     *
     * @param c the c
     * @return true, if is whitespace
     */
    public static boolean isWhitespace(char c) {
        int i = c;

        return i == 0 || Character.isWhitespace(c);
    }

    /**
     * Checks if is xml.
     *
     * @param s the s
     * @return true, if is xml
     */
    public static boolean isXml(String s) {
        return s.startsWith(_XML_BEGIN) || s.startsWith(_XML_EMPTY);
    }
}
