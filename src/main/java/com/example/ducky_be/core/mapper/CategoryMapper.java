package com.example.ducky_be.core.mapper;

import com.example.ducky_be.controller.protoco.request.CategoryRequest;
import com.example.ducky_be.controller.protoco.response.CategoryResponse;
import com.example.ducky_be.repository.entity.Categories;
import com.example.ducky_be.service.dto.CategoryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public interface CategoryMapper extends BaseMapper<CategoryDTO, Categories> {

    // request to entity
    @Mapping(target = "parentCategory.id", source = "parentCategory")
    Categories requestToEntity(CategoryRequest request);

    // DTO to response
    CategoryResponse dtoToResponse(CategoryDTO dto);

    // Request to DTO
    CategoryDTO pathFromRequestToModel(CategoryRequest productRequest);

    @Override
    @Mappings({
            @Mapping(target = "child", source = "child", qualifiedByName = "chillFomat"),
            @Mapping(target = "parentCategory", source = "parentCategory.id")

    })
    CategoryDTO toDto(Categories entity);

    @Override
    @Mapping(source = "parentCategory", target = "parentCategory", qualifiedByName = "mapParent")
    Categories toEntity(CategoryDTO dto);

    // entity to DTO custom
//    @Mappings({
//            @Mapping(target = "chill", source = "child", qualifiedByName = "chillFomat"),
//                })
//    CategoryDTO entityToDTOCustom(Categories categoryId);
//
//
    @Named("mapParent")
    default Categories mapParent(Long categoryParent) {
        Categories categories = new Categories();
        categories.setId(categoryParent);
        return categories;
    }

    @Named("chillFomat")
    default List<CategoryDTO> categoryChillConfig(List<Categories> chill) {
        List<CategoryDTO> categoryDTOS = new ArrayList<>();
        System.err.println("45 category mapper " + chill);
        for (Categories entity : chill) {
            CategoryDTO categoryDTO = new CategoryDTO();

            categoryDTO.setChild(categoryChillConfig(entity.getChild()));
            categoryDTO.setId(entity.getId());
            categoryDTO.setCategoryName(entity.getCategoryName());
            categoryDTO.setDescription(entity.getDescription());
            categoryDTO.setSortOrder(entity.getSortOrder());
            categoryDTO.setCreateDate(entity.getCreateDate());
            categoryDTO.setCreateBy(entity.getCreateBy());
            categoryDTO.setLastUpdate(entity.getLastUpdate());
            categoryDTO.setUpdateBy(entity.getUpdateBy());
            categoryDTO.setStatus(entity.getStatus());
            categoryDTO.setParentCategory(entity.getParentCategory().getId());
//            if (Validator.isNotNull(entity.getChild())){
//                categoryDTO.setChill(toDto(entity.getChild()));
//            }
//            categoryDTOS.add( this.toDto( categoryId ) );
            categoryDTOS.add(categoryDTO);
        }
        return categoryDTOS;
    }
}
