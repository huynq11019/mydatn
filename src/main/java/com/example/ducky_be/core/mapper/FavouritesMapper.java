package com.example.ducky_be.core.mapper;

import com.example.ducky_be.controller.protoco.request.FavouritesRequest;
import com.example.ducky_be.controller.protoco.response.FavouritesResponse;
import com.example.ducky_be.repository.entity.Favourites;
import com.example.ducky_be.service.dto.FavouritesDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "Spring")
public interface FavouritesMapper extends BaseMapper<FavouritesDTO, Favourites> {

    // Request to entity
    Favourites requestToEntity(FavouritesRequest favouritesRequest);

    // DTO to response
    FavouritesResponse dtoToResponse(FavouritesDTO dto);

    // Request to DTO
    FavouritesDTO pathFormResponseToModel(FavouritesRequest favouritesRequest);
}
