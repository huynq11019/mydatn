package com.example.ducky_be.core.mapper;

import com.example.ducky_be.controller.protoco.request.ProductAttributeRequest;
import com.example.ducky_be.controller.protoco.response.ProductAttributeResponse;
import com.example.ducky_be.repository.entity.ProductAttributes;
import com.example.ducky_be.service.dto.ProductAttributeDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductAttributeMapper extends BaseMapper<ProductAttributeDTO, ProductAttributes> {
    // request to entity
    ProductAttributes requestToEntity(ProductAttributeRequest request);

    // DTO to response
    ProductAttributeResponse dtoToResponse(ProductAttributeDTO dto);

    // Request to DTO
    ProductAttributeRequest pathFromRequestToModel(ProductAttributeDTO dto);
}
