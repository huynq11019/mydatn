package com.example.ducky_be.core.mapper;

import com.example.ducky_be.controller.protoco.request.ProductRequest;
import com.example.ducky_be.controller.protoco.response.ProductResponse;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.entity.Categories;
import com.example.ducky_be.repository.entity.Product;
import com.example.ducky_be.service.dto.ProductDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface ProductMapper extends BaseMapper<ProductDTO, Product> {

//    // request to entity
//    Product requestToEntity(ProductRequest request);

    // DTO to response
    ProductResponse dtoToEntity(ProductDTO dto);

    // Request to DTO
//    ProductDTO pathFromRequestToModel(ProductRequest productRequest);

    // dto to entity
//    @Override
//    @Mappings({
//            @Mapping(target = "categoryId", source = "categoryId", qualifiedByName = "idTocate"),
//    })
//    Product toEntity(ProductDTO dto);
//
//    @Override
//    @Mappings({
//            @Mapping(target = "categoryId", source = "categoryId", qualifiedByName = "mapToLong"),
//    })
//    ProductDTO toDto(Product entity);

    @Named("idTocate")
    default Categories mapParent(Long categoryParent) {
        Categories categories = new Categories();
        categories.setId(categoryParent);
        return categories;
    }

    @Named("mapToLong")
    default Long mapParent(Categories categoryParent) {
        if (Validator.isNull(categoryParent)) {
            return 1L;
        }
        return categoryParent.getId();
    }

}
