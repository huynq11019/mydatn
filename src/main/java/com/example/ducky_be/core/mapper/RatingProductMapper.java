package com.example.ducky_be.core.mapper;

import com.example.ducky_be.controller.protoco.request.RatingRequest;
import com.example.ducky_be.repository.entity.RatingProduct;
import com.example.ducky_be.service.dto.RatingDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RatingProductMapper extends BaseMapper<RatingDTO, RatingProduct> {
    // request to entity
    RatingProduct toEntity(RatingRequest request);
}
