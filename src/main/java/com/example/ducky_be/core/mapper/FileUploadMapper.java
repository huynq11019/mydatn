package com.example.ducky_be.core.mapper;

import com.example.ducky_be.repository.entity.FileUpload;
import com.example.ducky_be.service.dto.FileUploadDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface FileUploadMapper extends BaseMapper<FileUploadDTO, FileUpload> {
}
