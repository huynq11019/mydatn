package com.example.ducky_be.core.mapper;

import com.example.ducky_be.controller.protoco.request.AddressRequest;
import com.example.ducky_be.controller.protoco.response.AddressResponse;
import com.example.ducky_be.repository.entity.Address;
import com.example.ducky_be.service.dto.AddressDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AddressMapper extends BaseMapper<AddressDTO, Address> {

    // request to entity
    Address requestToEntity(AddressRequest addressRequest);

    // DTO to response
    AddressResponse dtoToResponse(AddressDTO addressDTO);

    // Request to DTO
    AddressDTO pathFormRequestToModel(AddressRequest addressRequest);

}
