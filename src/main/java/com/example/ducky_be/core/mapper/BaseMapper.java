package com.example.ducky_be.core.mapper;

import java.util.List;

public interface BaseMapper<D, E> {
    D toDto(E entity);

    List<D> toDto(List<E> entityList);

    E toEntity(D dto);

    List<E> toEntity(List<D> dtoList);


}
