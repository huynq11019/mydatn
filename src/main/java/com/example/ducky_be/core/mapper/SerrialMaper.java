package com.example.ducky_be.core.mapper;

import com.example.ducky_be.controller.protoco.request.SerriProductResquest;
import com.example.ducky_be.repository.entity.SerriProduct;
import com.example.ducky_be.service.dto.SerriProductDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SerrialMaper extends BaseMapper<SerriProductDTO, SerriProduct> {
    // request to entity
    SerriProduct requestToEntity(SerriProductResquest resquest);
}
