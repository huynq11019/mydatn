package com.example.ducky_be.core.mapper;


import com.example.ducky_be.controller.protoco.request.OrderRequest;
import com.example.ducky_be.controller.protoco.response.OrderResponse;
import com.example.ducky_be.repository.entity.Order;
import com.example.ducky_be.repository.entity.OrderDetail;
import com.example.ducky_be.service.dto.OrderDTO;
import com.example.ducky_be.service.dto.OrderDetailDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OrderMapper extends BaseMapper<OrderDTO, Order>{
    // request to entity
    Order requestToEntity(OrderRequest request);

    // DTO to response
    OrderResponse dtoToResponse(OrderDTO dto);

    // Request to DTO
    OrderRequest pathFromRequestToModel(OrderDTO dto);

    OrderDTO pathFromRequest(OrderRequest request);

    OrderDetail detailDtoToEntity(OrderDetailDTO dto);

    List<OrderDetail> detailDtoToEntity(List<OrderDetailDTO> dtos);

    OrderDetailDTO detailEntityToDto(OrderDetail entity);

    List<OrderDetailDTO> detailEntityToDto(List<OrderDetail> entities);
}

