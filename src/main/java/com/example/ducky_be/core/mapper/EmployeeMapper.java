package com.example.ducky_be.core.mapper;


import com.example.ducky_be.controller.protoco.request.EmployeeRequest;
import com.example.ducky_be.controller.protoco.response.EmployeeResponse;
import com.example.ducky_be.repository.entity.Employee;
import com.example.ducky_be.service.dto.EmployeeDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EmployeeMapper extends BaseMapper<EmployeeDTO, Employee> {
    // request to entity
    Employee requestToEntity(EmployeeRequest request);

    // DTO to response
    EmployeeResponse dtoToEntity(EmployeeDTO dto);

    // Request to DTO
    EmployeeDTO pathFromRequestToModel(EmployeeRequest employeeRequest);
}
