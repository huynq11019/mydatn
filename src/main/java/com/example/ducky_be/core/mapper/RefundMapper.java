package com.example.ducky_be.core.mapper;

import com.example.ducky_be.controller.protoco.request.RefundRequest;
import com.example.ducky_be.controller.protoco.response.RefundResponse;
import com.example.ducky_be.repository.entity.Refund;
import com.example.ducky_be.service.dto.RefundDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RefundMapper extends BaseMapper<RefundDTO, Refund> {
    // request to entity
    Refund requestToEntity(RefundRequest request);

    // DTO to response
    RefundResponse dtoToResponse(RefundDTO dto);

    // Request to DTO
    RefundDTO pathFromRequestToModel(RefundRequest request);
}
