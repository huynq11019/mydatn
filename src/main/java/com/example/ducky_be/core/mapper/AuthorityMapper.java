package com.example.ducky_be.core.mapper;

import com.example.ducky_be.controller.protoco.request.AuthoritesRequest;
import com.example.ducky_be.controller.protoco.response.AuthoritesResponse;
import com.example.ducky_be.repository.entity.Authorities;
import com.example.ducky_be.service.dto.AuthorityDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AuthorityMapper extends BaseMapper<AuthorityDTO, Authorities> {
    // Request to DTO
    AuthorityDTO toDTO(AuthoritesRequest request);

    // DTO to response
    AuthoritesResponse toResponse(AuthorityDTO dto);

    // request to entity
    Authorities toEntity(AuthoritesRequest request);

}
