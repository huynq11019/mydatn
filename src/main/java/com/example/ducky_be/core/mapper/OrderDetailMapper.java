package com.example.ducky_be.core.mapper;


import com.example.ducky_be.controller.protoco.request.OrderDetailRequest;
import com.example.ducky_be.controller.protoco.response.OrderDetailResponse;
import com.example.ducky_be.repository.entity.OrderDetail;
import com.example.ducky_be.service.dto.OrderDetailDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderDetailMapper extends BaseMapper<OrderDetailDTO, OrderDetail>{
    // request to entity
    OrderDetail requestToEntity(OrderDetailRequest request);

    // DTO to response
    OrderDetailResponse dtoToResponse(OrderDetailDTO dto);

    // Request to DTO
    OrderDetailRequest pathFromRequestToModel(OrderDetailDTO dto);
}
