package com.example.ducky_be.core.mapper;

import com.example.ducky_be.controller.protoco.request.AttributeRequest;
import com.example.ducky_be.controller.protoco.response.AttributeResponse;
import com.example.ducky_be.repository.entity.Attributes;
import com.example.ducky_be.service.dto.AttributeDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AttributeMapper extends BaseMapper<AttributeDTO, Attributes> {
    // request to entity
    Attributes requestToEntity(AttributeRequest request);

    // DTO to response
    AttributeResponse dtoToResponse(AttributeDTO dto);

    // Request to DTO
    AttributeDTO pathFromRequestToModel(AttributeRequest request);
}
