package com.example.ducky_be.core.mapper;

import com.example.ducky_be.controller.protoco.request.CustomerRequest;
import com.example.ducky_be.controller.protoco.response.CustomerResponse;
import com.example.ducky_be.repository.entity.Customer;
import com.example.ducky_be.service.dto.CustomerDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper extends BaseMapper<CustomerDTO, Customer> {
    // request to entity
    Customer toEntity(CustomerRequest request);

    // DTO to response
    CustomerResponse dtoToEntity(CustomerDTO dto);

    // Request to DTO
    CustomerDTO pathFromRequestToModel(CustomerRequest employeeRequest);
}
