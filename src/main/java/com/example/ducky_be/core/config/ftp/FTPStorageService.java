package com.example.ducky_be.core.config.ftp;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;

public interface FTPStorageService {

    /*void init();*/

    boolean validateExtension(MultipartFile file);

    String store(MultipartFile file);

    String store(byte[] data, String fileType);

    String store(InputStream input, String fileType);

    File load(String imageUrl);

    File loadFromTempDir(String imageUrl);

    Resource loadResourceFromTempDir(String imageUrl);

    void storeToTempDir(InputStream input, String imageUrl);

    boolean validateExtension(String fileName);

    Resource loadAsResource(String path);

    String loadAsBase64(String filename);

    byte[] readFileToByte(String filePath);
}
