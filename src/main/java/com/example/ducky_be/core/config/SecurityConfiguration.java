package com.example.ducky_be.core.config;

import com.example.ducky_be.core.sercurity.CustomAuthenticationFilter;
import com.example.ducky_be.core.sercurity.CustomOAuth2UserService;
import com.example.ducky_be.core.sercurity.CustomUserDetailService;
import com.example.ducky_be.core.sercurity.handler.EntryPonint;
import com.example.ducky_be.core.sercurity.handler.OAuthLoginSuccessHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.endpoint.DefaultAuthorizationCodeTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest;
import org.springframework.security.oauth2.client.web.AuthorizationRequestRepository;
import org.springframework.security.oauth2.client.web.HttpSessionOAuth2AuthorizationRequestRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Collections;

@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private AuthenticationProperties ap;

    @Autowired
    private CustomOAuth2UserService oauth2UserService;

    @Autowired
    private OAuthLoginSuccessHandler oauth2LoginSuccessHandler;
    /**
     * The Constant IGNOR_URLS.
     */
    private static final String[] IGNOR_URLS = {"/app/**/*.{js,html}", "/i18n/**", "/content/**", "/test/**"};

    /**
     * The Constant PUBLIC_URLS.
     */
    private static final String[] PUBLIC_URLS = { //
            "/api/forgotpassword/**", //
            "/api/createEmployee/**", // api tạo nhân viên
            "/api/authenticate", // api login
            "/api/getNewAccessToken", // get token fromt refresh token
            "/api/logout", // // api đăng xuát
            "/api/customer/registert", // khách hàng đăng ký tài khoản
            "/api/oauth/verifyAuthorizationCode",//
            "/api/file/downloadFile/**",
            "/api/createEmployee",//
            "/api/user/export",
            "/api/user/change-password",
            "/swagger-ui.html",
            "/swagger-ui.html/**",
            "**/springfox-swagger-ui/**"
    };

    private static final String[] PUBLIC_URL = {"/api/authenticate", "/api/register", "/api/forgotpassword", "/api/file/**"};

    /**
     * The Constant AUTHENTICATED_URLS.
     */
    private static final String[] AUTHENTICATED_URLS = {"**/me/**", "**/account/**"};

    @Autowired
    private CustomUserDetailService userDetailsService;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        // quản lý dữ liệu người dùng
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    //    @Bean
//    public CustomAuthenticationFilter jwtAuthenticationFilter() {
//        return new CustomAuthenticationFilter();
//    }
    @Autowired
    private CustomAuthenticationFilter jwtAuthenticationFilter;

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(HttpMethod.OPTIONS, "/**").antMatchers(IGNOR_URLS);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    public EntryPonint getEntryPoint() {
        return new EntryPonint();
    }

    @Bean
    public CorsConfigurationSource configCors() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

        CorsConfiguration config = new CorsConfiguration();

        config.setAllowedHeaders(Collections.singletonList(ap.getAllowedHeaders()));
        config.setAllowedMethods(Collections.singletonList(ap.getAllowedMethods()));
        config.addAllowedOrigin(ap.getAllowedOrigins());
        config.setExposedHeaders(Collections.singletonList(ap.getExposedHeaders()));
        config.setAllowCredentials(ap.isAllowCredentials());
        config.setMaxAge(ap.getMaxAge());
        if (config.getAllowedOrigins() != null && !config.getAllowedOrigins().isEmpty()) {
            source.registerCorsConfiguration("/api/**", config);
        }

        return source;
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        // phân quyền và sử dụng hình thwucs đăng nhập
        httpSecurity.cors().and()
                .csrf().disable();

        httpSecurity.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
                .antMatchers(PUBLIC_URLS).permitAll()
                .antMatchers(AUTHENTICATED_URLS).authenticated()// my account phải đăng nhập
                .antMatchers(IGNOR_URLS).permitAll()
                .antMatchers(("/api/**")).permitAll()
                .and().httpBasic();// .and().apply(this.securityConfigurerAdapter())

//                .and().formLogin()//
//                .disable().httpBasic().disable().logout().disable();
//        httpSecurity.oauth2Login()
//                .loginPage("/oauth2/login")
//                .defaultSuccessUrl("/oauth2/login/success", true)
//                .failureUrl("/api/oauth2/login/failure")
//                .authorizationEndpoint() //authroization endpoint
//                    .baseUri("/oauth2/authorize")
//                    .authorizationRequestRepository(getRepository()) // Oauth2 response
//                .and().tokenEndpoint()
//                .accessTokenResponseClient(getAccessTokenResponseClient());

        httpSecurity.oauth2Login()
                .loginPage("/login")
                .userInfoEndpoint()
                .userService(oauth2UserService)
                .and()
                .successHandler(oauth2LoginSuccessHandler)
                .and()
                .logout().logoutSuccessUrl("/").permitAll()
                .and()
                .exceptionHandling().accessDeniedPage("/403");
//        ;
        httpSecurity.exceptionHandling().authenticationEntryPoint(this.getEntryPoint()).and().sessionManagement();
        httpSecurity.addFilterBefore(this.jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public AuthorizationRequestRepository<OAuth2AuthorizationRequest> getRepository() {
        return new HttpSessionOAuth2AuthorizationRequestRepository();
    }

    @Bean
    public OAuth2AccessTokenResponseClient<OAuth2AuthorizationCodeGrantRequest> getAccessTokenResponseClient() {
        return new DefaultAuthorizationCodeTokenResponseClient();
    }
}
