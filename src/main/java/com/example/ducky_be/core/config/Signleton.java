package com.example.ducky_be.core.config;

public class Signleton {
    private static Signleton instance;

    private Signleton() {
    }

    public static Signleton getInstance() {
        if (instance == null) {
            instance = new Signleton();
        }
        return instance;
    }

    // get instance in multi thread environment
    public static Signleton getInstance2() {
        if (instance == null) {
            synchronized (Signleton.class) {
                if (instance == null) {
                    instance = new Signleton();
                }
            }
        }
        return instance;
    }


}
