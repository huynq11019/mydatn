package com.example.ducky_be.core.config.ftp;

import com.example.ducky_be.core.config.FTPProperties;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import java.io.Closeable;
import java.io.IOException;

@Log4j2
public class FTPClientPool implements Closeable {

    private final int KEEP_ALIVE_TIME_OUT = 300;

    private GenericObjectPool<FTPClient> objectPool;

    public FTPClientPool(FTPProperties ftpProperties, int maxActive, int maxIdle, long maxWait) {
        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setMaxIdle(maxIdle);
        config.setMaxTotal(maxActive);
        config.setMaxWaitMillis(maxWait);
        config.setTestOnBorrow(true);

        objectPool = new GenericObjectPool<FTPClient>(createFactory(ftpProperties), config);
    }

    private PooledObjectFactory<FTPClient> createFactory(final FTPProperties ftpProperties) {
        return new BasePooledObjectFactory<FTPClient>() {
            @Override
            public FTPClient create() throws Exception {

                FTPClient ftpClient = new FTPClient();
                try {
                    log.info("Create new connect ftp: " + ftpProperties.getServer());
                    ftpClient.connect(ftpProperties.getServer(), ftpProperties.getPort());
                    log.info("start get reply code");
                    int replyCode = ftpClient.getReplyCode();
                    if (!FTPReply.isPositiveCompletion(replyCode)) {
                        ftpClient.disconnect();
                        log.warn("FtpServer refused connection, replyCode:{}", replyCode);
                        return null;
                    }
                    log.info("start login");
                    if (!ftpClient.login(ftpProperties.getUsername(), ftpProperties.getPassword())) {
                        log.warn("FtpClient login failed... username is {}", ftpProperties.getUsername());
                    }
                    log.info("finish create ftp client");
                    ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
                    ftpClient.setFileTransferMode(FTP.BINARY_FILE_TYPE);
                    ftpClient.setControlKeepAliveTimeout(KEEP_ALIVE_TIME_OUT);
                    ftpClient.enterLocalPassiveMode();
                } catch (Exception e) {
                    log.error(e.getMessage(), "Connected ftpInfo: " + ftpProperties.getServer());
                    throw e;
                }
                return ftpClient;
            }

            @Override
            public PooledObject<FTPClient> wrap(FTPClient ftpClient) {
                return new DefaultPooledObject<>(ftpClient);
            }

            @Override
            public void destroyObject(PooledObject<FTPClient> ftpPooled) {
                if (ftpPooled == null) {
                    return;
                }

                FTPClient ftpClient = ftpPooled.getObject();

                try {
                    ftpClient.logout();
                } catch (IOException io) {
                    log.error("Ftp client is closed, logout failed...");
                } finally {
                    try {
                        if (ftpClient.isConnected()) {
                            ftpClient.disconnect();
                        }
                    } catch (IOException io) {
                        log.error("Close ftp client failed...");
                    }
                }
            }

            @Override
            public boolean validateObject(PooledObject<FTPClient> ftpPooled) {
                try {
                    FTPClient ftpClient = ftpPooled.getObject();
                    return ftpClient.sendNoOp();
                } catch (IOException e) {
                    log.error("Failed to validate client...");
                }
                return false;
            }
        };
    }

    public FTPClient allocateConnection() throws Exception {
        FTPClient ftpClient = objectPool.borrowObject();
        return ftpClient;
    }

    public void freeConnection(FTPClient ftpClient) {
        objectPool.returnObject(ftpClient);
    }

    public int getNumIdle() {
        return objectPool.getNumIdle();
    }

    public int getNumActive() {
        return objectPool.getNumActive();
    }

    @Override
    public void close() {
        objectPool.close();
        objectPool = null;
    }
}
