/*
 * FTPProperties.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * <PRE>
 * The Class FTPProperties.</BR>
 * </PRE>
 * <p>
 * Jul 12, 2021 - HaiTH: Create new
 *
 * @author HaiTH
 */
@Component
@ConfigurationProperties(prefix = "ftp")
public class FTPProperties {

    /**
     * The server.
     */
    private String server;

    /**
     * The username.
     */
    private String username;

    /**
     * The password.
     */
    private String password;

    /**
     * The port.
     */
    @Min(0)
    @Max(65535)
    private int port;

    /**
     * The keep alive timeout.
     */
    private int keepAliveTimeout;

    /**
     * The auto start.
     */
    private boolean autoStart;

    /**
     * Inits the.
     */
    @PostConstruct
    public void init() {
        if (port == 0) {
            port = 21;
        }
    }

    /**
     * Gets the server.
     *
     * @return the server
     */
    public String getServer() {
        return server;
    }

    /**
     * Sets the server.
     *
     * @param server the new server
     */
    public void setServer(String server) {
        this.server = server;
    }

    /**
     * Gets the username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username.
     *
     * @param username the new username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets the password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password.
     *
     * @param password the new password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets the port.
     *
     * @return the port
     */
    public int getPort() {
        return port;
    }

    /**
     * Sets the port.
     *
     * @param port the new port
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Gets the keep alive timeout.
     *
     * @return the keep alive timeout
     */
    public int getKeepAliveTimeout() {
        return keepAliveTimeout;
    }

    /**
     * Sets the keep alive timeout.
     *
     * @param keepAliveTimeout the new keep alive timeout
     */
    public void setKeepAliveTimeout(int keepAliveTimeout) {
        this.keepAliveTimeout = keepAliveTimeout;
    }

    /**
     * Checks if is auto start.
     *
     * @return true, if is auto start
     */
    public boolean isAutoStart() {
        return autoStart;
    }

    /**
     * Sets the auto start.
     *
     * @param autoStart the new auto start
     */
    public void setAutoStart(boolean autoStart) {
        this.autoStart = autoStart;
    }
}
