package com.example.ducky_be.core.config.ftp;

import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.exception.StorageException;
import com.example.ducky_be.core.exception.StorageFileNotFoundException;
import com.example.ducky_be.core.utils.HashUtil;
import com.example.ducky_be.core.utils.StringUtil;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

@Service
public class FTPFileSystemStorageService implements FTPStorageService {
    private final Logger log = LoggerFactory.getLogger(FTPFileSystemStorageService.class);

    private final Path rootLocation;

    private final String ROOT_FOLDER_FTP;

    private final FTPService ftpService;

    @Autowired
    public FTPFileSystemStorageService(FTPService ftpService) {
        this.rootLocation = Paths.get("");
        if (StringUtil.isBlank("")) {
            ROOT_FOLDER_FTP = Constants.ROOT_FOLDER_FTP;
        } else {
            ROOT_FOLDER_FTP = "";
        }
        this.ftpService = ftpService;
    }

    @Override
    public boolean validateExtension(MultipartFile file) {
        if (file.isEmpty() || file.getOriginalFilename() == null) {
            return false;
        }
        String filename = StringUtil.getSafeFileName(file.getOriginalFilename());
        int last = filename.lastIndexOf(".");
        if (last < 0) {
            return false;
        }
        String fileType = filename.substring(last + 1);
        return Constants.getValidExtensions().contains(fileType.toLowerCase());
    }

    @Override
    public String store(MultipartFile file) {
        if (file.isEmpty() || file.getOriginalFilename() == null) {
            throw new StorageException("Failed to store empty file/filename");
        }
        try {
            String originalName = StringUtil.getSafeFileName(file.getOriginalFilename());
            String filename = HashUtil.SHA256(file.getBytes()) + originalName.substring(originalName.lastIndexOf("."));
            String folder = this.getFolderPath() + "/" + filename.substring(0, 2);
            ftpService.saveFile(file.getInputStream(), ROOT_FOLDER_FTP + folder, filename, false);
            log.info(folder + "/" + filename);
            return folder + "/" + filename;
        } catch (IOException e) {
            throw new StorageException("Failed to store file " + file.getOriginalFilename(), e);
        }
    }

    @Override
    public String store(byte[] data, String fileType) {
        if (data.length == 0) {
            throw new StorageException("Failed to store empty file");
        }
        String filename = HashUtil.SHA256(data) + "." + fileType;
        String folder = getFolderPath() + "/" + filename.substring(0, 2);
        ftpService.saveFile(new ByteArrayInputStream(data), ROOT_FOLDER_FTP + folder, filename, false);
        return folder + "/" + filename;
    }

    @Override
    public String store(InputStream input, String fileType) {
        String filename = HashUtil.SHA256Random() + "." + fileType;
        String folder = getFolderPath() + "/" + filename.substring(0, 2);
        ftpService.saveFile(input, ROOT_FOLDER_FTP + folder, filename, false);
        return folder + "/" + filename;
    }

    @Override
    public File load(String imageUrl) {
        // check lưu file local và FTP
        String remotePath = ROOT_FOLDER_FTP + imageUrl;
//        String localPath = Constants.TEMP_DIR + "/" + remotePath;
        String localPath = imageUrl;
        File file = new File(localPath);
        if (!file.exists() || file.length() == 0) {
            try {
                log.info("Download file from FTP: " + remotePath);
                new File(file.getParent()).mkdirs();
                ftpService.loadFile(remotePath, new FileOutputStream(file));
            } catch (IOException e) {
                throw new StorageException("Failed to load file " + imageUrl, e);
            }
        }
        return file;
    }

    @Override
    public File loadFromTempDir(String imageUrl) {
        String localPath = Constants.TEMP_DIR + "/" + imageUrl;
        return new File(localPath);
    }

    public Resource loadResourceFromTempDir(String imageUrl) {
        try {
            String localPath = Constants.TEMP_DIR + "/" + imageUrl;
            Path file = new File(localPath).toPath();
            return new UrlResource(file.toUri());
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + imageUrl, e);
        }
    }

    @Override
    public void storeToTempDir(InputStream input, String imageUrl) {
        File file = new File(imageUrl);
        try {
            new File(file.getParent()).mkdirs();
            Files.copy(input, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new StorageException("Failed to load file " + imageUrl, e);
        } finally {
            try {
                input.close();
            } catch (Exception e) {
            }
        }
    }

    private String getFolderPath() {
        String format = "yyyy/MM/dd";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return "/" + sdf.format(new Date()).replace("\\", "/");
    }

    public boolean delete(String filename) {
        Path p = rootLocation.resolve(filename);
        File f = p.toFile();
        if (f.exists() && f.isFile()) {
            return f.delete();
        }
        return false;
    }

    public boolean validateExtension(String fileName) {
        if (StringUtil.isBlank(fileName)) {
            return false;
        }
        String fileType = null;
        String fullName = StringUtil.getSafeFileName(fileName);
        int last = fullName.lastIndexOf(".");
        if (last < 0) {
            return false;
        }
        fileType = fullName.substring(last + 1, fullName.length());
        log.info(fileName);
        log.info(fileType);

        return Constants.getValidExtensions().contains(fileType.toLowerCase());
    }

    @Override
    public Resource loadAsResource(String filePaht) {
//        log.debug(filename, rootLocation);
        try {
            Path file = this.load(filePaht).toPath();
            return new UrlResource(file.toUri());
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filePaht, e);
        }
    }

    // load as Base64
    @Override
    public String loadAsBase64(String filename) {
        try {
            Path file = this.load(filename).toPath();
            return Base64.getEncoder().encodeToString(Files.readAllBytes(file));
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        } catch (IOException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    @Override
    public byte[] readFileToByte(String filePath) {
        Resource inputStreamResource = this.loadAsResource(filePath);
        try {
            return FileUtils.readFileToByteArray(inputStreamResource.getFile());
        } catch (IOException e) {
            e.printStackTrace();
            throw new StorageFileNotFoundException("Could not read file: " + filePath, e);
        }
    }
}
