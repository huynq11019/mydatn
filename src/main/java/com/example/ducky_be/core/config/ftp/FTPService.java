package com.example.ducky_be.core.config.ftp;

import java.io.InputStream;
import java.io.OutputStream;

public interface FTPService {

    boolean loadFile(String remotePath, OutputStream outputStream);

    boolean saveFile(InputStream inputStream, String destPath, String fileName, boolean append);

    boolean saveFile(String sourcePath, String destPath, String fileName, boolean append);

    boolean deleteFile(String pathFile);
}
