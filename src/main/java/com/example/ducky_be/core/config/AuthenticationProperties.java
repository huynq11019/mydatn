package com.example.ducky_be.core.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class AuthenticationProperties {

    /**
     * The base 64 secret.
     */
    @Value("${mulesoft.jwt_key}")
    private String base64Secret;

//    /** The token duration. */
//    @Value("${security.authentication.jwt.access-token-duration}")
//    private int accessTokenDuration;

    /**
     * The token remember me duration.
     */
    @Value("${security.authentication.jwt.token-remember-me-duration}")
    private int tokenRememberMeDuration;

    /**
     * The refesh token duration.
     */
    @Value("${security.authentication.jwt.refesh-token-duration}")
    private int refeshTokenDuration;

    /**
     * The data duration.
     */
    @Value("${security.authentication.jwt.data-duration}")
    private int dataDuration;

    /**
     * The url patterns.
     */
    @Value("${security.cache.url-patterns}")
    private String[] urlPatterns;

    /**
     * The allowed origins.
     */
    @Value("${security.cors.allowed-origins}")
    private String allowedOrigins;

    /**
     * The allowed methods.
     */
    @Value("${security.cors.allowed-methods}")
    private String allowedMethods;

    /**
     * The allowed headers.
     */
    @Value("${security.cors.allowed-headers}")
    private String allowedHeaders;

    /**
     * The exposed headers.
     */
    @Value("${security.cors.exposed-headers}")
    private String exposedHeaders;

    /**
     * The allow credentials.
     */
    @Value("${security.cors.allow-credentials}")
    private boolean allowCredentials;

    /**
     * The max age.
     */
    @Value("${security.cors.max-age}")
    private long maxAge;

    /**
     * The token duration.
     */
    @Value("${security.authentication.jwt.access-token-duration}")
    private int accessTokenDuration;
}
