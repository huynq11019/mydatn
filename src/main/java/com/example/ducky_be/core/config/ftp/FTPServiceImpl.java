package com.example.ducky_be.core.config.ftp;

import com.example.ducky_be.core.config.FTPProperties;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.IOUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Objects;

@Service
@Log4j2
public class FTPServiceImpl implements FTPService {
    private static final Logger logger = LoggerFactory.getLogger(FTPServiceImpl.class);

    private final int KEEP_ALIVE_TIME_OUT = 300;
    private String host;
    private Integer port;
    private String user;
    private String password;
    private FTPClientPool ftpClientPool;

    public FTPServiceImpl(FTPProperties ftpProperties) {
        host = ftpProperties.getServer();
        user = ftpProperties.getUsername();
        password = ftpProperties.getPassword();
        port = ftpProperties.getPort();
        ftpClientPool = new FTPClientPool(ftpProperties, 24, 8, 100000);
    }

    @Override
    public boolean loadFile(String remotePath, OutputStream outputStream) {
        FTPClient ftpClient = null;
        try {
            logger.info("Trying to load a file from remote path " + remotePath);
            ftpClient = ftpClientPool.allocateConnection();
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            ftpClient.setFileTransferMode(FTP.BINARY_FILE_TYPE);
            logger.info("Trying to retrieve a file from remote path " + remotePath);
            if (outputStream == null) {
                logger.error("OutputStream is null");
            }
            // boolean retrieveFile = ftpClient.retrieveFile(remotePath, outputStream);
            // if (!retrieveFile) {
            // logger.error("Retrieve file not successful");
            // }
            // return retrieveFile;
            InputStream inputStream = ftpClient.retrieveFileStream(remotePath);
            if (Objects.nonNull(inputStream)) {
                IOUtils.copy(inputStream, outputStream);
                outputStream.flush();
            }
            outputStream.close();
            inputStream.close();
            return ftpClient.completePendingCommand();
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return false;
        } finally {
            ftpClientPool.freeConnection(ftpClient);
        }
    }

    @Override
    public boolean saveFile(InputStream inputStream, String destPath, String fileName, boolean append) {
        FTPClient ftpClient = null;
        try {
            ftpClient = ftpClientPool.allocateConnection();
            logger.info("Trying to store a file to destination path " + destPath);
            if (append) {
                return ftpClient.appendFile(destPath, inputStream);
            } else {
                String[] folders = destPath.replace("//", "/").split("/");
                StringBuilder partialDirPath = new StringBuilder();
                for (String eachDir : folders) {
                    partialDirPath.append(eachDir).append("/");
                    ftpClient.makeDirectory(partialDirPath.toString());
                }
                ftpClient.changeWorkingDirectory("~");
                ftpClient.changeWorkingDirectory(destPath);
                ftpClient.enterLocalPassiveMode();
                boolean result = ftpClient.storeFile(fileName, inputStream);
                logger.info("FTP reply string = " + ftpClient.getReplyString());
                inputStream.close();
                ftpClient.changeWorkingDirectory("~");
                return result;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return false;
        } finally {
            ftpClientPool.freeConnection(ftpClient);
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
//                    logger.debug("ignore");
                }
            }
        }
    }

    @Override
    public boolean saveFile(String sourcePath, String destPath, String fileName, boolean append) {
        InputStream inputStream;
        File file = new File(sourcePath);
        try {
            inputStream = new FileInputStream(file);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
        return this.saveFile(inputStream, destPath, fileName, append);
    }

    @Override
    public boolean deleteFile(String pathFile) {
        logger.info("FTP Service Delete File Start");
        FTPClient ftpClient = null;
        boolean deleted = true;
        try {
            ftpClient = ftpClientPool.allocateConnection();
            ftpClient.deleteFile("" + pathFile);
        } catch (Exception e) {
            logger.error("FTP Service Delete File Error: {}", e);
            deleted = false;
        } finally {
            ftpClientPool.freeConnection(ftpClient);
        }
        logger.info("FTP Service Delete File End");
        return deleted;
    }

    private FTPClient createFTPClient() {
        logger.info("Connecting to ftp: " + host);
        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.connect(host, port);
            ftpClient.login(user, password);
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            ftpClient.setControlKeepAliveTimeout(KEEP_ALIVE_TIME_OUT);
            ftpClient.enterLocalPassiveMode();
            logger.info("Connected ftp: " + host);
        } catch (Exception e) {
            logger.error(e.getMessage(), "Connected ftpInfo: " + host);
        }
        return ftpClient;
    }
}
