/*
 * StorageException.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.exception;

import com.example.ducky_be.core.message.ErrorCode;

import java.text.MessageFormat;

/**
 * The Class StorageException.
 */
public class StorageException extends RuntimeException {

    private ErrorCode error;
    private Object[] params;

    public StorageException(String message, ErrorCode error, String tmp) {
        this(message, null, error);
    }

    public StorageException(ErrorCode error) {
        this(error.getMessage(), error, "");
    }

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new storage exception.
     *
     * @param message the message
     */
    public StorageException(String message) {
        super(message);
    }

    /**
     * Instantiates a new storage exception.
     *
     * @param message the message
     * @param cause   the cause
     */
    public StorageException(String message, Throwable cause) {
        super(message, cause);
    }

    public StorageException(String message, Throwable cause, ErrorCode error) {
        this(message, cause, error, null);
    }

    public StorageException(String message, Throwable cause, ErrorCode error, Object... params) {
        super(MessageFormat.format(message, params), cause);
        this.error = error;
        this.params = params == null ? new Object[0] : params;
    }
}
