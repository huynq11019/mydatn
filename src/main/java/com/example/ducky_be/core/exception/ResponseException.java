package com.example.ducky_be.core.exception;


import com.example.ducky_be.core.message.ErrorCode;

import java.text.MessageFormat;

public class ResponseException extends RuntimeException {
    private ErrorCode error;
    private Object[] params;

    private String entityName;

    public ResponseException(String message, ErrorCode error) {
        this(message, null, error);
    }

    public ResponseException(ErrorCode error) {
        this(error.getMessage(), null, error);
    }

    public ResponseException(String message, Throwable cause, ErrorCode error) {
        this(message, cause, error, null);
    }

    public ResponseException(String message, ErrorCode error, Object... params) {
        this(message, null, error, params);
    }

    public ResponseException(String message, Throwable cause, ErrorCode error, Object... params) {
        super(MessageFormat.format(message, params), cause);
        this.error = error;
        this.params = params == null ? new Object[0] : params;
    }

    public ErrorCode getError() {
        return this.error;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getEntityName() {
        return entityName;
    }

    public Object[] getParams() {
        return this.params;
    }

    public void setError(final ErrorCode error) {
        this.error = error;
    }

    public void setParams(final Object[] params) {
        this.params = params;
    }
}
