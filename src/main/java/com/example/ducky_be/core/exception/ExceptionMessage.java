package com.example.ducky_be.core.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExceptionMessage {
    private int statusCode;
    private String message;
    private String errorCode;
    private String entity;

    public ExceptionMessage() {
    }

    public ExceptionMessage(int statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    public ExceptionMessage(String message, String errorCode, String entity) {
        this.message = message;
        this.errorCode = errorCode;
        this.entity = entity;
    }
}
