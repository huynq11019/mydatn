package com.example.ducky_be.core.exception;

import org.springframework.context.NoSuchMessageException;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

@RestControllerAdvice

public class MyExceptionHandle {
    /*
     * xủ lý validate form
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    public ExceptionMessage userLoginFail(Exception ex, WebRequest request) {
        return new ExceptionMessage(401, ex.getMessage());
    }

    //    xử lý ngoại lệ đối tượng không tồn tại
    @ExceptionHandler(NoSuchElementException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ExceptionMessage objectNotFound(Exception ex, WebRequest request) {

        return new ExceptionMessage(404, ex.getMessage());
    }

    //    someThing không tồn tại
    @ExceptionHandler(ResponseException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ExceptionMessage someThingNotFound(ResponseException ex,
                                              NativeWebRequest request) {
        return new ExceptionMessage(ex.getMessage(), ex.getError().toString(), ex.getEntityName());

    }

    @ExceptionHandler(NoSuchMessageException.class)
    public ExceptionMessage exceptionMessage(Exception ex, WebRequest request) {

        return new ExceptionMessage(404, ex.getMessage());
    }

    @ExceptionHandler(BadRequestAlertException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ExceptionMessage handleBadRequestAlertException(BadRequestAlertException ex,
                                                           NativeWebRequest request) {
//        return this.create(ex, request,
//                HeaderUtil.createFailureAlert(true, ex.getEntityName(), ex.getErrorKey(), ex.getMessage()));
        return new ExceptionMessage(ex.getMessage(), ex.getErrorKey(), ex.getEntityName());
    }


}
