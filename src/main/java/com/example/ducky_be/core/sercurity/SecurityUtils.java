package com.example.ducky_be.core.sercurity;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

public final class SecurityUtils {
    /**
     * Get the JWT of the current user.
     *
     * @return the JWT of the current user.
     */
    public static Optional<String> getCurrentUserJWT() {
        SecurityContext securityContext = SecurityContextHolder.getContext();

        return Optional.ofNullable(securityContext.getAuthentication())
                .filter(authentication -> authentication.getCredentials() instanceof String)
                .map(authentication -> (String) authentication.getCredentials());
    }

    /**
     * Get the login of the current user.
     *
     * @return the login of the current user.
     */
    public static Optional<String> getCurrentUserLogin() {
        SecurityContext securityContext = SecurityContextHolder.getContext();

        return Optional.ofNullable(securityContext.getAuthentication()).map(authentication -> {
            if (authentication.getPrincipal() instanceof UserDetails) {
                UserDetails springSecurityUser = (UserDetails) authentication.getPrincipal();

                return springSecurityUser.getUsername();
            }
            if (authentication.getPrincipal() instanceof String) {
                return (String) authentication.getPrincipal();
            }

            return null;
        });
    }

    // public static Optional<Users> getUserLogin() {
    // SecurityContext securityContext = SecurityContextHolder.getContext();
    //
    // return Optional.ofNullable(securityContext.getAuthentication()).map(authentication -> {
    // if (authentication.getPrincipal() instanceof CustomUserDetail) {
    // CustomUserDetail springSecurityUser = (CustomUserDetail) authentication.getPrincipal();
    //
    // return springSecurityUser.getUsers();
    // }
    //
    // return null;
    // });
    // }

    public static Optional<CustomUserDetail> geUserDetails() {
        SecurityContext securityContext = SecurityContextHolder.getContext();

        return Optional.ofNullable(securityContext.getAuthentication()).map(authentication -> {
            if (authentication.getPrincipal() instanceof CustomUserDetail) {
                return (CustomUserDetail) authentication.getPrincipal();
            }

            return null;
        });
    }

    public static Optional<String> getLoginUserId() {
        Optional<CustomUserDetail> userLogin = geUserDetails();
        if (userLogin.isPresent()) {
            return Optional.ofNullable(userLogin.get().getUserId());
        }

        return Optional.empty();
    }

    /**
     * Check if a user is authenticated.
     *
     * @return true if the user is authenticated, false otherwise.
     */
    public static boolean isAuthenticated() {
        SecurityContext securityContext = SecurityContextHolder.getContext();

        return Optional.ofNullable(securityContext.getAuthentication())
                .map(authentication -> !authentication.getAuthorities().isEmpty()).orElse(false);
    }

    /**
     * If the current user has a specific authority (security role).
     * <p>
     * The name of this method comes from the {@code isUserInRole()} method in the Servlet API.
     *
     * @param authority the authority to check.
     * @return true if the current user has the authority, false otherwise.
     */
    public static boolean isCurrentUserHasPrivilege(String authority) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        return Optional
                .ofNullable(securityContext.getAuthentication()).map(authentication -> authentication.getAuthorities()
                        .stream().anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals(authority)))
                .orElse(false);
    }

    private SecurityUtils() {
    }
}
