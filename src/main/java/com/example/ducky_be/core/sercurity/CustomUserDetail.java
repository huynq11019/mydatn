package com.example.ducky_be.core.sercurity;

import com.example.ducky_be.repository.entity.Customer;
import com.example.ducky_be.repository.entity.Employee;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;

@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CustomUserDetail implements UserDetails, Serializable {
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The users.
     */
    private Employee employee;

    private Customer customer;
    /**
     * The password.
     */
    private transient String password;

    /**
     * The status.
     */
    private transient int status;

    /**
     * The user id.
     */
    private String userId;

    /**
     * The username.
     */
    private String username;

    /**
     * The full name
     */
    private String fullName;

    /**
     * The email.
     */
    private String email;

    private String userType;

    private String hashKey;

    /**
     * The authorities.
     */
    private Collection<SimpleGrantedAuthority> authorities;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // TODO Auto-generated method stub
        return authorities;
    }

    @Override
    public String getPassword() {
        // TODO Auto-generated method stub
        return password;
    }

    @Override
    public String getUsername() {
        // TODO Auto-generated method stub
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isEnabled() {
        // TODO Auto-generated method stub
        return true;
    }

}
