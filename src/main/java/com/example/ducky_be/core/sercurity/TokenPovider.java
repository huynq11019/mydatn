package com.example.ducky_be.core.sercurity;

//import com.example.ducky_be.core.config.AuthenticationProperties;

import com.example.ducky_be.core.config.AuthenticationProperties;
import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.constants.SecurityConsants;
import com.example.ducky_be.core.utils.DateUtils;
import com.example.ducky_be.core.utils.JsonUtil;
import com.example.ducky_be.core.utils.StringPool;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.service.EmployeeService;
import io.jsonwebtoken.*;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

import static com.example.ducky_be.core.constants.SecurityConsants.JWTEXPIRATIONINMS;
import static com.example.ducky_be.core.constants.SecurityConsants.REFRESH_JWTEXPIRATIONINMS;

@Service
@Log4j2
public class TokenPovider {
    @Value("${jwt.secret}")
    private String secret;

    private long jwtExpirationInMs = JWTEXPIRATIONINMS;

    private long refreshExpirationDateInMs = REFRESH_JWTEXPIRATIONINMS;

    //
//    @Autowired
//    private AuthenticationProperties properties;
    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private AuthenticationProperties properties;

    /**
     * @author: huynq
     * @since: 9/12/2021 12:45 PM
     * @description: Thực hiện tạo token
     * @update:
     */
    public String generateToken(CustomUserDetail userDetails) {
        Map<String, Object> claims = new HashMap<>();

        Collection<? extends GrantedAuthority> roles = userDetails.getAuthorities();

        List<String> authrority = new ArrayList<>();
        roles.forEach(role -> {
            log.info(role);
            authrority.add(role.toString());
        });
        // email của user hiện tại
        claims.put(SecurityConsants.TokenConstants.EMAIL, userDetails.getEmail());
        // user type
        claims.put(SecurityConsants.TokenConstants.USER_TYPE, userDetails.getUserType());
        // quyền của user hiện tại
        claims.put(SecurityConsants.TokenConstants.AUTHORITY, JsonUtil.convertToJsonString(authrority));

        return doGenerateToken(claims, userDetails.getUserId());
    }

    private String doGenerateToken(Map<String, Object> claims, String subject) {
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + jwtExpirationInMs);

        return Jwts.builder().setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, secret).compact();

    }

    /**
     * @author: huynq
     * @since: 9/12/2021 3:27 AM
     * @description: thực hiện tạo refresh token
     * @update:
     */
    public String doGenerateRefreshToken(Map<String, Object> claims, String subject) {

        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + refreshExpirationDateInMs))
                .signWith(SignatureAlgorithm.HS512, secret).compact();

    }

    /**
     * @author: huynq
     * @since: 9/13/2021 1:06 AM
     * @description: Create refresh token của haith
     * @update:
     */
    public JwtToken createRefreshToken(Authentication authentication) {
        // chưa truyền claims
        Map<String, Object> claims = new HashMap<>();
        return this.createRefreshToken(claims, authentication.getName());
    }

    /**
     * @author: huynq
     * @since: 9/13/2021 1:32 AM
     * @description: Phần này là refesh token của anh haith
     * @update:
     */
    public JwtToken createRefreshToken(Map<String, Object> claims, String username) {
//        String refreshToken = HMACUtil.hashSha256(UUID.randomUUID().toString() + username);

//        Date dateAfter = DateUtils.getSecondAfter(new Date(), 86400);
        Date exDate = new Date(System.currentTimeMillis() + refreshExpirationDateInMs);
        String refreshToken = Jwts.builder().setClaims(claims)
                .setSubject(username)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(exDate)
                .signWith(SignatureAlgorithm.HS512, secret).compact();
        return JwtToken.builder()
                .token(refreshToken)
                .tokenExpDate(exDate)
                .createdDate(new Date(System.currentTimeMillis()))
                .userName(username).build();

    }

    private String getHashKey(String username) {
        return Validator.isNotNull(username) ? DigestUtils.md5DigestAsHex(username.getBytes()) : StringPool.BLANK;
    }

    /**
     * @author: huynq
     * @since: 10/9/2021 11:50 PM
     * @description: lấy token mới từ refresh token
     * @update:
     */
    public Optional<JwtToken> refreshToken(String username, String refreshToken) {
        String hashKey = this.getHashKey(username);
        // Check refresh token
//        Optional<Object> rfToken = Optional.ofNullable(
//                this.redisService.hget(this.getRedisKey(username, hashKey), SecurityConstants.Jwt.REFRESH_TOKEN));

//        if (rfToken.isPresent()) {
//            return Optional.ofNullable(this.doGenerateRefreshToken(null, username));
//        }

        return Optional.empty();
    }

    /**
     * @author: huynq
     * @since: 9/12/2021 3:28 AM
     * @description: Thực hiện validate token
     * @update:
     */
    public boolean validateToken(String authToken) {
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(authToken);
            return true;
        } catch (MalformedJwtException ex) {
            log.error("Invalid JWT token");
            return false;
            // throw new InvalidTokenRequestException("JWT", authToken, "Malformed jwt token");
        } catch (ExpiredJwtException ex) {
            log.error("Expired JWT token");
            return false;
            // throw new InvalidTokenRequestException("JWT", authToken, "Token expired. Refresh required");
        } catch (UnsupportedJwtException ex) {
            log.error("Unsupported JWT token");
            return false;
            // throw new InvalidTokenRequestException("JWT", authToken, "Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            log.error("JWT claims string is empty.");
            return false;
            // throw new InvalidTokenRequestException("JWT", authToken, "Illegal argument token");
        } catch (Exception e) {
            log.error("Invalid JWT signature.", e);
            return false;
        }
    }

    /**
     * @author: huynq
     * @since: 9/12/2021 3:28 AM
     * @description: lấy thông tin user từ token
     * @update:
     */
    public String getUserIdFromtToken(String accessToken) {
        Jws<Claims> claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(accessToken);
        System.out.println(claims);
        return claims.getBody().getSubject();

    }

    /*
     * @author: huynq
     * @since: 9/12/2021 3:29
     * @description: Get danh sach các role cho  accesstoekn
     * @update:
     */
    public List<SimpleGrantedAuthority> getRolesFromToken(String token) {

//        Claims claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
        Jws<Claims> claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token);

        List<SimpleGrantedAuthority> roles = null;
//        log.info(claims);
//        Boolean isAdmin = claims.get("isAdmin", Boolean.class);
//        Boolean isUser = claims.get("isUser", Boolean.class);

//        if (Validator.isNotNull(isAdmin) && isAdmin) {
        roles = Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
//        }
//
//        if (Validator.isNotNull(isUser) && isAdmin) {
//            roles = Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
//        }
        return roles;
    }

    public JwtToken createAccessTokenResetPassword(String email) {
        try {
            String authorities = StringPool.BLANK;
            CustomUserDetail principal = (CustomUserDetail) this.userDetailsService.loadUserByUsername(email);
            Date exDate = new Date(System.currentTimeMillis() + jwtExpirationInMs);
            return this.createAccessTokenResetPassword( principal, email, authorities, exDate);
        } catch (UsernameNotFoundException e) {
            throw e;
        }
    }

    public JwtToken createAccessTokenResetPassword(CustomUserDetail userPrincipal, String username, String authorities,
                                                   Date duration) {
        String hashKey = this.getHashKey(username);

        userPrincipal.setHashKey(hashKey);

        String jwt = Jwts.builder().setSubject(JsonUtil.convertToJsonString(userPrincipal))
                .claim(SecurityConsants.Jwt.HASHKEY, hashKey).signWith(SignatureAlgorithm.HS512, secret)
                .setIssuedAt(new Date()).setExpiration(duration).compact();

        return JwtToken.builder().token(jwt).tokenExpDate(duration).userId(userPrincipal.getUserId())
                .build();
    }

    /*
     * @author: huynq
     * @since: 11/15/2021 9:26 PM
     * @description:  lấy token từ token
     * @update:
     */
    public Authentication getAuthentication(String token) {
        log.info("current token {}", token);
        Claims claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
        String userid = claims.getSubject();
        String email = claims.get("email", String.class);
        String userType = claims.get("userType", String.class);

        CustomUserDetail userDetail = new CustomUserDetail();
        userDetail.setUserId(userid);
        userDetail.setEmail(email);
        userDetail.setUserType(userType);

        Collection<SimpleGrantedAuthority> grantedAuthorities = new ArrayList<>();

        if (Constants.mainRole.CUSTOMER.equals(userType)) {
            // trả về role customer
            grantedAuthorities.add(new SimpleGrantedAuthority(Constants.mainRole.CUSTOMER));
        } else {
            // trả về role admin
            grantedAuthorities = this.grantedAuthoritiesByid(userid);
            grantedAuthorities.add(new SimpleGrantedAuthority(Constants.mainRole.EMPLOYEE));

//            String rawAuthorities[] = this.employeeService.getUserGrantedAuthority(userid);
//            if (Validator.isNotNull(rawAuthorities) && rawAuthorities.length > 0) {
//                grantedAuthorities = Arrays.stream(rawAuthorities)
//                        .filter(StringUtils::isNotEmpty)
//                        .map(SimpleGrantedAuthority::new)
//                        .collect(Collectors.toList());
//            }
        }
        userDetail.setAuthorities(grantedAuthorities);

        System.err.println(userDetail);
        return new UsernamePasswordAuthenticationToken(userDetail, token, grantedAuthorities);
    }

    /*
     * @author: huynq
     * @since: 11/16/2021 12:26 AM
     * @description:  lấy list role của người dùng
     * @update:
     */
    public Collection<SimpleGrantedAuthority> grantedAuthoritiesByid(String userId) {
        Collection<SimpleGrantedAuthority> grantedAuthorities = new ArrayList<>();
        String rawAuthorities[] = this.employeeService.getUserGrantedAuthority(userId);
        log.info(rawAuthorities);
        if (Validator.isNotNull(rawAuthorities) && rawAuthorities.length > 0) {
            grantedAuthorities = Arrays.stream(rawAuthorities)
                    .filter(StringUtils::hasLength)
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList());
        }
        return grantedAuthorities;
    }

    /**
     * @author: huynq
     * @since: 9/13/2021 1:31 AM
     * @description: Thực hiện hủy token khi đăng xuất
     * @update:
     */
    public void invalidateToken() {
        Optional<CustomUserDetail> userDetails = SecurityUtils.geUserDetails();
        log.info(userDetails.toString());
    }

    /**
     * @author: huynq
     * @since: 9/13/2021 1:30 AM
     * @description: Thực hiện kiểm tra xem token còn được hoặc động không
     * @update:
     */
    public boolean isTokenInBlackList(String userName, String token) {
        return false;
    }
}
