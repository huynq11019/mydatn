/*
 * AuthenticationFailureHandlerImpl.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */

package com.example.ducky_be.core.sercurity.handler;

import com.example.ducky_be.repository.entity.UserLogin;
import com.example.ducky_be.service.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;

/**
 * The Class AuthenticationFailureHandlerImpl.
 */
@Component
public class AuthenticationFailureHandlerImpl implements AuthenticationFailureHandler {

    /**
     * The user login service.
     */
    @Autowired
    private UserLoginService userLoginService;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                                        AuthenticationException exception) throws IOException, ServletException {
        // Save authentication failure to user login table
        UserLogin loginLog = UserLogin.builder().username(request.getUserPrincipal().getName())
                .ip(request.getRemoteAddr()).loginTime(Instant.now()).success(false).description(exception.getMessage())
                .build();

        this.userLoginService.save(loginLog);

    }

}
