package com.example.ducky_be.core.sercurity;

import lombok.*;

import java.time.Instant;
import java.util.Date;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class JwtToken {
    /**
     * The Constant serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * The token.
     */
    private String token;


    /**
     * The token exp date.
     */
    private Date tokenExpDate;

    /**
     * The created by.
     */
    private String createdBy;

    /**
     * The created date.
     */
    private Date createdDate;

    /**
     * The updated by.
     */
    private String updatedBy;

    /**
     * The updated date.
     */
    private Instant updatedDate;

    /**
     * The user id.
     */
    private String userId;

    /**
     * The user name.
     */
    private String userName;
}
