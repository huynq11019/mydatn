package com.example.ducky_be.core.sercurity.handler;

import com.example.ducky_be.core.sercurity.CustomOAuth2User;
import com.example.ducky_be.core.sercurity.CustomUserDetail;
import com.example.ducky_be.core.sercurity.CustomUserDetailService;
import com.example.ducky_be.core.sercurity.TokenPovider;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Log4j2
@Component
public class OAuthLoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
    @Autowired
    CustomUserDetailService userService;
    @Autowired
    private TokenPovider tokenPovider;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
//        super.onAuthenticationSuccess(request, response, authentication);

        CustomOAuth2User oauth2User = (CustomOAuth2User) authentication.getPrincipal();
        String oauth2ClientName = oauth2User.getOauth2ClientName();
        String username = oauth2User.getEmail();
        String name = oauth2User.getName();
//        String picture = oauth2User.getPicture();
        log.info("oauth2ClientName : {}", oauth2User.getAuthorities());
        log.info("username : {}", username);
        log.info("name : {}", name);
//        log.info("picture : {}", oauth2User.getAuthorities());
        // Biến khởi tạo user
        CustomUserDetail userDetail;
        String token = "";
        String targetUrl = "";
        // load user from db
        try {
            userDetail = (CustomUserDetail) userService.loadUserByUsername("user_" + username);
            token = this.tokenPovider.generateToken(userDetail);
            targetUrl = "http://localhost:4200/login?tokendeep=" + token;
        } catch (Exception e) {
            log.error("error : {}", e.getMessage());
            targetUrl = "http://localhost:4200/register?err=usernotfound"+"&email="+username;
        }


        request.setAttribute("tokendeep", token);
        response.setStatus(response.SC_MOVED_TEMPORARILY);
//        response.setHeader("Location", "localhost:4200/login/success?token=" + token);
        getRedirectStrategy().sendRedirect(request, response, targetUrl);
//        response.addCookie(new Cookie("token", token));
//        super.onAuthenticationSuccess(request, response, authentication);
//        super.onAuthenticationSuccess(request, response, authentication);


    }
}
