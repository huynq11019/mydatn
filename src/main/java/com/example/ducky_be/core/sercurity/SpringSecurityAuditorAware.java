package com.example.ducky_be.core.sercurity;

import org.springframework.data.domain.AuditorAware;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class SpringSecurityAuditorAware implements AuditorAware<String> {
    @Override
    public Optional<String> getCurrentAuditor() {
        Optional<String> loginUserId = SecurityUtils.getLoginUserId();
        return Optional.ofNullable(loginUserId.orElse("System"));
    }
}
