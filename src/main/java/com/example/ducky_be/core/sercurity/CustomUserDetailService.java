package com.example.ducky_be.core.sercurity;

import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.constants.SecurityConsants;
import com.example.ducky_be.core.exception.BadRequestAlertException;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.CustomerRepository;
import com.example.ducky_be.repository.EmployeeRepository;
import com.example.ducky_be.repository.entity.Customer;
import com.example.ducky_be.repository.entity.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.NoSuchElementException;

@Slf4j
@Service
public class CustomUserDetailService implements UserDetailsService {
    /**
     * Locates the user based on the username. In the actual implementation, the search
     * may possibly be case sensitive, or case insensitive depending on how the
     * implementation instance is configured. In this case, the <code>UserDetails</code>
     * object that comes back may have a username that is of a different case than what
     * was actually requested..
     *
     * @param username the username identifying the user whose data is required.
     * @return a fully populated user record (never <code>null</code>)
     * @throws UsernameNotFoundException if the user could not be found or the user has no
     * GrantedAuthority
     */
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private TokenPovider tokenPovider;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        String userNamFormat = "";
        Collection<SimpleGrantedAuthority> roles = null;

        if (StringUtils.hasText(username) && username.startsWith("admin_")) {
            userNamFormat = username.substring(6);
            log.info(userNamFormat);
            Employee employee = employeeRepository.findEmployeeByEmailAndStatus(userNamFormat, Constants.EntityStatus.ACTIVE).orElseThrow(
                    () -> new NoSuchElementException("Employee không tồn tại trong hệ thống")
            );
            log.info("employee login =>" + employee);
            if (Validator.isNotNull(employee)) {
//                roles = Arrays.asList(new SimpleGrantedAuthority(Constants.mainRole.EMPLOYEE));
                roles = this.tokenPovider.grantedAuthoritiesByid(employee.getId());
                roles.add(new SimpleGrantedAuthority(Constants.mainRole.EMPLOYEE));
                return new CustomUserDetail.CustomUserDetailBuilder()
                        .email(employee.getEmail())
                        .employee(employee)
                        .password(employee.getPassword())
                        .username(employee.getFullName())
                        .userType(SecurityConsants.ADMIN_TYPE)
                        .userId(employee.getId())
                        .authorities(roles)
                        .build();
            }

        }

        if (StringUtils.hasText(username) && username.startsWith("user_")) {
            userNamFormat = username.substring(5);
            //  Mặc định sẽ là role khách hàng
            roles = Arrays.asList(new SimpleGrantedAuthority(Constants.mainRole.CUSTOMER));

            log.info(userNamFormat);
            Customer customer = this.customerRepository.findByEmail(userNamFormat
            ).orElseThrow(() ->
                    new NoSuchElementException("User không tồn tại trong hệ thống"));
            if (customer.getStatus() == 0) {
//                throw new NoSuchElementException("Tài khoản chưa được xác thực");
            } else if (customer.getStatus() != 1) {
//                throw new NoSuchElementException("Lỗi không xác định");
            }
            log.info(customer.toString());

            return new CustomUserDetail.CustomUserDetailBuilder()
                    .userId(customer.getId())
                    .email(customer.getEmail())
                    .password(customer.getPasswordHash())
                    .username(customer.getFullName())
                    .authorities(roles)
                    .customer(customer)
                    .userType(SecurityConsants.CUSTOMER_TYPE)
                    .build();

//            if (username.equals("user")) {
//                roles = Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
//                return new User("user", "$2a$10$N.p.qv711KH/wFrkfpE/Ze6R8bOoPNwcdLZHK8yBhxVgdPmrDQfDC", roles);
//            }
        }
//        throw new UsernameNotFoundException("User not found with the name " + username);
        throw new BadRequestAlertException("Tài khoản mật khẩu không đúng", ErrorCode.USER_NOT_FOUND);
    }


}
