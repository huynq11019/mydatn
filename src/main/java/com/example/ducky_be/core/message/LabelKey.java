/*
 * LabelKey.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.message;

/**
 * <PRE>
 * The Interface LabelKey.</BR>
 * </PRE>
 * <p>
 * Jul 12, 2021 - HaiTH: Create new
 *
 * @author HaiTH
 */
public interface LabelKey {

    /**
     * The system successful.
     */
    String SYSTEM_SUCCESSFUL = "system.success";

    /**
     * The sytem failed.
     */
    String SYTEM_FAILED = "system.failed";

    /**
     * The error action invalid.
     */
    String ERROR_ACTION_INVALID = "error.action.invalid";

    /**
     * The error invalid token.
     */
    String ERROR_INVALID_TOKEN = "error.token.invalid";

    /**
     * The error login input invalid.
     */
    String ERROR_LOGIN_INPUT_INVALID = "error.login.input.invalid";

    /**
     * The error invalid username.
     */
    String ERROR_INVALID_USERNAME = "error.invalid.username";

    /**
     * The error invalid user or password.
     */
    String ERROR_INVALID_USER_OR_PASSWORD = "error.invalid.username.or.password";

    /**
     * The error constraint violation.
     */
    String ERROR_CONSTRAINT_VIOLATION = "error.constraint.violation";

    /**
     * The error method argument not valid.
     */
    String ERROR_METHOD_ARGUMENT_NOT_VALID = "error.method.argument.not.valid";

    /**
     * The error concurrency failure.
     */
    String ERROR_CONCURRENCY_FAILURE = "error.concurrency.failure";

    /**
     * The error file storage failure.
     */
    String ERROR_FILE_STORAGE_FAILURE = "error.file.storage.failure";

    /**
     * The login successful.
     */
    String LOGIN_SUCCESSFUL = "login.successful";

    /**
     * The error input invalid.
     */
    String ERROR_INPUT_INVALID = "error.input.invalid";

    /**
     * The error role code duplicate.
     */
    String ERROR_ROLE_CODE_DUPLICATE = "error.role.code.duplicate";

    /**
     * The delete email false.
     */
    String DELETE_EMAIL_FALSE = "error.delete.email.false";

    /**
     * The email id not exist.
     */
    String EMAIL_ID_NOT_EXIST = "error.emailId.not.existed";

    /**
     * The error code data is existed.
     */
    String ERROR_CODE_DATA_IS_EXISTED = "error.code.data.is.existed";

    /**
     * The error can not delete approval type.
     */
    String ERROR_CAN_NOT_DELETE_APPROVAL_TYPE = "error.can.not.delete.approval.type";

    /**
     * The error can not delete work flow.
     */
    String ERROR_CAN_NOT_DELETE_WORK_FLOW = "error.can.not.delete.workflow";

    /**
     * The error file storage failed.
     */
    String ERROR_FILE_STORAGE_FAILED = "error.file.storage.failed";
    ;

    /**
     * The mount is invalid.
     */
    String MOUNT_IS_INVALID = "error.mount.invalid";

    /**
     * The error department invalid.
     */
    String ERROR_DEPARTMENT_INVALID = "error.department.invalid";

    /**
     * The error department not exist.
     */
    String ERROR_DEPARTMENT_NOT_EXIST = "error.department.not.existed";

    /**
     * The error record is waiting.
     */
    String ERROR_RECORD_IS_WAITING = "error.record.is.waiting";

    /**
     * The department not summarize.
     */
    String DEPARTMENT_NOT_SUMMARIZE = "department.not.sumarize";

    /**
     * The ci duplicated.
     */
    String CI_DUPLICATED = "error.ci.duplicated";

    /**
     * The user current does not have permission.
     */
    String USER_CURRENT_DOES_NOT_HAVE_PERMISSION = "error.user.current.does.not.have.permission";

    /**
     * The step user is expired.
     */
    String STEP_USER_IS_EXPIRED = "error.step.user.is.expired";

    /**
     * The error can not delete budget plan.
     */
    String ERROR_CAN_NOT_DELETE_BUDGET_PLAN = "error.can.not.delete.budget.plan";

    String ERR_NOT_SUM_RECORD_WITH_DEPARTMENT = "error.budget.notSum.recordWithDepartment";

    String ERR_NOT_RECORD_WITH_BUDGET_PLAN_ID = "error.budgetPlan.noRecordWithBudgetPlanId";

    /**
     * The error criteria not exist
     */
    String ERR_CRITERIA_NOT_EXIST = "error.criteria.not.exist";

    /**
     * The user can not be edit
     */
    String ERROR_CAN_NOT_EDIT_THIS_USER = "error.can.not.update.this.user";

    /**
     * Update user successful
     */
    String UPDATE_USER_SUCCESSFUL = "update.user.successful";

    /**
     * Error user not exist
     */
    String ERR_USER_NOT_EXIST = "error.user.not.exist";

    /**
     * The error not exist.
     */
    String ERR_NOT_EXIST = "error.not.existed";

    /**
     * The error is null.
     */
    String ERR_IS_NULL = "error.is.null";

    /**
     * The error invalid data
     */
    String ERROR_DATA_INVALID = "error.data-invalid";

    /**
     * The record does not exist
     */
    String ERROR_RECORD_DOES_NOT_EXIT = "error.record-does-not-exist";

    /**
     * The error user not permission.
     */
    String ERR_USER_NOT_PERMISSION = "error.user.not.permission";

    /**
     * supplier evaluate error
     */
    String ERROR_SUPPLIER_EVALUATE_SCORE_INVALID = "error.sqm.audit-supplier.evaluate-score-invalid";

    String ERROR_SUPPLIER_NOT_EXIST = "error.sqm.supplier.not-exist";

    String ERROR_ACCESSORY_NOT_EXIST = "error.sqm.accessory.not-exist";

    String ERROR_CRITERIA_NOT_EXIST = "error.sqm.criteria.not-exist";

    /**
     * Rating type error
     */
    String ERROR_RATING_TYPE_NOT_EXIST = "error.sqm.rating-type.not-exist";

    /**
     * The error user not have email
     */
    String USER_NOT_HAVE_EMAIL = "error.not.have.email";

    /**
     * Ranking error
     */
    String ERROR_RANKING_LOT_NG_BIGGER_LOT = "error.sqm.ranking.lot-ng-bigger-lot";

    String ERROR_RANKING_IMPROVE_QPN_BIGGER_TOTAL_QPN = "error.sqm.ranking.improve-qpn-bigger-total-qpn";

    String ERROR_RANKING_FEEDBACK_TWENTY_FOUR_BIGGER_TOTAL_FEEDBACK = "error.sqm.ranking.feedback-twenty-four-bigger-total-feedback";

    // bản đánh giá xếp hạng tháng đã tồn tại -> 1 tháng chỉ có 1 bản đánh giá
    String ERROR_MONTHLY_RANKING_ALREADY_EXISTS = "error.sqm.ranking.monthly-ranking-already-exists";

    /**
     * Vendor error
     */
    String ERROR_VENDOR_DOES_NOT_EXIT = "error.sqm.vendor.not-exist";

    String ERROR_USERNAME_ALREADY_EXIST = "error.sqm.user.already-exist";

    String ERROR_TELEPHONE_ALREADY_EXIST = "error.sqm.telephone.already-exist";

    String ERROR_EMAIL_ALREADY_EXIST = "error.sqm.email.already-exist";

    String ERROR_MULTIPART_FILE_INVALID = "error.multipart.file.invalid";

    String ERROR_FILE_CORRUPTED = "error.file.corrupted";

    String LOGIN_FAIL_BLOCK_ACCOUNT = "error.login-fail.block-user";

    String LOGIN_FAIL_WARNING_BEFORE_BLOCK = "error.login-fail.warning-before-block";

    String MIN_DATE_INVALID = "error.mindate.invalid";

    String CONTACT_VENDOR_NOT_EXISTED = "error.sqm.vendor.not-exist";

    String IMPROVER_NOT_EXISTED = "error.sqm.improver.not-exist";

    String CONTACT_VENDOR_PHONE_EXISTED = "error.sqm.vendor.phone-exist";

    String CONTACT_VENDOR_EMAIL_EXISTED = "error.sqm.vendor.email-exist";

    String CONTACT_VENDOR_FULL_NAME_REQUIRED = "error.sqm.vendor.full-name-required";

    String CONTACT_VENDOR_PHONE_REQUIRED = "error.sqm.vendor.phone-required";

    String CONTACT_VENDOR_EMAIL_REQUIRED = "error.sqm.vendor.email-required";

    String CONTACT_VENDOR_CODE_REQUIRED = "error.sqm.vendor.code-required";

    String CONTACT_VENDOR_NAME_REQUIRED = "error.sqm.vendor.name-required";

    String CONTACT_VENDOR_PHONE_FORMAT = "error.sqm.vendor.phone-format";

    String CONTACT_VENDOR_EMAIL_FORMAT = "error.sqm.vendor.email-format";

    String FILE_IS_EMPTY = "error.sqm.vendor.file-empty";

    String FILE_NOT_FOUND = "error.sqm.vendor.file-not-found";

    String FILE_NOT_EXIST = "error.file-not-exist";

    String CONTACT_VENDOR_PHONE_DUPLICATE = "error.sqm.vendor.phone-duplicate";

    String CONTACT_VENDOR_EMAIL_DUPLICATE = "error.sqm.vendor.email-duplicate";

    String CONTACT_VENDOR_CODE_NOT_FOUND = "error.sqm.vendor.code-not-found";

    String FILE_TEMPLATE_ERROR = "error.sqm.vendor.file-template-error";

    String FORM_DATA_NULL = "error.formData.null";

    String AUTHENTICATION_REQUIRED = "error.authentication-required";

    String DO_NOT_HAVE_PERMISSION_GET_EVALUATE_ISSUE = "error.do-not-have-permission-get-evaluate-issue";

    String USER_MUST_BELONG_IN_VENDOR = "error.user-must-belong-in-vendor";

    String TYPE_INVALID = "error.sqm.change-management-type-invalid";

    String MST_VENDOR_NOT_FOUND = "error.sqm.mst-vendor-not-found";

    String CHANGE_MANAGEMENT_NOT_FOUND = "error.sqm.change-management-not-found";

    String DO_NOT_HAVE_PERMISSION = "error.sqm.do-not-have-permission";

    String USER_NOT_FOUND = "error.user-not-found";

    String USER_EXISTED_WITH_OTHER_VENDOR = "error.sqm.vendor.user-existed-with-other-vendor";

    String USER_NOT_ACCESS = "error.http.403";

    String SUPPLIERID_INVALID = "error.ppap.supplierId.invalid";

    String SUPPLIER_DEADLINE_REQUIRED = "error.proposal.supplierDeadline.required";

    String REPRENTATIVE_SUPPLIER = "error.contactvendor.usercontact.reqired";

    String PROPOSAL_REQUIRED = "error.proposal.required";

    String SQE_COMMENT_REQUIRED = "error.proposal.sqecomment.required";

    String PROPOSAL_DEADLINE = "error.proposal.deadline.required";

    String OTHER_SPECIFYCONTENT_REQUIRED = "error.ppapPsw.specifyContent.required";

    String RESULT_MEET_CONTENT_REQUIRED = "error.ppapPsw.resultMeetContent.required";

    String OTHER_DISPOSITION_CONTENT_REQUIRED = "error.ppapPsw.dispositionOtherContent.required";

    String PSW_DUPLICATE = "error.ppapPsw.duplicate";

    String PRIMARY_EXISTS = "error.primary.exists";

    String ADDRESS_REQUEST_NOT_BLANK = "error.address.request.notblank";
}
