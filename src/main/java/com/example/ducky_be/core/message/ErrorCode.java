/*
 * ErrorCode.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.message;

import com.example.ducky_be.core.utils.StringPool;
import com.example.ducky_be.core.utils.Validator;
import org.springframework.http.HttpStatus;

import java.util.Locale;

import static com.example.ducky_be.core.message.LabelKey.PRIMARY_EXISTS;

/**
 * <PRE>
 * The Enum ErrorCode.</BR>
 * </PRE>
 * <p>
 * Jul 5, 2021 - HaiTH: Create new
 *
 * @author HaiTH
 */
public enum ErrorCode {

    NA(-1, StringPool.BLANK),

    /**
     * The err 00000.
     */
    ERR_00000(0, LabelKey.SYSTEM_SUCCESSFUL),

    /**
     * The err 00001.
     */
    ERR_SYSTEM_00001(1, LabelKey.SYTEM_FAILED),

    /**
     * The err 00100.
     */
    ERR_INVALID_USER_OR_PASSWORD(401, 2, LabelKey.ERROR_INVALID_USER_OR_PASSWORD),

    /**
     * The err authen 00003.
     */
    ERR_LOGIN_INPUT_INVALID(3, LabelKey.ERROR_LOGIN_INPUT_INVALID),

    /**
     * The err authen 00002.
     */
    ERR_INVALID_TOKEN(4, LabelKey.ERROR_INVALID_TOKEN),

    /**
     * The error input invalid.
     */
    ERR_INPUT_INVALID(5, LabelKey.ERROR_INPUT_INVALID),

    /**
     * The error role code duplicate.
     */
    ERR_ROLE_CODE_DUPLICATE(6, LabelKey.ERROR_ROLE_CODE_DUPLICATE),

    /**
     * The err data is exist.
     */
    ERR_DATA_IS_EXIST(7, LabelKey.ERROR_CODE_DATA_IS_EXISTED),

    /**
     * The err 00100.
     */
    ERR_00100(100, LabelKey.DELETE_EMAIL_FALSE),

    /**
     * The err 00101.
     */
    ERR_00101(101, LabelKey.EMAIL_ID_NOT_EXIST),

    /**
     * The err can not delete approval type.
     */
    ERR_CAN_NOT_DELETE_APPROVAL_TYPE(8, LabelKey.ERROR_CAN_NOT_DELETE_APPROVAL_TYPE),

    /**
     * The err can not delete work flow.
     */
    ERR_CAN_NOT_DELETE_WORK_FLOW(9, LabelKey.ERROR_CAN_NOT_DELETE_WORK_FLOW),

    /**
     * The err file storage failed.
     */
    ERR_FILE_STORAGE_FAILED(10, LabelKey.ERROR_FILE_STORAGE_FAILED),

    /**
     * The user not access.
     */
    USER_NOT_ACCESS(11, LabelKey.USER_NOT_ACCESS),

    /**
     * The mount is invalid.
     */
    MOUNT_IS_INVALID(110, LabelKey.MOUNT_IS_INVALID),

    /**
     * The error department invalid.
     */
    ERROR_DEPARTMENT_INVALID(111, LabelKey.ERROR_DEPARTMENT_INVALID),

    /**
     * The error department not exist.
     */
    ERR_DEPARTMENT_NOT_EXIST(112, LabelKey.ERROR_DEPARTMENT_NOT_EXIST),

    /**
     * The error record is waiting.
     */
    ERROR_RECORD_IS_WAITING(113, LabelKey.ERROR_RECORD_IS_WAITING),

    /**
     * The department not summarize.
     */
    DEPARTMENT_NOT_SUMMARIZE(114, LabelKey.DEPARTMENT_NOT_SUMMARIZE),

    CI_DUPLICATED(115, LabelKey.CI_DUPLICATED),

    ERR_USER_CURRENT_DOES_NOT_HAVE_PERMISSION(704, LabelKey.USER_CURRENT_DOES_NOT_HAVE_PERMISSION),

    ERR_STEP_USER_IS_EXPIRED(705, LabelKey.STEP_USER_IS_EXPIRED),

    /**
     * The err can not delete approval type.
     */
    ERR_CAN_NOT_DELETE_BUDGET_PLAN(116, LabelKey.ERROR_CAN_NOT_DELETE_BUDGET_PLAN),

    ERR_NOT_SUM_RECORD_WITH_DEPARTMENT(117, LabelKey.ERR_NOT_SUM_RECORD_WITH_DEPARTMENT),

    ERR_NOT_RECORD_WITH_BUDGET_PLAN_ID(117, LabelKey.ERR_NOT_RECORD_WITH_BUDGET_PLAN_ID),

    /**
     * The err criteria not exist
     */
    ERR_CRITERIA_NOT_EXIST(120, LabelKey.ERR_CRITERIA_NOT_EXIST),

    /**
     * Can not update this user.
     */
    ERR_CAN_NOT_UPDATE_THIS_USER(140, LabelKey.ERROR_CAN_NOT_EDIT_THIS_USER),

    /**
     * The error user not exist.
     */
    ERR_USER_NOT_EXIST(141, LabelKey.ERR_USER_NOT_EXIST),

    /**
     * The error not exist.
     */
    ERR_NOT_EXIST(144, LabelKey.ERR_NOT_EXIST),

    /**
     * The error is null.
     */
    ERR_IS_NULL(145, LabelKey.ERR_IS_NULL),

    ERROR_DATA_INVALID(146, "CHƯA CHỌN OPTION SẢN PHẨM"),

    ERROR_SUPPLIER_EVALUATE_SCORE_INVALID(147, LabelKey.ERROR_SUPPLIER_EVALUATE_SCORE_INVALID),

    ERROR_SUPPLIER_NOT_EXIST(148, LabelKey.ERROR_SUPPLIER_NOT_EXIST),

    ERROR_ACCESSORY_NOT_EXIST(149, LabelKey.ERROR_ACCESSORY_NOT_EXIST),

    /**
     * The error user not permission.
     */
    ERR_USER_NOT_PERMISSION(150, LabelKey.ERR_USER_NOT_PERMISSION),

    ERROR_RECORD_DOES_NOT_EXIT(151, LabelKey.ERROR_RECORD_DOES_NOT_EXIT),

    ERROR_CRITERIA_NOT_EXIST(152, LabelKey.ERROR_CRITERIA_NOT_EXIST),

    ERROR_RATING_TYPE_NOT_EXIST(153, LabelKey.ERROR_RATING_TYPE_NOT_EXIST),

    /**
     * The error is null.
     */
    USER_NOT_HAVE_EMAIL(154, LabelKey.USER_NOT_HAVE_EMAIL),

    /**
     * Ranking supplier error
     */
    ERROR_RANKING_LOT_NG_BIGGER_LOT(155, LabelKey.ERROR_RANKING_LOT_NG_BIGGER_LOT),

    ERROR_RANKING_IMPROVE_QPN_BIGGER_TOTAL_QPN(156, LabelKey.ERROR_RANKING_IMPROVE_QPN_BIGGER_TOTAL_QPN),

    ERROR_RANKING_FEEDBACK_TWENTY_FOUR_BIGGER_TOTAL_FEEDBACK(156, LabelKey.ERROR_RANKING_FEEDBACK_TWENTY_FOUR_BIGGER_TOTAL_FEEDBACK),

    ERROR_MONTHLY_RANKING_ALREADY_EXISTS(158, LabelKey.ERROR_MONTHLY_RANKING_ALREADY_EXISTS),

    /**
     * Vendor error
     */
    ERROR_VENDOR_DOES_NOT_EXIT(157, LabelKey.ERROR_VENDOR_DOES_NOT_EXIT),
    USERNAME_ALREADY_EXIST(158, LabelKey.ERROR_USERNAME_ALREADY_EXIST),
    TELEPHONE_ALREADY_EXIST(158, LabelKey.ERROR_TELEPHONE_ALREADY_EXIST),
    EMAIL_ALREADY_EXIST(158, LabelKey.ERROR_EMAIL_ALREADY_EXIST),
    ERR_MULTIPART_FILE_INVALID(1010, LabelKey.ERROR_MULTIPART_FILE_INVALID),
    ERR_FILE_CORRUPTED(1998, LabelKey.ERROR_FILE_CORRUPTED),

    LOGIN_FAIL_BLOCK_ACCOUNT(401, 4010001, LabelKey.LOGIN_FAIL_BLOCK_ACCOUNT),
    LOGIN_FAIL_WARNING_BEFORE_BLOCK(401, 4010002, LabelKey.LOGIN_FAIL_WARNING_BEFORE_BLOCK),
    /**
     * shipping control
     */
    MIN_DATE_INVALID(160, LabelKey.MIN_DATE_INVALID),
    CONTACT_VENDOR_NOT_EXIST(2120, LabelKey.CONTACT_VENDOR_NOT_EXISTED),
    IMPROVER_NOT_EXIST(2121, LabelKey.IMPROVER_NOT_EXISTED),
    CONTACT_VENDOR_PHONE_EXISTED(161, LabelKey.CONTACT_VENDOR_PHONE_EXISTED),
    CONTACT_VENDOR_EMAIL_EXISTED(162, LabelKey.CONTACT_VENDOR_EMAIL_EXISTED),
    CONTACT_VENDOR_FULL_NAME_REQUIRED(163, LabelKey.CONTACT_VENDOR_FULL_NAME_REQUIRED),
    CONTACT_VENDOR_PHONE_REQUIRED(164, LabelKey.CONTACT_VENDOR_PHONE_REQUIRED),
    CONTACT_VENDOR_EMAIL_REQUIRED(165, LabelKey.CONTACT_VENDOR_EMAIL_REQUIRED),
    CONTACT_VENDOR_CODE_REQUIRED(166, LabelKey.CONTACT_VENDOR_CODE_REQUIRED),
    CONTACT_VENDOR_NAME_REQUIRED(167, LabelKey.CONTACT_VENDOR_NAME_REQUIRED),
    CONTACT_VENDOR_PHONE_FORMAT(168, LabelKey.CONTACT_VENDOR_PHONE_FORMAT),
    CONTACT_VENDOR_EMAIL_FORMAT(169, LabelKey.CONTACT_VENDOR_EMAIL_FORMAT),
    FILE_IS_EMPTY(170, LabelKey.FILE_IS_EMPTY),
    FILE_NOT_FOUND(252, LabelKey.FILE_NOT_FOUND),
    /**
     * The error not exist.
     */
    FILE_NOT_EXIST(222, LabelKey.FILE_NOT_EXIST),
    CONTACT_VENDOR_PHONE_DUPLICATE(171, LabelKey.CONTACT_VENDOR_PHONE_DUPLICATE),
    CONTACT_VENDOR_EMAIL_DUPLICATE(172, LabelKey.CONTACT_VENDOR_EMAIL_DUPLICATE),
    CONTACT_VENDOR_CODE_NOT_FOUND(173, LabelKey.CONTACT_VENDOR_CODE_NOT_FOUND),
    FILE_TEMPLATE_ERROR(174, LabelKey.FILE_TEMPLATE_ERROR),
    FORM_DATA_NULL(175, LabelKey.FORM_DATA_NULL),
    AUTHENTICATION_REQUIRED(176, LabelKey.AUTHENTICATION_REQUIRED),
    DO_NOT_HAVE_PERMISSION_GET_EVALUATE_ISSUE(177, LabelKey.DO_NOT_HAVE_PERMISSION_GET_EVALUATE_ISSUE),
    USER_MUST_BELONG_IN_VENDOR(178, LabelKey.USER_MUST_BELONG_IN_VENDOR),
    TYPE_INVALID(179, LabelKey.TYPE_INVALID),
    MST_VENDOR_NOT_FOUND(180, LabelKey.MST_VENDOR_NOT_FOUND),
    CHANGE_MANAGEMENT_NOT_FOUND(181, LabelKey.CHANGE_MANAGEMENT_NOT_FOUND),
    DO_NOT_HAVE_PERMISSION(182, LabelKey.DO_NOT_HAVE_PERMISSION),
    USER_NOT_FOUND(183, LabelKey.USER_NOT_FOUND),
    USER_EXISTED_WITH_OTHER_VENDOR(184, LabelKey.USER_EXISTED_WITH_OTHER_VENDOR),
    SUPPLIERID_INVALID(185, LabelKey.SUPPLIERID_INVALID),
    SUPPLIER_DEADLINE_REQUIRED(186, LabelKey.SUPPLIER_DEADLINE_REQUIRED),
    REPRENTATIVE_SUPPLIER(187, LabelKey.REPRENTATIVE_SUPPLIER),
    PROPOSAL_REQUIRED(188, LabelKey.PROPOSAL_REQUIRED),
    SQE_COMMENT_REQUIRED(189, LabelKey.SQE_COMMENT_REQUIRED),
    PROPOSAL_DEADLINE_REQUIRED(190, LabelKey.SUPPLIER_DEADLINE_REQUIRED),
    OTHER_SPECIFYCONTENT_REQUIRED(191, LabelKey.OTHER_SPECIFYCONTENT_REQUIRED),
    RESULT_MEET_CONTENT_REQUIRED(192, LabelKey.RESULT_MEET_CONTENT_REQUIRED),
    OTHER_DISPOSITION_CONTENT_REQUIRED(193, LabelKey.OTHER_DISPOSITION_CONTENT_REQUIRED),
    ERR_PSW_DUPLICATE(194, LabelKey.PSW_DUPLICATE),
    EXIST_PRIMARY(195, PRIMARY_EXISTS),
    ERROR_USER_NOT_FOUND(196, "Tài khoản không tồn tại"),
    PRODUCT_NOT_FOUND(197, "Sản phẩm không tồn tại"),
    AUTHORITY_EXISTED(198, "AUTHORITY đã tồn tại"),
    AUTHORITY_NOT_EXISTED(199, "Quyền không tồn tại"),
    ERR_USER_EMAIL_EXIST(200, "Email đã tồn tại"),
    ORDER_NOT_FOUND(201, "Order không tồn tại"),
    ERROR_CAPCHA_CODE(202, "captCha không hợp lệ"),
    PRODUCT_ORDER_ISREUIRED(203, "Sản phẩm order là bắt buộc");

    /**
     * Find by error code.
     *
     * @param errorCode the error code
     * @return the error code
     */
    public static ErrorCode findByErrorCode(int errorCode) {
        ErrorCode[] arrays = values();

        for (ErrorCode emotion : arrays) {
            if (emotion.getErrorCode() == errorCode) {
                return emotion;
            }
        }

        return NA;
    }

    /**
     * The error code.
     */
    private final int errorCode;

    /**
     * The status code.
     */
    private final int statusCode;

    /**
     * The message key.
     */
    private final String messageKey;

    /**
     * Instantiates a new error code.
     *
     * @param errorCode  the error code
     * @param messageKey the message key
     */
    ErrorCode(int errorCode, String messageKey) {
        this.errorCode = errorCode;
        this.messageKey = messageKey;
        this.statusCode = HttpStatus.OK.value();
    }

    ErrorCode(int statusCode, int errorCode, String messageKey) {
        this.errorCode = errorCode;
        this.messageKey = messageKey;
        this.statusCode = statusCode;
    }

    /**
     * Gets the error code.
     *
     * @return the error code
     */
    public int getErrorCode() {
        return this.errorCode;
    }

    /**
     * Gets the status code.
     *
     * @return the status code
     */
    public int getStatusCode() {
        return this.statusCode;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage() {
        if (Validator.isNotNull(this.messageKey)) {
            return Labels.getLabels(this.messageKey, Labels.getDefaultLocale());
        }

        return StringPool.BLANK;
    }

    /**
     * Gets the message.
     *
     * @param locale the locale
     * @return the message
     */
    public String getMessage(Locale locale) {
        if (Validator.isNotNull(this.messageKey)) {
            return Labels.getLabels(this.messageKey, locale);
        }

        return StringPool.BLANK;
    }

    /**
     * Gets the message.
     *
     * @param locale the locale
     * @param objs   the objs
     * @return the message
     */
    public String getMessage(Locale locale, Object[] objs) {
        if (Validator.isNotNull(this.messageKey)) {
            return Labels.getLabels(this.messageKey, objs, locale);
        }

        return StringPool.BLANK;
    }

    /**
     * Gets the message key.
     *
     * @return the message key
     */
    public String getMessageKey() {
        return this.messageKey;
    }


    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return this.name();
    }
}
