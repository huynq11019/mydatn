package com.example.ducky_be.core.constants;

public class SecurityConsants {
    /**
     * The Interface Account.
     */
    public interface Account {

        /**
         * The system.
         */
        String SYSTEM = "system";
    }

    public static final Long JWTEXPIRATIONINMS = 6_400_000L;

    public static final Long REFRESH_JWTEXPIRATIONINMS = 186_400_000L;

    public static final String CUSTOMER_TYPE = "CUSTOMER";
    public static final String ADMIN_TYPE = "ADMIN";

    /**
     * The Interface Jwt.
     */
    public interface Jwt {

        /**
         * The authorization header.
         */
        String AUTHORIZATION_HEADER = "Authorization";

        /**
         * The token start.
         */
        String TOKEN_START = "Bearer ";

        /**
         * The privileges.
         */
        String PRIVILEGES = "privileges";

        /**
         * The hashkey.
         */
        String HASHKEY = "hash-key";

        /**
         * The refresh token.
         */
        String REFRESH_TOKEN = "refresh-token";

        /**
         * The user detail.
         */
        String USER_DETAIL = "user-detail";

        /**
         * The microsoft access token.
         */
        String MICROSOFT_ACCESS_TOKEN = "microsoft-access-token";
    }

    /**
     * The Interface SystemRole.
     */
    public interface SystemRole {

        /**
         * The admin.
         */
        String ADMIN = "ADMIN";

        /**
         * The user.
         */
        String USER = "USER";

        /**
         * The anonymous.
         */
        String ANONYMOUS = "ANONYMOUS";

        /**
         * The super admin.
         */
        String SUPER_ADMIN = "SUPER_ADMIN";
    }

    public interface TokenConstants {
        public static final String EMAIL = "email";
        public static final String USER_TYPE = "userType";
        public static final String AUTHORITY = "authority";
    }
}
