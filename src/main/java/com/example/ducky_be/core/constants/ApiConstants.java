/*
 * ApiConstants.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.constants;

import java.net.URI;

/**
 * The Class ApiConstants.
 */
public final class ApiConstants {

    /**
     * The Interface ErrorKey.
     */
    public interface ErrorKey {

        /**
         * The field errors.
         */
        String FIELD_ERRORS = "fieldErrors";

        /**
         * The message.
         */
        String MESSAGE = "message";

        /**
         * The error key.
         */
        String ERROR_KEY = "errorKey";

        /**
         * The params.
         */
        String PARAMS = "params";

        /**
         * The path.
         */
        String PATH = "path";

        /**
         * The violations.
         */
        String VIOLATIONS = "violations";
    }

    /**
     * The Interface ErrorType.
     */
    public interface ErrorType {

        /**
         * The problem base url.
         */
        String PROBLEM_BASE_URL = "http://www.vinsmart.vn/problem";

        /**
         * The default type.
         */
        URI DEFAULT_TYPE = URI.create(PROBLEM_BASE_URL + "/problem-with-message");

        /**
         * The constraint violation type.
         */
        URI CONSTRAINT_VIOLATION_TYPE = URI.create(PROBLEM_BASE_URL + "/constraint-violation");

        /**
         * The invalid password type.
         */
        URI INVALID_PASSWORD_TYPE = URI.create(PROBLEM_BASE_URL + "/invalid-password");

        /**
         * The email already used type.
         */
        URI EMAIL_ALREADY_USED_TYPE = URI.create(PROBLEM_BASE_URL + "/email-already-used");

        /**
         * The login already used type.
         */
        URI LOGIN_ALREADY_USED_TYPE = URI.create(PROBLEM_BASE_URL + "/login-already-used");

        /**
         * The phone already used type.
         */
        URI PHONE_ALREADY_USED_TYPE = URI.create(PROBLEM_BASE_URL + "/phone-already-used");

        /**
         * The appointment time available type.
         */
        URI APPOINTMENT_TIME_AVAILABLE_TYPE = URI.create(PROBLEM_BASE_URL + "/appointment-time-used");
    }

    /**
     * The Interface HttpHeaders.
     */
    public interface HttpHeaders {

        /**
         * The x total count.
         */
        String X_TOTAL_COUNT = "X-Total-Count";

        /**
         * The x action params.
         */
        String X_ACTION_PARAMS = "X-Action-Params";

        /**
         * The x action mesage.
         */
        String X_ACTION_MESAGE = "X-Action-Mesage";

        /**
         * The link format.
         */
        String LINK_FORMAT = "<{0}>; rel=\"{1}\"";
    }

    /**
     * The Interface Pagination.
     */
    public interface Pagination {

        /**
         * The next.
         */
        String NEXT = "next";

        /**
         * The prev.
         */
        String PREV = "prev";

        /**
         * The last.
         */
        String LAST = "last";

        /**
         * The first.
         */
        String FIRST = "first";

        /**
         * The page.
         */
        String PAGE = "page";

        /**
         * The size.
         */
        String SIZE = "size";
    }
}
