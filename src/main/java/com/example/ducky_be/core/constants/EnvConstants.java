/*
 * EnvConstants.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.constants;

/**
 * The Interface EnvConstants.
 */
public interface EnvConstants {

    /**
     * The Interface Profile.
     */
    public interface Profile {

        /**
         * The development.
         */
        String DEVELOPMENT = "dev";

        /**
         * The production.
         */
        String PRODUCTION = "prod";

        /**
         * The default.
         */
        String DEFAULT = "spring.profiles.default";
    }

    /**
     * The Interface Properties.
     */
    public interface Properties {

        /**
         * The server port.
         */
        String SERVER_PORT = "server.port";

        /**
         * The server servlet context path.
         */
        String SERVER_SERVLET_CONTEXT_PATH = "server.servlet.context-path";

        /**
         * The server ssl key store.
         */
        String SERVER_SSL_KEY_STORE = "server.ssl.key-store";

        /**
         * The spring application name.
         */
        String SPRING_APPLICATION_NAME = "spring.application.name";
    }

    /**
     * The Interface Protocol.
     */
    public interface Protocol {

        /**
         * The http.
         */
        String HTTP = "http";

        /**
         * The https.
         */
        String HTTPS = "https";
    }
}
