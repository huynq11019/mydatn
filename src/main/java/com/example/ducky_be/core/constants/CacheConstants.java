/*
 * CacheConstants.java
 *
 * Copyright (C) 2021 by Vinsmart. All right reserved.
 * This software is the confidential and proprietary information of Vinsmart
 */
package com.example.ducky_be.core.constants;

/**
 * The Interface CacheConstants.
 */
public interface CacheConstants {

    /**
     * The Interface Entity.
     */
    public interface Entity {

        /**
         * The users by username.
         */
        String USERS_BY_USERNAME = "users-by-username";

        /**
         * The users by email.
         */
        String USERS_BY_EMAIL = "users-by-email";

        /**
         * The privilege by role.
         */
        String PRIVILEGE_BY_ROLE_ID = "privilege-by-role-id";

        /**
         * The cf code.
         */
        String CF_CODE_ALL = "cf-code-all";

        /**
         * The user level.
         */
        String USER_LEVEL = "user-level";
    }
}
