package com.example.ducky_be.service.dto;

import com.example.ducky_be.core.mapper.SerrialMaper;
import com.example.ducky_be.repository.entity.Categories;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class CategoryDTO implements Serializable {
    /*
     * @author: NamTH
     * @since: 31/10/2021 3:33 CH
     * @description:  category dto
     * @update:
     *
     * */
    private Long id;

    private String categoryName;

    private String description;

    private Integer sortOrder;

    private Instant createDate;

    private String createBy;

    private Instant lastUpdate;

    private String updateBy;

    // status -1 là nháp, 0 xóa, 1 active
    private Integer status;

    private Long parentCategory;

    private Integer countProduct;

    private List<CategoryDTO> child;


    public CategoryDTO(Categories categories) {
        this.id = categories.getId();
        this.categoryName = categories.getCategoryName();
        this.description = categories.getDescription();
        this.sortOrder = categories.getSortOrder();
        this.createDate = categories.getCreateDate();
        this.lastUpdate = categories.getLastUpdate();
        this.updateBy = categories.getUpdateBy();
        this.createBy = categories.getCreateBy();
        this.status = categories.getStatus();
        this.countProduct = categories.getCountProduct();

    }
}
