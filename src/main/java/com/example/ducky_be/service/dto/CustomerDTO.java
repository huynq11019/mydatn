package com.example.ducky_be.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
//@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerDTO implements Serializable {

    private String id;

    //    private String userName;
    @JsonIgnore
    private String passwordHash;
    @JsonIgnore
    private String passsowordSlat;

    private String phoneNumber;

    private String email;

    private String address;

    private String fullName;

    private Integer gender;

    private String dob;

    private String avatar;

    private Integer rewardPoint;

    private String classify;

    private String ipAddress;

    /**
     * @author: huynq
     * @since: 10/23/2021 9:07 PM
     * @description: Thông tin này là bắt buộc với mỗi Entity
     * @update:
     */
    private Instant createDate;

    private String createBy;

    private Instant lastUpdate;

    private String updateBy;

    // status -1 là nháp, 0 xóa, 1 active
    private Integer status;


    private List<AddressDTO> addressDTOS;


}
