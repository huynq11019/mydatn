package com.example.ducky_be.service.dto;

import com.example.ducky_be.core.utils.GetterUtil;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.entity.Product;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

import java.time.Instant;
import java.util.Objects;

@Data
@Log4j2
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CartDTO {

    private String id;

    private String customerId;

    private String productId;

    private Long productPrice;

    private String productName;

    private int quantity;

    private Instant createDate;

    private String createBy;

    private Instant lastUpdate;

    private String updateBy;

    private Float discount;

    private String optionProductId;

    private String optionProductName;

    public void endRichProduct(Product product) {
        this.productName = product.getProductName();
        if (Validator.isNotNull(product.getPrice())) {
            this.productPrice = product.getPrice();
        }

    }

    public void setPrice(Long price) {
        System.out.println("setPrice: " + price);

        if (Objects.isNull(price)) {
            this.productPrice = 0L;
        }
        this.productPrice = price;
    }
}
