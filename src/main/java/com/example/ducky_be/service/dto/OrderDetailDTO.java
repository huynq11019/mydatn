package com.example.ducky_be.service.dto;

import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.repository.entity.OptionProduct;
import com.example.ducky_be.repository.entity.Product;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class OrderDetailDTO  implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String orderId;

    private String productId;

    private Long productPrice;

    private Long discount;

    private String productName;

    private String optionName;

    private Integer quantity;

    private Date warrantyTime;

    private String cartItemId;

    private Integer status;

    private String optionProductId;

    public void setStatus(Integer status) {
        if (Objects.isNull(status)) {
            this.status = Constants.EntityStatus.ACTIVE;
        } else {
            this.status = status;
        }

    }

    public void toDeletedOrderDetailDTO() {
        this.setStatus(Constants.EntityStatus.REMOTED);
    }

    public void enrichProduct(Product product) {
        this.setProductName(product.getProductName());
        this.setProductPrice(product.getPrice());

    }

    public void enrichOption(OptionProduct optionProduct) {
        this.setProductPrice(optionProduct.getPrice());
        this.optionName = optionProduct.getName();
    }

    public Long getTotalPrice() {
        if (Objects.isNull(this.productPrice) || Objects.isNull(this.quantity)) {
            return 0L;
        }
        return this.productPrice * this.quantity;
    }
}
