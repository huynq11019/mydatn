package com.example.ducky_be.service.dto;

import com.example.ducky_be.controller.protoco.request.ProductRequest;
import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.exception.BadRequestAlertException;
import com.example.ducky_be.core.exception.ResponseException;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.core.utils.CollectionUtil;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.entity.OptionProduct;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@RedisHash("productDTO")
public class ProductDTO implements Serializable {

    private String id;

    private String productName;

    private String thumbNail;

    private String subCateName;

    private String description;

    private Float ratingPoint;

    private Long price;

    private float discount;

    private Boolean isOrder;

    private String metaData;

    private long quantity;

    private Instant createDate;

    private String createBy;

    private Instant lastUpdate;

    private String updateBy;

    // status -1 là nháp, 0 xóa, 1 active

    private Integer status = 1;

    private Long serial;

    private Long categoryId;

    private String categoryName;

    private Integer estimateTime;

    private List<FileUploadDTO> files;

    private List<OptionProduct> optionProducts;

    public ProductDTO(ProductRequest productRequest, List<OptionProduct> optionProducts) {
        this.id = UUID.randomUUID().toString();

        this.productName = productRequest.getProductName();
        this.subCateName = productRequest.getSubCateName();
        this.description = productRequest.getDescription();
        this.discount = productRequest.getDiscount();
        this.isOrder = productRequest.getIsOrder();
        if (Validator.isNull(productRequest.getIsOrder()) ) {
            throw new BadRequestAlertException("is order không được null",ErrorCode.ERR_INPUT_INVALID);
        }
        if (isOrder && Validator.isNull(productRequest.getEstimateTime()) ) {
            throw new BadRequestAlertException("estimate time không được null",ErrorCode.ERR_INPUT_INVALID);
        }
        this.estimateTime = productRequest.getEstimateTime();

        this.metaData = productRequest.getMetaData();
        this.serial = productRequest.getSerial();
        this.categoryId = productRequest.getCategoryId();

        this.optionProducts = optionProducts.stream().map(optionProduct -> {
            optionProduct.setProductId(this.id);
            return optionProduct;
        }).collect(Collectors.toList());
    }



    public void updateProduct(ProductRequest productRequest, List<OptionProduct> optionProducts) {
        this.productName = productRequest.getProductName();
        this.subCateName = productRequest.getSubCateName();
        this.description = productRequest.getDescription();
        this.discount = productRequest.getDiscount();
        this.isOrder = productRequest.getIsOrder();
        this.estimateTime = productRequest.getEstimateTime();
        this.metaData = productRequest.getMetaData();

        this.serial = productRequest.getSerial();
        this.categoryId = productRequest.getCategoryId();

        if (Validator.isNull(productRequest.getOptionProducts())) {
            throw new ResponseException(ErrorCode.ERROR_DATA_INVALID);
        }
        if (CollectionUtils.isEmpty(optionProducts)) {
            this.optionProducts = new ArrayList<>();
        }
        this.updateOptionProduct(optionProducts);

        this.status = Constants.EntityStatus.ACTIVE;
    }
    /**
     * @author: huynq
     * @since: 11/24/2021 12:06 AM
     * @description:  cập nhật option product
     * @param : optionProducts
     * @return : void
     * @update:
     */

    private void updateOptionProduct(List<OptionProduct> optionProducts) {
        this.optionProducts.forEach(OptionProduct::delete);
        optionProducts.forEach(optionProduct -> {
            this.optionProducts.stream()
                    .filter(oPro -> oPro.getId().equals(optionProduct.getId()))
                    .findFirst().ifPresentOrElse(oPro -> {
                        oPro.update(optionProduct);
                    }, () -> {
                        optionProduct.setProductId(this.id);
                        this.optionProducts.add(new OptionProduct(optionProduct));
                    });
        });
    }

    public Float getAvgRatingPoint() {
        if (Validator.isNull(ratingPoint)) {
            return 0f;
        }
        return ratingPoint;
    }

    public Long getMinPrice() {
        if (Validator.isNull(this.optionProducts) && org.springframework.util.CollectionUtils.isEmpty(this.optionProducts)) {
            return 0L;
        }
        // get min price
        return this.optionProducts.stream().map(OptionProduct::getPrice).min(Comparator.naturalOrder()).orElse(0L);
        // get avarage price
//        return this.optionProducts.stream()
//                .map(OptionProduct::getPrice)
//                .reduce(0L, (a, b) -> a + b) / this.optionProducts.size();
    }

    public Long getMaxPrice() {
        if (Validator.isNull(this.optionProducts) && org.springframework.util.CollectionUtils.isEmpty(this.optionProducts)) {
            return 0L;
        }
        // get max price
        return this.optionProducts.stream().map(OptionProduct::getPrice).max(Comparator.naturalOrder()).orElse(0L);
    }

    public Long getQuantity() {
        if (CollectionUtils.isEmpty(optionProducts)) {
            return 0L;
        }
        return Long.valueOf(this.optionProducts
                .stream()
                .map(OptionProduct::getStockQuantity).reduce(0, Integer::sum));
    }

    public float getMaxDiscount() {
        if (Validator.isNull(this.optionProducts)) {
            return 0f;
        }
        return this.optionProducts.stream().map(OptionProduct::getDiscount).max(Comparator.naturalOrder()).orElse(0f);
    }
}
