package com.example.ducky_be.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RatingDTO {
    private String id;

    private String userRating;

    private String productId;

    private Integer point;

    private String rateContent;

    private Instant createDate;

    private String createBy;

    private Instant lastUpdate;

    private String updateBy;

    // status -1 là nháp, 0 xóa, 1 active

    private Integer status;

    private String orderId;

}
