package com.example.ducky_be.service.dto;

import com.example.ducky_be.controller.protoco.request.EmployeeRequest;
import com.example.ducky_be.core.utils.Validator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@ToString
public class EmployeeDTO implements Serializable {

    private String id;
    private String userName;
    @JsonIgnore
    private String password;
    private String email;
    private String address;
    private String fullName;
    private Integer gender;
    private Date dob;
    private String avatar;
    private Long role;
    private String phoneNumber;

    private Instant createDate;
    private String createBy;
    private Instant lastUpdate;
    private String updateBy;

    // status -1 là nháp, 0 xóa, 1 active
    private Integer status;

    private List<AuthorityDTO> authorities;


    public void updateEmployee(EmployeeRequest employeeRequest) {
        this.phoneNumber = employeeRequest.getPhoneNumber();
        this.fullName = employeeRequest.getFullName();
        this.dob = employeeRequest.getDob();
        this.address = employeeRequest.getAddress();
        this.gender = employeeRequest.getGender();
        if (Validator.isNotNull(this.authorities)) {
            this.authorities = new ArrayList<>();
        }
        this.authorities = employeeRequest.getAuthorities();
    }

    public void updatePassword(String newPasswordHash){
        this.password = newPasswordHash;
    }


}
