package com.example.ducky_be.service.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class AddressDTO {

    private Long id;

    private String nameOfRecipient;

    private String province;

    private String district;

    private String ward;

    private String addressDetail;

    private String latitude;

    private String createBy;

    private Instant createDate;

    private Instant lastUpdate;

    // status -1 là nháp, 0 xóa, 1 active
    private Integer status;

    private String updateBy;

    private String  customerId;

    private String addressType;

    private String phoneNumber;

    private Boolean isDefault;
    private Integer provinceId;
    private Integer districtId;
    private String wardId;

}
