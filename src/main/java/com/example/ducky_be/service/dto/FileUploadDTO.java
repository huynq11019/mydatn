package com.example.ducky_be.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * The type File upload model.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FileUploadDTO implements Serializable {

    private String id;

    private String originalName;

    private String filePath;

    private Long fileSize;

    private String fileType;

    private String ownerId;

    private String ownerType;

    private byte[] file;

    private Integer status;

    private String createdBy;

    private Date updatedDate;

    private String updateBy;

    private Date createdDate;
}
