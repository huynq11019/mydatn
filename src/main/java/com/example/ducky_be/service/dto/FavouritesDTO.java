package com.example.ducky_be.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class FavouritesDTO {
    private String id;

    private String customerId;

    private String productId;

    private String productThumnail; // huy sửa

    private String productName;

    private Long likeCount; // huy sửa

    private Long price; // huy sửa

    private Long numberFavourites; // số lượt yêu thích theo sản phẩm

    private Instant dateAt;
}
