package com.example.ducky_be.service.dto;

import com.example.ducky_be.controller.protoco.request.OrderRequest;
import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.utils.Validator;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Accessors(chain = true)
public class OrderDTO implements Serializable {


    private String id;

    private String receiver; // tên người nhận

    private String address;

    private Long discount;

    private String customerId;

    private String phoneNumber;

    private Constants.PaymentMethod paymentMethod;

    private String note;

    private String signature;

    private Long shippingCost;

    private Long addressId;

    private Constants.OrderStatus orderStatus;

    private List<OrderDetailDTO> orderDetails;

    private Instant createDate = Instant.now();

    private String createBy;

    private Instant lastUpdate = Instant.now();

    private String updateBy;

    // status -1 là nháp, 0 xóa, 1 active
    private Integer status = 1;

    private String shippingid;

    public void updateOrder(OrderRequest orderRequest) {
        this.setNote(orderRequest.getNote());
        this.setReceiver(orderRequest.getReceiver());
        this.setPhoneNumber(orderRequest.getPhoneNumber());
        this.setAddress(orderRequest.getAddress());
        this.orderStatus = orderRequest.getOrderStatus();
    }

    public void deteleItem(String itemId) {
        this.orderDetails.stream().filter(orderDetailDTO -> orderDetailDTO.getId().equals(itemId)).findFirst()
                .ifPresent(OrderDetailDTO::toDeletedOrderDetailDTO);
    }

    public void updateStatus(Constants.OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    private Integer totalQuantity;
    private Long totalPrice;
    private Long totalDiscount;
    private Long totalShip;

    public Integer getTotalQuantity() {
        if (CollectionUtils.isEmpty(this.orderDetails)) {
            return 0;
        }
        return this.orderDetails.size();
    }

    public Long getTotalPrice() {
        if ( Validator.isNotNull(this.orderDetails) && !CollectionUtils.isEmpty(this.orderDetails)) {
            return this.orderDetails.stream().mapToLong(OrderDetailDTO::getTotalPrice).sum();
        }
        return 0L;
    }


    public Long getTotalDiscount() {
        if (Validator.isNotNull(this.orderDetails) && CollectionUtils.isEmpty(this.orderDetails)) {
            return this.orderDetails.stream().mapToLong(OrderDetailDTO::getDiscount).sum();

        }
        return 0L;
    }

}
