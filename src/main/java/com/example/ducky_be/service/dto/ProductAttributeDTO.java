package com.example.ducky_be.service.dto;

import com.example.ducky_be.controller.protoco.AbstractRequestMessage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.Instant;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class ProductAttributeDTO extends AbstractRequestMessage {
    private String id;

    private String attributeId;

    private String productId;

    private String value;

    private String attributeName;

    private Instant createDate;

    private String createBy;

    private Instant lastUpdate;

    private String updateBy;

    private Integer status;

}
