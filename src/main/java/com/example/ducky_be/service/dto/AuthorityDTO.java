package com.example.ducky_be.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthorityDTO {

    private String authorityId;

    private String authorityCode;

    private String authorityName;

    private String roleId;

    private Instant createDate;
    private String createBy;
    private Instant lastUpdate;
    private String updateBy;
    private Integer status;
}
