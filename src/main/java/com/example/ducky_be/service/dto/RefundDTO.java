package com.example.ducky_be.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class RefundDTO {
    private String id;

    private String orderDetailId;

    private Long quantity;

    private Long moneyBack;
    // processing status = 0 :Chưa sử lí
    //                   = 1 :Đã sử lí

    private Integer processingStatus;

    private String description;

    private List<FileUploadDTO> files;
}
