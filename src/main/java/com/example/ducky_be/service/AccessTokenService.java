package com.example.ducky_be.service;

import com.example.ducky_be.repository.entity.AccessToken;

import java.util.Optional;

public interface AccessTokenService {
    /**
     * @param accessToken
     */
    AccessToken create(AccessToken accessToken);

    long deleteAllExpired();

    /**
     * @param username
     * @return
     */
    int expiredTokenByUsername(String username);

    /**
     * @param tokenId
     * @return
     */
    Optional<AccessToken> findById(Long tokenId);

    /**
     * Find by user name and token.
     *
     * @param userName the user name
     * @param token    the token
     * @return the optional
     */
    Optional<AccessToken> findByUserNameAndToken(String userName, String token);

    /**
     * @param token
     * @return
     */
    Optional<AccessToken> findByValue(String token);
}
