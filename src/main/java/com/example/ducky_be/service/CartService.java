package com.example.ducky_be.service;

import com.example.ducky_be.controller.protoco.request.CartRequest;
import com.example.ducky_be.controller.protoco.response.CartResponse;
import org.apache.coyote.Request;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;


public interface CartService {

   CartResponse searchmyCart(MultiValueMap<String, String> params, Pageable pageable);

   CartResponse addCartOrUpdate(CartRequest cartRequest);

   CartResponse deleteCartItem(CartRequest cartItemId);

   CartResponse updateCart(CartRequest cartRequest);

   CartResponse getCartByCustomerId(String customerId);

   CartResponse searchCartByCustomer(String customerId);

   CartResponse getMyTotalWeight();

   CartResponse findByIds(List<String> ids);
}
