package com.example.ducky_be.service;

import com.example.ducky_be.repository.entity.UserLogin;

public interface UserLoginService {
    /**
     * @param loginLog
     */
    UserLogin save(UserLogin loginLog);

}
