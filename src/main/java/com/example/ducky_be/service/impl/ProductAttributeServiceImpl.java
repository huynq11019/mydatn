package com.example.ducky_be.service.impl;

import com.example.ducky_be.controller.protoco.request.ProductAttributeRequest;
import com.example.ducky_be.controller.protoco.response.ProductAttributeResponse;
import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.exception.BadRequestAlertException;
import com.example.ducky_be.core.mapper.ProductAttributeMapper;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.ProductAttributeRepository;
import com.example.ducky_be.repository.ProductRepository;
import com.example.ducky_be.repository.entity.ProductAttributes;
import com.example.ducky_be.service.ProductAttributeService;
import com.example.ducky_be.service.dto.ProductAttributeDTO;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Service
@Transactional
@AllArgsConstructor
public class ProductAttributeServiceImpl implements ProductAttributeService {
    private final ProductAttributeRepository productAttributeRepository;
    private final ProductAttributeMapper productAttributeMapper;
    private final ProductRepository productRepository;

    @Override
    public ProductAttributeResponse createProductAttribute(ProductAttributeRequest request) {
        ProductAttributeResponse response = new ProductAttributeResponse();
        if (Validator.isNull(request)) {
            throw new BadRequestAlertException(ErrorCode.ERROR_DATA_INVALID);
        }
        List<ProductAttributes> attributes = productAttributeMapper.toEntity(request.getProductAttributes());
        log.info(attributes);
        attributes.forEach(productAttributes -> productAttributes.setStatus(Constants.EntityStatus.ACTIVE));
        List<ProductAttributeDTO> dto = productAttributeMapper.toDto(productAttributeRepository.saveAll(attributes));
        response.setProductAttributeList(dto);
        return response;
    }

    @Override
    public ProductAttributeResponse findProductAttributeByProductId(String productId, MultiValueMap<String, String> queryParams, Pageable pageable) {
        ProductAttributeResponse response = new ProductAttributeResponse();
        Long total = this.productAttributeRepository.countByparam(productId, queryParams);
        List<ProductAttributeDTO> list = new ArrayList<>();
        if (total > 0) {
            List<ProductAttributes> productAttributes = productAttributeRepository.findAttributeProductByProductId(productId, queryParams, pageable);
            log.info(productAttributes);
            list = productAttributeMapper.toDto(productAttributes);
        }
        response.setProductAttributeList(list);
        response.setTotalItem(total);
        return response;
    }

    @Override
    public Long countProductAttribute(String productId, MultiValueMap<String, String> queryParams) {
        return productAttributeRepository.countByparam(productId, queryParams);
    }

    @Override
    public ProductAttributeResponse updateProductAttribute(ProductAttributeRequest request, String id, String productId) {
        ProductAttributeResponse response = new ProductAttributeResponse();
        if (Validator.isNull(request)) {
            response.setMessage("giá trị truyền vào không lợp lệ");
            return response;
        }
        if (!productRepository.existsById(productId)) {
            response.setMessage("Không tồn tại sản phẩm này");
            return response;
        }
        ProductAttributes attributes = productAttributeMapper.requestToEntity(request);

        attributes.setId(id);
        attributes.setStatus(Constants.EntityStatus.ACTIVE);
        ProductAttributeDTO dto = productAttributeMapper.toDto(productAttributeRepository.save(attributes));
        response.setProductAttribute(dto);
        return response;

    }
}
