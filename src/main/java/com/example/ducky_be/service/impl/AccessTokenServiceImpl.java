package com.example.ducky_be.service.impl;

import com.example.ducky_be.repository.entity.AccessToken;
import com.example.ducky_be.service.AccessTokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class AccessTokenServiceImpl implements AccessTokenService {
    /**
     * @param accessToken
     */
    @Override
    public AccessToken create(AccessToken accessToken) {
        return null;
    }

    @Override
    public long deleteAllExpired() {
        return 0;
    }

    /**
     * @param username
     * @return
     */
    @Override
    public int expiredTokenByUsername(String username) {
        return 0;
    }

    /**
     * @param tokenId@return
     */
    @Override
    public Optional<AccessToken> findById(Long tokenId) {
        return Optional.empty();
    }

    /**
     * Find by user name and token.
     *
     * @param userName the user name
     * @param token    the token
     * @return the optional
     */
    @Override
    public Optional<AccessToken> findByUserNameAndToken(String userName, String token) {
        return Optional.empty();
    }

    /**
     * @param token
     * @return
     */
    @Override
    public Optional<AccessToken> findByValue(String token) {
        return Optional.empty();
    }
}
