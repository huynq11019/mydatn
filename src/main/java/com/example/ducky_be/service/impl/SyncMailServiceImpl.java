package com.example.ducky_be.service.impl;

import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.exception.ResponseException;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.core.sercurity.JwtToken;
import com.example.ducky_be.core.sercurity.TokenPovider;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.CustomerRepository;
import com.example.ducky_be.repository.EmployeeRepository;
import com.example.ducky_be.repository.MailTemplateRepository;
import com.example.ducky_be.repository.SendMailLogRepository;
import com.example.ducky_be.repository.entity.Customer;
import com.example.ducky_be.repository.entity.Employee;
import com.example.ducky_be.repository.entity.MailTemplate;
import com.example.ducky_be.repository.entity.Order;
import com.example.ducky_be.repository.entity.SendMailLog;
import com.example.ducky_be.service.SyncMailService;
import com.example.ducky_be.service.dto.OrderDTO;
import com.example.ducky_be.service.dto.OrderDetailDTO;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Log4j2
public class SyncMailServiceImpl implements SyncMailService {

//    private final MailTemplateRepository mailTemplateRepository;

    private final TokenPovider tokenPovider;

    private final EmployeeRepository employeeRepository;

    private final SendMailLogRepository sendMailLogRepository;

    private final MailTemplateRepository mailTemplateRepository;

    private final CustomerRepository customerRepository;


    @Value("${spring.employee.change.password.redirectUrl}")
    private String urlEmployeeResetPassword;

    @Value("${spring.active.profile}")
    private String urlCustomerResetPassword;
    @Value("${spring.my.order}")
    private String urlOrder;

    public SyncMailServiceImpl(TokenPovider tokenPovider, EmployeeRepository employeeRepository, SendMailLogRepository sendMailLogRepository, MailTemplateRepository mailTemplateRepository, CustomerRepository customerRepository) {
        this.tokenPovider = tokenPovider;
        this.employeeRepository = employeeRepository;
        this.sendMailLogRepository = sendMailLogRepository;
        this.mailTemplateRepository = mailTemplateRepository;
        this.customerRepository = customerRepository;
    }


    @Override
    public String employeeResetPassWord(String employeeid) {

        Employee employeeEntity = employeeRepository.findByIdAndStatus(employeeid, Constants.EntityStatus.ACTIVE).orElseThrow(
                () -> new ResponseException("Employee not found", ErrorCode.ERR_USER_NOT_EXIST));

        JwtToken accessToken = this.tokenPovider.createAccessTokenResetPassword("admin_" + employeeEntity.getEmail());

        Optional<MailTemplate> mailTemplate = mailTemplateRepository.findById(1L);
        if (mailTemplate.isPresent()) {
            MailTemplate emailTemplate = mailTemplate.get();
            String content = this.setContentMailAndTeam(emailTemplate, accessToken.getToken(), true);
            emailTemplate.setContent(content);
            emailTemplate.setSubject("Cập nhật lại mật khẩu");
            List<String> email = new ArrayList<>();
            email.add(employeeEntity.getEmail());
            sendEmail(emailTemplate, email);
        }


        return null;
    }

    @Override
    public String notificationEvent(String content) {

        return null;
    }

    @Override
    public String customerActiveAccount(String customerId) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(
                () -> new ResponseException("Customer not found ", ErrorCode.ERR_USER_NOT_EXIST));
        JwtToken accessToken = this.tokenPovider.createAccessTokenResetPassword("user_" + customer.getEmail());
        String urlToken = generateUrl(accessToken.getToken());
        Optional<MailTemplate> mailTemplate = mailTemplateRepository.findById(1L);
        if (mailTemplate.isPresent()) {
            // Thong bao template mail ko tồn tại, để lại vì chưa rõ content của email
            MailTemplate emailTemplate = mailTemplate.get();
            String content = this.setContentMailAndTeam(emailTemplate, accessToken.getToken(), false);
            emailTemplate.setContent(content);
            emailTemplate.setSubject("Kích hoạt tài khoản");
            List<String> email = new ArrayList<>();
            email.add(customer.getEmail());
            sendEmail(emailTemplate, email);
        }
        return urlToken;
    }

    @Override
    public String customerForgotPassword(String email) {
        Customer customer = customerRepository.findByEmail(email).orElseThrow(
                () -> new ResponseException("Customer not found ", ErrorCode.ERR_USER_NOT_EXIST));
        JwtToken accessToken = this.tokenPovider.createAccessTokenResetPassword("user_" + customer.getEmail());
        String urlToken = generateUrl(accessToken.getToken());
        Optional<MailTemplate> mailTemplate = mailTemplateRepository.findById(1L);
        if (mailTemplate.isPresent()) {
            MailTemplate emailTemplate = mailTemplate.get();
            String content = this.setContentMailAndTeam(emailTemplate, accessToken.getToken(), false);
            emailTemplate.setContent(content);
            emailTemplate.setSubject("Quên mật khẩu");
            List<String> listEmail = new ArrayList<>();
            listEmail.add(customer.getEmail());
            sendEmail(emailTemplate, listEmail);
        }
        return urlToken;
    }

    @Override
    public void notificationOrderActive(OrderDTO order) {
        Customer customer = this.customerRepository.findById(order.getCustomerId()).orElseThrow(() -> new ResponseException("Customer not found ", ErrorCode.ERR_USER_NOT_EXIST));
        Optional<MailTemplate> mailTemplate = mailTemplateRepository.findById(3L);
        if (mailTemplate.isPresent()) {
            // Thong bao template mail ko tồn tại, để lại vì chưa rõ content của email
            MailTemplate emailTemplate = mailTemplate.get();
            String content = this.setContentForOrder(emailTemplate, order);
            if (content == null) {
                throw new ResponseException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT);
            }
            emailTemplate.setContent(content);
            emailTemplate.setSubject("Kích hoạt tài khoản");
            List<String> email = new ArrayList<>();
            email.add(customer.getEmail());
            sendEmail(emailTemplate, email);
        }

    }

    @Override
    public void notificationRefundStatus(Integer status, Order order, String productName) {
        Customer customer = this.customerRepository.findById(order.getCustomerId()).orElseThrow(() -> new ResponseException("Customer not found ", ErrorCode.ERR_USER_NOT_EXIST));


        // Thong bao template mail ko tồn tại, để lại vì chưa rõ content của email
        MailTemplate emailTemplate = new MailTemplate();
        String content = null;
        if (Constants.ProcessingStatus.UNTREATED.equals(status)) {
            emailTemplate = mailTemplateRepository.findById(5L).orElseThrow(() ->
                    new ResponseException("Email not found ", ErrorCode.ERR_USER_NOT_EXIST));
            content = setContentNotRefund(emailTemplate, customer);
        } else if (Constants.ProcessingStatus.PROCESSING.equals(status)) {
            emailTemplate = mailTemplateRepository.findById(4L).orElseThrow(() ->
                    new ResponseException("Email not found ", ErrorCode.ERR_USER_NOT_EXIST));
            content = setContentRefundProcessing(emailTemplate, customer, productName);
        } else if (Constants.ProcessingStatus.DONE.equals(status)) {
            emailTemplate = mailTemplateRepository.findById(6L).orElseThrow(() ->
                    new ResponseException("Email not found ", ErrorCode.ERR_USER_NOT_EXIST));
            content = setContentRefundDone(emailTemplate, customer);
        }
        if (content == null) {
            throw new ResponseException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT);
        }
        emailTemplate.setContent(content);
        List<String> email = new ArrayList<>();
        email.add(customer.getEmail());
        sendEmail(emailTemplate, email);


    }


    private String generateUrl(String token) {
        return urlEmployeeResetPassword + "?token=" + token;
    }

    private String subject() {
        return "Thay đổi mật khẩu";
    }

    private String getRedisKey(String username, String hashKey) {
        return String.format("%s:%s", username, hashKey);
    }

    public void sendEmail(MailTemplate template, List<String> listEmail) {
        List<SendMailLog> list = new ArrayList<>();
        SendMailLog sendMailLog = new SendMailLog();
        for (String email : listEmail) {
            sendMailLog.setToEmail(email);
            sendMailLog.setTilte(template.getSubject());
            sendMailLog.setContent(template.getContent());
            sendMailLog.setStatus(Constants.MAIL_STATUS.WAITING);
            list.add(sendMailLog);
        }
        this.sendMailLogRepository.saveAll(list);
    }

    private String setContentMailAndTeam(MailTemplate emailTemplate, String urlToken, boolean isEmployee) {
        // Lấy ra các biến cần thiết truyền vào mail
        String contentEmailTemplate = emailTemplate.getContent();

        if (Validator.isNull(contentEmailTemplate)) {
            throw new ResponseException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT);
        }
        String link = "";
        if (isEmployee) {
            link = String.format("%s?token=%s", urlEmployeeResetPassword, urlToken);
        } else {
            link = urlCustomerResetPassword + "?token=" + urlToken;
        }

        return contentEmailTemplate.replace("{{link}}", link);
    }


    private String setContentForOrder(MailTemplate emailTemplate, OrderDTO order) {
        String contentOrder = emailTemplate.getContent();
        if (Validator.isNull(contentOrder)) {
            throw new ResponseException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT);
        }
        StringBuilder contentOrderDetail = new StringBuilder();
        if (order != null && !order.getOrderDetails().isEmpty()) {


            List<OrderDetailDTO> listOrderDetail = order.getOrderDetails();
            for (OrderDetailDTO orderDetail : listOrderDetail) {
                String data = "\n<tr style=\"border-collapse:collapse\"> \n" +
                        "                          <td style=\"padding:0;Margin:0;width:60px;text-align:center\">" + orderDetail.getProductName() + "</td> \n" +
                        "                          <td style=\"padding:0;Margin:0;width:60px;text-align:center\">" + orderDetail.getQuantity() + "</td> \n" +
                        "                          <td style=\"padding:0;Margin:0;width:100px;text-align:center\">" + orderDetail.getProductPrice() + "VND</td> \n" +
                        "                         </tr> ";
                contentOrderDetail.append(data);
            }
            String totalItem = "";
            if (order.getTotalQuantity() != null) {
                totalItem = String.valueOf(order.getTotalQuantity());
            }
            String link = urlOrder + "/" + order.getId();
            return contentOrder.replace("{{data}}", contentOrderDetail)
                    .replace("{{orderTotal}}", totalItem)
                    .replace("{{totalItem}}", totalItem)
                    .replace("priceShipping", "0")
                    .replace("{{total}}", order.getTotalPrice() != null ? String.valueOf(order.getTotalPrice()) : "0")
                    .replace("{{link}}", link)
                    .replace("{{orderCode}}", Objects.isNull(order.getId())? "TH#": order.getId())
                    .replace("{{orderCreateAt}}", LocalDate.now().toString())
                    .replace("{{addressShipping}}", order.getAddress() != null ? order.getAddress() : "Chưa có");
        }
        return null;
    }

    private String setContentRefundProcessing(MailTemplate emailTemplate, Customer customer, String productName) {
        String content = emailTemplate.getContent();
        if (Validator.isNull(content)) {
            throw new ResponseException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT);
        }

        return content.replace("{{Name}}", customer.getFullName() != null ? customer.getFullName() : "")
                .replace("{{productName}}", productName);
    }

    private String setContentNotRefund(MailTemplate emailTemplate, Customer customer) {
        String content = emailTemplate.getContent();
        if (Validator.isNull(content)) {
            throw new ResponseException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT);
        }

        return content.replace("{{Name}}", customer.getFullName() != null ? customer.getFullName() : "");
    }

    private String setContentRefundDone(MailTemplate emailTemplate, Customer customer) {
        String content = emailTemplate.getContent();
        if (Validator.isNull(content)) {
            throw new ResponseException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT);
        }

        return content.replace("{{Name}}", customer.getFullName() != null ? customer.getFullName() : "");
    }
}
