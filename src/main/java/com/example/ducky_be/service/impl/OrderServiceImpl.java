package com.example.ducky_be.service.impl;

import com.example.ducky_be.adapter.VerifyUtils;
import com.example.ducky_be.adapter.telegramBot.TelegramUtils;
import com.example.ducky_be.controller.protoco.request.OrderRequest;
import com.example.ducky_be.controller.protoco.response.OrderResponse;
import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.exception.BadRequestAlertException;
import com.example.ducky_be.core.exception.ResponseException;
import com.example.ducky_be.core.mapper.OrderMapper;
import com.example.ducky_be.core.mapper.ProductMapper;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.core.sercurity.SecurityUtils;
import com.example.ducky_be.core.utils.HeaderRequestInterceptor;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.AddressRepository;
import com.example.ducky_be.repository.CartRepository;
import com.example.ducky_be.repository.OptionRepository;
import com.example.ducky_be.repository.OrderDetailRepository;
import com.example.ducky_be.repository.OrderRepository;
import com.example.ducky_be.repository.ProductRepository;
import com.example.ducky_be.repository.entity.Address;
import com.example.ducky_be.repository.entity.OptionProduct;
import com.example.ducky_be.repository.entity.Order;
import com.example.ducky_be.repository.entity.OrderDetail;
import com.example.ducky_be.repository.entity.Product;
import com.example.ducky_be.service.OrderService;
import com.example.ducky_be.service.SyncMailService;
import com.example.ducky_be.service.dto.OrderDTO;
import com.example.ducky_be.service.dto.OrderDetailDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.data.domain.Pageable;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Log4j2
@Service
@Transactional
@AllArgsConstructor
public class OrderServiceImpl implements OrderService {
    private final OrderRepository orderRepository;

    private final OrderMapper orderMapper;

    private final CartRepository cartRepository;

    private final OrderDetailRepository orderDetailRepository;

    private final ProductRepository productRepository;

    private final ProductMapper productMapper;

    private final SyncMailService syncMailService;

    private final OptionRepository optionRepository;

    private final AddressRepository addressRepository;

    @Override
    public OrderResponse createMyOrder(OrderRequest request) {
        OrderResponse response = new OrderResponse();
        if (Validator.isNull(request)) {
            throw new BadRequestAlertException(ErrorCode.ERROR_DATA_INVALID);
        }
        log.info("verify tokken");
//        if (VerifyUtils.verify(request.getCapchaCode())) {
//            throw new ResponseException(ErrorCode.ERROR_CAPCHA_CODE);
//        }

        OrderDTO orderDTO = orderMapper.pathFromRequest(request);
        orderDTO.setStatus(Constants.EntityStatus.ACTIVE);
        if (request.getPaymentMethod() != null) {
            if (request.getPaymentMethod().equals(Constants.PaymentMethod.ZALO_PAY)) {
                orderDTO.setOrderStatus(Constants.OrderStatus.WAITING_FOR_PAYMENT);
            } else if (request.getPaymentMethod().equals(Constants.PaymentMethod.COD)) {
                orderDTO.setOrderStatus(Constants.OrderStatus.WAIT_FOR_CONFIRM);
            }
        }


        List<String> cartItemIds = orderDTO.getOrderDetails().stream()
                .map(OrderDetailDTO::getId).collect(Collectors.toList());
        if (Validator.isNull(cartItemIds)) {
            throw new BadRequestAlertException(ErrorCode.PRODUCT_ORDER_ISREUIRED);
        }
        // lưu thông tin đơn hàngorderor

        OrderDTO orderSaved = this.saveOrder(orderDTO);
        response.setOrder(orderSaved);

        // update số lượng sản phẩm
        List<String> optionsProductIds = orderDTO.getOrderDetails()
                .stream().map(OrderDetailDTO::getOptionProductId)
                .collect(Collectors.toList());
        List<OptionProduct> optionProducts = this.optionRepository.findAllById(optionsProductIds);


        optionProducts.forEach(option -> option.setStockQuantity(option.getStockQuantity() - orderDTO.getOrderDetails().stream()
                .filter(orderDetailDTO ->
                        orderDetailDTO.getOptionProductId().equals(option.getId()))
                .mapToInt(OrderDetailDTO::getQuantity).sum())
        );
        this.optionRepository.saveAll(optionProducts);
        // invalid cart item
        this.cartRepository.deleteListCart(cartItemIds, Constants.EntityStatus.BUYER_APPROVAL);
        String currentUserLogin = SecurityUtils.getCurrentUserLogin().orElseThrow(() ->
                new BadRequestAlertException("User is not logged in", "userManagement", "not_login"));
        TelegramUtils.sendMessage(String.format("%s đã tạo một đơn hàng chờ bạn xác nhận %s", currentUserLogin, "link xác nhận"));
        syncMailService.notificationOrderActive(orderDTO);
        return response;
    }

    @Override
    public OrderResponse updateOrder(String orderId, OrderRequest request) {
        OrderResponse response = new OrderResponse();
        if (Validator.isNull(orderId)) {
            throw new BadRequestAlertException(ErrorCode.ERROR_DATA_INVALID);
        }
        // check có phải là nhân viên không
        OrderDTO orderDTO = this.enrichOrderDto(orderId);
        orderDTO.updateOrder(request);

        response.setOrder(this.saveOrder(orderDTO));
        return response;
    }

    /**
     * @param orderId
     * @param status
     * @return
     */
    @Override
    public OrderResponse customerCancelOrder(String orderId, Constants.OrderStatus status) {
        OrderResponse response = new OrderResponse();
        if (Validator.isNull(orderId)) {
            throw new BadRequestAlertException(ErrorCode.ERROR_DATA_INVALID);
        }
        SecurityUtils.getLoginUserId().orElseThrow(() -> new BadCredentialsException("User is not logged in"));
        Order order = this.updateStatusOrder(orderId, status);
        response.setOrder(orderMapper.toDto(order));
        return response;
    }

    private Order updateStatusOrder(String orderId, Constants.OrderStatus status) {
        Order order = this.orderRepository.findById(orderId).orElseThrow(() -> new EntityNotFoundException("Order not found"));

        order.setOrderStatus(status);
        return this.orderRepository.save(order);

    }

    @Override
    public OrderResponse searchOrder(MultiValueMap<String, String> queryParams, Pageable pageable) {
        OrderResponse response = new OrderResponse();
        Long total = this.orderRepository.countAll(queryParams);
        if (total == 0) {
            log.info("Không tìm thấy đơn hàng");
            return response;
        }
        List<OrderDTO> order = this.orderMapper.toDto(orderRepository.search(queryParams, pageable));
        List<String> orderIds = order.stream().map(OrderDTO::getId).collect(Collectors.toList());
        Optional<List<OrderDetail>> orderDetail = this.orderDetailRepository.findAllByOrderIdIn(orderIds);
        orderDetail.ifPresent(orderDetails -> order.forEach(item -> {
            item.setOrderDetails(
                    this.orderMapper.detailEntityToDto(orderDetails.stream()
                            .filter(o -> o.getOrderId().equals(item.getId()))
                            .collect(Collectors.toList()))
            );
        }));

        response.setOrderList(order);
        response.setTotalItem(total);
        return response;
    }

    @Override
    public Long countAllOrder(MultiValueMap<String, String> queryParams, Pageable pageable) {
        return this.orderRepository.countAll( queryParams);

    }

    @Override
    public OrderResponse findMyOrder(Pageable pageable) {
        OrderResponse response = new OrderResponse();
        String currentUserId = SecurityUtils.getLoginUserId().orElseThrow(() ->
                new BadCredentialsException(ErrorCode.AUTHENTICATION_REQUIRED.getMessage()));
        List<Order> myOrders = this.orderRepository.findAllByCustomerId(currentUserId, pageable);
        response.setOrderList(orderMapper.toDto(myOrders));
        return response;
    }

    @Override
    public OrderResponse changeStatusCheckOut(String orderId, String status) {
        if (Validator.isNull(orderId)) {
            throw new BadRequestAlertException(ErrorCode.ERROR_DATA_INVALID);
        }
        OrderResponse response = new OrderResponse();
        Optional<Order> optOrder = orderRepository.findById(orderId);
        if (optOrder.isEmpty()) {
            throw new EntityNotFoundException("Chưa có thông tin trong dữ liệu");
        }
        if (Constants.OrderStatus.CONFIRMED.name().equals(status)) {
           try {
               createOrderGHN(orderId);
           }catch (Exception e){
               e.printStackTrace();
               log.error("Lỗi khi tạo đơn hàng GHN");
           }
        }
        Order order = optOrder.get();
        order.setOrderStatus(Constants.OrderStatus.valueOf(status));
        OrderDTO dto = orderMapper.toDto(orderRepository.save(order));
        response.setOrder(dto);

        return response;
    }

    @Override
    public OrderResponse findOrderById(String orderId) {
        OrderResponse response = new OrderResponse();
        OrderDTO orderDTO = this.enrichOrderDto(orderId);
        response.setOrder(orderDTO);
        return response;
    }

    // get full order detail
    @Override
    public OrderResponse getOrderRicher(String orderId) {
        OrderDTO orderDTO = this.enrichOrderDto(orderId);


        return OrderResponse.builder()
                .order(orderDTO).build();
    }

    @Override
    public OrderResponse getOrderRicherByOrderId(String customerid, Pageable pageable) {
        OrderResponse response = new OrderResponse();
        List<OrderDTO> order = this.orderMapper.toDto(orderRepository.searchOrderByCustomerId(customerid, pageable));
        List<String> orderIds = order.stream().map(OrderDTO::getId).collect(Collectors.toList());
        Optional<List<OrderDetail>> orderDetail = this.orderDetailRepository.findAllByOrderIdIn(orderIds);
        orderDetail.ifPresent(orderDetails -> order.forEach(item -> {
            item.setOrderDetails(
                    this.orderMapper.detailEntityToDto(orderDetails.stream()
                            .filter(o -> o.getOrderId().equals(item.getId()))
                            .collect(Collectors.toList()))
            );
        }));

        response.setOrderList(order);
        return response;
    }


    @Transactional
    OrderDTO saveOrder(OrderDTO orderDTO) {
        Order order = orderMapper.toEntity(orderDTO);
        Order orderSaved = this.orderRepository.save(order);

        // xóa sản phẩm trong giỏ hàng
        List<OrderDetail> orderDetails = this.orderMapper.detailDtoToEntity(orderDTO.getOrderDetails());
        orderDetails.forEach(orderDetail -> orderDetail.setOrderId(orderSaved.getId()));
        this.orderDetailRepository.saveAll(orderDetails);

        return orderMapper.toDto(orderSaved);
    }

    // enrich data
    OrderDTO enrichOrderDto(String orderId) {
        OrderDTO orderDTO = orderMapper.toDto(orderRepository.findById(orderId).orElseThrow(() ->
                new ResponseException(ErrorCode.ORDER_NOT_FOUND)));
        List<OrderDetailDTO> orderDetailDTOS = this.orderMapper
                .detailEntityToDto(orderDetailRepository.findAllByOrderId(orderId));
        // get product detail
        if (Validator.isNotNull(orderDetailDTOS) && !CollectionUtils.isEmpty(orderDetailDTOS)) {
            List<String> productIds = orderDetailDTOS.stream()
                    .map(OrderDetailDTO::getProductId)
                    .collect(Collectors.toList());
            List<Product> products = this.productRepository.findAllById(productIds);
            List<String> optionIds = orderDetailDTOS.stream().map(OrderDetailDTO::getOptionProductId).collect(Collectors.toList());
            List<OptionProduct> optionProducts = this.optionRepository.findAllById(optionIds);
            orderDetailDTOS.forEach(orderDetailDTO -> {
                Optional<Product> product = products.stream()
                        .filter(p -> p.getId().equals(orderDetailDTO.getProductId()))
                        .findFirst();
                product.ifPresent(orderDetailDTO::enrichProduct);
                Optional<OptionProduct> optionProduct = optionProducts.stream().filter(op -> op.getId().equals(orderDetailDTO.getOptionProductId())).findFirst();
                optionProduct.ifPresent(orderDetailDTO::enrichOption);
            });

        }

        orderDTO.setOrderDetails(orderDetailDTOS);

        return orderDTO;
    }


    public static final String BASE_URL = "https://dev-online-gateway.ghn.vn/shiip/public-api/v2/";
    public static final String API_KEY = "339ee9f5-5b37-11ec-ac64-422c37c6de1b";
    public static final String SHOP_ID = "84307";

    private RestTemplate getRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        ArrayList<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
        interceptors.add(new HeaderRequestInterceptor("Token", API_KEY));
        interceptors.add(new HeaderRequestInterceptor("ShopId", SHOP_ID));
        interceptors.add(new HeaderRequestInterceptor("Content-Type", "application/json"));
        restTemplate.setInterceptors(interceptors);
        return restTemplate;
    }

    @Override
    public String createOrderGHN(String orderId) {

        try {
            Order order = this.orderRepository.findById(orderId).orElseThrow(
                    () -> new EntityNotFoundException("Order not found"));
            if (order != null && order.getAddressId() == null) {
                throw new NullPointerException("Address Id is null");
            }
            Address address = this.addressRepository.findById(order.getAddressId()).orElseThrow(
                    () -> new EntityNotFoundException("Address not found"));
            OrderDTO dto = enrichOrderDto(orderId);

            String url = BASE_URL + "shipping-order/create";
            Map<String, Object> params = new HashMap<>();
            params.put("to_name", order.getReceiver());
            params.put("to_phone", order.getPhoneNumber());
            params.put("to_address", order.getAddress());
            params.put("to_ward_code", address.getWardId());
            params.put("to_district_id", address.getDistrictId());
            params.put("return_phone", "0339485712");
            params.put("return_address", "Số 148-150 Cầu Giấy, 165 Cầu Giấy, Quan Hoa, Cầu Giấy, Hà Nội, Vietnam");
            if (order.getPaymentMethod() != null && order.getPaymentMethod().equals(Constants.PaymentMethod.COD)) {
                params.put("cod_amount", dto.getTotalPrice());
                params.put("payment_type_id", 2);
            } else {
                params.put("payment_type_id", 1);
            }
            params.put("content", order.getNote());
            params.put("weight", 200);
            params.put("length", 1);
            params.put("width", 19);
            params.put("height", 10);
            // params.put("insurance_value", dto.getTotalPrice());
            params.put("service_type_id", 2);
            params.put("service_id", 0);
            params.put("required_note", "CHOXEMHANGKHONGTHU");
            params.put("items", new JSONArray());
            params.put("quantity", dto.getTotalQuantity());
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(params);
            String result = this.getRestTemplate().postForObject(builder.toUriString(), json, String.class);
            JSONObject jsonObject = new JSONObject(result);
            log.info(result);
            log.info(jsonObject);
            String orderShippingCode = jsonObject.getJSONObject("data").getString("order_code");

            if (!orderShippingCode.isBlank()) {
                order.setShippingid(orderShippingCode);
                this.orderRepository.save(order);
            }
            return orderShippingCode;

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


        return null;
    }

    @Override
    public Map<String, Long> statisticOrder(MultiValueMap<String, String> queryParams) {
        return this.orderRepository.statisticOrderByDay(queryParams);
    }

    @Override
    public Map<String, Long> statisticMoney(MultiValueMap<String, String> queryParams) {
        return this.orderRepository.statisticMoney(queryParams);
    }
}
