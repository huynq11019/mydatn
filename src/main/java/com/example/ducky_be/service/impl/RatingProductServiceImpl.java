package com.example.ducky_be.service.impl;

import com.example.ducky_be.controller.protoco.request.RatingRequest;
import com.example.ducky_be.controller.protoco.response.RatingResponse;
import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.exception.BadRequestAlertException;
import com.example.ducky_be.core.mapper.RatingProductMapper;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.RatingProductRepository;
import com.example.ducky_be.repository.entity.RatingProduct;
import com.example.ducky_be.service.RatingProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@RequiredArgsConstructor
@Service
@Transactional
public class RatingProductServiceImpl implements RatingProductService {

    private final RatingProductRepository ratingProductRepository;

    private final RatingProductMapper ratingProductMapper;

    @Override
    public void addRatingProduct(String productId, RatingRequest rating) {
        if (Validator.isNull(rating)) {
            throw new BadRequestAlertException(ErrorCode.ERR_INPUT_INVALID);
        }
        // kiểm tra người dùng đã mua hàng chưa
        // kiểm tra sản phẩm đã được đánh giá chưa
        ratingProductRepository.save(ratingProductMapper.toEntity(rating));
    }

    /**
     * @param ratingId ratingId
     * @author: huynq
     * @since: 11/16/2021 10:26 PM
     * @description: người dùng cập nhật lại thông tin đánh giá
     * @update:
     */
    @Override
    public void updateRatingProduct(String ratingId, RatingRequest rating) {
        if (Validator.isNull(rating)) {
            throw new BadRequestAlertException(ErrorCode.ERR_INPUT_INVALID);
        }
        ratingProductRepository.save(ratingProductMapper.toEntity(rating));
    }

    @Override
    public void deleteRatingProduct(String productId) {
        // check rating có tồn tại trong hệ thống hay không
        RatingProduct ratingProduct = this.ratingProductRepository.findById(productId).orElseThrow(() ->
                new BadRequestAlertException(ErrorCode.ERR_INPUT_INVALID));
        // check rating có phải của người dùng đang đăng nhập hay không
//        String currentUserLogin = SecurityUtils.getLoginUserId().orElseThrow(()->
//                new BadRequestAlertException(ErrorCode.AUTHENTICATION_REQUIRED));
        // xóa rating
        ratingProduct.setStatus(Constants.EntityStatus.REMOTED);
        this.ratingProductRepository.save(ratingProduct);
    }

    @Override
    public RatingResponse getRatingProductByProductId(String productId, Pageable pageable) {
        // get list rating của sản phẩm
        List<RatingProduct> ratingProducts =
                this.ratingProductRepository
                        .findAllByProductIdOrderByPointDesc(productId, pageable);
        return RatingResponse.builder().ratingDTOList(ratingProductMapper.toDto(ratingProducts)).build();
    }

    @Override
    public RatingResponse getRatingById(String ratingId) {
        RatingProduct ratingProduct = this.ratingProductRepository.findById(ratingId).orElseThrow(() ->
                new BadRequestAlertException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT));
        return RatingResponse.builder().ratingDTO(ratingProductMapper.toDto(ratingProduct)).build();
    }
}
