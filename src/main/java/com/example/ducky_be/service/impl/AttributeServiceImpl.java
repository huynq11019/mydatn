package com.example.ducky_be.service.impl;

import com.example.ducky_be.controller.protoco.request.AttributeRequest;
import com.example.ducky_be.controller.protoco.response.AttributeResponse;
import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.exception.BadRequestAlertException;
import com.example.ducky_be.core.mapper.AttributeMapper;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.AttributesRepository;
import com.example.ducky_be.repository.CategoryRepository;
import com.example.ducky_be.repository.entity.Attributes;
import com.example.ducky_be.service.AttributeService;
import com.example.ducky_be.service.dto.AttributeDTO;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Service
@Transactional
@AllArgsConstructor
public class AttributeServiceImpl implements AttributeService {

    private final AttributeMapper attributeMapper;

    private final AttributesRepository attributeRepository;

    private final CategoryRepository categoryRepository;

    @Override
    public AttributeResponse createAttribute(AttributeRequest request) {
        AttributeResponse response = new AttributeResponse();

        if (Validator.isNull(request)) {
            throw new BadRequestAlertException(ErrorCode.ERROR_DATA_INVALID);
        }
        Attributes attributes = attributeMapper.requestToEntity(request);
        log.info(attributes);
        attributes.setStatus(Constants.EntityStatus.ACTIVE);
        AttributeDTO dto = attributeMapper.toDto(attributeRepository.save(attributes));
        response.setAttribute(dto);
        return response;
    }

    @Override
    public AttributeResponse findByAttributeByCategoryId(String categoryId, MultiValueMap<String, String> queryParams, Pageable pageable) {
        AttributeResponse response = new AttributeResponse();
        Long total = this.attributeRepository.countParam(categoryId, queryParams);
        List<AttributeDTO> list = new ArrayList<>();
        if (total > 0) {
            List<Attributes> attributes = attributeRepository.findAttributeByCategoryId(categoryId, queryParams, pageable);
            log.info(attributes);
            list = attributeMapper.toDto(attributes);
        }
        response.setAttributeList(list);
        response.setTotalItem(total);
        return response;
    }

    @Override
    public Long countAttribute(String categoryId, MultiValueMap<String, String> queryParams) {
        return attributeRepository.countParam(categoryId, queryParams);

    }

    @Override
    public AttributeResponse updateAttribute(AttributeRequest request, String id, Long categoryId) {
        AttributeResponse response = new AttributeResponse();
        if (Validator.isNull(request)) {
            response.setMessage("giá trị truyền vào không lợp lệ");
            return response;
        }
        if (!categoryRepository.existsById(categoryId)) {
            response.setMessage("Không tồn tại loại sản phẩm này");
            return response;
        }
        Attributes attributes = attributeMapper.requestToEntity(request);

        attributes.setId(id);
        attributes.setStatus(Constants.EntityStatus.ACTIVE);
        AttributeDTO dto = attributeMapper.toDto(attributeRepository.save(attributes));
        response.setAttribute(dto);
        return response;
    }
}
