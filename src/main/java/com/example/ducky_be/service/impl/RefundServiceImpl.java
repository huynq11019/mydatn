package com.example.ducky_be.service.impl;

import com.example.ducky_be.controller.protoco.request.RefundRequest;
import com.example.ducky_be.controller.protoco.response.RefundResponse;
import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.mapper.RefundMapper;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.OrderDetailRepository;
import com.example.ducky_be.repository.OrderRepository;
import com.example.ducky_be.repository.RefundRepository;
import com.example.ducky_be.repository.entity.Order;
import com.example.ducky_be.repository.entity.OrderDetail;
import com.example.ducky_be.repository.entity.Refund;
import com.example.ducky_be.service.FileUploadService;
import com.example.ducky_be.service.RefundService;
import com.example.ducky_be.service.SyncMailService;
import com.example.ducky_be.service.dto.FileUploadDTO;
import com.example.ducky_be.service.dto.RefundDTO;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Log4j2
@Service
@Transactional
@AllArgsConstructor
public class RefundServiceImpl implements RefundService {
    private final OrderDetailRepository orderDetailRepository;
    private final OrderRepository orderRepository;
    private final RefundMapper refundMapper;
    private final RefundRepository refundRepository;
    private final SyncMailService syncMailService;
    private final FileUploadService fileUploadService;

    @Override
    public RefundResponse createRefund(RefundRequest request, String orderDetailId, List<MultipartFile> files) {
        RefundResponse response = new RefundResponse();
        OrderDetail orderDetail = orderDetailRepository.findById(orderDetailId).orElseThrow(() -> new EntityNotFoundException("Không tìm thấy đơn hàng"));
        Order order = orderRepository.findById(orderDetail.getOrderId()).orElseThrow(() -> new EntityNotFoundException("Không tìm thấy hóa đơn"));
        if (orderDetail.getStatus() == 0) {
            response.setMessage("Đơn hàng này không đủ điều kiện để hoàn tiền");
            return response;
        }
        if (order.getCreateDate() != null) {
            Instant dayLatest = Instant.now().minus(14, ChronoUnit.DAYS);
            if (order.getCreateDate().isBefore(dayLatest)) {
                response.setMessage("Đơn hàng này đã quá hạn để hoàn tiền");
                return response;
            }
        }
        if (files.isEmpty()) {
            response.setMessage("Yêu cầu phải bổ sung đủ hình ảnh");
            return response;
        }
        List<FileUploadDTO> fileUploadDTO = this.fileUploadService.createFile(files, orderDetailId, Constants.OwnerType.REFUND);
        Refund refund = refundMapper.requestToEntity(request);
        refund.setProcessingStatus(Constants.ProcessingStatus.UNPROCESSED);
        RefundDTO dto = refundMapper.toDto(refundRepository.save(refund));
        dto.setFiles(fileUploadDTO);
        response.setRefundDTO(dto);


        return response;
    }

    @Override
    public RefundResponse getAllRefund(MultiValueMap<String, String> queryParams, Pageable pageable) {
        RefundResponse response = new RefundResponse();
        response.setRefundDTOList(this.refundMapper.toDto(this.refundRepository.getAllRefund(queryParams, pageable)));
        return response;
    }

    @Override
    public RefundResponse updateStatus(RefundRequest request) {
        RefundResponse response = new RefundResponse();
        if (Validator.isNull(request)) {
            response.setMessage("Gía trị truyền vào không hợp lệ");
            return response;
        }
        OrderDetail orderDetail = orderDetailRepository.findById(request.getOrderDetailId()).orElseThrow(() -> new EntityNotFoundException("Không tìm thấy đơn hàng"));
        Order order = orderRepository.findById(orderDetail.getOrderId()).orElseThrow(() -> new EntityNotFoundException("Không tìm thấy hóa đơn"));
        if (request.getId() != null) {
            syncMailService.notificationRefundStatus(request.getProcessingStatus(), order, orderDetail.getProductName());
            RefundDTO dto = refundMapper.toDto(refundRepository.save(this.refundMapper.requestToEntity(request)));
            response.setRefundDTO(dto);
        }

        return response;
    }

    @Override
    public RefundResponse printReturnOderProduct(String orderId) {
        return null;
    }


}
