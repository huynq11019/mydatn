package com.example.ducky_be.service.impl;

import com.example.ducky_be.adapter.service.StorageService;
import com.example.ducky_be.controller.protoco.response.FileUploadResponse;
import com.example.ducky_be.core.config.ftp.FTPFileSystemStorageService;
import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.exception.BadRequestAlertException;
import com.example.ducky_be.core.exception.ResponseException;
import com.example.ducky_be.core.exception.StorageException;
import com.example.ducky_be.core.mapper.FileUploadMapper;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.core.utils.FileUtil;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.FileuploadRepository;
import com.example.ducky_be.repository.entity.FileUpload;
import com.example.ducky_be.service.FileUploadService;
import com.example.ducky_be.service.dto.FileUploadDTO;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Log4j2
@Service
@AllArgsConstructor
public class FileUploadSerivceImpl implements FileUploadService {

    private final FileuploadRepository fileuploadRepository;

    private final FileUploadMapper fileUploadMapper;
    //handle fileupload FTP
    private final FTPFileSystemStorageService fileSystemStorageService;

    // handler  file upload local
    private final StorageService storageService;

    @Override
    public Long countProduct(MultiValueMap<String, String> queryParams) {
        return null;
    }

    @Override
    public List<FileUploadDTO> getListByOwnerAndOwnerType(String ownerId, String ownerType, boolean isImage) {
        List<FileUpload> fileUpload = this.fileuploadRepository.findOneByOwnerIdAndOwnerTypeAndStatusOrderByCreateDateDesc(ownerId, ownerType, Constants.EntityStatus.ACTIVE);
        List<FileUploadDTO> listFileUploadDto = this.fileUploadMapper.toDto(fileUpload);
        if (isImage) {
            listFileUploadDto.forEach(fDTO -> {
                fDTO.setFile(this.fileSystemStorageService.readFileToByte(fDTO.getFilePath()));
            });
        }
        return listFileUploadDto;
    }

    @Override
    public List<FileUploadDTO> getByListFileId(List<String> fileID) {
        List<FileUpload> fileUpload = this.fileuploadRepository.findAllById(fileID);
        if (Validator.isNull(fileUpload) || fileUpload.isEmpty()) {
            throw new ResponseException(ErrorCode.FILE_NOT_EXIST);
        }
        return this.fileUploadMapper.toDto(fileUpload);
    }

    @Override
    public FileUploadDTO findByFileId(String fileId) {
        if (Validator.isNull(fileId)) {
            throw new ResponseException(ErrorCode.FILE_NOT_EXIST);
        }
        FileUpload fileUpload = this.fileuploadRepository.findByIdAndStatus(fileId, Constants.EntityStatus.ACTIVE).orElseThrow(
                () -> new ResponseException(ErrorCode.FILE_NOT_EXIST));
        FileUploadDTO fileUploadDTO = this.fileUploadMapper.toDto(fileUpload);
        return fileUploadDTO;
    }

    @Override
    public void saveOrUpdate(FileUploadDTO fileModel) {
        List<FileUpload> fileOnDb = this.fileuploadRepository.getListByFilePath(fileModel.getFilePath());
        if (Validator.isNull(fileOnDb)) {
            FileUpload file = fileUploadMapper.toEntity(fileModel);
            // validate
            this.fileuploadRepository.save(file);
        }
    }

    @Override
    public List<FileUploadDTO> createFile(List<MultipartFile> files, String ownerId, String ownerType) {
        // kiểm tra file rỗng
        if (Validator.isNull(files) || files.size() < 1) {
            throw new ResponseException(ErrorCode.FILE_NOT_EXIST);
        }
        files.forEach(item -> {
            if (Validator.isNull(item) || Validator.isNull(item.getOriginalFilename())) {
                throw new ResponseException(ErrorCode.FILE_NOT_EXIST);
            }
        });
        // kiểm tra file có hợp lệ không?
        if (!FileUtil.isValidFileExtension(files)) {
            String format = String.format(ErrorCode.ERR_MULTIPART_FILE_INVALID.getMessage(),
                    new ArrayList<>(FileUtil.FILE_TYPE_MAP.keySet()).toString());
            throw new BadRequestAlertException(format, "File Upload", ErrorCode.ERR_MULTIPART_FILE_INVALID.getMessageKey());
        }
        List<FileUploadDTO> fileUploadModels = new ArrayList<>();
        try {
            files.forEach(file -> {
//                String originalName = StringUtils.cleanPath(file.getOriginalFilename());
//                // lưu file vào ftp server
//                String filePath = this.fileSystemStorageService.store(file);
//                // lưu file vào local
//                String filepathLocal = this.storageService.store(file);
//                log.info("fileLocal =>>>>>>>>>"+filepathLocal);
//                log.info("fileFTP =>>>>>>>>"+ filePath);
//                FileUploadDTO uFile = new FileUploadDTO();
//                uFile.setStatus(Constants.EntityStatus.ACTIVE);
//                uFile.setOriginalName(originalName);
//                uFile.setFilePath(filePath);
//                String mineType = file.getContentType();
//                uFile.setFileType(mineType);
//                uFile.setOwnerId(objectId);
//                uFile.setFileSize(file.getSize());
//                FileUpload fileUpload = this.fileUploadMapper.toEntity(uFile);
//                this.fileuploadRepository.save(fileUpload);
                FileUploadDTO fileUploadDTO = this.createFile(file, ownerId, ownerType);
                fileUploadModels.add(fileUploadDTO);
            });
        } catch (StorageException se) {
            throw new StorageException(ErrorCode.FILE_IS_EMPTY);
        }
        return fileUploadModels;
    }

    @Override
    public FileUploadDTO createFile(MultipartFile file, String objectId, String objectType) {
        // lưu file tải lên
        if (Validator.isNull(file) || Validator.isNull(file.getOriginalFilename())) {
            throw new ResponseException(ErrorCode.ERR_INPUT_INVALID);
        }
        FileUploadDTO fileUploadDTO = new FileUploadDTO();
        try {
            String originalName = StringUtils.cleanPath(file.getOriginalFilename()); // tên file gốc
            // lưu file vào ftp
//            String filePath = fileSystemStorageService.store(file); // tên file đã mã hóa
            // lưu file vào local
            String filepathLocal = this.storageService.store(file);
            log.info("fileLocal =>>>>>>>>>"+filepathLocal);
//            log.info("fileFTP =>>>>>>>>" + filePath);
            fileUploadDTO.setStatus(Constants.EntityStatus.ACTIVE);
            fileUploadDTO.setOriginalName(originalName);
            fileUploadDTO.setFilePath(filepathLocal);
            String mimeType = file.getContentType();
            fileUploadDTO.setFileType(mimeType);
            fileUploadDTO.setOwnerId(objectId);
            fileUploadDTO.setOwnerType(objectType);
            fileUploadDTO.setFileSize(file.getSize());
            FileUpload fileUpload = fileUploadMapper.toEntity(fileUploadDTO);

            fileUploadDTO = fileUploadMapper.toDto(this.fileuploadRepository.save(fileUpload));
        } catch (StorageException se) {
            throw new StorageException("File upload is corrupted");
        }

        return fileUploadDTO;
    }

    @Override
    public List<FileUploadDTO> findAllByOwnerIdAndOwnerType(String ownerId, String ownerType) {
        List<FileUpload> fileUpload = this.fileuploadRepository.findOneByOwnerIdAndOwnerTypeAndStatusOrderByCreateDateDesc(ownerId, ownerType, Constants.EntityStatus.ACTIVE);
        return this.fileUploadMapper.toDto(fileUpload);
    }


    @Override
    public FileUploadResponse removeFileById(String fileId) {
        FileUploadResponse response = new FileUploadResponse();
        FileUpload fileUpload = this.fileuploadRepository.findByIdAndStatus(fileId, Constants.EntityStatus.ACTIVE).orElse(null);
//        FileUpload fileUpload = fileDAO.findByIdAndStatus(fileId, Constants.Status.ACTIVE).orElse(null);
        if (Validator.isNotNull(fileUpload)) {

//            fileUpload.setStatus(Constants.EntityStatus.REMOTED);
//            this.fileuploadRepository.save(fileUpload);
//            this.fileuploadRepository.delete(fileUpload);
            this.fileuploadRepository.deleteById(fileId);
            return response;
        }
        response.setErrorCode(ErrorCode.ERR_INPUT_INVALID);
        return response;
    }

    @Override
    public FileUploadResponse removeFIleByOwnerIdAndOwnerType(String ownerId, String OwnerType) {
        FileUploadResponse response = new FileUploadResponse();
        List<FileUpload> fileUploads = this.fileuploadRepository.findOneByOwnerIdAndOwnerTypeAndStatusOrderByCreateDateDesc(ownerId, OwnerType, Constants.EntityStatus.ACTIVE);
        if (Validator.isNull(fileUploads) || fileUploads.isEmpty()) {
            throw new BadRequestAlertException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT);
        }
        fileUploads.forEach(file -> file.setStatus(Constants.EntityStatus.REMOTED));
        return response;
    }

    @Override
    public List<FileUploadDTO> createFileImage(List<MultipartFile> files, String objectId, String objectType) {
        // kiểm tra file có hợp lệ không?
        if (!FileUtil.isValidFileImage(files)) {
            String format = String.format(ErrorCode.ERR_MULTIPART_FILE_INVALID.getMessage(),
                    new ArrayList<>(FileUtil.FILE_IMAGE_MAP.keySet()).toString());
            throw new BadRequestAlertException(format, "File Upload", ErrorCode.ERR_MULTIPART_FILE_INVALID.getMessageKey());
        }
        return null;
    }
}
