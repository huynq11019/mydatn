package com.example.ducky_be.service.impl;

import com.example.ducky_be.controller.protoco.request.CategoryRequest;
import com.example.ducky_be.controller.protoco.response.CategoryResponse;
import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.exception.BadRequestAlertException;
import com.example.ducky_be.core.mapper.CategoryMapper;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.CategoryRepository;
import com.example.ducky_be.repository.entity.Categories;
import com.example.ducky_be.service.CategoryService;
import com.example.ducky_be.service.dto.CategoryDTO;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Service
@Transactional
@AllArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;

    private final CategoryMapper categoryMapper;

    /**
     * @author: NamTH
     * @since: 31/10/2021 4:04 CH
     * @description: lấy danh sách tree category
     * @update:
     */

    @Override
    @Cacheable(value = "category-tree", unless = "#result == null")
    public CategoryResponse searchTreeCategory(MultiValueMap<String, String> queryParams, Pageable pageable) {
        CategoryResponse response = new CategoryResponse();
        Long count = this.categoryRepository.countCategory(queryParams);
        List<CategoryDTO> lstDTO = new ArrayList<>();
        if (count > 0) {
            log.info(categoryRepository.searchTree(queryParams));
//            lstDTO = this.categoryMapper.toDto(categoryRepository.searchCategory(queryParams, pageable));
            lstDTO = this.categoryMapper.toDto(categoryRepository.searchTree(queryParams));
        }

        response.setCategoryDTOS(lstDTO);
        response.setTotalItem(count);
        return response;
    }

    /**
     * @author: NamTH
     * @since: 31/10/2021 4:04 CH
     * @description: lấy danh sách category
     * @update:
     */
    @Override
    @Cacheable(value = "category", unless = "#result == null")
    public CategoryResponse searchAllCategory(MultiValueMap<String, String> queryParams, Pageable pageable) {
        CategoryResponse response = new CategoryResponse();
        Long count = this.categoryRepository.countCategory(queryParams);
        if (count.equals(0L) ) {
            response.setTotalItem(count);
            return response;
        }
        List<Categories> listCategory = categoryRepository.searchAllCategory(queryParams, pageable);
        response.setCategoryDTOS(this.categoryMapper.toDto(listCategory));
        response.setTotalItem(count);
        return response;
    }

    /**
     * @author: NamTH
     * @since: 31/10/2021 4:05 CH
     * @description: tạo category
     * @update:
     */
    @Override
    @CacheEvict(value = "category-tree", allEntries = true)
    public CategoryResponse createCategory(CategoryRequest request) {

        CategoryResponse response = new CategoryResponse();
        if (Validator.isNull(request)) {
            throw new BadRequestAlertException(ErrorCode.ERROR_DATA_INVALID);
        }
        Categories category = categoryMapper.requestToEntity(request);
        log.info(category);
        category.setStatus(Constants.EntityStatus.ACTIVE);
        CategoryDTO categoryDTO = categoryMapper.toDto(categoryRepository.save(category));
        response.setCategory(categoryDTO);
        log.info(category.toString());

        return response;


    }

    @Override
    @CacheEvict(value = "category-tree", allEntries = true)
    public CategoryResponse updateCategory(CategoryRequest request, Long id) {
        CategoryResponse response = new CategoryResponse();
        if (Validator.isNull(request)) {
            response.setMessage("Kiểm tra lại giáo trị truyền vào");
            return response;
        }
        if (Validator.isNull(id)) {
            response.setMessage("Mã loại sản phẩm đang trống");
            return response;
        }
        Categories category = categoryMapper.requestToEntity(request);
        category.setId(id);
        category = categoryRepository.save(category);
//        CategoryDTO categoryDTO = categoryMapper.toDto(category);
        //    response.setCategory(categoryDTO);
//        log.info(categoryDTO);
        return response;
    }

    @Override
    public CategoryResponse getById(Long category) {
        return null;
    }

    @Override
    public long countCategories(MultiValueMap<String, String> queryParams) {
        return categoryRepository.countCategory(queryParams);
    }


}
