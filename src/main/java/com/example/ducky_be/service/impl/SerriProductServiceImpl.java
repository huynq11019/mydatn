package com.example.ducky_be.service.impl;

import com.example.ducky_be.controller.protoco.request.SerriProductResquest;
import com.example.ducky_be.controller.protoco.response.SerriProductResponse;
import com.example.ducky_be.core.exception.BadRequestAlertException;
import com.example.ducky_be.core.mapper.SerrialMaper;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.repository.SerriProductrepository;
import com.example.ducky_be.repository.entity.SerriProduct;
import com.example.ducky_be.service.SerriProductService;
import com.example.ducky_be.service.dto.SerriProductDTO;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@Log4j2
@AllArgsConstructor
public class SerriProductServiceImpl implements SerriProductService {

    private final SerriProductrepository serriProductrepository;

    private final SerrialMaper serrialMaper;

    @Override
    public SerriProductResponse serriProductSearch(String keyword, Pageable pageable) {
        SerriProductResponse serriProductResponse = new SerriProductResponse();
        long count = serriProductrepository.coutnSerriByKyword(keyword);
        if (count > 0) {
            List<SerriProductDTO> serriProductDTOList =
                    this.serrialMaper.toDto(this.serriProductrepository.simpleSearch(keyword, pageable));
            serriProductResponse.setSerriProductDTOList(serriProductDTOList);
        }
        serriProductResponse.setTotalItem(count);
        return serriProductResponse;
    }

    @Override
    public SerriProductResponse countSerri(MultiValueMap<String, String> queryParams, Pageable pageable) {
        SerriProductResponse serriProductResponse = new SerriProductResponse();
        long count = serriProductrepository.countSerriProduct(queryParams);
        serriProductResponse.setTotalItem(count);
        return serriProductResponse;
    }

    @Override
    public SerriProductResponse createOrupdateSerri(SerriProductResquest serriProductResquest) {
        SerriProductResponse serriProductResponse = new SerriProductResponse();
        SerriProduct serrial = this.serriProductrepository.save(this.serrialMaper.requestToEntity(serriProductResquest)) ;
        serriProductResponse.setId(serrial.getId());
        return serriProductResponse;
    }

    @Override
    public SerriProductResponse deleteSerri(Long serrialId) {
        // check có tồn tại id này không
        SerriProduct serrail = this.serriProductrepository.findById(serrialId).orElseThrow(() ->
                new BadRequestAlertException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT));
        serrail.deleteSerriProduct();
        this.serriProductrepository.save(serrail);
        return SerriProductResponse.builder().build();
    }
}
