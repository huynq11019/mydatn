package com.example.ducky_be.service.impl;

import com.example.ducky_be.repository.UserLoginRepository;
import com.example.ducky_be.repository.entity.UserLogin;
import com.example.ducky_be.service.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserLoginServiceImpl implements UserLoginService {
    @Autowired
    private UserLoginRepository loginRepository;

    @Override
    public UserLogin save(UserLogin loginLog) {
        return this.loginRepository.save(loginLog);
    }
}
