package com.example.ducky_be.service.impl;

import com.example.ducky_be.controller.protoco.request.AuthoritesRequest;
import com.example.ducky_be.controller.protoco.request.EmployeeRequest;
import com.example.ducky_be.controller.protoco.response.EmployeeResponse;
import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.exception.ResponseException;
import com.example.ducky_be.core.mapper.AuthorityMapper;
import com.example.ducky_be.core.mapper.EmployeeMapper;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.core.sercurity.SecurityUtils;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.EmployeeRepository;
import com.example.ducky_be.repository.entity.Authorities;
import com.example.ducky_be.repository.entity.Employee;
import com.example.ducky_be.service.EmployeeService;
import com.example.ducky_be.service.SyncMailService;
import com.example.ducky_be.service.dto.EmployeeDTO;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Log4j2
@Service
@AllArgsConstructor
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    private final EmployeeMapper employeeMapper;

    private final PasswordEncoder passwordEncoder;

    private final SyncMailService syncMailService;

    private final AuthorityMapper authorityMapper;

    @Override
    public EmployeeResponse createEmployee(EmployeeRequest employeeRequest) {
        // check email exists
        Optional<Employee> employeeExists = employeeRepository.findEmployeeByEmail(employeeRequest.getEmail());
        if (employeeExists.isPresent()) {
            throw new ResponseException("Email already exists", ErrorCode.ERR_USER_EMAIL_EXIST);
        }
        EmployeeResponse employeeResponse = new EmployeeResponse();
        if (!Validator.isNotNull(employeeResponse.getId())) {

            Employee employee = employeeMapper.requestToEntity(employeeRequest);
            employee.setStatus(Constants.EntityStatus.ACTIVE);
            employee.setPassword(passwordEncoder.encode(employee.getPassword()));

            employeeResponse.setEmployeeDTO(employeeMapper.toDto(employeeRepository.save(employee)));
            log.info(employeeResponse);

//            this.fileUploadService.createFile(files, employeeResponse.getId(), Constants.WONERTYPE.AVATAR);
        }
        return employeeResponse;
    }

    // Cập nhật thông tin employee + phân quyền
    @Override
    public EmployeeResponse updateEmployee(EmployeeRequest employeeRequest) {
        EmployeeResponse employeeResponse = new EmployeeResponse();
        // check employee exists
        EmployeeDTO employeeDTO = this.enrichEmployee(employeeRequest.getId());
        employeeDTO.updateEmployee(employeeRequest);
        Employee employee = this.employeeMapper.toEntity(employeeDTO);
        employeeResponse.setEmployeeDTO(employeeMapper.toDto(employeeRepository.save(employee)));
        log.info(employeeResponse);

        return employeeResponse;
    }

    @Override
    public EmployeeResponse searchEmployee(MultiValueMap<String, String> queryParams, Pageable pageable) {
        EmployeeResponse employeeResponse = new EmployeeResponse();
        List<Employee> employeeList = this.employeeRepository.searchEmployee(queryParams, pageable);
        employeeResponse.setEmployeeDTOs(employeeMapper.toDto(employeeList));
        return employeeResponse;
    }

    @Override
    public EmployeeResponse getEmployeeById(String employeeId) {
        EmployeeResponse employeeResponse = new EmployeeResponse();
        Employee employeeEntity = employeeRepository.findById(employeeId).orElseThrow(
                () -> new ResponseException("Employee not found", ErrorCode.ERR_USER_NOT_EXIST));
        log.info(employeeEntity);
        employeeResponse.setEmployeeDTO(employeeMapper.toDto(employeeEntity));
        this.getUserGrantedAuthority(employeeId);
        return employeeResponse;
    }

    @Override
    public EmployeeResponse resetPassword(String employeeId) {

        this.syncMailService.employeeResetPassWord(employeeId);
        return EmployeeResponse.builder().build();
    }

    @Override
    public EmployeeResponse changePassword(String employeeId, String oldPassword, String newPassword) {
        return null;
    }

    @Override
    public String[] getUserGrantedAuthority(String employeeId) {
        log.info("get authority employeeId: {}", employeeId);
        List<String> grantedAuthorities = new ArrayList<>();

        return grantedAuthorities.toArray(new String[0]);
    }

    @Override
    public EmployeeResponse changeStatusEmployee(String employeeId) {
        EmployeeResponse employeeResponse = new EmployeeResponse();
        Employee employeeEntity = employeeRepository.findById(employeeId).orElseThrow(
                ()-> new ResponseException("Employee not found", ErrorCode.ERR_USER_NOT_EXIST));
        employeeEntity.changeLock();
        employeeResponse.setEmployeeDTO(employeeMapper.toDto(employeeRepository.save(employeeEntity)));
        return employeeResponse;
    }


    @Override
    public EmployeeResponse getCurrentEmployee() {
        String employeeId = SecurityUtils.getLoginUserId().orElseThrow(
                () -> new BadCredentialsException("bạn chưa đăng nhập"));
        EmployeeDTO employeeDTO = employeeMapper.toDto(employeeRepository.findById(employeeId).orElseThrow(
                () -> new UsernameNotFoundException("Employee not found")));
        return EmployeeResponse.builder().employeeDTO(employeeDTO).build();
    }

    @Override
    public EmployeeResponse setAuthorityForEmployee(String employeeId, AuthoritesRequest request) {
        Employee employee = this.employeeRepository.findById(employeeId).orElseThrow(
                () -> new ResponseException("Employee not found", ErrorCode.ERR_USER_NOT_EXIST));
        employee.updateAuthorities(request.getAuthorityDTOS().stream().map(Authorities::new).collect(Collectors.toList()));
        EmployeeResponse response = new EmployeeResponse();
        response.setEmployeeDTO(employeeMapper.toDto(this.employeeRepository.save(employee)));
        return response;
    }

    /**
     * @param
     * @return
     * @author: huynq
     * @since: 11/21/2021 10:08 AM
     * @description: enrich employee
     * @update:
     */
    private EmployeeDTO enrichEmployee(String employeeId) {

        Employee employee = this.employeeRepository.findById(employeeId).orElseThrow(
                () -> new UsernameNotFoundException("Employee not found"));

        return this.employeeMapper.toDto(employee);
    }

}
