package com.example.ducky_be.service.impl;

import com.example.ducky_be.controller.protoco.request.FavouritesRequest;
import com.example.ducky_be.controller.protoco.response.FavouritesResponse;
import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.exception.BadRequestAlertException;
import com.example.ducky_be.core.mapper.FavouritesMapper;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.core.sercurity.SecurityUtils;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.FavouritesRepository;
import com.example.ducky_be.repository.ProductRepository;
import com.example.ducky_be.repository.entity.Favourites;
import com.example.ducky_be.repository.entity.OptionProduct;
import com.example.ducky_be.repository.entity.Product;
import com.example.ducky_be.service.FavouritesService;
import com.example.ducky_be.service.dto.FavouritesDTO;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
@Transactional
public class FavouriteServiceImpl implements FavouritesService {
    @Autowired
    private FavouritesRepository favouritesRepository;

    @Autowired
    private ProductRepository productRepository;


    @Autowired
    private FavouritesMapper favouritesMapper;

    @Override
    public FavouritesResponse searchMyFavourites(MultiValueMap<String, String> queryParams, Pageable pageable) {
        FavouritesResponse response = new FavouritesResponse();
        Long count = this.favouritesRepository.countFavourites(queryParams);
        List<FavouritesDTO> lstFavoriteDTO = new ArrayList<>();

        if (Validator.isNotNull(count)) {
            lstFavoriteDTO = this.favouritesMapper.toDto(favouritesRepository.searchFavourites(queryParams, pageable));
        }
        lstFavoriteDTO.forEach(
                (item) -> {
                    // Lấy tên sản phẩm
                    Optional<Product> productOptional = productRepository.findById(item.getProductId());
                    if (productOptional.isPresent()) {
                        item.setProductName(productOptional.get().getProductName());
                        item.setPrice(productOptional.get().getPrice());
                    }
                    // Lấy số lượt yêu thích
                    Long number = favouritesRepository.countByProductId(item.getProductId());
                    item.setNumberFavourites(number);
                    log.info(item.getPrice());
                }
        );
        response.setFavouritesDTOS(lstFavoriteDTO);
        response.setTotalItem(count);
        return response;
    }

    @Override
    public FavouritesResponse searchFavouritesByUser(MultiValueMap<String, String> queryParams, String userId, Pageable pageable) {
        FavouritesResponse response = new FavouritesResponse();
        Long count = this.favouritesRepository.countFavourites(queryParams);
        List<FavouritesDTO> lstDTO = new ArrayList<>();

        if (Validator.isNotNull(count)) {
            lstDTO = this.favouritesMapper.toDto(favouritesRepository.searchFavouritesByUser(queryParams, userId, pageable));
        }
        lstDTO.forEach(
                (item) -> {
                    // Lấy tên sản phẩm
                    Product product = productRepository.findById(item.getCustomerId())
                            .orElseThrow(() -> new BadRequestAlertException(ErrorCode.PRODUCT_NOT_FOUND));

                    item.setProductName(product.getProductName());
                    item.setLikeCount(product.getLikeCount());

                    // Lấy số lượt yêu thích
                    Long number = favouritesRepository.countByProductId(item.getProductId());
                    item.setNumberFavourites(number);
                }
        );
        response.setFavouritesDTOS(lstDTO);
        response.setTotalItem(count);
        return response;
    }

    @Override
    public Long countProduct(MultiValueMap<String, String> queryParams) {
        return favouritesRepository.countFavourites(queryParams);
    }

    @Override
    public FavouritesResponse createFavourites(FavouritesRequest favouritesRequest) {
        FavouritesResponse response = new FavouritesResponse();
        String currentid = SecurityUtils.getLoginUserId().orElseThrow(
                () -> new BadRequestAlertException(ErrorCode.AUTHENTICATION_REQUIRED));
        if (Validator.isNull(favouritesRequest)) {
            response.setMessage("Gía trị truyền vào không hợp lệ");
            return response;
        }

        if (Validator.isNotNull(favouritesRequest.getId())) {
            response.setMessage("Có tồn tại giá trị khóa chính truyền vào");
        }
        // check customer đã thích sản phẩm trước đó hay chưa
        Optional<Favourites> favouritesExists = this.favouritesRepository
                .findByCustomerIdAndProductId(currentid, favouritesRequest.getProductId());
        if (favouritesExists.isPresent()) {
            response.setMessage("Sản phẩm đã được thích");
            return response;
        }

        favouritesRequest.setCustomerId(currentid);
        Favourites favourites = favouritesMapper.requestToEntity(favouritesRequest);

        favourites.setStatus(Constants.EntityStatus.ACTIVE);

        FavouritesDTO favouritesDTO = favouritesMapper.toDto(favouritesRepository.save(favourites));

        response.setFavouritesDTO(favouritesDTO);

        return response;
    }

    @Override
    public FavouritesResponse updateFavourite(FavouritesRequest favouritesRequest) {
        FavouritesResponse response = new FavouritesResponse();

        if (Validator.isNull(favouritesRequest)) {
            response.setMessage("Gía trị truyền vào không hợp lệ");
            return response;
        }

        if (Validator.isNotNull(favouritesRequest.getId())) {
            response.setMessage("Có tồn tại giá trị khóa chính truyền vào");
        }

        Favourites favourites = favouritesMapper.requestToEntity(favouritesRequest);

        favourites.setStatus(Constants.EntityStatus.ACTIVE);

        FavouritesDTO favouritesDTO = favouritesMapper.toDto(favouritesRepository.save(favourites));

        response.setFavouritesDTO(favouritesDTO);

        return response;
    }

    @Override
    public FavouritesResponse findFavouritesById(String favouritesId) {
        FavouritesResponse response = new FavouritesResponse();
        Favourites favourites = favouritesRepository.findById(favouritesId).get();

        FavouritesDTO favouritesDTO = favouritesMapper.toDto(favourites);

        // Lấy tên sản phẩm
        Optional<Product> productOptional = productRepository.findById(favourites.getCustomerId());
        if (productOptional.isPresent()) {
            favouritesDTO.setProductName(productOptional.get().getProductName());
        }
        // Lấy số lượt yêu thích
        Long number = favouritesRepository.countByProductId(favourites.getProductId());
        favouritesDTO.setNumberFavourites(number);

        response.setFavouritesDTO(favouritesDTO);

        return response;
    }

    @Override
    public FavouritesResponse deleteFavourites(String favouritesId) {
        FavouritesResponse response = new FavouritesResponse();
        if (!favouritesId.isEmpty()) {
            try {
                Optional<Favourites> favourites = favouritesRepository.findById(favouritesId);
                if (favourites.isPresent()) {
                    favouritesRepository.deleteById(favouritesId);
                }
                FavouritesDTO favouritesDTO = favouritesMapper.toDto(favourites.get());
                response.setFavouritesDTO(favouritesDTO);
                return response;
            } catch (Exception e) {
                return response;
            }
        }
        return response;
    }

    @Override
    public FavouritesResponse findFavouritesByUserId(String userId) {
        FavouritesResponse response = new FavouritesResponse();
        if (StringUtils.hasLength(userId)) {
            try {
                    List<FavouritesDTO> favouritesDTOs = favouritesMapper.toDto(this.favouritesRepository.findAllByCustomerIdAndStatus(userId, Constants.EntityStatus.ACTIVE));
                    response.setFavouritesDTOS(favouritesDTOs);
                    return response;
            } catch (Exception e) {
                return response;
            }
        }
        return response;
    }

    @Override
    public FavouritesResponse findFavoritesProductByCurrentUser() {
        return null;
    }

    @Override
    public FavouritesResponse changeStatus(String favouritesId, Integer status) {
        FavouritesResponse response = new FavouritesResponse();
        Favourites favourites = this.favouritesRepository.findById(favouritesId).orElseThrow(
                () -> new BadRequestAlertException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT)
        );
        favourites.setStatus(status);
        this.favouritesRepository.save(favourites);
        return response;
    }
}
