package com.example.ducky_be.service.impl;

import com.example.ducky_be.controller.protoco.request.CreateOrderZaloPayRequest;
import com.example.ducky_be.core.utils.zalopay.HMACUtil;
import com.example.ducky_be.service.ZaloPayCheckoutService;
import lombok.extern.log4j.Log4j2;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

@Log4j2
@Service
public class ZaloPayCheckoutServiceImpl implements ZaloPayCheckoutService {
    private static Map<String, String> config = new HashMap<String, String>() {{
        put("appid", "2554");
        put("key1", "sdngKKJmqEMzvh5QQcdD2A9XBSKUNaYn");
        put("key2", "trMrHtvjo6myautxDUiAcYsVtaeQ8nhf");
        put("endpoint", "https://sandbox.zalopay.com.vn/v001/tpe/createorder");
    }};

    public static String getCurrentTimeString(String format) {
        Calendar cal = new GregorianCalendar(TimeZone.getTimeZone("GMT+7"));
        SimpleDateFormat fmt = new SimpleDateFormat(format);
        fmt.setCalendar(cal);
        return fmt.format(cal.getTimeInMillis());
    }

    @Override
    public JSONObject createOrder(CreateOrderZaloPayRequest request) {

        try {
            final Map embeddata = new HashMap() {{
                put("orderId", request.getOrderId());
            }};

            final Map<String, Object> item = new HashMap();
            item.put("itemid", "knb");
            item.put("itemname", "kim nguyen bao");
            item.put("itemprice", 198400);
            item.put("itemquantity", 1);

            Map<String, Object> order = new HashMap<>();
            order.put("appid", config.get("appid"));
            order.put("apptransid", getCurrentTimeString("yyMMdd") + "_" + request.getOrderId()); // mã giao dich có định dạng yyMMdd_xxxx
            order.put("apptime", System.currentTimeMillis()); // miliseconds
            order.put("appuser", "demo");
            order.put("amount", request.getAmount());
            order.put("description", "ZaloPay Intergration Demo");
            order.put("bankcode", "zalopayapp");
            order.put("item", new JSONObject(item).toString());
            order.put("embeddata", new JSONObject(embeddata).toString());
            // appid +”|”+ apptransid +”|”+ appuser +”|”+ amount +"|" + apptime +”|”+ embeddata +"|" +item
            String data = order.get("appid") + "|" + order.get("apptransid") + "|" + order.get("appuser") + "|" + order.get("amount")
                    + "|" + order.get("apptime") + "|" + order.get("embeddata") + "|" + order.get("item");
            order.put("mac", HMACUtil.HMacHexStringEncode(HMACUtil.HMACSHA256, config.get("key1"), data));

            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(config.get("endpoint"));

            List<NameValuePair> params = new ArrayList<>();
            for (Map.Entry<String, Object> e : order.entrySet()) {
                params.add(new BasicNameValuePair(e.getKey(), e.getValue().toString()));
            }

            // Content-Type: application/x-www-form-urlencoded
            post.setEntity(new UrlEncodedFormEntity(params));

            CloseableHttpResponse res = client.execute(post);
            BufferedReader rd = new BufferedReader(new InputStreamReader(res.getEntity().getContent()));

            StringBuilder resultJsonStr = new StringBuilder();
            String line;

            while ((line = rd.readLine()) != null) {
                resultJsonStr.append(line);
            }

            JSONObject result = new JSONObject(resultJsonStr.toString());
            for (String key : result.keySet()) {
                System.out.format("%s = %s\n", key, result.get(key));
            }
            return result;

        } catch (Exception e) {
            log.info(e);
        }
        return null;
    }
}
