package com.example.ducky_be.service.impl;

import com.example.ducky_be.controller.protoco.request.AddressRequest;
import com.example.ducky_be.controller.protoco.response.AddressResponse;
import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.exception.BadRequestAlertException;
import com.example.ducky_be.core.mapper.AddressMapper;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.core.sercurity.SecurityUtils;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.AddressRepository;
import com.example.ducky_be.repository.entity.Address;
import com.example.ducky_be.service.AddressService;
import com.example.ducky_be.service.dto.AddressDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AddresssServiceImpl implements AddressService {
    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private AddressMapper addressMapper;

    @Override
    public AddressResponse searchAddress(MultiValueMap<String, String> queryParams, Pageable pageable) {
        AddressResponse response = new AddressResponse();
        Long count = this.addressRepository.countAddress(queryParams);
        List<AddressDTO> lstDTO = new ArrayList<>();

        if (Validator.isNotNull(count)) {
            lstDTO = this.addressMapper.toDto(addressRepository.searchAddress(queryParams, pageable));
        }

        response.setAddressDTOS(lstDTO);
        response.setTotalItem(count);
        return response;
    }

    @Override
    public Long countAddress(MultiValueMap<String, String> queryParams) {
        return addressRepository.countAddress(queryParams);
    }

    @Override
    public AddressResponse createAdress(AddressRequest addressRequest) {
        AddressResponse response = new AddressResponse();

        if (Validator.isNull(addressRequest)) {
            response.setMessage("Giá trị truyền vào không hợp lệ");
            return response;
        }
        if (Validator.isNotNull(addressRequest.getId())) {
            response.setMessage("Có tồn tại giá trị khóa chính truyền vào");
        }
        // check user đã đăng nhập

        Address address = addressMapper.requestToEntity(addressRequest);

        address.setStatus(Constants.EntityStatus.ACTIVE);

        AddressDTO addressDTO = addressMapper.toDto(addressRepository.save(address));

        response.setAddressDTO(addressDTO);
        log.info(address.toString());

        return response;
    }

    @Override
    public AddressResponse updateAddress(AddressRequest addressRequest) {

        AddressResponse response = new AddressResponse();

        if (Validator.isNull(addressRequest)) {
            response.setMessage("Giá trị truyền vào không hợp lệ");
            return response;
        }
        if (Validator.isNull(addressRequest)) {
            response.setMessage("Khóa chính truyền vào không được null");
            return response;
        }
        Address address = addressMapper.requestToEntity(addressRequest);

        address.setStatus(Constants.EntityStatus.ACTIVE);

        AddressDTO addressDTO = addressMapper.toDto(addressRepository.save(address));

        response.setAddressDTO(addressDTO);

        log.info(address.toString());

        return response;
    }

    @Override
    public AddressResponse findproductById(Long addressId) {
        AddressResponse response = new AddressResponse();

        Address address = addressRepository.findByIdAndStatus(addressId, Constants.EntityStatus.ACTIVE)
                .orElseThrow(()
                        -> new BadRequestAlertException("Không tìm thấy địa chỉ", "address", "notfound"));

        AddressDTO addressDTO = addressMapper.toDto(address);

        response.setAddressDTO(addressDTO);

        return response;
    }

    @Override
    public AddressResponse saveOrUpdateDraft(AddressRequest addressRequest) {
        AddressResponse response = new AddressResponse();

        if (Validator.isNull(addressRequest)) {
           throw new BadRequestAlertException("Giá trị truyền vào không hợp lệ", "address", "notfound");
        }

        Address address = addressMapper.requestToEntity(addressRequest);

        //  Chỉnh trạng thái là bảng nháp
        address.setStatus(Constants.EntityStatus.DRAFT);

        AddressDTO addressDTO = addressMapper.toDto(addressRepository.save(address));

        response.setAddressDTO(addressDTO);

        log.info(address.toString());

        return response;
    }

    @Override
    public AddressResponse myaddress(MultiValueMap<String, String> queryParams, Pageable pageable) {
        // check user đã đăng nhập
        String currentUser = SecurityUtils.getLoginUserId().orElseThrow(() -> new BadCredentialsException("User not found"));
        List<String> lstUser = new ArrayList<>();
        lstUser.add(currentUser);

        queryParams.put("userId", lstUser);
        AddressResponse response = new AddressResponse();
        Long count = this.addressRepository.countAddress(queryParams);
        if (Validator.isNotNull(count)) {
            response.setAddressDTOS(this.addressMapper.toDto(this.addressRepository.searchAddress(queryParams, pageable)));
        }
        return response;

    }

    @Override
    public AddressResponse findByUserId(String userId) {
        AddressResponse response = new AddressResponse();
        List<Address> addresses = addressRepository.findAllByCreateBy(userId).stream()
                .filter(address -> address.getStatus() == Constants.EntityStatus.ACTIVE)
                .collect(Collectors.toList());
        response.setAddressDTOS(addressMapper.toDto(addresses));
        return response;
    }

    @Override
    public void deleteAddress(Long addressId) {
        String currentUser = SecurityUtils.getLoginUserId().orElseThrow(() -> new BadCredentialsException(ErrorCode.AUTHENTICATION_REQUIRED.getMessage()));
        Address address = addressRepository.findByIdAndStatus(addressId, Constants.EntityStatus.ACTIVE)
                .orElseThrow(() -> new BadRequestAlertException("Không tìm thấy địa chỉ", "address", "notfound"));
        address.setStatus(Constants.EntityStatus.REMOTED);
        this.addressRepository.save(address);
    }

    @Override
    public void setDefaultAddress(Long addressId) {
        String currentUser = SecurityUtils.getLoginUserId().orElseThrow(() -> new BadCredentialsException(ErrorCode.AUTHENTICATION_REQUIRED.getMessage()));
        List<Address> address = addressRepository.findAllByCreateBy(currentUser);
        address.forEach(fomat -> {
            if (fomat.getId() == addressId) {
                fomat.setIsDefault(true);
            }else {
                fomat.setIsDefault(false);
            }
        });
        this.addressRepository.saveAll(address);

    }
}
