package com.example.ducky_be.service.impl;

import com.example.ducky_be.controller.protoco.request.OrderDetailRequest;
import com.example.ducky_be.controller.protoco.response.OrderDetailResponse;
import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.exception.BadRequestAlertException;
import com.example.ducky_be.core.mapper.OrderDetailMapper;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.OrderDetailRepository;
import com.example.ducky_be.repository.OrderRepository;
import com.example.ducky_be.repository.ProductRepository;
import com.example.ducky_be.repository.entity.OrderDetail;
import com.example.ducky_be.repository.entity.Product;
import com.example.ducky_be.service.OrderDetailService;
import com.example.ducky_be.service.dto.OrderDetailDTO;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log4j2
@Service
@Transactional
@AllArgsConstructor
public class OrderDetailServiceImpl implements OrderDetailService {
    private final OrderDetailMapper orderDetailMapper;
    private final OrderDetailRepository orderDetailRepository;
    private final ProductRepository productRepository;
    private final OrderRepository orderRepository;

    @Override
    public OrderDetailResponse createOrderDetailByOderId(OrderDetailRequest request, String orderId) {

        OrderDetailResponse response = new OrderDetailResponse();
        if (Validator.isNull(request)) {
            throw new BadRequestAlertException(ErrorCode.ERROR_DATA_INVALID);
        }
        OrderDetail orderDetail = orderDetailMapper.requestToEntity(request);
        log.info(orderDetail);
        orderDetail.setStatus(Constants.EntityStatus.ACTIVE);

        if (!orderRepository.existsById(orderId)) {
            throw new EntityNotFoundException("Chưa có thông tin trong dữ liệu");
        }
        orderDetail.setOrderId(orderId);
        OrderDetailDTO dto = orderDetailMapper.toDto(orderDetailRepository.save(orderDetail));
        if (request.getProductId() != null) {
            Optional<Product> otpProduct = productRepository.findById(request.getProductId());
            if (otpProduct.isEmpty()) {
                throw new EntityNotFoundException("Chưa có thông tin trong dữ liệu");
            }
            Product product = otpProduct.get();
            long newTotal = product.getQuantity() - request.getQuantity();
            if (newTotal > 0) {
                product.setQuantity(newTotal);
                productRepository.save(product);
            } else {
                throw new BadRequestAlertException(ErrorCode.ERROR_DATA_INVALID);
            }

        }
        response.setOrderDetail(dto);
        return response;
    }

    @Override
    public OrderDetailResponse findOderDetailByOderId(MultiValueMap<String, String> queryParams, Pageable pageable,String orderId) {
        OrderDetailResponse response = new OrderDetailResponse();
        Long total = this.orderDetailRepository.countByOrderId( queryParams,orderId);
        List<OrderDetailDTO> list = new ArrayList<>();
        if (total > 0) {
            List<OrderDetail> orderDetails = orderDetailRepository.findAllOrderDetailByOrderId(queryParams, pageable,orderId);
            log.info(orderDetails);
            list = orderDetailMapper.toDto(orderDetails);
        }
        response.setOrderDetailList(list);
        response.setTotalItem(total);
        return response;
    }

    @Override
    public Long countOderDetailByOrderId(MultiValueMap<String, String> queryParams, Pageable pageable,String orderId) {
        return this.orderDetailRepository.countByOrderId( queryParams,orderId);
    }


}
