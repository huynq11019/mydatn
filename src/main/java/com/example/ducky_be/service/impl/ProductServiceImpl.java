package com.example.ducky_be.service.impl;


import com.example.ducky_be.controller.protoco.request.ProductRequest;
import com.example.ducky_be.controller.protoco.response.ProductResponse;
import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.exception.BadRequestAlertException;
import com.example.ducky_be.core.exception.ResponseException;
import com.example.ducky_be.core.mapper.ProductMapper;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.core.model.resoonse.Response;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.CategoryRepository;
import com.example.ducky_be.repository.OptionRepository;
import com.example.ducky_be.repository.ProductRepository;
import com.example.ducky_be.repository.RatingProductRepository;
import com.example.ducky_be.repository.entity.Categories;
import com.example.ducky_be.repository.entity.OptionProduct;
import com.example.ducky_be.repository.entity.Product;
import com.example.ducky_be.service.FileUploadService;
import com.example.ducky_be.service.ProductService;
import com.example.ducky_be.service.dto.FileUploadDTO;
import com.example.ducky_be.service.dto.ProductDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Log4j2
@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    private final ProductMapper productMapper;

    private final FileUploadService fileUploadService;

    private final OptionRepository optionRepository;

    private final ObjectMapper mapper;

    private final CategoryRepository categoryRepository;

    private final RatingProductRepository ratingProductRepository;

    public ProductServiceImpl(ProductRepository productRepository, ProductMapper productMapper,
                              FileUploadService fileUploadService, OptionRepository optionRepository, ObjectMapper mapper, CategoryRepository categoryRepository, RatingProductRepository ratingProductRepository) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
        this.fileUploadService = fileUploadService;
        this.optionRepository = optionRepository;
        this.mapper = mapper;
        this.categoryRepository = categoryRepository;
        this.ratingProductRepository = ratingProductRepository;
    }

    @Override
    public ProductResponse searchProduct(MultiValueMap<String, String> queryParams, Pageable pageable) {
        ProductResponse response = new ProductResponse();
        // dem tong so ban ghi
        Long count = this.productRepository.countProduct(queryParams);
//         kiểm tra trong db có dữ liệu thì thực hiện query data
        if (Validator.isNull(count)) {
            response.setTotalItem(count);
            return response;
        }

        List<ProductDTO> lstDTO = this.productMapper.toDto(productRepository.searchProduct(queryParams, pageable));
        List<String> getProductIds = lstDTO.stream().map(ProductDTO::getId).collect(Collectors.toList());
        List<OptionProduct> lstOption = this.optionRepository.findAllByProductId(getProductIds);

        // map category name
        List<Long> categoryIds = lstDTO.stream().map(ProductDTO::getCategoryId).collect(java.util.stream.Collectors.toList());
        List<Categories> lstCategory = this.categoryRepository.findAllById(categoryIds);
        lstDTO.forEach(productDTO -> {
            Optional<Categories> oCategory = lstCategory.stream()
                    .filter(c -> c.getId().equals(productDTO.getCategoryId())).findFirst();
            oCategory.ifPresent(c -> productDTO.setCategoryName(c.getCategoryName()));
            List<OptionProduct> optionProducts = lstOption.stream().filter(o -> o.getProductId().equals(productDTO.getId()))
                    .collect(Collectors.toList());
            productDTO.setOptionProducts(optionProducts);
            productDTO.setDescription("");

        });

        response.setListProductDTO(lstDTO);
        response.setTotalItem(count);
        return response;
    }

    @Override
    public ProductResponse searchProductCache(MultiValueMap<String, String> queryParams, Pageable pageable) {
        return null;
    }

    /**
     * @return
     * @return productDTO
     * @author: huynq
     * @since: 9/19/2021 10:30 AM
     * @description: Thực hiện lấy product theo ID gồm ảnh và thông tin đánh giá
     * @update:
     */
    @Override
    @Cacheable(value = "ProductResponse", key = "#productId", unless = "#result == null")
    public ProductResponse findproductById(String productId) {
        ProductResponse response = new ProductResponse();


        ProductDTO productDTO = this.enrichProduct(productId, false);

//        List<FileUploadDTO> fileUploadDTOS = this.fileUploadService
//                .getListByOwnerAndOwnerType(productid, Constants.OwnerType.PRODUCT, true);
//        productDTO.setFiles(fileUploadDTOS);
        response.setProductDTO(productDTO);

        return response;

    }

    /**
     * @author: huynq
     * @since: 11/14/2021 4:17 AM
     * @description: count product by params
     * @update:
     */
    @Override
    public Long countProduct(MultiValueMap<String, String> queryParams) {
        return productRepository.countProduct(queryParams);
    }


    /**
     * @author: huynq
     * @since: 9/19/2021 4:16 PM
     * @description:
     * @update:
     */
    @Override
    public ProductResponse createProduct(ProductRequest productRequest, List<MultipartFile> files) {
        ProductResponse response = new ProductResponse();
        log.info(productRequest);
        if (Validator.isNull(productRequest)) {
            throw new BadRequestAlertException(ErrorCode.ERROR_DATA_INVALID);
        }
        if (Validator.isNotNull(productRequest.getId())) {
            throw new BadRequestAlertException("Tồn tại khóa chính trong khi tạo ", ErrorCode.EXIST_PRIMARY);
        }
//        Product product = productMapper.requestToEntity(productRequest);
        List<OptionProduct> optionProducts = this.getListOptionProduct(productRequest.getOptionProducts());

        ProductDTO productDTO = new ProductDTO(productRequest, optionProducts);

        List<FileUploadDTO> fileUploadDTO = this.fileUploadService.createFile(files, productDTO.getId(), Constants.OwnerType.PRODUCT);
        log.info(productDTO);

        ProductDTO productSaved = this.saveRicherProduct(productDTO);

        productDTO.setFiles(fileUploadDTO);
        response.setProductDTO(productSaved);
//        response.setProductDTO(productSaved);

        return response;
    }

    /**
     * @param productRequest
     * @author: huynq
     * @since: 9/19/2021 11:05 AM
     * @description: Thực hiện update sản phẩm theo ID product
     * @update:
     */
    @Override
//    @CacheEvict(value = "product", key = "#productId")
    @CacheEvict(value = "ProductResponse", key = "#productId")
    public ProductResponse updateProduct(String productId, ProductRequest productRequest, List<MultipartFile> files) {

        ProductResponse response = new ProductResponse();

        if (Validator.isNull(productRequest)) {
            throw new BadRequestAlertException(ErrorCode.ERROR_DATA_INVALID);
        }
        if (Validator.isNull(productRequest)) {
            throw new BadRequestAlertException("khóa chính không tồn tại", ErrorCode.FORM_DATA_NULL);
        }
        List<OptionProduct> optionProducts = this.getListOptionProduct(productRequest.getOptionProducts());
        log.info(optionProducts);
        ProductDTO productDTO = this.enrichProduct(productId, false);
        productDTO.updateProduct(productRequest, optionProducts);

        ProductDTO product = this.saveRicherProduct(productDTO);
        log.info(product);
        if (Validator.isNotNull(files)) {
            List<FileUploadDTO> fileUploadDTO = this.fileUploadService.createFile(files
                    , productDTO.getId(), Constants.OwnerType.PRODUCT
            );
            log.info("cập nhật file " + fileUploadDTO);

        }
        response.setProductDTO(product);
        return response;
    }

    @Override
    @CacheEvict(value = "ProductResponse", key = "#productId")
    public ProductResponse changeStatus(String productId, Integer status) {
        ProductResponse response = new ProductResponse();
        Product product = this.productRepository.findById(productId).orElseThrow(
                () -> new BadRequestAlertException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT)
        );

        product.setStatus(Constants.EntityStatus.REMOTED);
        Product product1 = this.productRepository.save(product);
        log.info("delete product " + product1);
        return response;
    }

    @Override
    @Cacheable(value = "ProductResponse", key = "#productId", unless = "#result == null")
    public ProductResponse getRicherProductById(String productId) {
        ProductDTO productDTO = this.enrichProduct(productId, true);
        productDTO.setRatingPoint(this.ratingProductRepository.countAverageRating(productId));
//        getPointProduct(productDTO);
        return ProductResponse.builder().productDTO(productDTO).build();
    }

    /**
     * enrich product
     *
     * @param productId
     * @return
     */
//    @Cacheable(value = "productDTO", key = "#productId")
    public ProductDTO   enrichProduct(String productId, boolean isLoadReate) {
        Product product = this.productRepository.findById(productId).orElseThrow(() ->
                new ResponseException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT));
        ProductDTO productDTO = productMapper.toDto(product);

        // load option product
        List<OptionProduct> optionProducts = this.optionRepository
                .findAllByProductIdAndStatus(productId, Constants.EntityStatus.ACTIVE);
        productDTO.setOptionProducts(optionProducts);

        // load cateogory name
        Optional<Categories> category = this.categoryRepository.findById(productDTO.getCategoryId());
        category.ifPresent(categories -> productDTO.setCategoryName(categories.getCategoryName()));
        // load file upload
        List<FileUploadDTO> fileUploadDTOs = this.fileUploadService
                .getListByOwnerAndOwnerType(productId, Constants.OwnerType.PRODUCT, true);
        productDTO.setFiles(fileUploadDTOs);
        return productDTO;
    }

    /**
     * save richer product
     *
     * @param productDTO
     * @return productDTO
     */
    private ProductDTO saveRicherProduct(ProductDTO productDTO) {
        Product product = productMapper.toEntity(productDTO);

        // save product
        if (Validator.isNotNull(product)) {
            product.setPrice(productDTO.getMinPrice());
          try {
              productRepository.save(product);
          }catch (Exception e){
              log.error(e.getMessage());
              e.printStackTrace();
          }

        } else {
            throw new BadRequestAlertException("Sản phẩm không tồn tại", ErrorCode.FORM_DATA_NULL);
        }

        if (Validator.isNotNull(productDTO.getOptionProducts())) {
            log.info("save option product"+ productDTO.getOptionProducts());
            // save option product
            this.optionRepository.saveAll(productDTO.getOptionProducts());
        }
        return productDTO;
    }

    private List<OptionProduct> getListOptionProduct(String json) {
        try {
            return mapper.readValue(json, new TypeReference<List<OptionProduct>>() {
            });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        throw new BadRequestAlertException("Invalid json", "", "");
    }
}
