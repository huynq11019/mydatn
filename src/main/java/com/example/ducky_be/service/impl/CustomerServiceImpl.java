package com.example.ducky_be.service.impl;

import com.example.ducky_be.adapter.VerifyUtils;
import com.example.ducky_be.adapter.telegramBot.TelegramUtils;
import com.example.ducky_be.controller.protoco.request.CustomerRequest;
import com.example.ducky_be.controller.protoco.response.CustomerResponse;
import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.exception.BadRequestAlertException;
import com.example.ducky_be.core.exception.ResponseException;
import com.example.ducky_be.core.mapper.AddressMapper;
import com.example.ducky_be.core.mapper.CustomerMapper;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.core.sercurity.SecurityUtils;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.AddressRepository;
import com.example.ducky_be.repository.CustomerRepository;
import com.example.ducky_be.repository.entity.Address;
import com.example.ducky_be.repository.entity.Customer;
import com.example.ducky_be.service.CustomerService;
import com.example.ducky_be.service.FileUploadService;
import com.example.ducky_be.service.SyncMailService;
import com.example.ducky_be.service.dto.CustomerDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Slf4j
@AllArgsConstructor
@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

    private final CustomerMapper customerMapper;

    private final CustomerRepository customerRepository;

    private final PasswordEncoder passwordEncoder;

    private final AddressRepository addressRepository;

    private final FileUploadService fileUploadService;

    private final AddressMapper addressMapper;

    private final SyncMailService syncMailService;


    @Override
    public CustomerResponse registerUser(CustomerRequest request) {
        CustomerResponse response = new CustomerResponse();
        if (Validator.isNull(request)) {
            log.error("Lỗi input");
            throw new BadRequestAlertException(ErrorCode.ERR_INPUT_INVALID);
        }
        // check địa chỉ email đã tồn tại chưa
        if (this.customerRepository.findByEmail(request.getEmail()).isPresent()) {
            throw new BadRequestAlertException("Địa chỉ email đã tồn tại trong hệ thống", ErrorCode.EMAIL_ALREADY_EXIST);
        }

        // verify recapcha
        if (!VerifyUtils.verify(request.getCaptcha())) {
            log.error("lỗi capcha");
            throw new BadRequestAlertException("Capcha không hợp lệ", ErrorCode.ERROR_DATA_INVALID);
        }
        Customer customer = this.customerMapper.toEntity(request);
        customer.setPasswordHash(passwordEncoder.encode(request.getPasswordHash()));
        customer.setStatus(Constants.EntityStatus.DRAFT);
        TelegramUtils.sendMessage(customer.getEmail() + " Đã đăng ký tải khoản tại web của bạn");
        // lưu thông tin người dùng
        log.info("current customer =>>>" + customer);
        Customer customerSaved = this.customerRepository.save(customer);
        response.setId(customerSaved.getId());
        return response;
    }

    @Override
    public CustomerResponse searchCustomer(MultiValueMap<String, String> queryParams, Pageable pageable) {
        CustomerResponse response = new CustomerResponse();
        long countCustomer = this.customerRepository.countAllByStatus(Constants.EntityStatus.ACTIVE);
        log.info(countCustomer + "");


        if (countCustomer > 0) {
            List<Customer> customers = this.customerRepository.searchCustomer(queryParams, pageable);
            response.setCustomerDTOS(this.customerMapper.toDto(customers));
            response.setTotalItem(countCustomer);
        } else {
            response.setErrorCode(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT);
            return response;
        }
        return response;
    }

    @Override
    public long countCustomer(MultiValueMap<String, String> queryParams) {
        return this.customerRepository.countCus(queryParams);
    }

    @Override
    public CustomerResponse findCustomerByid(String idCustomer) {
        CustomerResponse customerResponse = new CustomerResponse();
        Optional<Customer> customer = Optional.ofNullable(this.customerRepository.findById(idCustomer).orElseThrow(
                () -> new ResponseException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT)));
        if (!customer.isPresent()) {
            log.error("không tìm thấy khách hàng theo id" + idCustomer);
            throw new BadRequestAlertException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT);
        }
        CustomerDTO customerDTO = this.customerMapper.toDto(customer.get());
        customerResponse.setCustomerDTO(customerDTO);
        return customerResponse;
    }

    @Override
    public CustomerResponse verifyAccount(String customerId) {

        this.syncMailService.customerActiveAccount(customerId);
        return CustomerResponse.builder().build();

    }

    @Override
    public CustomerResponse lockCustomer(String customerid, Integer status) {
        CustomerResponse customerResponse = new CustomerResponse();
        int recoreChange = this.customerRepository.changeStatusProduct(customerid, status);
        customerResponse.setRecordChange(recoreChange);
        return customerResponse;
    }

    @Override
    public CustomerResponse forgotPassword(String email) {
        this.syncMailService.customerForgotPassword(email);
        return CustomerResponse.builder().build();
    }

    @Override
    public CustomerResponse myProfile() {
        String currentUser = SecurityUtils.getLoginUserId().orElseThrow(() ->
                new BadCredentialsException(ErrorCode.AUTHENTICATION_REQUIRED.getMessage()));

        log.info("current user =>>>" + currentUser);
        Customer customer = this.customerRepository.findById(currentUser).orElseThrow(() ->
                new ResponseException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT));

        List<Address> addresses = this.addressRepository.findAllByCreateBy(currentUser);
        CustomerDTO customerDTO = this.customerMapper.toDto(customer);

        customerDTO.setAddressDTOS(this.addressMapper.toDto(addresses));

        return CustomerResponse.builder()
                .customerDTO(customerDTO)
                .build();
    }

    @Override
    public CustomerResponse updateProfile(CustomerRequest request) {
        String currentUser = SecurityUtils.getLoginUserId().orElseThrow(() ->
                new BadCredentialsException(ErrorCode.AUTHENTICATION_REQUIRED.getMessage()));
        log.info("current user =>>>" + currentUser);
        // kiểm tra xem có tồn tại khách hàng không
        Customer customer = this.customerRepository.findById(currentUser).orElseThrow(() ->
                new ResponseException(ErrorCode.USER_NOT_FOUND));

        // Lưu thay đổi khác vào database
        Customer customerUpdate = this.customerMapper.toEntity(request);
        customerUpdate.setId(customer.getId());


        return CustomerResponse.builder()
                .customerDTO(this.customerMapper.toDto(this.customerRepository.save(customerUpdate)))
                .build();
    }

    @Override
    public CustomerResponse activeAccount() {
        String currentUserId = SecurityUtils.getLoginUserId().orElseThrow(() ->
                new BadCredentialsException(ErrorCode.AUTHENTICATION_REQUIRED.getMessage()));

        log.info("current user =>>>" + currentUserId);
        Customer customer = this.customerRepository.findById(currentUserId).orElseThrow(
                () -> new ResponseException("Customer not found ", ErrorCode.ERR_USER_NOT_EXIST));
        customer.setStatus(Constants.EntityStatus.ACTIVE);
        CustomerResponse response = new CustomerResponse();
        response.setCustomerDTO(this.customerMapper.toDto(this.customerRepository.save(customer)));
        return response;
    }

    @Override
    public CustomerResponse resetPassword(String password) {
        String userId = SecurityUtils.getLoginUserId().orElseThrow(
                () -> new ResponseException("Token not found ", ErrorCode.ERR_USER_NOT_EXIST));
        Customer customer = this.customerRepository.findById(userId).orElseThrow(
                () -> new ResponseException("userId not found ", ErrorCode.ERR_USER_NOT_EXIST));
        customer.changePassword(passwordEncoder.encode(password));
        CustomerResponse response = new CustomerResponse();
        response.setCustomerDTO(this.customerMapper.toDto(this.customerRepository.save(customer)));
        return response;
    }


}
