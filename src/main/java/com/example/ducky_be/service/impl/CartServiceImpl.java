package com.example.ducky_be.service.impl;

import com.example.ducky_be.controller.protoco.request.CartRequest;
import com.example.ducky_be.controller.protoco.response.CartResponse;
import com.example.ducky_be.core.constants.Constants;
import com.example.ducky_be.core.exception.BadRequestAlertException;
import com.example.ducky_be.core.exception.ResponseException;
import com.example.ducky_be.core.mapper.CartMapper;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.core.sercurity.SecurityUtils;
import com.example.ducky_be.core.utils.Validator;
import com.example.ducky_be.repository.CartRepository;
import com.example.ducky_be.repository.OptionRepository;
import com.example.ducky_be.repository.ProductRepository;
import com.example.ducky_be.repository.entity.Cart;
import com.example.ducky_be.repository.entity.OptionProduct;
import com.example.ducky_be.repository.entity.Product;
import com.example.ducky_be.service.CartService;
import com.example.ducky_be.service.dto.CartDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MultiValueMap;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Log4j2
@Transactional
@RequiredArgsConstructor
public class CartServiceImpl implements CartService {

    private final CartRepository cartRepository;

    private final CartMapper cartMapper;

    private final ProductRepository productRepository;

    private final OptionRepository optionRepository;

    @Override
    public CartResponse searchmyCart(MultiValueMap<String, String> params, Pageable pageable) {
        String currentId = SecurityUtils.getLoginUserId().orElseThrow(() ->
                new BadCredentialsException("Yêu cầu đăng nhập"));
        List<String> customer = new ArrayList<>();
        customer.add(currentId);
        params.put("customerId", customer);
        Long total = cartRepository.countCartOfCustomer(params, pageable);
        if (total == 0) {
            return CartResponse.builder().build();
        }
        List<CartDTO> carts = this.cartMapper.toDto(cartRepository.searchMyCustomer(params, pageable));
        List<String> productIds = carts.stream()
                .map(CartDTO::getProductId)
                .collect(Collectors.toList());
        List<String> optionIds = carts.stream().map(CartDTO::getOptionProductId).collect(Collectors.toList());
        List<Product> products = this.productRepository.findAllById(productIds);
        List<OptionProduct> optionProducts = this.optionRepository.findAllByIds(optionIds);
        for (CartDTO cart : carts) {
            Optional<Product> product1 = products.stream().filter(p -> p.getId().equals(cart.getProductId())).findFirst();
            if (product1.isPresent()) {
                Optional<OptionProduct> optionProduct = optionProducts.stream()
                        .filter(p -> p.getId().equals(cart.getOptionProductId())).findFirst();
                optionProduct.ifPresent(product -> cart.setPrice(product.getPrice()));
                cart.endRichProduct(product1.get());
            }

        }
        return CartResponse.builder().cartDTOs(carts)
                .totalCartItem(Math.toIntExact(total)).build();

    }

    @Override
    public CartResponse addCartOrUpdate(CartRequest cartRequest) {
        // check user đã đăng nhập chưua
        String currentId = SecurityUtils.getLoginUserId().orElseThrow(()
                -> new BadCredentialsException(ErrorCode.AUTHENTICATION_REQUIRED.getMessage()));
        cartRequest.setCustomerId(currentId);

        // check option product id có tồn tại
        if (Validator.isNull(cartRequest.getOptionProductId())) {
            List<OptionProduct> optionProductIds = this.optionRepository
                    .findAllByProductIdAndStatus(cartRequest.getProductId(), Constants.EntityStatus.ACTIVE);
            if (!CollectionUtils.isEmpty(optionProductIds)) {
                cartRequest.setOptionProductId(optionProductIds.get(0).getId());
            } else {
                throw new BadRequestAlertException("Sản phẩm không có option", "product", "not_found_option");
            }
        }
        // check sản phẩm đã có trong giỏ hàng chưa
        Optional<Cart> cart = cartRepository.findByCustomerIdAndOptionProductIdAndStatus(cartRequest.getCustomerId(),
                cartRequest.getOptionProductId(), Constants.EntityStatus.ACTIVE);
        if (cart.isPresent()) {
            int updateQuantity = cart.get().getQuantity() + cartRequest.getQuantity();
            cart.get().setQuantity(updateQuantity);
            this.cartRepository.save(cart.get());
            return CartResponse.builder()
                    .cartDTO(this.cartMapper.toDto(this.cartRepository.save(cart.get())))
                    .build();
        }
        OptionProduct optionProducts = this.optionRepository
                .findById(cartRequest.getOptionProductId())
                .orElseThrow(() -> new BadRequestAlertException("option id không tồn tại"
                        , ErrorCode.ERROR_RECORD_DOES_NOT_EXIT));
        Cart newCart = new Cart(cartRequest);
        newCart.setOptionProductName(optionProducts.getName());
        newCart.setDiscount(optionProducts.getDiscount());
        newCart.setProductPrice(optionProducts.getPrice());
        this.cartRepository.save(newCart);
        return CartResponse.builder().cartDTO(this.cartMapper.toDto(newCart)).build();
    }

    @Override
    public CartResponse deleteCartItem(CartRequest cartRequest) {
        // check cartItemId có tồn tại
        this.cartRepository.deleteListCart(cartRequest.getDeletedProductIds(), Constants.EntityStatus.REMOTED);
        return CartResponse.builder().build();
    }

    @Override
    public CartResponse updateCart(CartRequest cartRequest) {
        // check cart có tồn tại
        Cart cart = cartRepository.findById(cartRequest.getId()).orElseThrow(() ->
                new ResponseException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT));

        cart.updateCartItem(cartRequest);

        return CartResponse.builder()
                .cartDTO(this.cartMapper.toDto(this.cartRepository.save(cart)))
                .build();
    }

    @Override
    public CartResponse getCartByCustomerId(String customerId) {
        List<Cart> carts = cartRepository.findAllByCustomerId(customerId);
        return CartResponse.builder().cartDTOs(this.cartMapper.toDto(carts)).build();
    }

    @Override
    public CartResponse searchCartByCustomer(String customerId) {
        List<Cart> carts = cartRepository.findAllByCustomerId(customerId);
        return CartResponse.builder().cartDTOs(this.cartMapper.toDto(carts)).build();
    }

    @Override
    public CartResponse getMyTotalWeight() {
        String currentId = SecurityUtils.getLoginUserId().orElse(null);
        if (Validator.isNull(currentId)) {
            return CartResponse.builder().totalCartItem(0).build();
        }
        Long totalCartItem = cartRepository.countByCustomerId(currentId);
        return CartResponse.builder().totalCartItem(Math.toIntExact(totalCartItem)).build();
    }
    @Override
    public CartResponse findByIds(List<String> ids) {
        List<Cart> carts = cartRepository.findAllById(ids);
        List<String> productIds = carts.stream().map(Cart::getProductId).collect(Collectors.toList());
        List<Product> products = productRepository.findAllById(productIds);
        List<CartDTO> cartDTOs = this.cartMapper.toDto(carts);
        cartDTOs.forEach(cartDTO -> {
            Optional<Product> product = products.stream().filter(p -> {
                if (p.getId().equals(cartDTO.getProductId())
                        && p.getStatus().equals(Constants.EntityStatus.ACTIVE)) {
                    return true;
                }
                return false;
            }).findFirst();
            product.ifPresent(cartDTO::endRichProduct);
        });
        return CartResponse.builder().cartDTOs(cartDTOs).build();
    }

    private CartDTO saveRichCherCart(CartDTO cartDTO) {
        Product product = productRepository.findById(cartDTO.getProductId()).orElseThrow(() ->
                new ResponseException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT));
        cartDTO.endRichProduct(product);
        return cartDTO;
    }

    private CartDTO getRicherCart(String cartId) {
        CartDTO newCartDTO = this.cartRepository.findById(cartId).map(this.cartMapper::toDto).orElseThrow(() ->
                new ResponseException(ErrorCode.ERROR_RECORD_DOES_NOT_EXIT));
//        Product product = productRepository.findById(newCartDTO.getProductId()).orElseThrow(() ->
//                new ResponseException("product không tồn tại",ErrorCode.ERROR_RECORD_DOES_NOT_EXIT);

        return newCartDTO;
    }
}
