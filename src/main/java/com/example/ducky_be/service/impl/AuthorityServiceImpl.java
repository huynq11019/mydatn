package com.example.ducky_be.service.impl;

import com.example.ducky_be.controller.protoco.request.AuthoritesRequest;
import com.example.ducky_be.controller.protoco.response.AuthoritesResponse;
import com.example.ducky_be.core.exception.BadRequestAlertException;
import com.example.ducky_be.core.mapper.AuthorityMapper;
import com.example.ducky_be.core.message.ErrorCode;
import com.example.ducky_be.repository.AuthorityRepository;
import com.example.ducky_be.repository.entity.Authorities;
import com.example.ducky_be.service.AuthorityService;
import com.example.ducky_be.service.dto.AuthorityDTO;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import javax.transaction.Transactional;
import java.util.Optional;

@AllArgsConstructor
@Service
@Transactional
public class AuthorityServiceImpl implements AuthorityService {

    private final AuthorityRepository authorityRepository;

    private final AuthorityMapper authorityMapper;

    /**
     * @param : [authoritesRequest]
     * @return : com.example.ducky_be.core.model.resoonse.Response
     * @author: huynq
     * @since: 11/21/2021 2:34 AM
     * @description: Tạo mới quyền
     * @update: 11/21/2021 2:34 AM
     */
    @Override
    public AuthoritesResponse createAuthorities(AuthoritesRequest authoritesRequest) {
        // kiểm tra quyền đã tồn tại chưa
        Optional<Authorities> authoritiesExisted = authorityRepository.findByAuthorityCode(authoritesRequest.getAuthrotityCode());
        if (authoritiesExisted.isPresent()) {
            throw new BadRequestAlertException("Quyền đã tồn tại", ErrorCode.AUTHORITY_EXISTED);
        }
        Authorities authorities = authorityMapper.toEntity(authoritesRequest);
        AuthorityDTO authorityDTO = this.authorityMapper.toDto(authorityRepository.save(authorities));

        return AuthoritesResponse.builder().authorityDTO(authorityDTO).build();
    }

    @Override
    public AuthoritesResponse updateAuthority(String authorityId, AuthoritesRequest authoritesRequest) {
        // kiểm tra quyền đã tồn tại chưa
        Authorities authoritiesExisted = authorityRepository.findById(authorityId)
                .orElseThrow(() -> new BadRequestAlertException("Quyền không tồn tại", ErrorCode.AUTHORITY_NOT_EXISTED));
        authoritiesExisted.update(this.authorityMapper.toEntity(authoritesRequest));
        this.authorityRepository.save(authoritiesExisted);
        return AuthoritesResponse.builder().authorityDTO(this.authorityMapper.toDto(authoritiesExisted)).build();
    }

    @Override
    public AuthoritesResponse getAuthority(String employeeId) {

        return null;
    }

    @Override
    public AuthoritesResponse searchAuthority(MultiValueMap<String, String> params, Pageable pageable) {
        AuthoritesResponse response = new AuthoritesResponse();
        response.setAuthorityDTOS(authorityMapper.toDto(this.authorityRepository.getListAuthority(params, pageable)));
        return response;
    }

    @Override
    public AuthoritesResponse deleteAuthorities(String authorityId) {
        Authorities authorities = this.authorityRepository.findById(authorityId).orElseThrow((
                () -> new BadRequestAlertException("Quyền không tồn tại", ErrorCode.AUTHORITY_NOT_EXISTED)));
        authorities.delete();
        return AuthoritesResponse.builder().authorityDTO(this.authorityMapper.toDto(authorities)).build();
    }
}

