package com.example.ducky_be.service;

import com.example.ducky_be.repository.entity.Order;
import com.example.ducky_be.service.dto.OrderDTO;

public interface SyncMailService {
    String employeeResetPassWord(String employeeId);

    String notificationEvent(String content);

    String customerActiveAccount(String customerId);

    String customerForgotPassword(String email);

    void notificationOrderActive(OrderDTO order);

    void notificationRefundStatus(Integer status, Order orderDTO, String productName);


}
