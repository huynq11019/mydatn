package com.example.ducky_be.service;

import com.example.ducky_be.controller.protoco.request.RefundRequest;
import com.example.ducky_be.controller.protoco.response.RefundResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface RefundService {
    RefundResponse createRefund(RefundRequest request, String orderDetailId, List<MultipartFile> files);

    RefundResponse getAllRefund(MultiValueMap<String, String> queryParams, Pageable pageable);

    RefundResponse updateStatus(RefundRequest request);

    RefundResponse printReturnOderProduct(String orderCode);


}
