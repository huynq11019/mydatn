package com.example.ducky_be.service;

import com.example.ducky_be.controller.protoco.request.AttributeRequest;
import com.example.ducky_be.controller.protoco.response.AttributeResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

public interface AttributeService {
    AttributeResponse createAttribute(AttributeRequest attributeRequest);

    AttributeResponse findByAttributeByCategoryId(String categoryId, MultiValueMap<String, String> queryParams, Pageable pageable);

    Long countAttribute(String categoryId, MultiValueMap<String, String> queryParams);

    AttributeResponse updateAttribute(AttributeRequest attributeRequest, String id, Long categoryId);


}
