package com.example.ducky_be.service;

import com.example.ducky_be.controller.protoco.request.AuthoritesRequest;
import com.example.ducky_be.controller.protoco.response.AuthoritesResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

public interface AuthorityService {

    AuthoritesResponse createAuthorities(AuthoritesRequest authoritesRequest);

    AuthoritesResponse searchAuthority(MultiValueMap<String, String> params, Pageable pageable);

    AuthoritesResponse deleteAuthorities(String authorityId);

    AuthoritesResponse updateAuthority(String authorityId, AuthoritesRequest authoritesRequest);

    AuthoritesResponse getAuthority(String userId);

}
