package com.example.ducky_be.service;

import com.example.ducky_be.controller.protoco.request.CategoryRequest;
import com.example.ducky_be.controller.protoco.response.CategoryResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

public interface CategoryService {
    CategoryResponse createCategory(CategoryRequest request);

    CategoryResponse searchTreeCategory(MultiValueMap<String, String> queryParams, Pageable pageable);

    CategoryResponse searchAllCategory(MultiValueMap<String, String> queryParams, Pageable pageable);

    CategoryResponse getById(Long category);

    long countCategories(MultiValueMap<String, String> queryParams);

    CategoryResponse updateCategory(CategoryRequest request, Long id);


}
