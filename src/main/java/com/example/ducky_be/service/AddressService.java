package com.example.ducky_be.service;

import com.example.ducky_be.controller.protoco.request.AddressRequest;
import com.example.ducky_be.controller.protoco.response.AddressResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

public interface AddressService {

    AddressResponse searchAddress(MultiValueMap<String, String> queryParams, Pageable pageable);

    Long countAddress(MultiValueMap<String, String> queryParams);

    AddressResponse createAdress(AddressRequest addressRequest);

    AddressResponse updateAddress(AddressRequest addressRequest);

    AddressResponse findproductById(Long addressId);

    AddressResponse saveOrUpdateDraft(AddressRequest addressRequest);

    AddressResponse myaddress(MultiValueMap<String, String> queryParams, Pageable pageable);

    AddressResponse findByUserId(String userId);

    void deleteAddress(Long addressId);

    void setDefaultAddress(Long addressId);
}
