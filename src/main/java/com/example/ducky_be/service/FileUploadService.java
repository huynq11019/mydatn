package com.example.ducky_be.service;

import com.example.ducky_be.controller.protoco.response.FileUploadResponse;
import com.example.ducky_be.service.dto.FileUploadDTO;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface FileUploadService {
    Long countProduct(MultiValueMap<String, String> queryParams);

//======================== SAVE =====================================


    FileUploadDTO findByFileId(String id);

    // lấy thông tin file kèm base 64
    List<FileUploadDTO> getListByOwnerAndOwnerType(String ownerId, String ownerType, boolean isImage);

    List<FileUploadDTO> getByListFileId(List<String> fileID);


    public void saveOrUpdate(FileUploadDTO fileModel);


    List<FileUploadDTO> createFile(List<MultipartFile> files, String objectId, String objectType);

    FileUploadDTO createFile(MultipartFile file, String objectId, String objectType);

    // lấy thông tin file json
    List<FileUploadDTO> findAllByOwnerIdAndOwnerType(String ownerId, String ownerType);

    /**
     * Xóa mềm file
     *
     * @param fileId
     */
    FileUploadResponse removeFileById(String fileId);

    /**
     * @author: huynq
     * @since: 11/11/2021 2:01 PM
     * @description: Xóa danh sách file theo owner Id và owner type
     * @update:
     */
    FileUploadResponse removeFIleByOwnerIdAndOwnerType(String ownerId, String OwnerType);

    /**
     * Creates the file.
     *
     * @param files      image(png,jpeg,gif,jpg)
     *                   the files
     * @param objectId   the object id
     * @param objectType the object type
     * @return the list
     */
    List<FileUploadDTO> createFileImage(List<MultipartFile> files, String objectId, String objectType);


}
