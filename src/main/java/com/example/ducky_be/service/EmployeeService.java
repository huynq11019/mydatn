package com.example.ducky_be.service;

import com.example.ducky_be.controller.protoco.request.AuthoritesRequest;
import com.example.ducky_be.controller.protoco.request.EmployeeRequest;
import com.example.ducky_be.controller.protoco.response.EmployeeResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

public interface EmployeeService {
    EmployeeResponse createEmployee(EmployeeRequest employeeRequest);

    EmployeeResponse updateEmployee(EmployeeRequest employeeRequest);

    EmployeeResponse searchEmployee(MultiValueMap<String, String> queryParams, Pageable pageable);

    EmployeeResponse getEmployeeById(String employeeId);

    EmployeeResponse resetPassword(String employeeId);

    EmployeeResponse changePassword(String employeeId, String oldPassword, String newPassword);

    String[] getUserGrantedAuthority(String userId);

    EmployeeResponse changeStatusEmployee(String employeeId);

//    ==============================

    EmployeeResponse getCurrentEmployee();

    EmployeeResponse setAuthorityForEmployee(String employeeId, AuthoritesRequest request);

}
