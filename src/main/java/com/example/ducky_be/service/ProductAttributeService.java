package com.example.ducky_be.service;

import com.example.ducky_be.controller.protoco.request.ProductAttributeRequest;
import com.example.ducky_be.controller.protoco.response.ProductAttributeResponse;
import com.example.ducky_be.service.dto.ProductAttributeDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface ProductAttributeService {
    ProductAttributeResponse createProductAttribute(ProductAttributeRequest request);

    ProductAttributeResponse findProductAttributeByProductId(String productId, MultiValueMap<String, String> queryParams, Pageable pageable);

    Long countProductAttribute(String productId, MultiValueMap<String, String> queryParams);

    ProductAttributeResponse updateProductAttribute(ProductAttributeRequest attributeRequest, String id, String productId);
}
