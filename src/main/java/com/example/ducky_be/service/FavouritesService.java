package com.example.ducky_be.service;

import com.example.ducky_be.controller.protoco.request.FavouritesRequest;
import com.example.ducky_be.controller.protoco.response.FavouritesResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

public interface FavouritesService {

    FavouritesResponse searchMyFavourites(MultiValueMap<String, String> queryParams, Pageable pageable);

    Long countProduct(MultiValueMap<String, String> queryParams);

    FavouritesResponse createFavourites(FavouritesRequest favouritesRequest);

    FavouritesResponse updateFavourite(FavouritesRequest favouritesRequest);

    FavouritesResponse findFavouritesById(String favouritesId);


    FavouritesResponse changeStatus(String favouritesId, Integer status);

    /**
     * @author: huynq
     * @since: 11/15/2021 11:10 PM
     * @description:
     * @update:
     */
    FavouritesResponse deleteFavourites(String favouritesId);

    FavouritesResponse findFavouritesByUserId(String userId);

    FavouritesResponse findFavoritesProductByCurrentUser();

    FavouritesResponse searchFavouritesByUser(MultiValueMap<String, String> queryParams, String userId, Pageable pageable);
}
