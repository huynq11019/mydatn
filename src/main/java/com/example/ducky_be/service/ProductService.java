package com.example.ducky_be.service;

import com.example.ducky_be.controller.protoco.request.ProductRequest;
import com.example.ducky_be.controller.protoco.response.ProductResponse;
import org.apache.commons.math3.stat.descriptive.summary.Product;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public interface ProductService {
    ProductResponse searchProduct(MultiValueMap<String, String> queryParams, Pageable pageable);

    ProductResponse searchProductCache(MultiValueMap<String, String> queryParams, Pageable pageable);

    Long countProduct(MultiValueMap<String, String> queryParams);

    ProductResponse createProduct(ProductRequest productRequest, List<MultipartFile> files);

    ProductResponse updateProduct(String productId,ProductRequest productRequest, List<MultipartFile> files);

    ProductResponse findproductById(String productid);

    ProductResponse changeStatus(String productId, Integer status);

    ProductResponse getRicherProductById(String productId);

}
