package com.example.ducky_be.service;

import com.example.ducky_be.controller.protoco.request.SerriProductResquest;
import com.example.ducky_be.controller.protoco.response.SerriProductResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

public interface SerriProductService {
    SerriProductResponse serriProductSearch(String keyword , Pageable pageable);

    SerriProductResponse countSerri(MultiValueMap<String, String> queryParams, Pageable pageable);

    SerriProductResponse createOrupdateSerri(SerriProductResquest serriProductResquest);

    SerriProductResponse deleteSerri(Long serriId);
}
