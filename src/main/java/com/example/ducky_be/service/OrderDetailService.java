package com.example.ducky_be.service;

import com.example.ducky_be.controller.protoco.request.OrderDetailRequest;
import com.example.ducky_be.controller.protoco.response.OrderDetailResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

public interface OrderDetailService {
    OrderDetailResponse createOrderDetailByOderId(OrderDetailRequest request, String orderId);

    OrderDetailResponse findOderDetailByOderId(MultiValueMap<String, String> queryParams, Pageable pageable,String orderId);

    Long countOderDetailByOrderId(MultiValueMap<String, String> queryParams, Pageable pageable,String orderId);

}
