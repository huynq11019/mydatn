package com.example.ducky_be.service;

import com.example.ducky_be.controller.protoco.request.RatingRequest;
import com.example.ducky_be.controller.protoco.response.RatingResponse;
import org.springframework.data.domain.Pageable;

public interface RatingProductService {
    void addRatingProduct(String productId, RatingRequest rating);

    void updateRatingProduct(String ratingId, RatingRequest rating);

    void deleteRatingProduct(String productId);

    RatingResponse getRatingProductByProductId(String productId,
                                               Pageable pageable);

    RatingResponse getRatingById(String ratingId);
}
