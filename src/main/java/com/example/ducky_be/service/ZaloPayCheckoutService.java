package com.example.ducky_be.service;

import com.example.ducky_be.controller.protoco.request.CreateOrderZaloPayRequest;
import org.json.JSONObject;

import javax.validation.Valid;

public interface ZaloPayCheckoutService {
    JSONObject createOrder(@Valid CreateOrderZaloPayRequest request);
}
