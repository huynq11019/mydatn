package com.example.ducky_be.service;

import com.example.ducky_be.controller.protoco.request.OrderRequest;
import com.example.ducky_be.controller.protoco.response.OrderResponse;
import com.example.ducky_be.core.constants.Constants;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

import java.util.Map;

public interface OrderService {
    OrderResponse createMyOrder(OrderRequest request);

    OrderResponse customerCancelOrder(String orderId, Constants.OrderStatus status);

    OrderResponse updateOrder(String orderId, OrderRequest request );

    OrderResponse searchOrder(MultiValueMap<String, String> queryParams, Pageable pageable);

    Long countAllOrder(MultiValueMap<String, String> queryParams, Pageable pageable);

    OrderResponse findMyOrder(Pageable pageable);

    OrderResponse changeStatusCheckOut(String orderId, String status);

    OrderResponse findOrderById(String orderId);

    OrderResponse getOrderRicher(String customerId);

    OrderResponse getOrderRicherByOrderId(String customerid, Pageable pageable);

    String createOrderGHN(String orderId);


    Map<String, Long> statisticOrder(MultiValueMap<String, String> queryParams);

    Map<String, Long> statisticMoney(MultiValueMap<String, String> queryParams);

}
