package com.example.ducky_be.service;

import com.example.ducky_be.controller.protoco.request.CustomerRequest;
import com.example.ducky_be.controller.protoco.response.CustomerResponse;
import org.springframework.data.domain.Pageable;
import org.springframework.util.MultiValueMap;

public interface CustomerService {
    CustomerResponse registerUser(CustomerRequest request);

    CustomerResponse searchCustomer(MultiValueMap<String, String> queryParams, Pageable pageable);

    long countCustomer(MultiValueMap<String, String> queryParams);

    CustomerResponse findCustomerByid(String id);

    CustomerResponse verifyAccount(String customerI);

    CustomerResponse lockCustomer(String customerid, Integer status);

    CustomerResponse forgotPassword(String request);

    CustomerResponse myProfile();

    CustomerResponse updateProfile(CustomerRequest request);

    CustomerResponse activeAccount();

    CustomerResponse resetPassword(String password);
}
